// const socket = io('http://localhost:3000/');
const socket = io("http://192.168.1.125:3000/")
var typing=false
var timeout=undefined

const form = document.getElementById('send-container');
const messageInput = document.getElementById('messageInp');
const messageContainer = document.querySelector('.container');

const append = (message, position) => {
    const messageElement = document.createElement('div');
    messageElement.innerHTML = message;
    messageElement.classList.add('message');
    messageElement.classList.add(position);
    messageContainer.append(messageElement)
}

const name = prompt("enter your name");
socket.emit('new-user-joined', name);



$(document).ready(function(){
    $('#messageInp').keypress((e)=>{
      if(e.which!=13){
        typing=true
        socket.emit('typing', {user:name, typing:true})
        clearTimeout(timeout)
        timeout=setTimeout(typingTimeout, 3000)
      }else{
        clearTimeout(timeout)
        typingTimeout()
        //sendMessage() function will be called once the user hits enter
        sendMessage()
      }
    })

    function typingTimeout(){
        typing=false
        socket.emit('typing', {user:name, typing:false})
    }

    //code explained later
    socket.on('display', (data)=>{
      if(data.typing==true)
        $('.typing').text(`${data.user} is typing...`)
      else
        $('.typing').text("")
    })
})

form.addEventListener('submit', (e) => {
    e.preventDefault();
    const message = messageInput.value;
    append(`you : ${message}`, 'right');
    socket.emit('send', message);
    messageInput.value = '';
})




socket.on('user-joined', name => {
    alert(`${name} joined the chat`)
    // append(`${name} joined the chat`, 'center')
})

socket.on('receive', data => {
    append(`${data.name}: ${data.message}`, 'left')
})

socket.on('left', name => {
    alert(`${name} left the chat`,)
    // append(`${name} left the chat`, 'left')
})


socket.on('retrive', data => {
    if (data.rejoin == data.name) {
        append(`You : ${data.message}`, 'right')

    } else {
        append(`${data.name}: ${data.message}`, 'left')
    }
})