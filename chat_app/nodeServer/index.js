const server = require('http').createServer(
    function (req, res) {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write('Hello World!');
        res.end();
    }
);
const io = require('socket.io')
const redis = require('redis');
const { log } = require('console');
const client = redis.createClient();
client.connect();
const users = {}
const socketConection = io(server, {
    cors: {
        origins: "*"
    },
    methods: [
        'GET',
        'POST',
    ],
    allowedHeaders: [
        'Content-Type',
    ],
})


async function sendMessage(socket, rejoin) {
    let data = await client.LRANGE("messages", 0, -1);
    // console.log(data);
    let name = []
    data.map(y => {
        const usernameMessage = y.split(":");
        const redisUsername = usernameMessage[0];
        name.push(redisUsername)
    })
        // console.log(name);
        ;
    if (name.includes(rejoin)) {
        data.map(x => {
            const usernameMessage = x.split(":");
            const redisUsername = usernameMessage[0];
            const redisMessage = usernameMessage[1];
            socket.emit("retrive", { message: redisMessage, name: redisUsername, rejoin: rejoin })

        })
    }
}

socketConection.on('connection', socket => {
    // sendMessage(socket);
    socket.on('new-user-joined', name => {
        // console.log("new user : ", name);
        if (name) {
            sendMessage(socket, name);
        }
        users[socket.id] = name;
        socket.broadcast.emit('user-joined', name);
    });

    socket.on('send', message => {
        client.rPush("messages", `${users[socket.id]}:${message}`)
        socket.broadcast.emit('receive', { message: message, name: users[socket.id] })
    });

    socket.on('disconnect', message => {
        socket.broadcast.emit('left', users[socket.id])
        delete users[socket.id];
    });


    socket.on('typing', (data) => {
        if (data.typing == true)
        socket.broadcast.emit('display', data)
        else
        socket.broadcast.emit('display', data)
    })
})




server.listen(3000, () => {
    // console.log("wrkjbhf");
});