const async = require("hbs/lib/async");
const AccountabilityModel = require("../model/AccountabilityModel");
const Paginator = require("../library/PaginatorLibrary");
const moment = require('moment');

exports.index = async (req, res) => {

    if (req.session.email) {

        var accountabilityData = await AccountabilityModel.find().populate({
            path:'agentId',
            select:'iUserID vFirstName vLastName'
        })
        is_layout = true;
        res.render("../view/accountability/index", {
            is_layout: is_layout,
            accountabilityData:accountabilityData
        });
    } else {
        res.redirect("/");
    }

};
