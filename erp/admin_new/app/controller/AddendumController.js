const async = require("hbs/lib/async");
const BlogModel = require("../model/BlogModel");
const Paginator = require("../library/PaginatorLibrary");
const AddendumModel = require("../model/AddendumModel");
const ProjectTypeModel = require("../model/ProjectTypeModel");
const CompanyDetailModel = require("../model/CompanyDetailModel");
const QuickQuoteModel = require("../model/QuickQuoteModel");

exports.index = async (req, res) => {
    if (req.session.email) {

        var AddendumData = await AddendumModel.find().populate({
            path: 'iProjectTypeId',
            select: 'vTitle'
        });

        is_layout = true;
        res.render("../view/addendums/index", {
            is_layout: is_layout,
            AddendumData: AddendumData,
        });
    } else {
        res.redirect("/");
    }
};

exports.add = async (req, res) => {
    if (req.session.email) {
        try {
            // var AddendumData = await AddendumModel.find();
            var projectTypeData = await ProjectTypeModel.find();
            var quoteData = await QuickQuoteModel.find().sort({ iOrderId: 'asc' });

            is_layout = true;
            res.render("../view/addendums/add", {
                is_layout: is_layout,
                // AddendumData: AddendumData,
                projectTypeData: projectTypeData,
                quoteData: quoteData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {
    if (req.session.email) {

        try {

            var projectTypeData = await ProjectTypeModel.findOne({
                _id: req.body.iProjectTypeId
            });

            const addendumData = {
                eType: req.body.eType,
                vName: req.body.vName,
                iProjectTypeId: req.body.iProjectTypeId,
                iPresident: req.body.iPresident,
                iPresidentMargin: req.body.iPresidentMargin,
                iVicePresident: req.body.iVicePresident,
                iVicePresidentMargin: req.body.iVicePresidentMargin,
                iSeniorManagement: req.body.iSeniorManagement,
                iSeniorManagementMargin: req.body.iSeniorManagementMargin,
                iSalesDirector: req.body.iSalesDirector,
                iSalesDirectorMargin: req.body.iSalesDirectorMargin,
                iDevelopment: req.body.iDevelopment,
                iSalesRepresentative: req.body.iSalesRepresentative,
                iBillableHours: req.body.iBillableHours,
                iMinPrice: req.body.iMinPrice,
                eIsRecurring: projectTypeData.eRecurrying,
            }

            if (projectTypeData.vTitle === 'Website Design and Development') {
                var vQuote = req.body.vQuote;
                addendumData.vQuote = vQuote;
                if (vQuote === 'Quick Quote') {
                    addendumData.eQuoteLevel = req.body.eQuoteLevel;
                    addendumData.iQuoteId = req.body.iQuoteId;
                }
            }

            if (req.body.iAddendumId) {
                try {

                    AddendumModel.findOneAndUpdate({
                        _id: req.body.iAddendumId
                    }, {
                        $set: addendumData,
                    }).exec(function (error, result) {
                        if (!error) {
                            res.redirect("/addendum");
                        } else {
                            console.log(error);
                        }
                    });

                } catch (error) {
                    console.log(error);
                }
            } else {

                const addendum = new AddendumModel(addendumData);
                try {
                    addendum.save(function (error, result) {
                        if (error) {
                            console.log(error);
                        } else {
                            res.redirect("/addendum");
                        }
                    });
                } catch (error) {
                    console.log(error);
                }
            }

        } catch (error) {
            console.log(error);
        }
    }
};

exports.edit = async (req, res) => {

    if (req.session.email) {
        var iAddendumId = req.params.iAddendumId;
        try {
            var AddendumData = await AddendumModel.findOne({
                _id: iAddendumId
            }).exec();
            var projectTypeData = await ProjectTypeModel.find();
            var quoteData = await QuickQuoteModel.find().sort({ iOrderId: 'asc' });

            is_layout = true;
            res.render("../view/addendums/add", {
                is_layout: is_layout,
                AddendumData: AddendumData,
                projectTypeData: projectTypeData,
                quoteData: quoteData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.delete = async (req, res) => {

    if (req.session.email) {
        const vAction = req.body.vAction;
        const iAddendumId = req.body.iAddendumId;
        try {
            if (iAddendumId) {
                //single record deleted
                if (vAction === "delete") {
                    await AddendumModel.deleteOne({
                        _id: iAddendumId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let addendumID = iAddendumId.split(',');
                    await AddendumModel.deleteMany({
                        _id: {
                            $in: addendumID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}