const async = require("hbs/lib/async");
const UserModel = require("../model/UserModel");
const SystemEmailModel = require("../model/SystemEmailModel");
const EmailLibrary = require("../library/EmailLibrary");
const DepartmentModel = require("../model/DepartmentModel");
const DepartmentRoleModel = require("../model/DepartmentRoleModel");
const GeneralLibrary = require('../library/GeneralLibrary');
const crypto = require("crypto");
const bcrypt = require("bcrypt");
const moment = require("moment");

exports.index = async (req, res) => {

    if (req.session.email) {
        is_layout = true;

        var departmentData = await DepartmentModel.findOne({ vTitle: 'CEO & Administration' });
        var departmentRoleData = await DepartmentRoleModel.findOne({ iDepartmentId: departmentData._id });

        var adminData = await UserModel.find({vRole: departmentRoleData._id});
        res.setHeader('Cache-Control', 'max-age=3600');
        res.render("../view/admin/index", {
            is_layout: is_layout,
            adminData: adminData,
            iAgentId: req.session.userid
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/admin/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.check_uniq_email = async (req, res) => {
    var adminData = await UserModel.find({
        vEmail: req.body.vEmail
    }).exec();

    if (adminData.length === 0) {
        res.send('data_not_found');
    } else {
        res.send('data_found');
    }
};

exports.add_action = async (req, res) => {
    if (req.session.email) {
        let image_name = null;

        if (req.body.iAdminId) {
            try {
                if (req.files != null) {

                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vImage,
                        path: 'profile'
                    };
                    image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }

                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vImage": image_name,
                            "vEmail": req.body.vEmail,
                            "vCity": req.body.vCity,
                            "vCountry": req.body.vCountry,
                            "vZipCode": req.body.vZipCode,
                            "vState": req.body.vState,
                            "vPhone": req.body.vPhone,
                            "vAddress": req.body.vAddress,
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vEmail": req.body.vEmail,
                            "vCity": req.body.vCity,
                            "vCountry": req.body.vCountry,
                            "vZipCode": req.body.vZipCode,
                            "vState": req.body.vState,
                            "vPhone": req.body.vPhone,
                            "vAddress": req.body.vAddress,
                        }
                    }
                }

                UserModel.findOneAndUpdate({
                    _id: req.body.iAdminId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/admin");
                    } else {
                        console.log(error);
                    }
                });

            } catch (error) {
                console.log(error);
            }
        } else {

            var departmentData = await DepartmentModel.findOne({ vTitle: 'CEO & Administration' });
            var departmentRoleData = await DepartmentRoleModel.findOne({ iDepartmentId: departmentData._id });

            const addedDate = new Date().toLocaleDateString();
            var randomNumber = Math.floor(100000 + Math.random() * 900000);
            var verifyCode = Math.floor(1000000 + Math.random() * 9000000);
            var verifyToken = crypto.randomBytes(20).toString("hex");

            const vPassword = crypto.randomBytes(10).toString("hex");

            if (req.files != null) {
                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vImage,
                    path: 'profile'
                };
                image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }

            const User = new UserModel({
                vFirstName: req.body.vFirstName,
                vLastName: req.body.vLastName,
                vEmail: req.body.vEmail,
                vCity: req.body.vCity,
                vCountry: req.body.vCountry,
                vZipCode: req.body.vZipCode,
                vState: req.body.vState,
                vPassword: vPassword,
                vImage: image_name,
                eType: 'Admin',            //only for super admin
                vDepartment: departmentData._id,
                vRole: departmentRoleData._id,
                vPhone: req.body.vPhone,
                vAddress: req.body.vAddress,
                dtAddedDate: addedDate,
                iUserID: randomNumber,
                vVerifyCode: verifyCode,
                vVerifyToken: verifyToken
            });

            let email_template = await SystemEmailModel.find({
                'vEmailCode': 'USER_REGISTER'
            });

            try {
                User.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        const vVerifyURL = process.env.APP_URL + "verify/" + result.vVerifyToken;

                        let criteria = {
                            user_name: result.vFirstName + ' ' + result.vLastName,
                            email_to: result.vEmail,
                            password: vPassword,
                            email_subject: "ERP",
                            email_template: email_template,
                            verifycode: result.vVerifyCode,
                            verifyurl: vVerifyURL
                        }
                        EmailLibrary.send_email(criteria);
                        res.redirect("/admin");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async (req, res) => {

    if (req.session.email) {
        var iAdminId = req.params.iAdminId;
        try {
            var adminData = await UserModel.findOne({
                _id: iAdminId
            });
            is_layout = true;
            res.render("../view/admin/add", {
                is_layout: is_layout,
                adminData: adminData,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iAdminId = req.body.iAdminId;
        try {
            if (iAdminId) {
                //single record deleted
                if (vAction === "delete") {
                    await UserModel.deleteOne({
                        _id: iAdminId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let adminID = iAdminId.split(',');
                    await UserModel.deleteMany({
                        _id: {
                            $in: adminID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}