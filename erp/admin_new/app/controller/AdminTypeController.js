const async = require("hbs/lib/async");
const AdminTypeModel = require("../model/AdminTypeModel");

exports.index = async (req, res) => {

    if (req.session.email) {
        is_layout = true;

        var adminTypeData = await AdminTypeModel.find();
        res.render("../view/admin_type/index", {
            is_layout: is_layout,
            adminTypeData: adminTypeData
        });
    } else {
        res.redirect("/");
    }

};
exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/admin_type/add", {
                is_layout: is_layout,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iAdminTypeId) {
            try {
                AdminTypeModel.findOneAndUpdate({
                    _id: req.body.iAdminTypeId
                }, {
                    "$set": {
                        "vTitle": req.body.vTitle,
                        "eStatus": req.body.eStatus
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/admin-type");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const AdminType = new AdminTypeModel({
                vTitle: req.body.vTitle,
                eStatus: req.body.eStatus
            });
            try {
                AdminType.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/admin-type");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iAdminTypeId = req.params.iAdminTypeId;

    if (req.session.email) {
        try {
            var adminTypeData = await AdminTypeModel.findOne({
                _id: iAdminTypeId
            });
            is_layout = true;
            res.render("../view/admin_type/add", {
                is_layout: is_layout,
                adminTypeData: adminTypeData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iAdminTypeId = req.body.iAdminTypeId;
        try {
            if (iAdminTypeId) {

                //single record deleted
                if (vAction === "delete") {
                    await AdminTypeModel.deleteOne({
                        _id: iAdminTypeId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let adminTypeID = iAdminTypeId.split(',');
                    await AdminTypeModel.deleteMany({
                        _id: {
                            $in: adminTypeID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}