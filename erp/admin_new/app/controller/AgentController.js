const async = require("hbs/lib/async");
const AgentModel = require("../model/AgentModel");
const UpdateAgentModel = require("../model/UpdateAgentModel");
const AgentTypeModel = require("../model/AgentTypeModel");
const SystemEmailModel = require("../model/SystemEmailModel");
const AgentAssignAgentModel = require("../model/AgentAssignAgentModel");
const EmailLibrary = require("../library/EmailLibrary");
const Paginator = require("../library/PaginatorLibrary");
const crypto = require("crypto");
const bcrypt = require("bcrypt");
const moment = require("moment");
const GeneralLibrary = require("../library/GeneralLibrary");
const CompanyDetailModel = require("../model/CompanyDetailModel");
const AgentAssignClientModel = require("../model/AgentAssignClientModel");
const ProjectContractModel = require("../model/ProjectContractModel");
const ProjectModel = require("../model/ProjectModel");
const ProjectPaymentModel = require("../model/ProjectPaymentModel");
const AccountabilityModel = require("../model/AccountabilityModel");
const ClientResetPwModel = require("../model/ClientResetPwModel");

exports.index = async (req, res) => {

    if (req.session.email) {

        var agentData = await AgentModel.find().populate({
            path: 'iAgentType',
            select: 'vTitle iPosition'
        }).populate({
            path: "vReferrer",
            select: "iUserID vFirstName vLastName",
        }).sort({
            _id: 'desc'
        });
        is_layout = true;
        res.render("../view/agent/index", {
            is_layout: is_layout,
            agentData: agentData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            var agentTypeData = await AgentTypeModel.find();
            is_layout = true;
            res.render("../view/agent/add", {
                is_layout: is_layout,
                agentTypeData: agentTypeData,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {
        var image_name = null;

        if (req.body.iAgentId) {
            try {
                if (req.files != null) {
                    // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'profile/');
                    // image_name = awsURL;
                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vImage,
                        path: 'profile'
                    };
                    image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }

                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vEmail": req.body.vEmail,
                            "vCity": req.body.vCity,
                            "vCountry": req.body.vCountry,
                            "vZipCode": req.body.vZipCode,
                            "iAgentType": req.body.iAgentType,
                            "vPhone": req.body.vPhone,
                            "vAddress": req.body.vAddress,
                            "eStatus": req.body.eStatus,
                            "vImage": image_name
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vEmail": req.body.vEmail,
                            "vCity": req.body.vCity,
                            "vCountry": req.body.vCountry,
                            "vZipCode": req.body.vZipCode,
                            "iAgentType": req.body.iAgentType,
                            "vPhone": req.body.vPhone,
                            "vAddress": req.body.vAddress,
                            "eStatus": req.body.eStatus,
                        }
                    }
                }

                AgentModel.findOneAndUpdate({
                    _id: req.body.iAgentId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        UpdateAgentModel.deleteOne({
                            iAgentId: req.body.iAgentId
                        }).exec();
                        res.redirect("/agent");
                    } else {

                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            if (req.files != null) {
                // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'profile/');
                // image_name = awsURL;

                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vImage,
                    path: 'profile'
                };
                image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }
            const addedDate = moment().format('MM/DD/YYYY');

            var randomNumber = Math.floor(100000 + Math.random() * 900000);
            var vPassword = crypto.randomBytes(10).toString("hex");
            var verifyCode = Math.floor(1000000 + Math.random() * 9000000);
            var verifyToken = ('agent' + '-' + crypto.randomBytes(20).toString("hex"));

            let email_template = await SystemEmailModel.find({
                'vEmailCode': 'AGENT_REGISTER'
            });

            const agentData = new AgentModel({
                iUserID: randomNumber,
                vFirstName: req.body.vFirstName,
                vLastName: req.body.vLastName,
                vEmail: req.body.vEmail,
                vPassword: vPassword,
                vCity: req.body.vCity,
                vCountry: req.body.vCountry,
                vZipCode: req.body.vZipCode,
                vReferrer: req.body.vReferrer ? req.body.vReferrer : null,
                iAgentType: req.body.iAgentType,
                vPhone: req.body.vPhone,
                vAddress: req.body.vAddress,
                eStatus: req.body.eStatus,
                vImage: image_name,
                dtAddedDate: addedDate,
                vVerifyCode: verifyCode,
                vVerifyToken: verifyToken
            });
            try {
                agentData.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        const vVerifyURL = process.env.AGENT_APP_URL + "verify/" + result.vVerifyToken;

                        if (req.body.vReferrer) {

                            //send notification
                            AgentModel.findOne({
                                _id: req.body.vReferrer
                            }).exec().then(function (res) {
                                const CRITERIA = {
                                    vNotification: `${res.iUserID}- ${res.vFirstName} ${res.vLastName} is assign a new agent ${result.iUserID}-${result.vFirstName} ${result.vLastName}`,
                                    iUserId: result._id,
                                    vLink: '/notification',
                                    vTimezone: req.session.vTimezone,
                                }
                                GeneralLibrary.sendNotification(CRITERIA);

                                const CRITERIA2 = {
                                    vNotification: `${res.iUserID}- ${res.vFirstName} ${res.vLastName} is assign a new agent ${result.iUserID}-${result.vFirstName} ${result.vLastName}`,
                                    iUserId: res._id,
                                    vLink: '/notification',
                                    vTimezone: req.session.vTimezone,
                                }
                                GeneralLibrary.sendNotification(CRITERIA2);
                            })

                            const subAgentData = new AgentAssignAgentModel({
                                iAgentId: req.body.vReferrer,
                                iSubAgentId: result.id,
                            });
                            subAgentData.save(function (e, r) {
                                if (e) {
                                    console.log(e);
                                }
                            });
                        }

                        let criteria = {
                            user_name: result.vFirstName + ' ' + result.vLastName,
                            email_to: result.vEmail,
                            password: vPassword,
                            email_subject: "ERP",
                            email_template: email_template,
                            verifycode: result.vVerifyCode,
                            verifyurl: vVerifyURL
                        }

                        EmailLibrary.send_email(criteria);
                        res.redirect("/agent");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iAgentId = req.params.iAgentId;

    if (req.session.email) {
        try {
            var agentData = await AgentModel.findOne({
                _id: iAgentId
            });
            var agentTypeData = await AgentTypeModel.find();
            is_layout = true;
            res.render("../view/agent/add", {
                is_layout: is_layout,
                agentData: agentData,
                agentTypeData: agentTypeData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.referrerAgent = async (req, res) => {

    if (req.session.email) {
        try {
            var iPosition = req.body.iPosition;
            var agentTypeData = await AgentTypeModel.find({
                iPosition: {
                    $lt: iPosition
                }
            });

            var assignAgentData = [];
            for (let index = 0; index < agentTypeData.length; index++) {
                const element = agentTypeData[index];
                var data = await AgentModel.find({
                    iAgentType: element._id
                });
                assignAgentData.push(...data);

            }
            res.send(assignAgentData);
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_referrerAgent = async (req, res) => {
    if (req.session.email) {

        if (req.body.iAgentId) {
            try {
                AgentModel.findOneAndUpdate({
                    _id: req.body.iAgentId
                }, {
                    "$set": {
                        "vReferrer": req.body.vReferrer,
                    }
                }, {
                    returnDocument: 'after'
                }).exec(function (error, result) {
                    if (!error) {

                        //send notification
                        AgentModel.findOne({
                            _id: req.body.vReferrer
                        }).exec().then(function (res) {
                            const CRITERIA = {
                                vNotification: `${res.iUserID}- ${res.vFirstName} ${res.vLastName} is assign a new agent ${result.iUserID}-${result.vFirstName} ${result.vLastName}`,
                                iUserId: result._id,
                                vLink: '/notification',
                                vTimezone: req.session.vTimezone,
                            }
                            GeneralLibrary.sendNotification(CRITERIA);

                            const CRITERIA2 = {
                                vNotification: `${res.iUserID}- ${res.vFirstName} ${res.vLastName} is assign a new agent ${result.iUserID}-${result.vFirstName} ${result.vLastName}`,
                                iUserId: res._id,
                                vLink: '/notification',
                                vTimezone: req.session.vTimezone,
                            }
                            GeneralLibrary.sendNotification(CRITERIA2);
                        })

                        const subAgentData = new AgentAssignAgentModel({
                            iAgentId: req.body.vReferrer,
                            iSubAgentId: req.body.iAgentId,
                        });
                        subAgentData.save(function (e, r) {
                            if (e) {
                                console.log(e);
                            }
                        });
                        res.redirect("/agent");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.reset_action = async (req, res) => {
    try {
        const iAgentId = req.body.iAgentId;
        const hash = await bcrypt.hash(req.body.vPassword, 10);
        AgentModel.findOneAndUpdate({
            _id: iAgentId
        }, {
            "$set": {
                "vPassword": hash,
            }
        }).exec(function (error, result) {
            if (!error) {
                res.redirect('/agent');
            }
        });

    } catch (error) {
        console.log(error);
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iAgentId = req.body.iAgentId;

        try {
            if (iAgentId) {

                //single record deleted
                if (vAction === "delete") {
                    await AgentModel.deleteOne({
                        _id: iAgentId
                    }).then(function (result) {
                        var AgentCollections = [AccountabilityModel, AgentAssignClientModel, ProjectContractModel, ProjectModel, ProjectPaymentModel, UpdateAgentModel, ClientResetPwModel];
                        AgentCollections.forEach(async element => {
                            await element.deleteMany({
                                iAgentId: {
                                    $in: iAgentId
                                }
                            })
                        });

                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let agentID = iAgentId.split(',');
                    await AgentModel.deleteMany({
                        _id: {
                            $in: agentID
                        }
                    }).then(async (result)=> {
                        await AgentAssignAgentModel.deleteMany({
                            iSubAgentId: iAgentId
                        });
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}