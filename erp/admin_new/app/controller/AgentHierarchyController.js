const async = require("hbs/lib/async");
const Paginator = require("../library/PaginatorLibrary");
const AgentModel = require("../model/AgentModel");
const ClientModel = require("../model/ClientModel");
const ProjectModel = require("../model/ProjectModel");
const ProjectTypeModel = require("../model/ProjectTypeModel");
const AgentTypeModel = require("../model/AgentTypeModel");
const AgentAssignAgentModel = require("../model/AgentAssignAgentModel");
const AgentAssignClientModel = require("../model/AgentAssignClientModel");

exports.index = async(req, res) => {

    if (req.session.email) {

        is_layout = true;

        res.render("../view/agent_hierarchy/index", {
            is_layout: is_layout,
        });
    } else {
        res.redirect("/");
    }
};

exports.ajax_listing = async(req, res) => {

    if (req.session.email) {

        is_layout = false;

        var agentData = await AgentModel.find().populate({
            path: 'iAgentType',
            select: 'vTitle iPosition'
        }).populate("subagentCount").sort({ iAgentType: 'asc' });

        res.render("../view/agent_hierarchy/ajax_listing", {
            layout: false,
            agentData: agentData,
        });
    } else {
        res.redirect("/");
    }

}

exports.subagent = async(req, res) => {

    if (req.session.email) {

        is_layout = false;

        iAgentId = req.body.iAgentId;
        const subAgent = [];
        var subAgentData = await AgentAssignAgentModel.find({ iAgentId: iAgentId });


        for (let index = 0; index < subAgentData.length; index++) {
            const element = subAgentData[index];

            var agentnewData = await AgentModel.find({ _id: element.iSubAgentId }).populate({
                path: 'iAgentType',
                select: 'vTitle iPosition'
            }).populate("subagentCount").sort({ iAgentType: 'asc' });
            subAgent.push(...agentnewData);
        }

        res.send(subAgent);

    } else {
        res.redirect("/");
    }

}

exports.agentOverview = async(req, res) => {

    if (req.session.email) {
        try {

            var iAgentId = req.params.iAgentId;

            var agentData = await AgentModel.findOne({ _id: iAgentId }).populate('iAgentType').exec();

            var subAgent = await AgentAssignAgentModel.find({ iAgentId: iAgentId }).populate({
                path: 'iSubAgentId'
            })

            var subClient = await AgentAssignClientModel.find({ iAgentId: iAgentId }).populate({
                path: 'iClientId'
            });

            var projectData = await ProjectModel.find({ iAgentId: iAgentId }).populate('ClientUserId', 'vFirstName vLastName iUserID').populate({
                path: "iProjectTypeId"
            });

            is_layout = true;
            res.render("../view/agent_hierarchy/agentOverview", {
                is_layout: is_layout,
                agentData: agentData,
                subAgent: subAgent,
                subClient: subClient,
                projectData: projectData,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

}