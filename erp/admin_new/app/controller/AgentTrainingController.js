const async = require("hbs/lib/async");
const AgentTrainingModel = require("../model/AgentTrainingModel");
const TrainingCategoryModel = require("../model/TrainingCategoryModel");
const AgentTypeModel = require("../model/AgentTypeModel");
const GeneralLibrary = require("../library/GeneralLibrary");

exports.index = async (req, res) => {

    if (req.session.email) {

        var AgentTrainingData = await AgentTrainingModel.find().populate({
            path: 'iTrainingCategoryId',
            select: 'vCategory'
        }).populate({
            path: 'iAgentType',
            select: 'vTitle'
        }).exec();

        is_layout = true;
        res.render("../view/agent_training/index", {
            is_layout: is_layout,
            AgentTrainingData:AgentTrainingData
        });
    } else {
        res.redirect("/");
    }
};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            var AgentTrainingData = await AgentTrainingModel.find().exec();
            var trainingCategoryData = await TrainingCategoryModel.find().exec();
            var agentTypeData = await AgentTypeModel.find();

            is_layout = true;
            res.render("../view/agent_training/add", {
                is_layout: is_layout,
                AgentTrainingData: AgentTrainingData,
                trainingCategoryData: trainingCategoryData,
                agentTypeData: agentTypeData
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iAgentTrainingId) {

            try {

                let pdf_name = null;

                if (req.files != null) {
                    // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vFile, 'agent-training/');
                    // pdf_name = awsURL;
                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vFile,
                        path: 'agent-training'
                    };
                    pdf_name = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }

                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "iTrainingCategoryId": req.body.iTrainingCategoryId,
                            "vTitle": req.body.vTitle,
                            "vUrl": req.body.vUrl,
                            "iAgentType": req.body.iAgentType,
                            "tDescription": req.body.tDescription,
                            "vFile": pdf_name
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "iTrainingCategoryId": req.body.iTrainingCategoryId,
                            "vTitle": req.body.vTitle,
                            "vUrl": req.body.vUrl,
                            "iAgentType": req.body.iAgentType,
                            "tDescription": req.body.tDescription,
                        }
                    }
                }

                AgentTrainingModel.findOneAndUpdate({
                    _id: req.body.iAgentTrainingId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/agent-training");
                    } else {
                        console.log(error);
                    }
                })

            } catch (error) {
                console.log(error);
            }
        } else {
            let pdf_name = null;

            if (req.files != null) {
                // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vFile, 'agent-training/');
                // pdf_name = awsURL;

                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vFile,
                    path: 'agent-training'
                };
                pdf_name = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }

            const adminTraining = new AgentTrainingModel({
                vTitle: req.body.vTitle,
                tDescription: req.body.tDescription,
                iTrainingCategoryId: req.body.iTrainingCategoryId,
                iAgentType: req.body.iAgentType,
                vUrl: req.body.vUrl,
                tDescription: req.body.tDescription,
                // dtAddedDate: req.body.dtAddedDate,
                vFile: pdf_name
            });
            try {
                adminTraining.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/agent-training");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async (req, res) => {

    var iAgentTrainingId = req.params.iAgentTrainingId;

    if (req.session.email) {
        try {
            var AgentTrainingData = await AgentTrainingModel.findOne({
                _id: iAgentTrainingId
            }).exec();
            var trainingCategoryData = await TrainingCategoryModel.find().exec();
            var agentTypeData = await AgentTypeModel.find();
            is_layout = true;
            res.render("../view/agent_training/add", {
                is_layout: is_layout,
                AgentTrainingData: AgentTrainingData,
                trainingCategoryData: trainingCategoryData,
                agentTypeData: agentTypeData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {

    if (req.session.email) {
        const vAction = req.body.vAction;
        const iAgnetTrainingId = req.body.iAgnetTrainingId;
        try {
            if (iAgnetTrainingId) {
                //single record deleted
                if (vAction === "delete") {
                    await AgentTrainingModel.deleteOne({
                        _id: iAgnetTrainingId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let agentTrainingID = iAgnetTrainingId.split(',');
                    await AgentTrainingModel.deleteMany({
                        _id: {
                            $in: agentTrainingID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}