const async = require("hbs/lib/async");
const AgentTypeModel = require("../model/AgentTypeModel");
const GeneralLibrary = require("../library/GeneralLibrary");

exports.index = async (req, res) => {

    if (req.session.email) {
        var agentTypeData = await AgentTypeModel.find()
        is_layout = true;
        res.setHeader('Cache-Control', 'max-age=3600');
        res.render("../view/agent_type/index", {
            is_layout: is_layout,
            agentTypeData: agentTypeData
        });
    } else {
        res.redirect("/");
    }
};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/agent_type/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iAgentTypeId) {
            try {

                var vContractFile = null;
                if (req.files != null) {
                    // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vContractFile, 'agentTypeContract/');
                    // vContractFile = awsURL;

                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vContractFile,
                        path: 'agentTypeContract'
                    };
                    vContractFile = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }

                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vTitle": req.body.vTitle,
                            "iPosition": req.body.iPosition,
                            "vContractFile": vContractFile,
                            "eStatus": req.body.eStatus
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vTitle": req.body.vTitle,
                            "iPosition": req.body.iPosition,
                            "eStatus": req.body.eStatus
                        }
                    }
                }

                AgentTypeModel.findOneAndUpdate({
                    _id: req.body.iAgentTypeId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/agent-type");
                    } else {
                        console.log(error);
                    }
                });

            } catch (error) {
                console.log(error);
            }
        } else {

            var vContractFile = null;
            if (req.files != null) {
                // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vContractFile, 'agentTypeContract/');
                // vContractFile = awsURL;

                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vContractFile,
                    path: 'agentTypeContract'
                };
                vContractFile = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }
            const AgentType = new AgentTypeModel({
                vTitle: req.body.vTitle,
                iPosition: req.body.iPosition,
                vContractFile: vContractFile,
                eStatus: req.body.eStatus
            });
            try {
                AgentType.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/agent-type");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iAgentTypeId = req.params.iAgentTypeId;

    if (req.session.email) {
        try {
            var agentTypeData = await AgentTypeModel.findOne({
                _id: iAgentTypeId
            });
            is_layout = true;
            res.render("../view/agent_type/add", {
                is_layout: is_layout,
                agentTypeData: agentTypeData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iAgentTypeId = req.body.iAgentTypeId;

        try {
            if (iAgentTypeId) {
                //single record deleted
                if (vAction === "delete") {
                    await AgentTypeModel.deleteOne({
                        _id: iAgentTypeId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let agentTypeID = iAgentTypeId.split(',');
                    await AgentTypeModel.deleteMany({
                        _id: {
                            $in: agentTypeID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}