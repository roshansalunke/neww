const AgentVideoModel = require("../model/AgentVideoModel");
const GeneralLibrary = require("../library/GeneralLibrary");
const TrainingCategoryModel = require("../model/TrainingCategoryModel");

exports.index = async (req, res) => {
    if (req.session.email) {

        var agentVideoData = await AgentVideoModel.find();
        is_layout = true;
        res.render("../view/agent_video/index", {
            is_layout: is_layout,
            agentVideoData:agentVideoData
        });
    } else {
        res.redirect("/");
    }
};

exports.add = async (req, res) => {
    if (req.session.email) {
        try {
            var trainingCategory = await TrainingCategoryModel.find().exec();

            is_layout = true;
            res.render("../view/agent_video/add", {
                is_layout: is_layout,
                trainingCategory: trainingCategory
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {
    if (req.session.email) {
        if (req.body.iAgentVideoId) {
            try {
                let image_name = null;
                if (req.files != null) {
                    // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'agent-video/');
                    // image_name = awsURL;

                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vImage,
                        path: 'agent-video'
                    };
                    image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }
                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vTitle": req.body.vTitle,
                            "vLink": req.body.vLink,
                            "tDescription": req.body.tDescription,
                            "eStatus": req.body.eStatus,
                            "eCategory": req.body.eCategory,
                            "vImage": image_name
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vTitle": req.body.vTitle,
                            "vLink": req.body.vLink,
                            "tDescription": req.body.tDescription,
                            "eStatus": req.body.eStatus,
                            "eCategory": req.body.eCategory,
                        }
                    }
                }

                AgentVideoModel.findOneAndUpdate({
                    _id: req.body.iAgentVideoId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/agent-video");
                    } else {
                        console.log(error);
                    }
                });

            } catch (error) {
                console.log(error);
            }
        } else {
            let image_name = null;

            if (req.files != null) {
                // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'agent-video/');
                // image_name = awsURL;

                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vImage,
                    path: 'agent-video'
                };
                image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }

            const client_video = new AgentVideoModel({
                vTitle: req.body.vTitle,
                vLink: req.body.vLink,
                tDescription: req.body.tDescription,
                eCategory: req.body.eCategory,
                eStatus: req.body.eStatus,
                vImage: image_name
            });
            try {
                client_video.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/agent-video");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async (req, res) => {
    var iAgentVideoId = req.params.iAgentVideoId;
    if (req.session.email) {
        try {
            var agentVideoData = await AgentVideoModel.findOne({
                _id: iAgentVideoId
            }).exec();
            var trainingCategory = await TrainingCategoryModel.find().exec();
            is_layout = true;
            res.render("../view/agent_video/add", {
                is_layout: is_layout,
                agentVideoData: agentVideoData,
                trainingCategory: trainingCategory
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iAgentVideoId = req.body.iAgentVideoId;

        try {
            if (iAgentVideoId) {
                //single record deleted
                if (vAction === "delete") {
                    await AgentVideoModel.deleteOne({
                        _id: iAgentVideoId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let agentVideoID = iAgentVideoId.split(',');
                    await AgentVideoModel.deleteMany({
                        _id: {
                            $in: agentVideoID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}