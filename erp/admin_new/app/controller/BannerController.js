const async = require("hbs/lib/async");
const BannerModel = require("../model/BannerModel");
const GeneralLibrary = require("../library/GeneralLibrary");

exports.index = async (req, res) => {

    if (req.session.email) {
        var bannerData = await BannerModel.find()
        is_layout = true;

        res.render("../view/banner/index", {
            is_layout: is_layout,
            bannerData: bannerData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/banner/add", {
                is_layout: is_layout,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iBannerId) {
            try {
                if (req.files != null) {
                    // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'banner/');
                    // banner_url = awsURL;

                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vImage,
                        path: 'banner'
                    };
                    banner_url = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }

                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vTitle": req.body.vTitle,
                            "vPanel": req.body.vPanel,
                            "vPanelScreen": req.body.vPanelScreen,
                            "vImage": banner_url,
                            "eStatus": req.body.eStatus
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vTitle": req.body.vTitle,
                            "vPanel": req.body.vPanel,
                            "vPanelScreen": req.body.vPanelScreen,
                            "eStatus": req.body.eStatus
                        }
                    }
                }

                BannerModel.findOneAndUpdate({
                    _id: req.body.iBannerId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/banner");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            if (req.files != null) {
                // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'banner/');
                // banner_url = awsURL;
                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vImage,
                    path: 'banner'
                };
                banner_url = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }
            const Banner = new BannerModel({
                vTitle: req.body.vTitle,
                vImage: banner_url,
                vPanel: req.body.vPanel,
                vPanelScreen: req.body.vPanelScreen,
                eStatus: req.body.eStatus
            });
            try {
                Banner.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/banner");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async (req, res) => {

    var iBannerId = req.params.iBannerId;

    if (req.session.email) {
        try {
            var bannerData = await BannerModel.findOne({
                _id: iBannerId
            });
            is_layout = true;
            res.render("../view/banner/add", {
                is_layout: is_layout,
                bannerData: bannerData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iBannerId = req.body.iBannerId;

        try {
            if (iBannerId) {
                //single record deleted
                if (vAction === "delete") {
                    await BannerModel.deleteOne({
                        _id: iBannerId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let bannerID = iBannerId.split(',');
                    await BannerModel.deleteMany({
                        _id: {
                            $in: bannerID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}