const async = require("hbs/lib/async");
const BlogCategoryModel = require("../model/BlogCategoryModel");

exports.index = async (req, res) => {

    if (req.session.email) {
        is_layout = true;
        var blogCategoryData = await BlogCategoryModel.find()
        res.render("../view/blog_category/index", {
            is_layout: is_layout,
            blogCategoryData: blogCategoryData
        });
    } else {
        res.redirect("/");
    }
};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/blog_category/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iBlogCategoryId) {
            try {
                BlogCategoryModel.findOneAndUpdate({
                    _id: req.body.iBlogCategoryId
                }, {
                    "$set": {
                        "vTitle": req.body.vTitle,
                        "eStatus": req.body.eStatus
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/blog-category");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const blogCategory = new BlogCategoryModel({
                vTitle: req.body.vTitle,
                eStatus: req.body.eStatus
            });
            try {
                blogCategory.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/blog-category");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iBlogCategoryId = req.params.iBlogCategoryId;

    if (req.session.email) {
        try {
            var blogCategoryData = await BlogCategoryModel.findOne({
                _id: iBlogCategoryId
            });
            is_layout = true;
            res.render("../view/blog_category/add", {
                is_layout: is_layout,
                blogCategoryData: blogCategoryData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iBlogCategoryId = req.body.iBlogCategoryId;

        try {
            if (iBlogCategoryId) {
                //single record deleted
                if (vAction === "delete") {
                    await BlogCategoryModel.deleteOne({
                        _id: iBlogCategoryId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let blogCategoryID = iBlogCategoryId.split(',');
                    await BlogCategoryModel.deleteMany({
                        _id: {
                            $in: blogCategoryID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}