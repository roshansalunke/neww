const async = require("hbs/lib/async");
const BlogModel = require("../model/BlogModel");
const Paginator = require("../library/PaginatorLibrary");
const BlogCategoryModel = require("../model/BlogCategoryModel");
const GeneralLibrary = require("../library/GeneralLibrary");
const CompanyDetailModel = require("../model/CompanyDetailModel");
const moment = require("moment");

exports.index = async (req, res) => {

    if (req.session.email) {
        var blogData = await BlogModel.find().populate({
            path: 'iBlogCategoryId',
            select: 'vTitle'
        }).exec();
        is_layout = true;
        res.setHeader('Cache-Control', 'max-age=3600');
        res.render("../view/blog/index", {
            is_layout: is_layout,
            blogData: blogData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            var blogCategoryData = await BlogCategoryModel.find().exec();
            is_layout = true;
            res.render("../view/blog/add", {
                is_layout: is_layout,
                blogCategoryData: blogCategoryData
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        var dtAddedDate = moment().format('MM/DD/YYYY');

        if (req.body.iBlogId) {
            try {
                let image_name = null;

                if (req.files != null) {
                    // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'blog/');
                    // image_name = awsURL;

                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vImage,
                        path: 'blog'
                    };
                    image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }

                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vTitle": req.body.vTitle,
                            "tDescription": req.body.tDescription,
                            "iBlogCategoryId": req.body.iBlogCategoryId,
                            "vLink": req.body.vLink,
                            "eType": req.body.eType,
                            "eStatus": req.body.eStatus,
                            "vImage": image_name
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vTitle": req.body.vTitle,
                            "tDescription": req.body.tDescription,
                            "iBlogCategoryId": req.body.iBlogCategoryId,
                            "vLink": req.body.vLink,
                            "eType": req.body.eType,
                            "eStatus": req.body.eStatus,
                        }
                    }
                }

                BlogModel.findOneAndUpdate({
                    _id: req.body.iBlogId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/blog");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            let image_name = null;

            if (req.files != null) {
                // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'blog/');
                // image_name = awsURL;
                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vImage,
                    path: 'blog'
                };
                image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }

            const blog = new BlogModel({
                vTitle: req.body.vTitle,
                tDescription: req.body.tDescription,
                iBlogCategoryId: req.body.iBlogCategoryId,
                vLink: req.body.vLink,
                eType: req.body.eType,
                dtAddedDate: dtAddedDate,
                eStatus: req.body.eStatus,
                vImage: image_name
            });
            try {
                blog.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/blog");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async (req, res) => {

    var iBlogId = req.params.iBlogId;

    if (req.session.email) {
        try {
            var blogData = await BlogModel.findOne({
                _id: iBlogId
            }).exec();
            var blogCategoryData = await BlogCategoryModel.find().exec();
            is_layout = true;
            res.render("../view/blog/add", {
                is_layout: is_layout,
                blogData: blogData,
                blogCategoryData: blogCategoryData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iBlogId = req.body.iBlogId;

        try {
            if (iBlogId) {
                //single record deleted
                if (vAction === "delete") {
                    await BlogModel.deleteOne({
                        _id: iBlogId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let blogID = iBlogId.split(',');
                    await BlogModel.deleteMany({
                        _id: {
                            $in: blogID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}