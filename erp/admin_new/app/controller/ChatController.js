const async = require("hbs/lib/async");
const AgentModel = require("../model/AgentModel");
const ClientModel = require("../model/ClientModel");

exports.index = async (req, res) => {

    if (req.session.email) {
        var agentData = await AgentModel.find().exec();
        var clientData = await ClientModel.find().exec();
        var allData = [...agentData, ...clientData];

        is_layout = true;
        res.render("../view/chat/index", {
            is_layout: is_layout,
            agentData: agentData,
            clientData: clientData,
            allData: allData
        });
    } else {
        res.redirect("/");
    }

};