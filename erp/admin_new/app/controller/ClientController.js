const async = require("hbs/lib/async");
const ClientModel = require("../model/ClientModel");
const AgentModel = require("../model/AgentModel");
const SystemEmailModel = require("../model/SystemEmailModel");
const AgentAssignClientModel = require("../model/AgentAssignClientModel");
const ProjectModel = require("../model/ProjectModel");
const EmailLibrary = require("../library/EmailLibrary");
const crypto = require("crypto");
const bcrypt = require("bcrypt");
const GeneralLibrary = require('../library/GeneralLibrary');
const moment = require("moment");

exports.index = async(req, res) => {

    if (req.session.email) {

        var clientData = await ClientModel.find().populate({
            path: "vReferrer",
            select: "vFirstName vLastName vImage iUserID"
        });
        is_layout = true;
        res.render("../view/client/index", {
            is_layout: is_layout,
            clientData:clientData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async(req, res) => {

    if (req.session.email) {
        try {
            var clientData = await ClientModel.find();
            var agentData = await AgentModel.find();

            is_layout = true;
            res.render("../view/client/add", {
                is_layout: is_layout,
                clientData: clientData,
                agentData: agentData
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async(req, res) => {

    if (req.session.email) {

        if (req.body.iClientId) {
            try {
                ClientModel.findOneAndUpdate({
                    _id: req.body.iClientId
                }, {
                    "$set": {
                        "vFirstName": req.body.vFirstName,
                        "vLastName": req.body.vLastName,
                        "vEmail": req.body.vEmail,
                        "vCity": req.body.vCity,
                        "vCountry": req.body.vCountry,
                        "vZipCode": req.body.vZipCode,
                        "vReferrer": req.body.vReferrer,
                        "vPhone": req.body.vPhone,
                        "vAddress": req.body.vAddress,
                        "eStatus": req.body.eStatus
                    }
                }).exec(function(error, result) {
                    if (!error) {
                        if (req.body.vReferrer) {
                            //send notification
                            AgentModel.findOne({
                                _id: req.body.vReferrer
                            }).exec().then(function(res) {
                                const CRITERIA = {
                                    vNotification: `${res.iUserID}- ${res.vFirstName} ${res.vLastName} is assign a new client ${result.iUserID}-${result.vFirstName} ${result.vLastName}`,
                                    iUserId: result._id,
                                    vLink: '/notification',
                                    vTimezone: req.session.vTimezone,
                                }
                                GeneralLibrary.sendNotification(CRITERIA);

                                const CRITERIA2 = {
                                    vNotification: `${res.iUserID}- ${res.vFirstName} ${res.vLastName} is assign a new client ${result.iUserID}-${result.vFirstName} ${result.vLastName}`,
                                    iUserId: res._id,
                                    vLink: '/notification',
                                    vTimezone: req.session.vTimezone,
                                }
                                GeneralLibrary.sendNotification(CRITERIA2);
                            })
                        }
                        AgentAssignClientModel.findOneAndUpdate({
                            iClientId: req.body.iClientId
                        }, {
                            "$set": {
                                "iAgentId": req.body.vReferrer,
                            }
                        }).exec();
                        res.redirect("/client");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const addedDate = moment().format('MM/DD/YYYY');

            var randomNumber = Math.floor(100000 + Math.random() * 900000);
            var verifyCode = Math.floor(1000000 + Math.random() * 9000000);
            var verifyToken = crypto.randomBytes(20).toString("hex");

            let email_template = await SystemEmailModel.find({
                'vEmailCode': 'CLIENT_REGISTER'
            });

            // const hash = await bcrypt.hash(req.body.vPassword, 10);
            const vPassword = crypto.randomBytes(10).toString("hex");

            const Client = new ClientModel({
                vFirstName: req.body.vFirstName,
                vLastName: req.body.vLastName,
                vEmail: req.body.vEmail,
                vPassword: vPassword,
                vPhone: req.body.vPhone,
                vCity: req.body.vCity,
                vCountry: req.body.vCountry,
                vZipCode: req.body.vZipCode,
                vReferrer: req.body.vReferrer,
                vAddress: req.body.vAddress,
                eStatus: req.body.eStatus,
                dtAddedDate: addedDate,
                iUserID: randomNumber,
                vVerifyCode: verifyCode,
                vVerifyToken: verifyToken
            });
            try {
                Client.save(function(error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        const AgentAssignClient = new AgentAssignClientModel({
                            iAgentId: req.body.vReferrer,
                            iClientId: result._id
                        });

                        AgentAssignClient.save(function(e, r) {
                            if (e) {
                                console.log(e);
                            }
                        });


                        const vVerifyURL = process.env.CLIENT_APP_URL + "verify/" + result.vVerifyToken;

                        let criteria = {
                            user_name: result.vFirstName + ' ' + result.vLastName,
                            email_to: result.vEmail,
                            password: vPassword,
                            email_subject: "ERP",
                            email_template: email_template,
                            verifycode: result.vVerifyCode,
                            verifyurl: vVerifyURL
                        }

                        EmailLibrary.send_email(criteria);

                        res.redirect("/client");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async(req, res) => {

    var iClientId = req.params.iClientId;

    if (req.session.email) {
        try {
            var clientData = await ClientModel.findOne({
                _id: iClientId
            });
            var agentData = await AgentModel.find();
            is_layout = true;
            res.render("../view/client/add", {
                is_layout: is_layout,
                clientData: clientData,
                agentData: agentData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.view = async(req, res) => {

    if (req.session.email) {
        try {
            var iClientId = req.params.iClientId;
            var clientData = await ClientModel.findOne({
                _id: iClientId
            }).exec();

            var projectData = await ProjectModel.find({
                iClientUserId: clientData.iUserID
            }).populate({
                path: 'iProjectTypeId',
                select: 'vTitle'
            }).populate({
                path: 'iAgentId',
                select: 'vFirstName vLastName'
            });

            is_layout = true;
            res.render("../view/client/view", {
                is_layout: is_layout,
                clientData: clientData,
                projectData: projectData,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
}

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iClientId = req.body.iClientId;

        try {
            if (iClientId) {
                //single record deleted
                if (vAction === "delete") {
                    await ClientModel.deleteOne({
                        _id: iClientId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let clientID = iClientId.split(',');
                    await ClientModel.deleteMany({
                        _id: {
                            $in: clientID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}