const async = require("hbs/lib/async");
const ClientModel = require("../model/ClientModel");
const UserModel = require("../model/UserModel");
const AgentModel = require("../model/AgentModel");
const DeveloperModel = require("../model/DeveloperModel");
const ClientResetPwModel = require("../model/ClientResetPwModel");
const SystemEmailModel = require("../model/SystemEmailModel");
const EmailLibrary = require("../library/EmailLibrary");
const SecurityAnswerModel = require("../model/SecurityAnswerModel");
const crypto = require("crypto");

exports.index = async(req, res) => {

    if (req.session.email) {

        var adminData = await ClientResetPwModel.find({ "iUserId": { $exists: true } }).populate({
            path: 'iUserId',
            select: "vEmail vFirstName vLastName iUserID"
        });

        var agentData = await ClientResetPwModel.find({ "iAgentId": { $exists: true } }).populate({
            path: 'iAgentId',
            select: "vEmail vFirstName vLastName iUserID"
        });

        var clientData = await ClientResetPwModel.find({ "iClientId": { $exists: true } }).populate({
            path: 'iClientId',
            select: "iUserID vEmail vFirstName vLastName"
        });

        var devData = await ClientResetPwModel.find({ "iDeveloperId": { $exists: true } }).populate({
            path: 'iDeveloperId',
            select: "vEmail vFirstName vLastName iUserID"
        });

        is_layout = true;
        res.render("../view/client_reset_pw/index", {
            is_layout: is_layout,
            adminData:adminData,
            agentData:agentData,
            clientData:clientData,
            devData:devData
        });
    } else {
        res.redirect("/");
    }
};

exports.forgetPassword_action = async(req, res) => {

    if (req.session.email) {

        var iClientId = req.body.iClientId;
        var iUserId = req.body.iUserId;
        var iAgentId = req.body.iAgentId;
        var iDeveloperId = req.body.iDeveloperId;

        if (iClientId) {
            var verifyCode = Math.floor(1000000 + Math.random() * 9000000);
            // var verifyToken = ('client' + '-' + crypto.randomBytes(20).toString("hex"));
            var verifyToken = crypto.randomBytes(20).toString("hex");

            let email_template = await SystemEmailModel.find({
                'vEmailCode': 'CLIENT_REGISTER'
            });

            try {

                ClientModel.findOneAndUpdate({ _id: iClientId }, { "$set": { "vVerifyToken": verifyToken, "vVerifyCode": verifyCode, eVerify: '', vResetPasswordBy: 'admin' } }).exec(function(error, result) {
                    if (error) {
                        console.log(error);
                    } else {

                        SecurityAnswerModel.deleteMany({ iUserId: result._id }).exec();


                        //client panel url
                        // const vVerifyURL = "http://localhost:9002/verify/" + verifyToken;
                        const vVerifyURL = process.env.CLIENT_APP_URL + "verify/" + verifyToken;

                        let criteria = {
                            user_name: result.vFirstName + ' ' + result.vLastName,
                            email_to: result.vEmail,
                            email_subject: "ERP",
                            email_template: email_template,
                            verifycode: verifyCode,
                            verifyurl: vVerifyURL
                        }

                        EmailLibrary.send_email(criteria);

                        req.flash('success', 'Successfully send verify code into client email Account.');
                        res.redirect("/client-reset-request");
                    }
                });

            } catch (error) {
                console.log(error);
            }

        } else if (iAgentId) {

            var verifyCode = Math.floor(1000000 + Math.random() * 9000000);
            // var verifyToken = ('admin' + '-' + crypto.randomBytes(20).toString("hex"));
            var verifyToken = crypto.randomBytes(20).toString("hex");

            let email_template = await SystemEmailModel.find({
                'vEmailCode': 'AGENT_REGISTER'
            });

            try {
                AgentModel.findOneAndUpdate({ _id: iAgentId }, { "$set": { "vVerifyToken": verifyToken, "vVerifyCode": verifyCode, eVerify: '', vResetPasswordBy: 'admin' } }).exec(function(error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        SecurityAnswerModel.deleteMany({ iAgentId: result._id }).exec();

                        //agent panel url
                        const vVerifyURL = process.env.AGENT_APP_URL + "verify/" + verifyToken;

                        let criteria = {
                            user_name: result.vFirstName + ' ' + result.vLastName,
                            email_to: result.vEmail,
                            email_subject: "ERP",
                            email_template: email_template,
                            verifycode: verifyCode,
                            verifyurl: vVerifyURL
                        }

                        EmailLibrary.send_email(criteria);

                        req.flash('success', 'Successfully send verify code into admin email Account.');
                        res.redirect("/client-reset-request");
                    }
                });

            } catch (error) {
                console.log(error);
            }

        } else if (iUserId) {

            var verifyCode = Math.floor(1000000 + Math.random() * 9000000);
            // var verifyToken = ('admin' + '-' + crypto.randomBytes(20).toString("hex"));
            var verifyToken = crypto.randomBytes(20).toString("hex");

            let email_template = await SystemEmailModel.find({
                'vEmailCode': 'USER_REGISTER'
            });

            try {

                UserModel.findOneAndUpdate({ _id: iUserId }, { "$set": { "vVerifyToken": verifyToken, "vVerifyCode": verifyCode, eVerify: '', vResetPasswordBy: 'admin' } }).exec(function(error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        SecurityAnswerModel.deleteMany({ iUserId: result._id }).exec();


                        //client panel url
                        // const vVerifyURL = "http://localhost:9002/verify/" + verifyToken;
                        const vVerifyURL = process.env.APP_URL + "admin-verify/" + verifyToken;

                        let criteria = {
                            user_name: result.vFirstName + ' ' + result.vLastName,
                            email_to: result.vEmail,
                            email_subject: "ERP",
                            email_template: email_template,
                            verifycode: verifyCode,
                            verifyurl: vVerifyURL
                        }

                        EmailLibrary.send_email(criteria);

                        req.flash('success', 'Successfully send verify code into admin email Account.');
                        res.redirect("/client-reset-request");
                    }
                });

            } catch (error) {
                console.log(error);
            }

        } else if (iDeveloperId) {

            var verifyCode = Math.floor(1000000 + Math.random() * 9000000);
            // var verifyToken = ('admin' + '-' + crypto.randomBytes(20).toString("hex"));
            var verifyToken = crypto.randomBytes(20).toString("hex");

            let email_template = await SystemEmailModel.find({
                'vEmailCode': 'DEVELOPER_REGISTER'
            });

            try {

                DeveloperModel.findOneAndUpdate({ _id: iDeveloperId }, {
                    "$set": {
                        "vVerifyToken": verifyToken,
                        "vVerifyCode": verifyCode,
                        "eVerify": '',
                        "vResetPasswordBy": 'admin'
                    }
                }, { returnDocument: 'after' }).exec(function(error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        SecurityAnswerModel.deleteMany({ iDeveloperId: result._id }).exec();

                        //Developer panel url
                        const vVerifyURL = process.env.DEVELOPER_APP_URL + "verify/" + verifyToken;

                        let criteria = {
                            user_name: result.vFirstName + ' ' + result.vLastName,
                            email_to: result.vEmail,
                            email_subject: "ERP",
                            email_template: email_template,
                            verifycode: verifyCode,
                            verifyurl: vVerifyURL
                        }

                        EmailLibrary.send_email(criteria);

                        req.flash('success', 'Successfully send verify code into admin email Account.');
                        res.redirect("/client-reset-request");
                    }
                });

            } catch (error) {
                console.log(error);
            }

        } else {
            res.redirect("/")
        }

    }
};

exports.view = async(req, res) => {

    if (req.session.email) {
        try {
            var iClientResetId = req.params.iClientResetId;

            var ResetPwData = await ClientResetPwModel.findOne({ _id: iClientResetId });

            if (ResetPwData.iClientId != null) {
                var ResetPwDetail = await ClientResetPwModel.findOne({ _id: iClientResetId }).populate({
                    path: 'iClientId',
                    select: "iUserID vEmail vFirstName vLastName"
                });
            }
            if (ResetPwData.iUserId != null) {
                var ResetPwDetail = await ClientResetPwModel.findOne({ _id: iClientResetId }).populate({
                    path: 'iUserId',
                    select: "vEmail vFirstName vLastName"
                });
            }
            if (ResetPwData.iAgentId != null) {
                var ResetPwDetail = await ClientResetPwModel.findOne({ _id: iClientResetId }).populate({
                    path: 'iAgentId',
                    select: "vEmail vFirstName vLastName"
                });
            }

            if (ResetPwData.iDeveloperId != null) {
                var ResetPwDetail = await ClientResetPwModel.findOne({ _id: iClientResetId }).populate({
                    path: 'iDeveloperId',
                    select: "vEmail vFirstName vLastName"
                });
            }

            is_layout = true;

            res.render("../view/client_reset_pw/view", {
                is_layout: is_layout,
                ResetPwDetail: ResetPwDetail,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};