const ClientVideoModel = require("../model/ClientVideoModel");
const GeneralLibrary = require("../library/GeneralLibrary");

exports.index = async (req, res) => {
    if (req.session.email) {

        var clientVideoData = await ClientVideoModel.find();
        is_layout = true;
        res.render("../view/client_video/index", {
            is_layout: is_layout,
            clientVideoData:clientVideoData
        });
    } else {
        res.redirect("/");
    }
};

exports.add = async (req, res) => {
    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/client_video/add", {
                is_layout: is_layout,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {
    if (req.session.email) {
        if (req.body.iClientVideoId) {
            try {
                let image_name = null;
                if (req.files != null) {
                    // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'client-video/');
                    // image_name = awsURL;
                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vImage,
                        path: 'client-video'
                    };
                    image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }
                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vTitle": req.body.vTitle,
                            "vLink": req.body.vLink,
                            "tDescription": req.body.tDescription,
                            "eStatus": req.body.eStatus,
                            "vImage": image_name
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vTitle": req.body.vTitle,
                            "vLink": req.body.vLink,
                            "tDescription": req.body.tDescription,
                            "eStatus": req.body.eStatus,
                        }
                    }
                }

                ClientVideoModel.findOneAndUpdate({
                    _id: req.body.iClientVideoId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/client-video");
                    } else {
                        console.log(error);
                    }
                });

            } catch (error) {
                console.log(error);
            }
        } else {
            let image_name = null;

            if (req.files != null) {
                // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'client-video/');
                // image_name = awsURL;

                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vImage,
                    path: 'client-video'
                };
                image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }

            const client_video = new ClientVideoModel({
                vTitle: req.body.vTitle,
                vLink: req.body.vLink,
                tDescription: req.body.tDescription,
                eStatus: req.body.eStatus,
                vImage: image_name
            });
            try {
                client_video.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/client-video");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async (req, res) => {
    var iClientVideoId = req.params.iClientVideoId;
    if (req.session.email) {
        try {
            var clientVideoData = await ClientVideoModel.findOne({
                _id: iClientVideoId
            }).exec();
            is_layout = true;
            res.render("../view/client_video/add", {
                is_layout: is_layout,
                clientVideoData: clientVideoData,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iClientVideoId = req.body.iClientVideoId;

        try {
            if (iClientVideoId) {
                //single record deleted
                if (vAction === "delete") {
                    await iClientVideoId.deleteOne({
                        _id: iClientVideoId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let videoID = iClientVideoId.split(',');
                    await iClientVideoId.deleteMany({
                        _id: {
                            $in: videoID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}