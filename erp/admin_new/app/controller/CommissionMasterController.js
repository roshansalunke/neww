const async = require("hbs/lib/async");
const ProjectModel = require("../model/ProjectModel");
const ProjectPaymentModel = require("../model/ProjectPaymentModel");
const AgentAssignAgentModel = require("../model/AgentAssignAgentModel");
const AgentTypeModel = require("../model/AgentTypeModel");
const AgentModel = require("../model/AgentModel");
const ProjectCommissionModel = require("../model/ProjectCommissionModel");
const PayrollPaymentModel = require("../model/PayrollPaymentModel");
const moment = require("moment");

exports.index = async (req, res) => {

    if (req.session.email) {
        
        var commissionData = await ProjectCommissionModel.find().populate({
            path: 'projectId',
            select:'iProjectUniqueId vProject'
        }).populate({
            path:'agentId',
            select:'iUserID vFirstName vLastName'
        }).sort({_id:'desc'}).exec();

        is_layout = true;
        res.render("../view/commission_master/index", {
            is_layout: is_layout,
            commissionData:commissionData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            var projectData = await ProjectModel.find().exec();
            const iProjectId = req.body.iProjectId;

            is_layout = true;
            const data = {}
            data.is_layout = is_layout;
            data.projectData = projectData;
            if (iProjectId) {
                var project = await ProjectModel.findOne({ _id: iProjectId }).populate({
                    path: 'AgentId',
                    select: 'iUserID vFirstName vLastName',
                    populate: 'iAgentType'
                });
                data.getProjectData = project;
            }

            res.render("../view/commission_master/add", data);

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.get_project = async (req, res) => {

    if (req.session.email) {
        try {
            // project id
            var iProjectId = req.params.iProjectId;
            var projectData = await ProjectModel.find();

            // Check project
            var getProject = await ProjectModel.findOne({
                _id: iProjectId,
            }).populate('iProjectTypeId').populate('AgentId');

            var payrollPaymentData = await PayrollPaymentModel.find({ iProjectId: iProjectId }).sort({ "ePaymentStep": 'asc' });

            if (getProject != null) {

                // project payment data
                var paymentData = await ProjectPaymentModel.find({
                    iProjectId: iProjectId
                }).sort({
                    _id: 'asc'
                });

                const iAgentId = await getAgent(getProject.AgentId._id);

                //get all higher level agent
                const subAgentData = await getdata(iAgentId._id);
                subAgentData.splice(0, 0, iAgentId); //move current user into 0 index


                is_layout = true;
                res.render("../view/commission_master/payroll", {
                    is_layout: is_layout,
                    iProjectId: iProjectId,
                    projectData: projectData,
                    getProject: getProject,
                    paymentData: paymentData,
                    subAgentData: subAgentData.reverse(),
                    payrollPaymentData: payrollPaymentData
                });

            } else {
                req.flash('error', 'Project not found.');
                res.redirect('/commission-master');
            }

        } catch (error) {
            console.log(error);
            res.redirect('/commission-master/add');
        }
    } else {
        res.redirect("/");
    }

};

async function getAgent(iAgentId) {
    const getAgentData = await AgentModel.findOne({ _id: iAgentId }).select('iUserID vFirstName vLastName vEmail iAgentType').populate('iAgentType');
    return getAgentData;
}

async function getdata(id) {
    var detail = [];
    const data = await AgentAssignAgentModel.findOne({ iSubAgentId: id });
    if (data !== null) {
        const agentData = await getAgent(data.iAgentId);
        detail.push(agentData)

        var data2 = await getdata(data.iAgentId)

        detail = detail.concat(data2)
    }
    return detail;
}

exports.calCommission = async (req, res) => {

    var iProjectId = req.body.iProjectId;

    var paymentData = {
        iSalesPrice: req.body.iPaymentAmt,
        ePaymentPhases: req.body.ePaymentPhases,
        ePaymentStep: req.body.ePaymentStep
    }

    var calPayroll = [];

    // Check project
    var quoteLevelData = await ProjectModel.findOne({ _id: iProjectId });
    var getProject = await ProjectModel.findOne({
        _id: iProjectId,
    }).populate('iProjectTypeId').populate({
        path: 'AgentId',
        populate: 'iAgentType'
    }).populate({
        path: 'addendum',
        match: { eQuoteLevel: quoteLevelData.vQuoteLevel ? quoteLevelData.vQuoteLevel : null },
    });

    const iAgentId = await getAgent(getProject.AgentId._id);

    //get all higher level agent
    const subAgentData = await getdata(iAgentId._id);
    subAgentData.splice(0, 0, iAgentId); //move current user into 0 index

    //get all agent position
    var allPos = subAgentData.map(projectData => {
        return projectData.iAgentType.iPosition;
    });

    const demoData = await AgentTypeModel.find();
    const agentNotAvailable = [];
    const agentAvailable = [];
    for (let i = 0; i < demoData.length; i++) {
        const position = demoData[i].iPosition;
        const agentType = getProject.AgentId.iAgentType.iPosition;


        if (!allPos.includes(position)) {

            if (position < agentType) {
                agentNotAvailable.push(parseInt(position))
            }
        } else {
            agentAvailable.push(parseInt(position))
        }
    }

    for (let index = 0; index < subAgentData.length; index++) {
        const element = subAgentData[index];

        const commissiondate = moment().format('MM/DD/YYYY');
        const commissionObj = {
            iProjectId: iProjectId,
            // iProjectPaymentId: paymentData._id,
            dtAddedDate: commissiondate
        }

        //calculate President commission
        if (element.iAgentType.vTitle === 'President') {

            const iSalesPrice = paymentData.iSalesPrice;
            //if the current project added by president
            if (getProject.AgentId._id.equals(element._id) === true) {

                if (getProject.vQuote === 'Quick Quote') {

                    const addendumAmt = getProject.addendum.iPresident;
                    const paymentPhase = paymentData.ePaymentPhases
                    const paymentStep = paymentData.ePaymentStep;

                    if (paymentPhase === "ThreePhase") {
                        if (paymentStep === '1') {
                            const calPayAmt = addendumAmt / 2;

                            //insert data
                            commissionObj.iAgentId = element._id;
                            commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                            calPayroll.push({
                                iAgentId: element._id,
                                agentName: element.vFirstName + ' ' + element.vLastName,
                                amt: commissionObj.iAmount
                            })
                            // saveProjectCommission(commissionObj)
                            console.log("President", calPayAmt)
                        } else {
                            const calPayAmt = addendumAmt / 4;

                            //insert data
                            commissionObj.iAgentId = element._id;
                            commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                            calPayroll.push({
                                iAgentId: element._id,
                                agentName: element.vFirstName + ' ' + element.vLastName,
                                amt: commissionObj.iAmount
                            })
                            // saveProjectCommission(commissionObj)
                            console.log("President", calPayAmt)
                        }
                    } else if (paymentPhase === "FourPhase") {
                        const calPayAmt = addendumAmt / 4;

                        //insert data
                        commissionObj.iAgentId = element._id;
                        commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                        calPayroll.push({
                            iAgentId: element._id,
                            agentName: element.vFirstName + ' ' + element.vLastName,
                            amt: commissionObj.iAmount
                        })
                        // saveProjectCommission(commissionObj)
                        console.log("President", calPayAmt)
                    } else {
                        const calPayAmt = addendumAmt;

                        //insert data
                        commissionObj.iAgentId = element._id;
                        commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                        calPayroll.push({
                            iAgentId: element._id,
                            agentName: element.vFirstName + ' ' + element.vLastName,
                            amt: commissionObj.iAmount
                        })
                        // saveProjectCommission(commissionObj)
                        console.log("President", calPayAmt)
                    }

                } else {
                    const calPayAmt = iSalesPrice * getProject.addendum.iPresident / 100;

                    //insert data
                    commissionObj.iAgentId = element._id;
                    commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                    calPayroll.push({
                        iAgentId: element._id,
                        agentName: element.vFirstName + ' ' + element.vLastName,
                        amt: commissionObj.iAmount
                    })
                    // saveProjectCommission(commissionObj)
                    console.log("President", calPayAmt)
                }

            } else {

                if (agentNotAvailable.length > 0) {

                    var amt = 0;

                    if (getProject.vQuote === 'Quick Quote') {
                        const addendumAmt = getProject.addendum.iPresidentMargin;
                        const paymentPhase = paymentData.ePaymentPhases
                        const paymentStep = paymentData.ePaymentStep;

                        if (paymentPhase === "ThreePhase") {
                            if (paymentStep === '1') {
                                amt = addendumAmt / 2;
                            } else {
                                amt = addendumAmt / 4;
                            }
                        } else if (paymentPhase === "FourPhase") {
                            amt = addendumAmt / 4;
                        } else {
                            amt = addendumAmt;
                        }
                    } else {
                        amt = iSalesPrice * getProject.addendum.iPresidentMargin / 100;
                    }

                    for (let i = 0; i < agentNotAvailable.length; i++) {
                        const e1 = agentNotAvailable[i];
                        const position = element.iAgentType.iPosition;
                        if ((e1 - 1) == parseInt(position) || !agentAvailable.includes(e1 - 1)) {

                            switch (e1) {
                                case 2:

                                    if (getProject.vQuote === 'Quick Quote') {
                                        const addendumAmt = getProject.addendum.iVicePresidentMargin;
                                        const paymentPhase = paymentData.ePaymentPhases
                                        const paymentStep = paymentData.ePaymentStep;

                                        if (paymentPhase === "ThreePhase") {
                                            if (paymentStep === '1') {
                                                var calAmt = addendumAmt / 2;
                                            } else {
                                                var calAmt = addendumAmt / 4;
                                            }
                                        } else if (paymentPhase === "FourPhase") {
                                            var calAmt = addendumAmt / 4;
                                        } else {
                                            var calAmt = addendumAmt;
                                        }
                                    } else {
                                        var calAmt = iSalesPrice * getProject.addendum.iVicePresidentMargin / 100;
                                    }

                                    amt += calAmt
                                    break
                                case 3:

                                    if (getProject.vQuote === 'Quick Quote') {
                                        const addendumAmt = getProject.addendum.iSeniorManagementMargin;
                                        const paymentPhase = paymentData.ePaymentPhases
                                        const paymentStep = paymentData.ePaymentStep;

                                        if (paymentPhase === "ThreePhase") {
                                            if (paymentStep === '1') {
                                                var calAmt = addendumAmt / 2;
                                            } else {
                                                var calAmt = addendumAmt / 4;
                                            }
                                        } else if (paymentPhase === "FourPhase") {
                                            var calAmt = addendumAmt / 4;
                                        } else {
                                            var calAmt = addendumAmt;
                                        }
                                    } else {
                                        var calAmt = iSalesPrice * getProject.addendum.iSeniorManagementMargin / 100;
                                    }
                                    amt += calAmt
                                    break

                                case 4:

                                    if (getProject.vQuote === 'Quick Quote') {
                                        const addendumAmt = getProject.addendum.iSalesDirectorMargin;
                                        const paymentPhase = paymentData.ePaymentPhases
                                        const paymentStep = paymentData.ePaymentStep;

                                        if (paymentPhase === "ThreePhase") {
                                            if (paymentStep === '1') {
                                                var calAmt = addendumAmt / 2;
                                            } else {
                                                var calAmt = addendumAmt / 4;
                                            }
                                        } else if (paymentPhase === "FourPhase") {
                                            var calAmt = addendumAmt / 4;
                                        } else {
                                            var calAmt = addendumAmt;
                                        }
                                    } else {
                                        var calAmt = iSalesPrice * getProject.addendum.iSalesDirectorMargin / 100;
                                    }

                                    amt += calAmt
                                    break

                                default:
                                    break;
                            }
                        }
                    }

                    //insert data
                    commissionObj.iAgentId = element._id;
                    commissionObj.iAmount = parseFloat(amt).toFixed(2)


                    calPayroll.push({
                        iAgentId: element._id,
                        agentName: element.vFirstName + ' ' + element.vLastName,
                        amt: commissionObj.iAmount
                    })
                    // saveProjectCommission(commissionObj)

                    console.log("President margin", amt)
                } else {

                    const calPayAmt = iSalesPrice * getProject.addendum.iPresidentMargin / 100;
                    //insert data
                    commissionObj.iAgentId = element._id;
                    commissionObj.iAmount = parseFloat(amt).toFixed(2)


                    calPayroll.push({
                        iAgentId: element._id,
                        agentName: element.vFirstName + ' ' + element.vLastName,
                        amt: commissionObj.iAmount
                    })
                    // saveProjectCommission(commissionObj)
                    console.log("President margin", calPayAmt);
                }
            }

        } else if (element.iAgentType.vTitle === 'Vice President') {

            const iSalesPrice = paymentData.iSalesPrice;
            if (getProject.AgentId._id.equals(element._id) === true) {

                if (getProject.vQuote === 'Quick Quote') {

                    const addendumAmt = getProject.addendum.iVicePresident;
                    const paymentPhase = paymentData.ePaymentPhases
                    const paymentStep = paymentData.ePaymentStep;

                    if (paymentPhase === "ThreePhase") {
                        if (paymentStep === '1') {
                            const calPayAmt = addendumAmt / 2;

                            //insert data
                            commissionObj.iAgentId = element._id;
                            commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                            calPayroll.push({
                                iAgentId: element._id,
                                agentName: element.vFirstName + ' ' + element.vLastName,
                                amt: commissionObj.iAmount
                            })
                            // saveProjectCommission(commissionObj)
                            console.log("Vice President", calPayAmt)
                        } else {
                            const calPayAmt = addendumAmt / 4;

                            //insert data
                            commissionObj.iAgentId = element._id;
                            commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                            calPayroll.push({
                                iAgentId: element._id,
                                agentName: element.vFirstName + ' ' + element.vLastName,
                                amt: commissionObj.iAmount
                            })
                            // saveProjectCommission(commissionObj)
                            console.log("Vice President", calPayAmt)
                        }
                    } else if (paymentPhase === "FourPhase") {
                        const calPayAmt = addendumAmt / 4;

                        //insert data
                        commissionObj.iAgentId = element._id;
                        commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                        calPayroll.push({
                            iAgentId: element._id,
                            agentName: element.vFirstName + ' ' + element.vLastName,
                            amt: commissionObj.iAmount
                        })
                        // saveProjectCommission(commissionObj)
                        console.log("Vice President", calPayAmt)
                    } else {
                        const calPayAmt = addendumAmt;

                        //insert data
                        commissionObj.iAgentId = element._id;
                        commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                        calPayroll.push({
                            iAgentId: element._id,
                            agentName: element.vFirstName + ' ' + element.vLastName,
                            amt: commissionObj.iAmount
                        })
                        // saveProjectCommission(commissionObj)
                        console.log("Vice President", calPayAmt)
                    }

                } else {
                    const calPayAmt = iSalesPrice * getProject.addendum.iVicePresident / 100;

                    //insert data
                    commissionObj.iAgentId = element._id;
                    commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                    calPayroll.push({
                        iAgentId: element._id,
                        agentName: element.vFirstName + ' ' + element.vLastName,
                        amt: commissionObj.iAmount
                    })
                    // saveProjectCommission(commissionObj)
                    console.log("Vice President", calPayAmt)
                }
            } else {

                if (agentNotAvailable.length > 0) {

                    var amt = 0;
                    if (getProject.vQuote === 'Quick Quote') {
                        const addendumAmt = getProject.addendum.iVicePresidentMargin;
                        const paymentPhase = paymentData.ePaymentPhases
                        const paymentStep = paymentData.ePaymentStep;

                        if (paymentPhase === "ThreePhase") {
                            if (paymentStep === '1') {
                                amt = addendumAmt / 2;
                            } else {
                                amt = addendumAmt / 4;
                            }
                        } else if (paymentPhase === "FourPhase") {
                            amt = addendumAmt / 4;
                        } else {
                            amt = addendumAmt;
                        }
                    } else {
                        amt = iSalesPrice * getProject.addendum.iVicePresidentMargin / 100;
                    }

                    for (let i = 0; i < agentNotAvailable.length; i++) {
                        const e1 = agentNotAvailable[i];
                        const position = element.iAgentType.iPosition;
                        if ((e1 - 1) == parseInt(position) || !agentAvailable.includes(e1 - 1)) {

                            switch (e1) {
                                case 3:

                                    if (getProject.vQuote === 'Quick Quote') {
                                        const addendumAmt = getProject.addendum.iSeniorManagementMargin;
                                        const paymentPhase = paymentData.ePaymentPhases
                                        const paymentStep = paymentData.ePaymentStep;

                                        if (paymentPhase === "ThreePhase") {
                                            if (paymentStep === '1') {
                                                var calAmt = addendumAmt / 2;
                                            } else {
                                                var calAmt = addendumAmt / 4;
                                            }
                                        } else if (paymentPhase === "FourPhase") {
                                            var calAmt = addendumAmt / 4;
                                        } else {
                                            var calAmt = addendumAmt;
                                        }
                                    } else {
                                        var calAmt = iSalesPrice * getProject.addendum.iSeniorManagementMargin / 100;
                                    }
                                    amt += calAmt
                                    break

                                case 4:

                                    if (getProject.vQuote === 'Quick Quote') {
                                        const addendumAmt = getProject.addendum.iSalesDirectorMargin;
                                        const paymentPhase = paymentData.ePaymentPhases
                                        const paymentStep = paymentData.ePaymentStep;

                                        if (paymentPhase === "ThreePhase") {
                                            if (paymentStep === '1') {
                                                var calAmt = addendumAmt / 2;
                                            } else {
                                                var calAmt = addendumAmt / 4;
                                            }
                                        } else if (paymentPhase === "FourPhase") {
                                            var calAmt = addendumAmt / 4;
                                        } else {
                                            var calAmt = addendumAmt;
                                        }
                                    } else {
                                        var calAmt = iSalesPrice * getProject.addendum.iSalesDirectorMargin / 100;
                                    }
                                    amt += calAmt
                                    break

                                default:
                                    break;
                            }
                        }
                    }

                    //insert data
                    commissionObj.iAgentId = element._id;
                    commissionObj.iAmount = parseFloat(amt).toFixed(2)

                    calPayroll.push({
                        iAgentId: element._id,
                        agentName: element.vFirstName + ' ' + element.vLastName,
                        amt: commissionObj.iAmount
                    })
                    // saveProjectCommission(commissionObj)
                    console.log("Vice President amount", amt)
                } else {
                    const iSalesPrice = paymentData.iSalesPrice;
                    const calPayAmt = iSalesPrice * getProject.addendum.iVicePresidentMargin / 100;

                    //insert data
                    commissionObj.iAgentId = element._id;
                    commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                    calPayroll.push({
                        iAgentId: element._id,
                        agentName: element.vFirstName + ' ' + element.vLastName,
                        amt: commissionObj.iAmount
                    })
                    // saveProjectCommission(commissionObj)
                    console.log("Vice President margin", calPayAmt)
                }
            }

        } else if (element.iAgentType.vTitle === 'Senior Management') {

            const iSalesPrice = paymentData.iSalesPrice;
            //if the current project added by vice president
            if (getProject.AgentId._id.equals(element._id) === true) {

                if (getProject.vQuote === 'Quick Quote') {

                    const addendumAmt = getProject.addendum.iSeniorManagement;
                    const paymentPhase = paymentData.ePaymentPhases
                    const paymentStep = paymentData.ePaymentStep;

                    if (paymentPhase === "ThreePhase") {
                        if (paymentStep === '1') {
                            const calPayAmt = addendumAmt / 2;

                            //insert data
                            commissionObj.iAgentId = element._id;
                            commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                            calPayroll.push({
                                iAgentId: element._id,
                                agentName: element.vFirstName + ' ' + element.vLastName,
                                amt: commissionObj.iAmount
                            })
                            // saveProjectCommission(commissionObj)
                            console.log("Senior Management", calPayAmt)
                        } else {
                            const calPayAmt = addendumAmt / 4;

                            //insert data
                            commissionObj.iAgentId = element._id;
                            commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                            calPayroll.push({
                                iAgentId: element._id,
                                agentName: element.vFirstName + ' ' + element.vLastName,
                                amt: commissionObj.iAmount
                            })
                            // saveProjectCommission(commissionObj)
                            console.log("Senior Management", calPayAmt)
                        }
                    } else if (paymentPhase === "FourPhase") {
                        const calPayAmt = addendumAmt / 4;

                        //insert data
                        commissionObj.iAgentId = element._id;
                        commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                        calPayroll.push({
                            iAgentId: element._id,
                            agentName: element.vFirstName + ' ' + element.vLastName,
                            amt: commissionObj.iAmount
                        })
                        // saveProjectCommission(commissionObj)
                        console.log("Senior Management", calPayAmt)
                    } else {
                        const calPayAmt = addendumAmt;

                        //insert data
                        commissionObj.iAgentId = element._id;
                        commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                        calPayroll.push({
                            iAgentId: element._id,
                            agentName: element.vFirstName + ' ' + element.vLastName,
                            amt: commissionObj.iAmount
                        })
                        // saveProjectCommission(commissionObj)
                        console.log("Senior Management", calPayAmt)
                    }

                } else {
                    const calPayAmt = iSalesPrice * getProject.addendum.iSeniorManagement / 100;

                    //insert data
                    commissionObj.iAgentId = element._id;
                    commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                    calPayroll.push({
                        iAgentId: element._id,
                        agentName: element.vFirstName + ' ' + element.vLastName,
                        amt: commissionObj.iAmount
                    })
                    // saveProjectCommission(commissionObj)
                    console.log("Senior Management", calPayAmt)
                }

            } else {

                if (agentNotAvailable.length > 0) {

                    var amt = 0;

                    if (getProject.vQuote === 'Quick Quote') {
                        const addendumAmt = getProject.addendum.iSeniorManagementMargin;
                        const paymentPhase = paymentData.ePaymentPhases
                        const paymentStep = paymentData.ePaymentStep;

                        if (paymentPhase === "ThreePhase") {
                            if (paymentStep === '1') {
                                amt = addendumAmt / 2;
                            } else {
                                amt = addendumAmt / 4;
                            }
                        } else if (paymentPhase === "FourPhase") {
                            amt = addendumAmt / 4;
                        } else {
                            amt = addendumAmt;
                        }
                    } else {
                        amt = iSalesPrice * getProject.addendum.iSeniorManagementMargin / 100;
                    }

                    for (let i = 0; i < agentNotAvailable.length; i++) {
                        const e1 = agentNotAvailable[i];
                        const position = element.iAgentType.iPosition;
                        if ((e1 - 1) == parseInt(position) || !agentAvailable.includes(e1 - 1)) {

                            switch (e1) {

                                case 4:
                                    if (getProject.vQuote === 'Quick Quote') {
                                        const addendumAmt = getProject.addendum.iSalesDirectorMargin;
                                        const paymentPhase = paymentData.ePaymentPhases
                                        const paymentStep = paymentData.ePaymentStep;

                                        if (paymentPhase === "ThreePhase") {
                                            if (paymentStep === '1') {
                                                var calAmt = addendumAmt / 2;
                                            } else {
                                                var calAmt = addendumAmt / 4;
                                            }
                                        } else if (paymentPhase === "FourPhase") {
                                            var calAmt = addendumAmt / 4;
                                        } else {
                                            var calAmt = addendumAmt;
                                        }
                                    } else {
                                        var calAmt = iSalesPrice * getProject.addendum.iSalesDirectorMargin / 100;
                                    }
                                    amt += calAmt
                                    break

                                default:
                                    break;
                            }
                        }
                    }

                    //insert data
                    commissionObj.iAgentId = element._id;
                    commissionObj.iAmount = parseFloat(amt).toFixed(2)

                    calPayroll.push({
                        iAgentId: element._id,
                        agentName: element.vFirstName + ' ' + element.vLastName,
                        amt: commissionObj.iAmount
                    })
                    // saveProjectCommission(commissionObj)
                    console.log("Senior Management margin amt", amt)

                } else {

                    const iSalesPrice = paymentData.iSalesPrice;
                    const calPayAmt = iSalesPrice * getProject.addendum.iSeniorManagementMargin / 100;

                    //insert data
                    commissionObj.iAgentId = element._id;
                    commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                    calPayroll.push({
                        iAgentId: element._id,
                        agentName: element.vFirstName + ' ' + element.vLastName,
                        amt: commissionObj.iAmount
                    })
                    // saveProjectCommission(commissionObj)
                    console.log("Senior Management margin", calPayAmt)
                }

            }

        } else if (element.iAgentType.vTitle === 'National Sales Director') {

            const iSalesPrice = paymentData.iSalesPrice;

            if (getProject.AgentId._id.equals(element._id) === true) {

                if (getProject.vQuote === 'Quick Quote') {

                    const addendumAmt = getProject.addendum.iSalesDirector;
                    const paymentPhase = paymentData.ePaymentPhases
                    const paymentStep = paymentData.ePaymentStep;

                    if (paymentPhase === "ThreePhase") {
                        if (paymentStep === '1') {
                            const calPayAmt = addendumAmt / 2;

                            //insert data
                            commissionObj.iAgentId = element._id;
                            commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                            calPayroll.push({
                                iAgentId: element._id,
                                agentName: element.vFirstName + ' ' + element.vLastName,
                                amt: commissionObj.iAmount
                            })
                            // saveProjectCommission(commissionObj)
                            console.log("Sales Directo margin", calPayAmt)
                        } else {
                            const calPayAmt = addendumAmt / 4;

                            //insert data
                            commissionObj.iAgentId = element._id;
                            commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                            calPayroll.push({
                                iAgentId: element._id,
                                agentName: element.vFirstName + ' ' + element.vLastName,
                                amt: commissionObj.iAmount
                            })
                            // saveProjectCommission(commissionObj)
                            console.log("Sales Directo margin", calPayAmt)
                        }
                    } else if (paymentPhase === "FourPhase") {
                        const calPayAmt = addendumAmt / 4;

                        //insert data
                        commissionObj.iAgentId = element._id;
                        commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                        calPayroll.push({
                            iAgentId: element._id,
                            agentName: element.vFirstName + ' ' + element.vLastName,
                            amt: commissionObj.iAmount
                        })
                        // saveProjectCommission(commissionObj)
                        console.log("Sales Directo margin", calPayAmt)
                    } else {
                        const calPayAmt = addendumAmt;

                        //insert data
                        commissionObj.iAgentId = element._id;
                        commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                        calPayroll.push({
                            iAgentId: element._id,
                            agentName: element.vFirstName + ' ' + element.vLastName,
                            amt: commissionObj.iAmount
                        })
                        // saveProjectCommission(commissionObj)
                        console.log("Sales Directo margin", calPayAmt)
                    }

                } else {
                    const calPayAmt = iSalesPrice * getProject.addendum.iSalesDirector / 100;

                    //insert data
                    commissionObj.iAgentId = element._id;
                    commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                    calPayroll.push({
                        iAgentId: element._id,
                        agentName: element.vFirstName + ' ' + element.vLastName,
                        amt: commissionObj.iAmount
                    })
                    // saveProjectCommission(commissionObj)
                    console.log("Sales Directo", calPayAmt)
                }
            } else {

                if (agentNotAvailable.length > 0) {
                    var amt = 0;

                    if (getProject.vQuote === 'Quick Quote') {
                        const addendumAmt = getProject.addendum.iSalesDirectorMargin;

                        const paymentPhase = paymentData.ePaymentPhases
                        const paymentStep = paymentData.ePaymentStep;

                        if (paymentPhase === "ThreePhase") {
                            if (paymentStep === '1') {
                                amt = addendumAmt / 2;
                            } else {
                                amt = addendumAmt / 4;
                            }
                        } else if (paymentPhase === "FourPhase") {
                            amt = addendumAmt / 4;
                        } else {
                            amt = addendumAmt;
                        }
                    } else {
                        amt = iSalesPrice * getProject.addendum.iSalesDirectorMargin / 100;
                    }
                }

                //insert data
                commissionObj.iAgentId = element._id;
                commissionObj.iAmount = parseFloat(amt).toFixed(2)

                calPayroll.push({
                    iAgentId: element._id,
                    agentName: element.vFirstName + ' ' + element.vLastName,
                    amt: commissionObj.iAmount
                })
                // saveProjectCommission(commissionObj)

                console.log("National Sales Director margin amt", amt)
            }

        } else if (element.iAgentType.vTitle === 'National Sales Representative') {

            const iSalesPrice = paymentData.iSalesPrice;

            if (getProject.AgentId._id.equals(element._id) === true) {

                if (getProject.vQuote === 'Quick Quote') {

                    const addendumAmt = getProject.addendum.iSalesRepresentative;
                    const paymentPhase = paymentData.ePaymentPhases
                    const paymentStep = paymentData.ePaymentStep;

                    if (paymentPhase === "ThreePhase") {
                        if (paymentStep === '1') {
                            const calPayAmt = addendumAmt / 2;

                            //insert data
                            commissionObj.iAgentId = element._id;
                            commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                            calPayroll.push({
                                iAgentId: element._id,
                                agentName: element.vFirstName + ' ' + element.vLastName,
                                amt: commissionObj.iAmount
                            })
                            // saveProjectCommission(commissionObj)
                            console.log("National Sales Representative", calPayAmt)
                        } else {
                            const calPayAmt = addendumAmt / 4;

                            //insert data
                            commissionObj.iAgentId = element._id;
                            commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                            calPayroll.push({
                                iAgentId: element._id,
                                agentName: element.vFirstName + ' ' + element.vLastName,
                                amt: commissionObj.iAmount
                            })
                            // saveProjectCommission(commissionObj)
                            console.log("National Sales Representative", calPayAmt)
                        }
                    } else if (paymentPhase === "FourPhase") {
                        const calPayAmt = addendumAmt / 4;

                        //insert data
                        commissionObj.iAgentId = element._id;
                        commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                        calPayroll.push({
                            iAgentId: element._id,
                            agentName: element.vFirstName + ' ' + element.vLastName,
                            amt: commissionObj.iAmount
                        })
                        // saveProjectCommission(commissionObj)
                        console.log("National Sales Representative", calPayAmt)
                    } else {
                        const calPayAmt = addendumAmt;

                        //insert data
                        commissionObj.iAgentId = element._id;
                        commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                        calPayroll.push({
                            iAgentId: element._id,
                            agentName: element.vFirstName + ' ' + element.vLastName,
                            amt: commissionObj.iAmount
                        })
                        // saveProjectCommission(commissionObj)
                        console.log("National Sales Representative", calPayAmt)
                    }

                } else {
                    const calPayAmt = iSalesPrice * getProject.addendum.iSalesRepresentative / 100;

                    //insert data
                    commissionObj.iAgentId = element._id;
                    commissionObj.iAmount = parseFloat(calPayAmt).toFixed(2)

                    calPayroll.push({
                        iAgentId: element._id,
                        agentName: element.vFirstName + ' ' + element.vLastName,
                        amt: commissionObj.iAmount
                    })
                    // saveProjectCommission(commissionObj)
                    console.log("National Sales Representative", calPayAmt)
                }

            }

        }

    }

    res.send(calPayroll.reverse())
}

function saveProjectCommission(commissionObj) {
    const ProjectCommission = new ProjectCommissionModel({
        iProjectId: commissionObj.iProjectId,
        iProjectPaymentId: commissionObj.iProjectPaymentId,
        iAgentId: commissionObj.iAgentId,
        iAmount: commissionObj.iAmount,
        dtAddedDate: commissionObj.dtAddedDate
    });
}

exports.payroll_action = async (req, res) => {

    const iProjectId = req.body.iProId;
    const projectData = await ProjectModel.findOne({ _id: iProjectId });

    try {

        if (projectData.iPayrollAmt) {

            const vAgentCommission = JSON.parse(req.body.vAgentCommission);
            const payrollPaymentData = await PayrollPaymentModel.findOne({ _id: req.body.commissionId });

            if (payrollPaymentData.ePaymentStatus !== 'succeeded') {

                const paymentStep = parseInt(payrollPaymentData.ePaymentStep) - 1;

                const checkBeforePayment = await PayrollPaymentModel.findOne({iProjectId:iProjectId,ePaymentStep:paymentStep});

                if(checkBeforePayment.ePaymentPhases === 'succeeded'){
                    
                    PayrollPaymentModel.findOneAndUpdate({ _id: payrollPaymentData._id }, {
                        "$set": {
                            "ePaymentStatus": 'succeeded'
                        }
                    }).exec(function (error, result) {
    
                        const commissiondate = moment().format('MM/DD/YYYY');
                        const commissionObj = {
                            iProjectId: result.iProjectId,
                            iProjectPaymentId: result._id,
                            dtAddedDate: commissiondate,
                            vAgentCommission: vAgentCommission
                        }
                        addProjectCommission(commissionObj)
                    })
                }else{
                    req.flash('error', 'Please first complete the previous payment');
                    res.redirect("/commission-master/add/"+iProjectId);
                }

            }else{
                req.flash('error', 'This Payment is already done please select next payment');
                res.redirect("/commission-master/add/"+iProjectId);
            }

        } else {

            const iAmount = req.body.iAmount;
            const vAgentCommission = JSON.parse(req.body.vAgentCommission);
            const selectedPaymentStep = req.body.ePaymentStep;

            ProjectModel.findOneAndUpdate({ _id: iProjectId }, { "$set": { "iPayrollAmt": iAmount } }).exec(async function (error, result) {

                if (!error) {

                    //save payment phases
                    var dtProjectStartDate = new Date(result.dtProjectStartDate);
                    var dtDeadlineDate = new Date(result.dtDeadlineDate);
                    var diffDays = parseInt((dtDeadlineDate - dtProjectStartDate) / (1000 * 3600 * 24)); //gives day difference 

                    if (result.eIsRecurring == 'Yes') {

                        var amtPrice = iAmount;
                        var ePaymentStep = 1;
                        var dtPaymentDate = moment(dtProjectStartDate).add(1, 'days').format('MM/DD/YYYY');
                        savePaymentData(ePaymentStep, amtPrice, dtPaymentDate)
                    } else {

                        if (result.ePaymentPhases == 'ThreePhase') {
                            for (var i = 0; i < 3; i++) {
                                if (i == 0) {
                                    var amtPrice = iAmount / 2;
                                    var ePaymentStep = i + 1;

                                    //payment date
                                    var dtPaymentDate = calculateDate('ThreePhase', ePaymentStep, diffDays, dtProjectStartDate, dtDeadlineDate)
                                }
                                if (i == 1 || i == 2) {
                                    var partOne = iAmount / 2;
                                    var amtPrice = partOne / 2;
                                    var ePaymentStep = i + 1;

                                    //payment date
                                    var dtPaymentDate = calculateDate('ThreePhase', ePaymentStep, diffDays, dtProjectStartDate, dtDeadlineDate)
                                }
                                savePaymentData(ePaymentStep, amtPrice, dtPaymentDate)
                            }
                        }

                        if (projectData.ePaymentPhases == 'FourPhase') {
                            for (let i = 0; i < 4; i++) {
                                var amtPrice = iAmount / 4;
                                var ePaymentStep = i + 1;
                                //payment date
                                var dtPaymentDate = calculateDate('FourPhase', ePaymentStep, diffDays, dtProjectStartDate, dtDeadlineDate)
                                savePaymentData(ePaymentStep, amtPrice, dtPaymentDate)
                            }
                        }

                        if (projectData.ePaymentPhases == 'FullPayment') {

                            var amtPrice = iAmount;

                            //payment Date
                            var dtPaymentDate = moment(dtProjectStartDate).add(1, 'days').format('MM/DD/YYYY');
                            var ePaymentStep = 1;
                            savePaymentData(ePaymentStep, amtPrice, dtPaymentDate)
                        }

                        function calculateDate(paymentPhase, paymentStep, finishInDays, startDate, endDate) {

                            if (paymentStep == 1) {
                                var startDate1 = moment(startDate).add(1, 'days').format('MM/DD/YYYY');
                                return startDate1;

                            } else if (paymentStep == 2) {

                                var startDate2 = startDate;
                                if (paymentPhase == 'ThreePhase') {
                                    var date = moment(startDate2).add((finishInDays / 2), 'days').format('MM/DD/YYYY');
                                    return date;
                                }

                                if (paymentPhase == 'FourPhase') {
                                    var date = moment(startDate2).add((finishInDays / 4), 'days').format('MM/DD/YYYY');
                                    return date;
                                }
                            } else if (paymentStep == 3) {
                                if (paymentPhase == 'ThreePhase') {
                                    return moment(endDate).format('MM/DD/YYYY');
                                }
                                if (paymentPhase == 'FourPhase') {
                                    var date = moment(startDate).add(((finishInDays / 4) * 2), 'days').format('MM/DD/YYYY');
                                    return date;
                                }
                            } else {
                                return moment(endDate).format('MM/DD/YYYY');
                            }

                        }

                    }

                    function savePaymentData(ePaymentStep, amtPrice, dtPaymentDate) {

                        const ProjectPayment = new PayrollPaymentModel({
                            iProjectId: projectData._id,
                            iAmount: parseFloat(amtPrice).toFixed(2),
                            ePaymentPhases: projectData.ePaymentPhases,
                            dtPaymentDate: dtPaymentDate,
                            ePaymentStep: ePaymentStep,
                            ePaymentStatus: parseInt(ePaymentStep) == selectedPaymentStep ? 'succeeded' : null
                        });

                        ProjectPayment.save(function (error, result) {

                            if (!error) {

                                if (parseInt(result.ePaymentStep) == selectedPaymentStep) {

                                    const commissiondate = moment().format('MM/DD/YYYY');
                                    const commissionObj = {
                                        iProjectId: result.iProjectId,
                                        iProjectPaymentId: result._id,
                                        dtAddedDate: commissiondate,
                                        vAgentCommission: vAgentCommission
                                    }
                                    addProjectCommission(commissionObj)
                                }
                            }
                        });

                    }
                }
            });

            res.redirect("/commission-master");

        }
    } catch (error) {
        req.flash('error', 'Something to wrong');
        res.redirect("/dashboard");
    }
}

function addProjectCommission(commissionObj) {

    for (let index = 0; index < commissionObj.vAgentCommission.length; index++) {
        const element = commissionObj.vAgentCommission[index];

        commissionObj.iAgentId = element.iAgentId;
        commissionObj.iAmount = element.amt;

        const ProjectCommission = new ProjectCommissionModel(commissionObj);
        ProjectCommission.save();
    }

}