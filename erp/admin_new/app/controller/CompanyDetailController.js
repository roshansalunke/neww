const async = require("hbs/lib/async");
const CompanyDetailModel = require("../model/CompanyDetailModel");
const UserModel = require("../model/UserModel");
const AdminTypeModel = require("../model/AdminTypeModel");
const GeneralLibrary = require("../library/GeneralLibrary");

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/company_detail/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {
        let favicon_name = null;
        let logo_name = null;

        if (req.body.iCompanyDetailId) {
            try {
                if (req.files != null) {
                    if (req.files.vFavicon != null) {
                        // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vFavicon, 'company-detail/');
                        // favicon_name = awsURL;

                        /**** Local Files Upload ****/
                        let CRITERIA = {
                            files: req.files.vFavicon,
                            path: 'company-detail'
                        };
                        favicon_name = GeneralLibrary.localFilesUpload(CRITERIA);
                        /**** Local END ****/
                    }

                    if (req.files.vLogo != null) {
                        // var awsURL2 = await GeneralLibrary.awsFileUpload(req.files.vLogo, 'company-detail/');
                        // logo_name = awsURL2;

                        /**** Local Files Upload ****/
                        let CRITERIA = {
                            files: req.files.vLogo,
                            path: 'company-detail'
                        };
                        logo_name = GeneralLibrary.localFilesUpload(CRITERIA);
                        /**** Local END ****/
                    }
                }

                if (req.files != null) {

                    if (req.files.vFavicon && req.files.vLogo) {
                        var UPDATE_QUERY = {
                            "$set": {
                                "tAddress": req.body.tAddress,
                                "tDescription": req.body.tDescription,
                                "vEmail": req.body.vEmail,
                                "vFavicon": favicon_name,
                                "vLogo": logo_name,
                                "vName": req.body.vName,
                                "vNumber": req.body.vNumber,
                                "vWebsite": req.body.vWebsite,
                                "vCopyright": req.body.vCopyright,
                                "vItemPerPage": req.body.vItemPerPage,
                                "iNotificationAdminId": req.body.iNotificationAdminId
                            }
                        }
                    } else if (req.files.vFavicon != null) {
                        var UPDATE_QUERY = {
                            "$set": {
                                "tAddress": req.body.tAddress,
                                "tDescription": req.body.tDescription,
                                "vEmail": req.body.vEmail,
                                "vFavicon": favicon_name,
                                "vName": req.body.vName,
                                "vNumber": req.body.vNumber,
                                "vWebsite": req.body.vWebsite,
                                "vCopyright": req.body.vCopyright,
                                "vItemPerPage": req.body.vItemPerPage,
                                "iNotificationAdminId": req.body.iNotificationAdminId
                            }
                        }
                    } else if (req.files.vLogo != null) {
                        var UPDATE_QUERY = {
                            "$set": {
                                "tAddress": req.body.tAddress,
                                "tDescription": req.body.tDescription,
                                "vEmail": req.body.vEmail,
                                "vLogo": logo_name,
                                "vName": req.body.vName,
                                "vNumber": req.body.vNumber,
                                "vWebsite": req.body.vWebsite,
                                "vCopyright": req.body.vCopyright,
                                "vItemPerPage": req.body.vItemPerPage,
                                "iNotificationAdminId": req.body.iNotificationAdminId
                            }
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "tAddress": req.body.tAddress,
                            "tDescription": req.body.tDescription,
                            "vEmail": req.body.vEmail,
                            "vName": req.body.vName,
                            "vNumber": req.body.vNumber,
                            "vWebsite": req.body.vWebsite,
                            "vCopyright": req.body.vCopyright,
                            "vItemPerPage": req.body.vItemPerPage,
                            "iNotificationAdminId": req.body.iNotificationAdminId
                        }
                    }
                }

                CompanyDetailModel.findOneAndUpdate({
                    _id: req.body.iCompanyDetailId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("back");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            if (req.files != null) {
                // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vFavicon, 'company-detail/');
                // favicon_name = awsURL;

                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vFavicon,
                    path: 'company-detail'
                };
                favicon_name = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/

                // var awsURL2 = await GeneralLibrary.awsFileUpload(req.files.vLogo, 'company-detail/');
                // logo_name = awsURL2;

                /**** Local Files Upload ****/
                let CRITERIA2 = {
                    files: req.files.vLogo,
                    path: 'company-detail'
                };
                logo_name = GeneralLibrary.localFilesUpload(CRITERIA2);
                /**** Local END ****/
            }
            const Company = new CompanyDetailModel({
                tAddress: req.body.tAddress,
                tDescription: req.body.tDescription,
                vEmail: req.body.vEmail,
                vFavicon: favicon_name,
                vLogo: logo_name,
                vName: req.body.vName,
                vNumber: req.body.vNumber,
                vWebsite: req.body.vWebsite,
                vCopyright: req.body.vCopyright,
                vItemPerPage: req.body.vItemPerPage,
                iNotificationAdminId: req.body.iNotificationAdminId
            });
            try {
                Company.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("back");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iCompanyDetailId = req.params.iCompanyDetailId;

    if (req.session.email) {
        try {

            const adminData = await UserModel.find({
                eType: 'Admin'
            });
            var companyData = await CompanyDetailModel.findOne({
                _id: iCompanyDetailId
            });
            if (companyData != null) {
                is_layout = true;
                res.render("../view/company_detail/add", {
                    is_layout: is_layout,
                    companyData: companyData,
                    adminData: adminData
                });
            } else {
                res.redirect('/company-detail/add')
            }
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};