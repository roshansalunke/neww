const async = require("hbs/lib/async");
const ContractModel = require("../model/ContractModel");
const ProjectTypeModel = require("../model/ProjectTypeModel");
const Paginator = require("../library/PaginatorLibrary");
const AgentContractTypeModel = require("../model/AgentContractTypeModel");
const CompanyDetailModel = require("../model/CompanyDetailModel");

exports.index = async(req, res) => {

    if (req.session.email) {

        try {
            var contractData = await ContractModel.find().sort({_id:'asc'});
    
            for (let index = 0; index < contractData.length; index++) {
                const element = contractData[index];
                if (element.iProjectTypeId != '') {
                    var ProjectTypeData = await ProjectTypeModel.findOne({
                        _id: element.iProjectTypeId
                    });
                    if(ProjectTypeData){
                        element.iProjectTypeId = ProjectTypeData.vTitle
                    }
                }
    
                if (element.iAgentContractTypeId != '') {
                    var AgentContractTypeData = await AgentContractTypeModel.findOne({
                        _id: element.iAgentContractTypeId
                    });
                    element.iAgentContractTypeId = AgentContractTypeData.vTitle;
                }
            }
    
            is_layout = true;
            res.render("../view/contract/index", {
                is_layout: is_layout,
                contractData:contractData
            });
        } catch (error) {
            is_layout = true;
            res.render("../view/contract/index", {
                is_layout: is_layout,
                contractData:null
            });
        }

    } else {
        res.redirect("/");
    }

};

exports.add = async(req, res) => {

    if (req.session.email) {
        try {

            var ProjectTypeData = await ProjectTypeModel.find();
            var AgentContractTypeData = await AgentContractTypeModel.find();
            is_layout = true;
            res.render("../view/contract/add", {
                is_layout: is_layout,
                ProjectTypeData: ProjectTypeData,
                AgentContractTypeData: AgentContractTypeData
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async(req, res) => {

    if (req.session.email) {

        if (req.body.iContractId) {
            try {
                ContractModel.findOneAndUpdate({
                    _id: req.body.iContractId
                }, {
                    "$set": {
                        "iProjectTypeId": req.body.iProjectTypeId,
                        "ePaymentPhases": req.body.ePaymentPhases,
                        "iAgentContractTypeId": req.body.iAgentContractTypeId,
                        "vDetail": req.body.vDetail,
                        "vPdfDetail": req.body.vPdfDetail,
                        "eStatus": req.body.eStatus,
                    }
                }).exec(function(error, result) {
                    if (!error) {
                        res.redirect("/contract");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const Contract = new ContractModel({
                iProjectTypeId: req.body.iProjectTypeId,
                ePaymentPhases: req.body.ePaymentPhases,
                iAgentContractTypeId: req.body.iAgentContractTypeId,
                vDetail: req.body.vDetail,
                vPdfDetail: req.body.vPdfDetail,
                eStatus: req.body.eStatus,
            });
            try {
                Contract.save(function(error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/contract");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async(req, res) => {

    var iContractId = req.params.iContractId;

    if (req.session.email) {
        try {
            var contractData = await ContractModel.findOne({
                _id: iContractId
            });
            var ProjectTypeData = await ProjectTypeModel.find();
            var AgentContractTypeData = await AgentContractTypeModel.find();
            is_layout = true;
            res.render("../view/contract/add", {
                is_layout: is_layout,
                contractData: contractData,
                ProjectTypeData: ProjectTypeData,
                AgentContractTypeData: AgentContractTypeData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iContractId = req.body.iContractId;

        try {
            if (iContractId) {
                //single record deleted
                if (vAction === "delete") {
                    await ContractModel.deleteOne({
                        _id: iContractId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let contractID = iContractId.split(',');
                    await ContractModel.deleteMany({
                        _id: {
                            $in: contractID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}