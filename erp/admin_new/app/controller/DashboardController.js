const async = require("hbs/lib/async");
const UserModel = require("../model/UserModel");
const NotificationModel = require("../model/NotificationModel");
const TrainingCategoryModel = require("../model/TrainingCategoryModel");

exports.index = async (req, res) => {

    if (req.session.email) {
        var SQL = {};
        SQL.eStatus = "Active";
        var total_user = await UserModel.count(SQL).exec();
        var total_category = await TrainingCategoryModel.count(SQL).exec();

        var profile = await UserModel.findOne({
            _id: req.session.userid
        });

        var categoryData = await TrainingCategoryModel.find(SQL).limit(5);

        is_layout = true;


        var data = await UserModel.find()

        res.render("../view/dashboard/index", {
            is_layout: is_layout,
            total_category: total_category,
            total_user: total_user,
            categoryData: categoryData,
            data: data,
            profile: profile,
        });
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iAdminId = req.body.iAdminId;

        try {
            if (iAdminId) {

                //single record deleted
                if (vAction === "delete") {
                    await UserModel.deleteOne({
                        _id: iAdminId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let userID = iAdminId.split(',');
                    await UserModel.deleteMany({
                        _id: {
                            $in: userID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}