const async = require("hbs/lib/async");
const DepartmentModel = require("../model/DepartmentModel");
const Paginator = require("../library/PaginatorLibrary");

exports.index = async(req, res) => {

    if (req.session.email) {

        var departmentData = await DepartmentModel.find().sort({'iPosition':'asc'});
        is_layout = true;
        res.render("../view/department/index", {
            is_layout: is_layout,
            departmentData:departmentData
        });
    } else {
        res.redirect("/");
    }
};

exports.add = async(req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/department/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async(req, res) => {

    if (req.session.email) {

        if (req.body.iDepartmentId) {
            try {
                DepartmentModel.findOneAndUpdate({ _id: req.body.iDepartmentId }, {
                    "$set": {
                        "vTitle": req.body.vTitle,
                        "iPosition":req.body.iPosition,
                        "vDivId":req.body.vDivId,
                        "vDataTarget":req.body.vDataTarget,
                        "eStatus": req.body.eStatus
                    }
                }).exec(function(error, result) {
                    if (!error) {
                        res.redirect("/department");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const Department = new DepartmentModel({
                vTitle: req.body.vTitle,
                iPosition:req.body.iPosition,
                vDivId:req.body.vDivId,
                vDataTarget:req.body.vDataTarget,
                eStatus: req.body.eStatus
            });
            try {
                Department.save(function(error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/department");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async(req, res) => {
    if (req.session.email) {
        try {
            var iDepartmentId = req.params.iDepartmentId;

            var departmentData = await DepartmentModel.findOne({ _id: iDepartmentId });
            is_layout = true;
            res.render("../view/department/add", {
                is_layout: is_layout,
                departmentData: departmentData,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iDepartmentId = req.body.iDepartmentId;

        try {
            if (iDepartmentId) {

                //single record deleted
                if (vAction === "delete") {
                    await DepartmentModel.deleteOne({
                        _id: iDepartmentId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let departmentID = iDepartmentId.split(',');
                    await DepartmentModel.deleteMany({
                        _id: {
                            $in: departmentID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}