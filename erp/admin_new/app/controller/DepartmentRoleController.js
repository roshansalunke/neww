const async = require("hbs/lib/async");
const DepartmentRoleModel = require("../model/DepartmentRoleModel");
const DepartmentModel = require("../model/DepartmentModel");
const Paginator = require("../library/PaginatorLibrary");

exports.index = async(req, res) => {

    if (req.session.email) {
        is_layout = true;
        var departmentRoleData = await DepartmentRoleModel.find().populate('iDepartmentId')
            .sort('iDepartmentId.iPosition');
        res.render("../view/department_role/index", {
            is_layout: is_layout,
            departmentRoleData:departmentRoleData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async(req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            var departmentData = await DepartmentModel.find().sort({'iPosition':'asc'});
            res.render("../view/department_role/add", {
                is_layout: is_layout,
                departmentData:departmentData
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async(req, res) => {

    if (req.session.email) {

        if (req.body.iDepartmentRoleId) {
            try {
                var checkRole = req.body.isRole;
                if(checkRole === 'No'){
                    var setUrl =  req.body.vUrl;
                }else{
                    var setUrl =  `department-user/${req.body.iDepartmentRoleId}`;
                }
                DepartmentRoleModel.findOneAndUpdate({ _id: req.body.iDepartmentRoleId }, {
                    "$set": {
                        "vTitle": req.body.vTitle,
                        "iDepartmentId":req.body.iDepartmentId,
                        "iPosition":req.body.iPosition ? req.body.iPosition : 0,
                        "isRole":req.body.isRole,
                        "vUrl":setUrl,
                        "eStatus": req.body.eStatus
                    }
                }).exec(function(error, result) {
                    if (!error) {
                        res.redirect("/department-role");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {

            const Department = new DepartmentRoleModel({
                vTitle: req.body.vTitle,
                iDepartmentId:req.body.iDepartmentId,
                iPosition:req.body.iPosition ? req.body.iPosition : 0,
                isRole:req.body.isRole,
                vUrl:req.body.vUrl,
                eStatus: req.body.eStatus
            });
            try {
                Department.save(function(error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        if(result.isRole === 'Yes'){
                            DepartmentRoleModel.findOneAndUpdate({_id : result._id},{
                                "$set":{
                                    "vUrl" : `department-user/${result._id}`
                                }
                            }).exec();
                        }
                        res.redirect("/department-role");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async(req, res) => {
    if (req.session.email) {
        try {
            var iDepartmentRoleId = req.params.iDepartmentRoleId;
            var departmentData = await DepartmentModel.find().sort({'iPosition':'asc'});
            var departmentRoleData = await DepartmentRoleModel.findOne({ _id: iDepartmentRoleId });

            is_layout = true;
            res.render("../view/department_role/add", {
                is_layout: is_layout,
                departmentRoleData: departmentRoleData,
                departmentData: departmentData,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iDepartmentRoleId = req.body.iDepartmentRoleId;

        try {
            if (iDepartmentRoleId) {

                //single record deleted
                if (vAction === "delete") {
                    await DepartmentRoleModel.deleteOne({
                        _id: iDepartmentRoleId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let departmentRoleID = iDepartmentRoleId.split(',');
                    await DepartmentRoleModel.deleteMany({
                        _id: {
                            $in: departmentRoleID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}