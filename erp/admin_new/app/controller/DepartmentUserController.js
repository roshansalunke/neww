const async = require("hbs/lib/async");
const UserModel = require("../model/UserModel");
const SystemEmailModel = require("../model/SystemEmailModel");
const EmailLibrary = require("../library/EmailLibrary");
const DepartmentRoleModel = require("../model/DepartmentRoleModel");
const GeneralLibrary = require('../library/GeneralLibrary');
const crypto = require("crypto");

exports.index = async (req, res) => {

    if (req.session.email) {
        is_layout = true;
        var iRoleId = req.params.iRoleId;
        const roleData = await DepartmentRoleModel.findOne({ _id: iRoleId });
        var userData = await UserModel.find({vRole:roleData._id});
        res.render("../view/department_user/index", {
            is_layout: is_layout,
            roleData: roleData,
            userData:userData
        });
    } else {
        res.redirect("/");
    }
};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            var iRoleId = req.params.iRoleId;

            const roleData = await DepartmentRoleModel.findOne({ _id: iRoleId });
            const PositionRoleData = await DepartmentRoleModel.find({iDepartmentId:roleData.iDepartmentId, iPosition: { $gt : roleData.iPosition} }).sort({'iPosition':'asc'});
            res.render("../view/department_user/add", {
                is_layout: is_layout,
                roleData:roleData,
                PositionRoleData:PositionRoleData
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {
    if (req.session.email) {
        let image_name = null;

        if (req.body.iUserId) {
            try {
                if (req.files != null) {
                    // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'profile/');
                    // image_name = awsURL;

                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vImage,
                        path: 'profile'
                    };
                    image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }

                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vImage": image_name,
                            "vEmail": req.body.vEmail,
                            "vDepartment": req.body.vDepartment,
                            "vRole":req.body.vRole,
                            "vCity": req.body.vCity,
                            "vCountry": req.body.vCountry,
                            "vZipCode": req.body.vZipCode,
                            "vState": req.body.vState,
                            "vPhone": req.body.vPhone,
                            "vAddress": req.body.vAddress,
                            // "eStatus": req.body.eStatus
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vEmail": req.body.vEmail,
                            "vDepartment": req.body.vDepartment,
                            "vRole":req.body.vRole,
                            "vCity": req.body.vCity,
                            "vCountry": req.body.vCountry,
                            "vZipCode": req.body.vZipCode,
                            "vState": req.body.vState,
                            "vPhone": req.body.vPhone,
                            "vAddress": req.body.vAddress,
                            // "eStatus": req.body.eStatus
                        }
                    }
                }

                UserModel.findOneAndUpdate({
                    _id: req.body.iUserId
                }, UPDATE_QUERY,{
                    returnDocument: 'after'
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect(`/department-user/${result.vRole}`);
                    } else {
                        console.log(error);
                    }
                });

            } catch (error) {
                console.log(error);
            }
        } else {
            if (req.files != null) {
                // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'profile/');
                // image_name = awsURL;

                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vImage,
                    path: 'profile'
                };
                image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }

            const addedDate = new Date().toLocaleDateString();
            var randomNumber = Math.floor(100000 + Math.random() * 900000);
            var verifyCode = Math.floor(1000000 + Math.random() * 9000000);
            var verifyToken = crypto.randomBytes(20).toString("hex");

            const vPassword = crypto.randomBytes(10).toString("hex");

            const User = new UserModel({
                vFirstName: req.body.vFirstName,
                vLastName: req.body.vLastName,
                vEmail: req.body.vEmail,
                vCity: req.body.vCity,
                vCountry: req.body.vCountry,
                vZipCode: req.body.vZipCode,
                vState: req.body.vState,
                vPassword: vPassword,
                vImage: image_name,
                vDepartment:req.body.vDepartment,
                vRole: req.body.vRole,
                vPhone: req.body.vPhone,
                vAddress: req.body.vAddress,
                eStatus: 'Active',
                dtAddedDate: addedDate,
                iUserID: randomNumber,
                vVerifyCode: verifyCode,
                vVerifyToken: verifyToken
            });

            let email_template = await SystemEmailModel.find({
                'vEmailCode': 'USER_REGISTER'
            });

            try {
                User.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        const vVerifyURL = process.env.APP_URL + "verify/" + result.vVerifyToken;

                        let criteria = {
                            user_name: result.vFirstName + ' ' + result.vLastName,
                            email_to: result.vEmail,
                            password: vPassword,
                            email_subject: "ERP",
                            email_template: email_template,
                            verifycode: result.vVerifyCode,
                            verifyurl: vVerifyURL
                        }
                        EmailLibrary.send_email(criteria);
                        res.redirect(`/department-user/${result.vRole}`);

                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async (req, res) => {

    var iUserId = req.params.iUserId;

    if (req.session.email) {
        try {
            var userData = await UserModel.findOne({
                _id: iUserId
            });

            var iRoleId = userData.vRole;

            const roleData = await DepartmentRoleModel.findOne({ _id: iRoleId });
            const PositionRoleData = await DepartmentRoleModel.find({iDepartmentId:roleData.iDepartmentId, iPosition: { $gte : roleData.iPosition} }).sort({'iPosition':'asc'});
            is_layout = true;
            res.render("../view/department_user/add", {
                is_layout: is_layout,
                userData: userData,
                roleData:roleData,
                PositionRoleData:PositionRoleData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iUserId = req.body.iUserId;

        try {
            if (iUserId) {
                //single record deleted
                if (vAction === "delete") {
                    await UserModel.deleteOne({
                        _id: iUserId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let userID = iUserId.split(',');
                    await UserModel.deleteMany({
                        _id: {
                            $in: userID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}