const async = require("hbs/lib/async");
const DeveloperModel = require("../model/DeveloperModel");
const DeveloperTypeModel = require("../model/DeveloperTypeModel");
const SystemEmailModel = require("../model/SystemEmailModel");
const EmailLibrary = require("../library/EmailLibrary");
const crypto = require("crypto");
const bcrypt = require("bcrypt");
const GeneralLibrary = require("../library/GeneralLibrary");
const DeveloperProjectModel = require("../model/DeveloperProjectModel");
const moment = require("moment");

exports.index = async (req, res) => {
    if (req.session.email) {

        var developerData = await DeveloperModel.find().populate({
            path: "iDeveloperTypeId"
        }).sort({
            _id: 'desc'
        });
        is_layout = true;
        res.render("../view/developer/index", {
            is_layout: is_layout,
            developerData:developerData
        });
    } else {
        res.redirect("/");
    }
};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            var developerTypeData = await DeveloperTypeModel.find().sort('iPosition');

            is_layout = true;
            res.render("../view/developer/add", {
                is_layout: is_layout,
                developerTypeData: developerTypeData,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        var image_name = null;
        if (req.body.iDeveloperId) {
            try {
                if (req.files != null) {
                    // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'profile/');
                    // image_name = awsURL;

                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vImage,
                        path: 'profile'
                    };
                    image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }

                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vEmail": req.body.vEmail,
                            "vCity": req.body.vCity,
                            "vCountry": req.body.vCountry,
                            "vZipCode": req.body.vZipCode,
                            "iDeveloperTypeId": req.body.iDeveloperTypeId,
                            "vPhone": req.body.vPhone,
                            "vAddress": req.body.vAddress,
                            "eStatus": req.body.eStatus,
                            "vImage": image_name
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vEmail": req.body.vEmail,
                            "vCity": req.body.vCity,
                            "vCountry": req.body.vCountry,
                            "vZipCode": req.body.vZipCode,
                            "iDeveloperTypeId": req.body.iDeveloperTypeId,
                            "vPhone": req.body.vPhone,
                            "vAddress": req.body.vAddress,
                            "eStatus": req.body.eStatus
                        }
                    }
                }

                DeveloperModel.findOneAndUpdate({
                    _id: req.body.iDeveloperId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {

                        res.redirect("/developer");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            if (req.files != null) {
                // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'profile/');
                // image_name = awsURL;

                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vImage,
                    path: 'profile'
                };
                image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }
            const addedDate = moment().format('MM/DD/YYYY');
            var randomNumber = Math.floor(100000 + Math.random() * 900000);
            var verifyCode = Math.floor(1000000 + Math.random() * 9000000);
            var verifyToken = crypto.randomBytes(20).toString("hex");
            const vPassword = crypto.randomBytes(10).toString("hex");
            let email_template = await SystemEmailModel.find({
                'vEmailCode': 'DEVELOPER_REGISTER'
            });

            const Developer = new DeveloperModel({
                iUserID: randomNumber,
                iDeveloperTypeId: req.body.iDeveloperTypeId,
                vFirstName: req.body.vFirstName,
                vLastName: req.body.vLastName,
                vEmail: req.body.vEmail,
                vPassword: vPassword,
                vCountry: req.body.vCountry,
                vCity: req.body.vCity,
                vZipCode: req.body.vZipCode,
                vPhone: req.body.vPhone,
                vAddress: req.body.vAddress,
                eStatus: req.body.eStatus,
                vImage: image_name,
                dtAddedDate: addedDate,
                vVerifyCode: verifyCode,
                vVerifyToken: verifyToken
            });
            try {
                Developer.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {

                        //send notification
                        const CRITERIA = {
                            vNotification: `Admin is created a new developer account-${result.iUserID}-${result.vFirstName} ${result.vLastName}`,
                            iUserId: result._id,
                            vLink: '/profile/edit/'+result._id,
                            vTimezone: req.session.vTimezone,
                        }
                        GeneralLibrary.sendNotification(CRITERIA);

                        const vVerifyURL = process.env.DEVELOPER_APP_URL + "verify/" + result.vVerifyToken;

                        let criteria = {
                            user_name: result.vFirstName + ' ' + result.vLastName,
                            email_to: result.vEmail,
                            password: vPassword,
                            email_subject: "ERP",
                            email_template: email_template,
                            verifycode: result.vVerifyCode,
                            verifyurl: vVerifyURL
                        }
                        EmailLibrary.send_email(criteria);
                        res.redirect("/developer");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iDeveloperId = req.params.iDeveloperId;

    if (req.session.email) {
        try {
            var developerData = await DeveloperModel.findOne({
                _id: iDeveloperId
            });
            var developerTypeData = await DeveloperTypeModel.find();
            is_layout = true;
            res.render("../view/developer/add", {
                is_layout: is_layout,
                developerData: developerData,
                developerTypeData: developerTypeData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.view = async (req, res) => {

    if (req.session.email) {
        try {
            var iDeveloperId = req.params.iDeveloperId;
            var developerData = await DeveloperModel.findOne({
                _id: iDeveloperId
            }, 'iUserID vFirstName vLastName');

            var projectData = await DeveloperProjectModel.find({
                iDeveloperId: iDeveloperId
            }).populate({
                path: 'iProjectId',
                populate: {
                    path: 'iProjectTypeId',
                    select: 'vTitle'
                }
            }).sort({
                _id: 'desc'
            });

            is_layout = true;
            res.render("../view/developer/view", {
                is_layout: is_layout,
                developerData: developerData,
                projectData: projectData,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
}

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iDeveloperId = req.body.iDeveloperId;

        try {
            if (iDeveloperId) {
                //single record deleted
                if (vAction === "delete") {
                    await DeveloperModel.deleteOne({
                        _id: iDeveloperId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let userID = iDeveloperId.split(',');
                    await DeveloperModel.deleteMany({
                        _id: {
                            $in: userID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}