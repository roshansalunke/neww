const async = require("hbs/lib/async");
const DeveloperTypeModel = require("../model/DeveloperTypeModel");
const Paginator = require("../library/PaginatorLibrary");
const CompanyDetailModel = require("../model/CompanyDetailModel");

exports.index = async(req, res) => {

    if (req.session.email) {

        var developerTypeData = await DeveloperTypeModel.find().sort('iPosition');
        is_layout = true;
        res.render("../view/developer_type/index", {
            is_layout: is_layout,
            developerTypeData:developerTypeData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async(req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/developer_type/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async(req, res) => {

    if (req.session.email) {

        if (req.body.iDeveloperTypeId) {
            try {
                DeveloperTypeModel.findOneAndUpdate({
                    _id: req.body.iDeveloperTypeId
                }, {
                    "$set": {
                        "vTitle": req.body.vTitle,
                        "iPosition": req.body.iPosition,
                        "eStatus": req.body.eStatus
                    }
                }).exec(function(error, result) {
                    if (!error) {
                        res.redirect("/developer-type");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const DeveloperType = new DeveloperTypeModel({
                vTitle: req.body.vTitle,
                iPosition: req.body.iPosition,
                eStatus: req.body.eStatus
            });
            try {
                DeveloperType.save(function(error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/developer-type");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async(req, res) => {

    var iDeveloperTypeId = req.params.iDeveloperTypeId;

    if (req.session.email) {
        try {
            var developerTypeData = await DeveloperTypeModel.findOne({
                _id: iDeveloperTypeId
            });
            is_layout = true;
            res.render("../view/developer_type/add", {
                is_layout: is_layout,
                developerTypeData: developerTypeData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iDeveloperTypeId = req.body.iDeveloperTypeId;

        try {
            if (iDeveloperTypeId) {
                //single record deleted
                if (vAction === "delete") {
                    await DeveloperTypeModel.deleteOne({
                        _id: iDeveloperTypeId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let developerTypeID = iDeveloperTypeId.split(',');
                    await DeveloperTypeModel.deleteMany({
                        _id: {
                            $in: developerTypeID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}