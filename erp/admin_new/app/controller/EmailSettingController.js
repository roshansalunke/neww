const async = require("hbs/lib/async");
const EmailCredentialModel = require("../model/EmailCredentialModel");
const SystemEmailModel = require('../model/SystemEmailModel');
const EmailLibrary = require('../library/EmailLibrary');

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/email_setting/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iEmailSettingId) {
            try {
                EmailCredentialModel.findOneAndUpdate({
                    _id: req.body.iEmailSettingId
                }, {
                    "$set": {
                        "vMailer": req.body.vMailer,
                        "vHost": req.body.vHost,
                        "vPort": req.body.vPort,
                        "vUsername": req.body.vUsername,
                        "vPassword": req.body.vPassword,
                        "vEncryption": req.body.vEncryption,
                        "vFromAddress": req.body.vFromAddress,
                        "vFromName": req.body.vFromName
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("back");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const Email = new EmailCredentialModel({
                vMailer: req.body.vMailer,
                vHost: req.body.vHost,
                vPort: req.body.vPort,
                vUsername: req.body.vUsername,
                vPassword: req.body.vPassword,
                vEncryption: req.body.vEncryption,
                vFromAddress: req.body.vFromAddress,
                vFromName: req.body.vFromName
            });
            try {
                Email.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("back");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iEmailSettingId = req.params.iEmailSettingId;

    if (req.session.email) {
        try {
            var emailData = await EmailCredentialModel.findOne({
                _id: iEmailSettingId
            });
            if (emailData != null) {
                is_layout = true;
                res.render("../view/email_setting/add", {
                    is_layout: is_layout,
                    emailData: emailData
                });
            } else {
                res.redirect('/email-setting/add');
            }
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.test_email_action = async (req, res) => {
    if (req.session.email) {
        let email_template = await SystemEmailModel.find({
            'vEmailCode': 'TEST_EMAIL'
        });
        let criteria = {
            user_name: '',
            email_to: req.body.vTestEmail,
            password: '',
            email_subject: "ERP - Test Email",
            email_template: email_template,
        }
        req.flash('success', 'Email send successfully.');
        EmailLibrary.send_email(criteria);
        res.redirect("/email-setting/edit/" + req.body.idd);
    }
};