const async = require("hbs/lib/async");
const axios = require("axios");
const GeneralLibrary = require("../library/GeneralLibrary");
const request = require('request');
const {
    from
} = require("form-data");
var FormData = require("form-data");

const {
    ReturnDocument
} = require("mongodb");
const ChannelModel = require("../model/ChannelModel.js");

// Access scopes for token
const SCOPES = [
    "pages_read_engagement",
    "pages_show_list",
    "pages_manage_posts",
    "pages_read_user_content",
    "pages_manage_engagement",
    "read_insights"
];

const STRINGIFIED_SCOPES = SCOPES.join("%2c");
//login
exports.fbLogin = (req, res) => {
    try {
        res.redirect(
            `https://www.facebook.com/dialog/oauth?app_id=${process.env.APP_ID}&scope=${STRINGIFIED_SCOPES}&client_id=${process.env.APP_ID}&redirect_uri=${process.env.REDIRECT_URI}&response_type=code`
        );
    } catch (error) {
        console.log(error);
    }
}

//login callback
exports.callback = async(req, res) => {
    const code = req.query.code;
    const uri = `https://graph.facebook.com/oauth/access_token?client_id=${process.env.APP_ID}&redirect_uri=${process.env.REDIRECT_URI}&client_secret=${process.env.API_SECRET}&code=${code}`;
    try {
        const response = await axios.post(uri);
        req.session.userToken = response.data.access_token;
        let token = req.session.userToken
        let urlPageApi = `https://graph.facebook.com/v15.0/me/accounts?fields=name%2Cid%2Cpicture%7Burl%7D&access_token=${token}`
        let pageDataApi = await axios.get(urlPageApi)
        let pageData = pageDataApi.data.data

        res.render("../view/social_accounts/", {
            is_layout: true,
            pageData: pageData,
        });

    } catch (err) {
        console.log(err);
    }
}

//save the pagedetails in db
exports.pageDetails = async(req,res) => {
    if(req.session.email){
        try {

            let token = req.session.userToken
            let pageId = req.body.pageId
            var dtAddedDate = new Date().toLocaleDateString();

            const userData = await axios.get(
            `https://graph.facebook.com/v15.0/me?fields=id%2Cname%2Cpicture.type(large)&access_token=${token}`
            );
            
            let urlLongTokenUser = `https://graph.facebook.com/v15.0/oauth/access_token?  
                                    grant_type=fb_exchange_token&          
                                    client_id=${process.env.APP_ID}&
                                    client_secret=${process.env.API_SECRET}&
                                    fb_exchange_token=${token}`

            const longLiveUserTokenApi = await axios.get(urlLongTokenUser);
            let longLiveUserToken = longLiveUserTokenApi.data.access_token;

            const urlLongTokenPage = `https://graph.facebook.com/v15.0/${pageId}?fields=access_token%2Cid%2Cname%2Cpicture%7Burl%7D&access_token=${longLiveUserToken}`
            let longLivePageTokenApi = await axios.get(urlLongTokenPage);
            let pageData = longLivePageTokenApi.data;
            
           //profile upload aws
           let url;
           try {
               const response = await new Promise((resolve, reject) => {
                 request.get({
                   method: 'GET',
                   url: pageData.picture.data.url,
                   encoding: null, // If null, the body is returned as a Buffer.
                 }, (error, response, body) => {
                   if (error) {
                     reject(error);
                   } else {
                     resolve(response);
                   }
                 });
               });
           
               if (response.statusCode == 200) {
                 const data = {
                   name: response.request.uri.pathname.substring(1),
                   data: response.body,
                   mimetype: response.headers['content-type']
                 };
                 url = await GeneralLibrary.awsFileUpload(data, "socialPost/");
               } else {
                 throw new Error(`Request failed with status code ${response.statusCode}`);
               }
             } catch (error) {
               console.error(error);
             }
             pageData.picture.data.url = url
            

            let profileData = new ChannelModel({
                vTitle: "Facebook",
                vUserEmail: req.session.email,
                vUserData: {
                    iUserId: userData.data.id,
                    vUserProfile: userData.data.picture.data.url,
                    vUserName: userData.data.name,
                    vUserToken: longLiveUserToken,
                },
                vPageData: {
                    vPageId: pageData.id,
                    vPageName: pageData.name,
                    vPageToken: pageData.access_token,
                    vPageProfile: pageData.picture.data.url
                },
                dtAddedDate:dtAddedDate
            })
            profileData.save();
            res.send();


        } catch (error) {
            console.log(error);
            res.redirect("/");

        }
    }
}

//calendar
exports.fbCalendarPostData = async(vUserEmail)=> {
    try{
      let pageData = await ChannelModel.find({ vTitle: 'Facebook', vUserEmail: vUserEmail }).select('vPageData vTitle').exec();
    
        let fbSchedulePostDataArray = []
        let fbPostDataArray = []


        for (let i = 0; i < pageData.length; i++) {
          let pageAccessToken = pageData[i].vPageData.vPageToken
          let pageName = pageData[i].vPageData.vPageName
          let profileUrl = pageData[i].vPageData.vPageProfile
          let vTitle = pageData[i].vTitle

          let link = `https://graph.facebook.com/v15.0/me/posts?fields=message%2Cpermalink_url%2Ccreated_time%2Cattachments%7Bmedia%7D%2Cshares%2Clikes.summary(true)%2Ccomments.summary(true)%2Cinsights.metric(post_clicks,post_engaged_users)%7Bvalues%7D&limit=100&access_token=${pageAccessToken}`
          const postDataResponse = await axios.get(link);
          let postData = postDataResponse.data
          postData = postData.data;

                  for (let j = 0; j < postData.length; j++) {
                      let obj = {}
                      obj.title = postData[j].message
                      obj.created_time = postData[j].created_time
                      obj.src = postData[j].attachments?.data[0].media.image.src || ""
                      obj.like_count = postData[j].likes.summary.total_count || 0
                      obj.comments_count = postData[j].comments.summary.total_count || 0
                      obj.shares_count = postData[j].shares?.count || 0
                      obj.click_count = postData[j].insights.data[0].values[0].value
                      obj.view_count = postData[j].insights.data[1].values[0].value
                      obj.permalink_url = postData[j].permalink_url
                      obj.pageName = pageName
                      obj.profile = profileUrl
                      obj.socialMedia = vTitle
                      fbPostDataArray.push(obj)
                  }
          
                  let scheduleLink = `https://graph.facebook.com/v15.0/me/scheduled_posts?fields=message%2Ccreated_time%2Cattachments%7Bmedia%7D%2Cshares%2Clikes.summary(true)%2Ccomments.summary(true)%2Cinsights.metric(post_engaged_users)%7Bvalues%7D&limit=100&access_token=${pageAccessToken}`
                  const schedulePostDataResponse = await axios.get(scheduleLink);
                  let schedulePostData = schedulePostDataResponse.data
                  schedulePostData = schedulePostData.data;
              
                          for (let j = 0; j < schedulePostData.length; j++) {
                              let obj = {}
                              obj.title = schedulePostData[j].message
                              obj.created_time = schedulePostData[j].created_time
                              obj.src = schedulePostData[j].attachments?.data[0].media.image.src || ""
                              obj.like_count = schedulePostData[j].likes.summary.total_count || 0
                              obj.comments_count = schedulePostData[j].comments.summary.total_count || 0
                              obj.shares_count = schedulePostData[j].shares?.count || 0
                              obj.click_count = schedulePostData[j].insights.data[0].values[0].value
                              obj.pageName = pageName
                              obj.profile = profileUrl
                              obj.socialMedia = vTitle
                              fbSchedulePostDataArray.push(obj)
                          }
        }
        return {fbPostDataArray,fbSchedulePostDataArray}

    }catch(error){
      console.log(error)

    }

}

//this api retrurn img and post id in engagement
exports.fbPostEngamentData = async(req,res) => {
    if(req.session.email){
      try{
      
          let pageAccessToken = await ChannelModel.findOne({ vTitle: 'Facebook', vUserEmail: req.session.email }).select('vPageData').exec();
              pageAccessToken = pageAccessToken.vPageData.vPageToken
  
          let urlPostApi = `https://graph.facebook.com/v15.0/me/posts?fields=attachments%7Bmedia%7D&limit=100&access_token=${pageAccessToken}`
          const postDataResponse = await axios.get(urlPostApi);
          let postData = postDataResponse.data
          postData = postData.data;
  
          let postDataArray = []
  
          for (let i = 0; i < postData.length; i++) {
          let obj = {}
          obj.src = postData[i].attachments?.data[0].media.image.src || ""
          obj.id = postData[i].id
          postDataArray.push(obj)
          }
          is_layout = true;
          res.render("../view/social_accounts/engagement", {
          is_layout: is_layout,
          engagementData: postDataArray,
          });
  
  
      }catch(error){
        console.log(error)
        // res.redirect("/");
  
      }
    }else{
      res.redirect("/");
    }
}
  
//it's return comment and post detais in engagement
exports.fbEngagementPost = async(req,res) => {
    if(req.session.email){
        try{

            let pageAccessToken = await ChannelModel.findOne({ vTitle: 'Facebook', vUserEmail: req.session.email }).select('vPageData').exec();
                pageAccessToken = pageAccessToken.vPageData.vPageToken
            let postId= req.body.postId
            let urlAPI = `https://graph.facebook.com/v15.0/${postId}?fields=message%2Ccreated_time%2Cattachments%7Bmedia%7D%2Ccomments.summary(true).limit(100)%7Bcomments.limit(100).summary(true)%2Cmessage%2Ccreated_time%7D&access_token=${pageAccessToken}`
        
            const postDataResponse = await axios.get(urlAPI);
            let postData = postDataResponse.data
            
            let post = {}
            post.message = postData.message
            post.src = postData.attachments?.data[0].media.image.src || ""
            post.postComments = postData.comments.data
            // post.comments_count = postData.comments.summary.total_count  || 0
            let subCommentCount = 0
            for (let i = 0; i < postData.comments.data.length; i++) {
                subCommentCount += postData.comments.data[i].comments.summary.total_count 
            }
            post.total_count = postData.comments.summary.total_count + subCommentCount
            post.created_time = postData.created_time


            res.render("../view/social_accounts/ajax_listing",{
                layout:false,
                post: post
            });

        }catch(error){
        console.log(error)
        // res.redirect("/");

        }
    }else{
    // res.redirect("/");
    }
}
  
//comment reply api
exports.fbCommentReply = async (req,res) => {
    if(req.session.email){
        try{

            let pageAccessToken = await ChannelModel.findOne({ vTitle: 'Facebook', vUserEmail: req.session.email }).select('vPageData').exec();
                pageAccessToken = pageAccessToken.vPageData.vPageToken           
            let message = req.body.message;
            let commentId = req.body.commentId;
            let urlAPI = `https://graph.facebook.com/v15.0/${commentId}/comments?message=${message}&access_token=${pageAccessToken}`
            const commentResponse = await axios.post(urlAPI);
            let commentData = {}
            commentData.id= commentResponse.data
            commentData.message = message
            res.send(commentData)
            
        }catch(error){
            console.log(error)
            res.redirect("/");

        }
    }else{
    res.redirect("/");
    }
}

//facebook upload image/video
exports.fbImageVideoUpload = async(reqData,pageAccessToken)=>{
    try {
        let id = '';
            if (reqData.imageCount > 0) {
                //image upload code here 
                    for (let i = 0; i < reqData.image.length; i++) {
                        let multiPhotoSinglePostParams
                        if (reqData.unixTime) {
                            multiPhotoSinglePostParams = {
                                'url': reqData.image[i],
                                'published': 'false',
                                'temporary': 'true',
                                'access_token': pageAccessToken
                            }
                        } else {
                            multiPhotoSinglePostParams = {
                                'url': reqData.image[i],
                                'published': 'false',
                                'access_token': pageAccessToken
                            }
                        }

                        const multiPhotoSinglePost = await axios.post(
                            'https://graph.facebook.com/v15.0/me/photos',
                            '', {
                                params: multiPhotoSinglePostParams
                            }
                        );

                        id += `attached_media[${i}]={media_fbid:${multiPhotoSinglePost.data.id}}&`
                    }

                    //combine post 
                    let combineMultiPostParams = ''
                    if (reqData.unixTime) {

                        console.log("schedule true image", reqData.unixTime);
                        combineMultiPostParams = `message=${reqData.message}&${id}access_token=${pageAccessToken}&published=false&scheduled_publish_time=${reqData.unixTime}&unpublished_content_type=SCHEDULED`
                    } else {
                        combineMultiPostParams = `message=${reqData.message}&${id}access_token=${pageAccessToken}`
                    }

                    let combineMultiPost = await axios.post(
                        'https://graph.facebook.com/me/feed',
                        `${combineMultiPostParams}`, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        }
                    );
                return combineMultiPost

            } else {
                    const form = new FormData();
                    form.append('access_token', `${pageAccessToken}`);
                    form.append('file_url', `${reqData.image[0]}`);
                    form.append('description', `${reqData.message}`);

                    if (reqData.unixTime) {
                        // let scheduled_publish_time = '1667563719'
                        console.log("video schedule true", reqData.unixTime);

                        form.append('published', 'false')
                        form.append('scheduled_publish_time', `${reqData.unixTime}`)
                        form.append('unpublished_content_type', 'SCHEDULED')
                    }
                    const videoUpload = await axios.post(
                        'https://graph-video.facebook.com/v15.0/me/videos',
                        form, {
                            headers: {
                                ...form.getHeaders()
                            }
                        }
                    );

                    console.log("video upload succesfull");
                    return videoUpload
                
            }
    } catch (error) {
        throw error.response?.data || error
    }
}

//fb msg link
exports.fbMsgLink = async(reqData,pageAccessToken)=>{
  try {
    let params;
    if (reqData.unixTime) {
      params = {
        published: false,
        message: reqData.message,
        link: reqData.link,
        scheduled_publish_time: `${reqData.unixTime}`,
        unpublished_content_type: "SCHEDULED",
        access_token: pageAccessToken,
      };
    } else {
          params = {
            message: reqData.message,
            link: reqData.link,
            access_token: pageAccessToken,
          };
    }
    let massageLinkFunction = await axios.post(
      "https://graph.facebook.com/v15.0/me/feed",
      "",
      {
        params: params,
      }
    );
    return massageLinkFunction
  } catch (error) {
    throw error.response?.data || error
  }
}

//facebook refreshPage
exports.fbRefreshPage = async(reqData)=>{
    try {
        const userData = await axios.get(
            `https://graph.facebook.com/v15.0/me?fields=id%2Cname%2Cpicture.type(large)&access_token=${reqData.token}`
          );
    
          let urlLongTokenUser = `https://graph.facebook.com/v15.0/oauth/access_token?  
                                        grant_type=fb_exchange_token&          
                                        client_id=${process.env.APP_ID}&
                                        client_secret=${process.env.API_SECRET}&
                                        fb_exchange_token=${reqData.token}`;
    
          const longLiveUserTokenApi = await axios.get(urlLongTokenUser);
          let longLiveUserToken = longLiveUserTokenApi.data.access_token;
    
          const urlLongTokenPage = `https://graph.facebook.com/v15.0/${reqData.pageId}?fields=access_token%2Cid%2Cname%2Cpicture%7Burl%7D&access_token=${longLiveUserToken}`;
          let longLivePageTokenApi = await axios.get(urlLongTokenPage);
          let pageData = longLivePageTokenApi.data;

            //profile upload aws
            let url;
            try {
                const response = await new Promise((resolve, reject) => {
                  request.get({
                    method: 'GET',
                    url: pageData.picture.data.url,
                    encoding: null, // If null, the body is returned as a Buffer.
                  }, (error, response, body) => {
                    if (error) {
                      reject(error);
                    } else {
                      resolve(response);
                    }
                  });
                });
            
                if (response.statusCode == 200) {
                  const data = {
                    name: response.request.uri.pathname.substring(1),
                    data: response.body,
                    mimetype: response.headers['content-type']
                  };
                  url = await GeneralLibrary.awsFileUpload(data, "socialPost/");
                } else {
                  throw new Error(`Request failed with status code ${response.statusCode}`);
                }
              } catch (error) {
                console.error(error);
              }
              pageData.picture.data.url = url

          ChannelModel.findOneAndUpdate(
            { "vPageData.vPageId": reqData.pageId, vUserEmail: reqData.email },
            {
              $set: {
                vTitle: "Facebook",
                vUserEmail: reqData.email,
                vUserData: {
                  iUserId: userData.data.id,
                  vUserProfile: userData.data.picture.data.url,
                  vUserName: userData.data.name,
                  vUserToken: longLiveUserToken,
                },
                vPageData: {
                  vPageId: pageData.id,
                  vPageName: pageData.name,
                  vPageToken: pageData.access_token,
                  vPageProfile: pageData.picture.data.url,
                },
                dtAddedDate: reqData.dtAddedDate,
              },
            }
          ).exec(function (error, result) {
            if (!error) {
            //   res.redirect("/social-accounts/channels");
            } else {
              console.log(error);
            }
          });

    } catch (error) {
        throw error
    }
}