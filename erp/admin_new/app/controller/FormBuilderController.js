const FormBuilderModel = require('../model/FormBuilderModel');
const Paginator = require("../library/PaginatorLibrary");
const CompanyDetailModel = require('../model/CompanyDetailModel');
const RoleModuleMasterModel = require('../model/RoleModuleMasterModel');
const DepartmentModel = require("../model/DepartmentModel");
const DepartmentRoleModel = require("../model/DepartmentRoleModel");
const AssignFormBuilderModel = require("../model/AssignFormBuilderModel");
const fs = require('fs');
const path = require('path');
const puppeteer = require('puppeteer');
const moment = require('moment');

exports.index = async (req, res) => {
    if (req.session.email) {

        var formBuilderData = await FormBuilderModel.find();
        is_layout = true;
        res.render("../view/form_builder/index", {
            is_layout: is_layout,
            formBuilderData: formBuilderData
        });
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        var vAction = req.body.vAction;
        var iFormBuilderId = req.body.iFormBuilderId;
        try {
            if (iFormBuilderId) {
                //single record deleted
                if (vAction === "delete") {

                    await FormBuilderModel.findOneAndDelete({
                        _id: iFormBuilderId
                    }).then(function (result) {

                        var modelName = 'fb_' + result.vPage.replace(/\s+/g, "_") + '_Model'
                        const dropModel = require(`../model/formBuilder/${modelName}`);
                        dropModel.collection.drop();

                        // Form Builder Config Route Delete
                        const filePathDel = path.join(process.cwd(), '/app/config', 'FormBuilderConfig.js');
                        fs.readFile(filePathDel, 'utf8', (err, data) => {
                            if (err) throw err;
                            const searchString = 'require("../routes/formBuilder/fb_' + result.vPage.replace(/\s+/g, "_") + '_Route")(app);';
                            const lines = data.split('\n');
                            const filteredLines = lines.filter(line => !line.includes(searchString));
                            const updatedContent = filteredLines.join('\n');
                            fs.writeFile(filePathDel, updatedContent, (err) => {
                                if (err) throw err;
                                console.log('File updated successfully');
                            });
                        });

                        var deleteModel = path.join(process.cwd(), '/app/model/formBuilder', 'fb_' + result.vPage.replace(/\s+/g, "_") + '_Model.js');
                        var deleteController = path.join(process.cwd(), '/app/controller/formBuilder', 'fb_' + result.vPage.replace(/\s+/g, "_") + '_Controller.js');
                        var deleteRoute = path.join(process.cwd(), '/app/routes/formBuilder', 'fb_' + result.vPage.replace(/\s+/g, "_") + '_Route.js');
                        fs.unlink(deleteModel, (err) => {
                            if (err) {
                                console.error(err);
                                return;
                            }
                            console.log('Model deleted successfully');
                        });
                        fs.unlink(deleteController, (err) => {
                            if (err) {
                                console.error(err);
                                return;
                            }
                            console.log('Controller deleted successfully');
                        });
                        fs.unlink(deleteRoute, (err) => {
                            if (err) {
                                console.error(err);
                                return;
                            }
                            console.log('Route deleted successfully');
                        });

                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let fbId = iFormBuilderId.split(",");
                    await FormBuilderModel.deleteMany({
                        _id: {
                            $in: fbId
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            console.log(error)
            req.flash("error", error.message)
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}

exports.add = async (req, res) => {
    if (req.session.email) {
        is_layout = true;
        res.render("../view/form_builder/add", {
            is_layout: is_layout,
        });
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {
    if (req.session.email) {
        try {
            var vPageName = req.body.vPage;
            var vFormData = req.body.vFormBuilder;
            var vLayout = req.body.vLayout;
            var formUniqNo = Math.floor(Math.random() * 900000) + 100000;

            var slugLC = req.body.vPage.toLowerCase();
            var slug = slugLC.replace(/\s+/g, '-');

            const FormBuilder = new FormBuilderModel({
                vPage: vPageName,
                vFormBuilder: vFormData,
                vSlug: slug,
                vLayout: vLayout,
                vFormUnique: formUniqNo
            });

            FormBuilder.save(function (error, result) {
                if (error) {
                    console.log(error);
                } else {

                    // Generate
                    // Model Generate
                    var pageName = vPageName.replace(/\s+/g, "_");
                    var model_str = 'const mongoose = require("mongoose");' + "\n\n";
                    model_str += 'const ' + pageName + '_Schema = mongoose.Schema({' + "\n";
                    vFormData.forEach(element => {
                        if (element.type != 'button') {
                            model_str += "\t" + '' + element.name + ': {' + "\n";
                            if (element.type === 'checkbox-group') {
                                model_str += "\t\t" + 'type: Array,' + "\n";
                            } else {
                                model_str += "\t\t" + 'type: String,' + "\n";
                            }
                            model_str += "\t" + '},' + "\n";
                        }
                    });
                    model_str += '\t' + 'iUserId: {' + "\n";
                    model_str += '\t\t' + 'type: String,' + "\n";
                    model_str += '\t' + '},' + "\n";
                    model_str += '\t' + 'eFillUpStatus: {' + "\n";
                    model_str += '\t\t' + 'type: String,' + "\n";
                    model_str += '\t\t' + 'enum: ["Yes","No"],' + "\n";
                    model_str += '\t\t' + 'default: "No",' + "\n";
                    model_str += '\t' + '},' + "\n";

                    model_str += '});' + "\n\n";
                    model_str += 'module.exports = mongoose.model("fb_' + pageName + '", ' + pageName + '_Schema);';
                    var modelName = "fb_" + pageName + "_Model";
                    const filePath = path.join(process.cwd(), '/app/model/formBuilder', modelName + '.js');
                    fs.writeFile(filePath, model_str, function (err) {
                        if (err) throw err;
                        console.log('Model written to file');
                    });

                    // Controller Generate
                    var controller_str = 'const ' + modelName + ' = require("../../model/formBuilder/' + modelName + '");' + "\n";
                    controller_str += 'const Paginator = require("../../library/PaginatorLibrary");' + "\n";
                    controller_str += 'const CompanyDetailModel = require("../../model/CompanyDetailModel");' + "\n";
                    controller_str += 'const FormBuilderModel = require("../../model/FormBuilderModel");' + "\n\n"

                    // Index
                    controller_str += 'exports.index = async (req, res) => {' + "\n";
                    controller_str += '\t' + 'if (req.session.email) {' + "\n";
                    controller_str += '\t\t' + 'var SQL = {};' + "\n";
                    controller_str += '\t\t' + 'if(req.session.usertype !== "Admin"){' + "\n";
                    controller_str += '\t\t\t' + 'SQL.iUserId = req.session.userid;' + "\n";
                    controller_str += '\t\t' + '}' + "\n";
                    controller_str += '\t\t' + 'var ' + pageName + 'Data = await ' + modelName + '.find(SQL);' + "\n";
                    controller_str += '\t\t' + 'var data = await FormBuilderModel.findOne({ vSlug: res.locals.route }).exec();' + "\n";
                    controller_str += '\t\t' + 'res.render("../view/formBuilder/index", {' + "\n";
                    controller_str += '\t\t\t' + 'is_layout: true,' + "\n";
                    controller_str += '\t\t\t' + 'ajaxData: ' + pageName + 'Data,' + "\n";
                    controller_str += '\t\t\t' + 'fields: data.vFormBuilder,' + "\n";
                    controller_str += '\t\t\t' + 'userType:req.session.usertype,' + "\n";
                    controller_str += '\t\t' + '});' + "\n";
                    controller_str += '\t' + '} else {' + "\n";
                    controller_str += '\t\t' + 'res.redirect("/");' + "\n";
                    controller_str += '\t' + '}' + "\n";
                    controller_str += '};' + "\n\n";

                    // Add
                    controller_str += 'exports.add = async (req, res) => {' + "\n";
                    controller_str += '\t' + 'if (req.session.email) {' + "\n";
                    controller_str += '\t\t' + 'try {' + '\n';
                    controller_str += '\t\t\t' + 'var data = await FormBuilderModel.findOne({ vSlug: res.locals.route }).exec();' + "\n";
                    controller_str += '\t\t\t' + 'is_layout = true;' + "\n";
                    controller_str += '\t\t\t' + 'res.render("../view/formBuilder/add", {' + "\n";
                    controller_str += '\t\t\t\t' + 'is_layout: is_layout,' + "\n";
                    controller_str += '\t\t\t\t' + 'fields: data.vFormBuilder,' + "\n";
                    controller_str += '\t\t\t\t' + 'datas: data' + "\n";
                    controller_str += '\t\t\t' + '});' + "\n";
                    controller_str += '\t\t' + '} catch (error) {' + "\n";
                    controller_str += '\t\t\t' + 'console.log(error);' + "\n";
                    controller_str += '\t\t' + '}' + "\n";
                    controller_str += '\t' + '} else {' + "\n";
                    controller_str += '\t\t' + 'res.redirect("/");' + "\n";
                    controller_str += '\t' + '}' + "\n";
                    controller_str += '};' + "\n\n";

                    // Add Action
                    controller_str += 'exports.add_action = async (req, res) => {' + "\n";
                    controller_str += '\t' + 'if (req.session.email) {' + "\n";
                    controller_str += '\t\t' + 'if (req.body.iDataId) {' + "\n";
                    controller_str += '\t\t\t' + 'try {' + "\n";
                    controller_str += '\t\t\t\t' + '' + modelName + '.findOneAndUpdate({_id: req.body.iDataId}, {' + "\n";
                    controller_str += '\t\t\t\t\t' + '"$set": {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + '"iUserId": req.session.userid,' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'eFillUpStatus: "Yes",' + "\n";
                    vFormData.forEach(element => {
                        if (element.type != 'button') {
                            controller_str += '\t\t\t\t\t\t' + '"' + element.name + '": req.body.' + element.name + ',' + "\n";
                        }
                    });
                    controller_str += '\t\t\t\t\t' + '}' + "\n";
                    controller_str += '\t\t\t\t' + '}).exec(function (error, result) {' + "\n";
                    controller_str += '\t\t\t\t\t' + 'if (!error) {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'res.redirect("/" + res.locals.route);' + "\n";
                    controller_str += '\t\t\t\t\t' + '} else {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'console.log(error);' + "\n";
                    controller_str += '\t\t\t\t\t' + '}' + "\n";
                    controller_str += '\t\t\t\t' + '});' + "\n";
                    controller_str += '\t\t\t' + '} catch (error) {' + "\n";
                    controller_str += '\t\t\t\t' + 'console.log(error);' + "\n";
                    controller_str += '\t\t\t' + '}' + "\n";
                    controller_str += '\t\t' + '} else {' + "\n";
                    controller_str += '\t\t\t' + 'const ' + pageName + 'Json = new ' + modelName + '({' + "\n";
                    controller_str += '\t\t\t' + 'iUserId: req.session.userid,' + "\n";
                    controller_str += '\t\t\t' + 'eFillUpStatus: "Yes",' + "\n";

                    vFormData.forEach(element => {
                        if (element.type != 'button') {
                            controller_str += '\t\t\t' + '' + element.name + ': req.body.' + element.name + ',' + "\n";
                        }
                    });
                    controller_str += '\t\t\t' + '});' + "\n";
                    controller_str += '\t\t\t' + 'try {' + "\n";
                    controller_str += '\t\t\t\t' + '' + pageName + 'Json.save(function (error, result) {' + "\n";
                    controller_str += '\t\t\t\t\t' + 'if (error) {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'console.log(error);' + "\n";
                    controller_str += '\t\t\t\t\t' + '} else {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'res.redirect("/" + res.locals.route);' + "\n";
                    controller_str += '\t\t\t\t\t' + '}' + "\n";
                    controller_str += '\t\t\t\t' + '})' + "\n";
                    controller_str += '\t\t\t' + '} catch (error) {' + "\n";
                    controller_str += '\t\t\t\t' + 'console.log(error);' + "\n";
                    controller_str += '\t\t\t' + '}' + "\n";
                    controller_str += '\t\t' + '}' + "\n";
                    controller_str += '\t' + '}' + "\n";
                    controller_str += '};' + "\n\n";

                    // Edit
                    controller_str += 'exports.edit = async (req, res) => {' + "\n";
                    controller_str += '\t' + 'var iDataId = req.params.iDataId;' + "\n";
                    controller_str += '\t' + 'if (req.session.email) {' + "\n";
                    controller_str += '\t\t' + 'try {' + "\n";
                    controller_str += '\t\t\t' + 'var ' + pageName + 'Data = await ' + modelName + '.findOne({' + "\n";
                    controller_str += '\t\t\t\t' + '_id: iDataId' + "\n";
                    controller_str += '\t\t\t' + '});' + "\n";
                    controller_str += '\t\t\t' + 'var data = await FormBuilderModel.findOne({ vSlug: res.locals.route }).exec();' + "\n";
                    controller_str += '\t\t\t' + 'is_layout = true;' + "\n";
                    controller_str += '\t\t\t' + 'res.render("../view/formBuilder/add", {' + "\n";
                    controller_str += '\t\t\t\t' + 'is_layout: is_layout,' + "\n";
                    controller_str += '\t\t\t\t' + 'fbData: ' + pageName + 'Data,' + "\n";
                    controller_str += '\t\t\t\t' + 'fields: data.vFormBuilder,' + "\n";
                    controller_str += '\t\t\t\t' + 'datas: data' + "\n";
                    controller_str += '\t\t\t' + '});' + "\n";
                    controller_str += '\t\t' + '} catch (error) {' + "\n";
                    controller_str += '\t\t\t' + 'console.log(error);' + "\n";
                    controller_str += '\t\t' + '}' + "\n";
                    controller_str += '\t' + '} else {' + "\n";
                    controller_str += '\t\t' + 'res.redirect("/");' + "\n";
                    controller_str += '\t' + '}' + "\n";
                    controller_str += '};' + "\n\n";

                    // Delete
                    controller_str += 'exports.delete = async (req, res) => {' + "\n";
                    controller_str += '\t' + 'if (req.session.email) {' + "\n";
                    controller_str += '\t\t' + 'var vAction = req.body.vAction;' + "\n";
                    controller_str += '\t\t' + 'var iDataId = req.body.iDataId;' + "\n";
                    controller_str += '\t\t' + 'try {' + "\n";
                    controller_str += '\t\t\t' + 'if (iDataId) {' + "\n";
                    controller_str += '\t\t\t\t' + '//single record deleted' + "\n";
                    controller_str += '\t\t\t\t' + 'if (vAction === "delete") {' + "\n";
                    controller_str += '\t\t\t\t\t' + 'await ' + modelName + '.deleteOne({' + "\n";
                    controller_str += '\t\t\t\t\t\t' + '_id: iDataId' + "\n";
                    controller_str += '\t\t\t\t\t' + '}).then(function (result) {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'req.flash("success", "Successfully deleted");' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'res.send({ status: 200 })' + "\n";
                    controller_str += '\t\t\t\t\t' + '});' + "\n";
                    controller_str += '\t\t\t\t' + '}' + "\n";
                    controller_str += '\t\t\t\t' + '//miltiple records deleted' + "\n";
                    controller_str += '\t\t\t\t' + 'if (vAction === "multiple_delete") {' + "\n";
                    controller_str += '\t\t\t\t\t' + 'let fbDataId = iDataId.split(",");' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'await ' + modelName + '.deleteMany({' + "\n";
                    controller_str += '\t\t\t\t\t\t\t' + '_id: {' + "\n";
                    controller_str += '\t\t\t\t\t\t\t\t' + '$in: fbDataId' + "\n";
                    controller_str += '\t\t\t\t\t\t\t' + '}' + "\n";
                    controller_str += '\t\t\t\t\t\t' + '}).then(function (result) {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'req.flash("success", "Successfully deleted");' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'res.send({ status: 200 })' + "\n";
                    controller_str += '\t\t\t\t\t' + '});' + "\n";
                    controller_str += '\t\t\t\t' + '}' + "\n";
                    controller_str += '\t\t\t' + '}' + "\n";
                    controller_str += '\t\t' + '} catch (error) {' + "\n";
                    controller_str += '\t\t\t' + 'req.flash("error", "someting want wrong.")' + "\n";
                    controller_str += '\t\t\t' + 'res.send({ status: 500, message: error.message })' + "\n";
                    controller_str += '\t\t' + '}' + "\n";
                    controller_str += '\t' + '} else {' + "\n";
                    controller_str += '\t\t' + 'res.redirect("/");' + "\n";
                    controller_str += '\t' + '}' + "\n";
                    controller_str += '}' + "\n";

                    var controllerName = "fb_" + pageName + "_Controller";
                    const filePath2 = path.join(process.cwd(), '/app/controller/formBuilder', controllerName + '.js');
                    fs.writeFile(filePath2, controller_str, function (err) {
                        if (err) throw err;
                        console.log('Controller written to file');
                    });

                    // Route
                    var route_str = 'module.exports = app => {' + "\n\n";
                    route_str += '\t' + 'const express = require("express");' + "\n";
                    route_str += '\t' + 'const router = express.Router();' + "\n";
                    route_str += '\t' + 'const ' + controllerName + ' = require("../../controller/formBuilder/' + controllerName + '");' + "\n\n";
                    route_str += '\t' + 'router.get("/", ' + controllerName + '.index);' + "\n";
                    route_str += '\t' + 'router.get("/add", ' + controllerName + '.add);' + "\n";
                    route_str += '\t' + 'router.post("/add_action", ' + controllerName + '.add_action);' + "\n";
                    route_str += '\t' + 'router.get("/edit/:iDataId", ' + controllerName + '.edit);' + "\n";
                    route_str += '\t' + 'router.post("/delete", ' + controllerName + '.delete);' + "\n\n";
                    route_str += '\t' + 'app.use("/' + result.vSlug + '", router)' + "\n";
                    route_str += '}';

                    var routeName = "fb_" + pageName + "_Route";
                    const filePath3 = path.join(process.cwd(), '/app/routes/formBuilder', routeName + '.js');
                    fs.writeFile(filePath3, route_str, function (err) {
                        if (err) throw err;
                        console.log('Route data written to file');
                    });

                    // Form Builder Config Route
                    const filePath4 = path.join(process.cwd(), '/app/config', 'FormBuilderConfig.js');
                    fs.readFile(filePath4, 'utf8', (err, data) => {
                        if (err) throw err;

                        const lastIndex = data.lastIndexOf('}');
                        const newLine = '\t' + 'require("../routes/formBuilder/' + routeName + '")(app);' + "\n";
                        const modifiedData = `${data.slice(0, lastIndex)}${newLine}${data.slice(lastIndex)}`;

                        fs.writeFile(filePath4, modifiedData, (err) => {
                            if (err) throw err;
                            console.log('Config route modified');
                        });
                    });
                    // end

                    res.redirect("/form-builder");
                }
            });

        } catch (error) {
            console.log(error);
        }
    }
};

exports.edit = async (req, res) => {
    if (req.session.email) {
        var iFormBuilderId = req.params.iFormBuilderId;
        try {
            var formBuilderData = await FormBuilderModel.findOne({
                _id: iFormBuilderId
            });

            // console.log(JSON.stringify(formBuilderData));

            is_layout = true;
            res.render("../view/form_builder/edit", {
                is_layout: is_layout,
                formBuilderData: formBuilderData,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.update_action = async (req, res) => {
    if (req.session.email) {
        try {
            var fb = await FormBuilderModel.findOne({
                _id: req.body.iFormBuilderId
            });

            var deleteModel = path.join(process.cwd(), '/app/model/formBuilder', 'fb_' + fb.vPage.replace(/\s+/g, "_") + '_Model.js');
            var deleteController = path.join(process.cwd(), '/app/controller/formBuilder', 'fb_' + fb.vPage.replace(/\s+/g, "_") + '_Controller.js');
            var deleteRoute = path.join(process.cwd(), '/app/route/formBuilder', 'fb_' + fb.vPage.replace(/\s+/g, "_") + '_Route.js');
            fs.unlink(deleteModel, (err) => {
                if (err) {
                    console.error(err);
                    return;
                }
                console.log('Model deleted successfully');
            });
            fs.unlink(deleteController, (err) => {
                if (err) {
                    console.error(err);
                    return;
                }
                console.log('Controller deleted successfully');
            });
            fs.unlink(deleteRoute, (err) => {
                if (err) {
                    console.error(err);
                    return;
                }
                console.log('Route deleted successfully');
            });

            var slugLC = req.body.vPage.toLowerCase();
            var slug = slugLC.replace(/\s+/g, '-');

            FormBuilderModel.findOneAndUpdate({
                _id: req.body.iFormBuilderId
            }, {
                "$set": {
                    "vFormBuilder": req.body.vFormBuilder,
                    "vLayout": req.body.vLayout,
                }
            }, {
                returnOriginal: false
            }).exec(function (error, result) {
                if (!error) {

                    var vPageName = result.vPage;
                    var vFormData = result.vFormBuilder;

                    // Generate
                    // Model Generate
                    var pageName = vPageName.replace(/\s+/g, "_");
                    var model_str = 'const mongoose = require("mongoose");' + "\n\n";
                    model_str += 'const ' + pageName + '_Schema = mongoose.Schema({' + "\n";
                    vFormData.forEach(element => {
                        if (element.type != 'button') {
                            model_str += "\t" + '' + element.name + ': {' + "\n";
                            if (element.type === 'checkbox-group') {
                                model_str += "\t\t" + 'type: Array,' + "\n";
                            } else {
                                model_str += "\t\t" + 'type: String,' + "\n";
                            }
                            model_str += "\t" + '},' + "\n";
                        }
                    });
                    model_str += '\t' + 'iUserId: {' + "\n";
                    model_str += '\t\t' + 'type: String,' + "\n";
                    model_str += '\t' + '},' + "\n";
                    model_str += '\t' + 'eFillUpStatus: {' + "\n";
                    model_str += '\t\t' + 'type: String,' + "\n";
                    model_str += '\t\t' + 'enum: ["Yes","No"],' + "\n";
                    model_str += '\t\t' + 'default: "No",' + "\n";
                    model_str += '\t' + '},' + "\n";

                    model_str += '});' + "\n\n";
                    model_str += 'module.exports = mongoose.model("fb_' + pageName + '", ' + pageName + '_Schema);';
                    var modelName = "fb_" + pageName + "_Model";
                    const filePath = path.join(process.cwd(), '/app/model/formBuilder', modelName + '.js');
                    fs.writeFile(filePath, model_str, function (err) {
                        if (err) throw err;
                        console.log('Model data written to file');
                    });

                    // Controller Generate
                    var controller_str = 'const ' + modelName + ' = require("../../model/formBuilder/' + modelName + '");' + "\n";
                    controller_str += 'const Paginator = require("../../library/PaginatorLibrary");' + "\n";
                    controller_str += 'const CompanyDetailModel = require("../../model/CompanyDetailModel");' + "\n";
                    controller_str += 'const FormBuilderModel = require("../../model/FormBuilderModel");' + "\n\n"

                    // Index
                    controller_str += 'exports.index = async (req, res) => {' + "\n";
                    controller_str += '\t' + 'if (req.session.email) {' + "\n";
                    controller_str += '\t\t' + 'var SQL = {};' + "\n";
                    controller_str += '\t\t' + 'if(req.session.usertype !== "Admin"){' + "\n";
                    controller_str += '\t\t\t' + 'SQL.iUserId = req.session.userid;' + "\n";
                    controller_str += '\t\t' + '}' + "\n";
                    controller_str += '\t\t' + 'var ' + pageName + 'Data = await ' + modelName + '.find(SQL);' + "\n";
                    controller_str += '\t\t' + 'var data = await FormBuilderModel.findOne({ vSlug: res.locals.route }).exec();' + "\n";
                    controller_str += '\t\t' + 'res.render("../view/formBuilder/index", {' + "\n";
                    controller_str += '\t\t\t' + 'is_layout: true,' + "\n";
                    controller_str += '\t\t\t' + 'ajaxData: ' + pageName + 'Data,' + "\n";
                    controller_str += '\t\t\t' + 'fields: data.vFormBuilder,' + "\n";
                    controller_str += '\t\t\t' + 'userType:req.session.usertype,' + "\n";
                    controller_str += '\t\t' + '});' + "\n";
                    controller_str += '\t' + '} else {' + "\n";
                    controller_str += '\t\t' + 'res.redirect("/");' + "\n";
                    controller_str += '\t' + '}' + "\n";
                    controller_str += '};' + "\n\n";

                    // Add
                    controller_str += 'exports.add = async (req, res) => {' + "\n";
                    controller_str += '\t' + 'if (req.session.email) {' + "\n";
                    controller_str += '\t\t' + 'try {' + '\n';
                    controller_str += '\t\t\t' + 'var data = await FormBuilderModel.findOne({ vSlug: res.locals.route }).exec();' + "\n";
                    controller_str += '\t\t\t' + 'is_layout = true;' + "\n";
                    controller_str += '\t\t\t' + 'res.render("../view/formBuilder/add", {' + "\n";
                    controller_str += '\t\t\t\t' + 'is_layout: is_layout,' + "\n";
                    controller_str += '\t\t\t\t' + 'fields: data.vFormBuilder,' + "\n";
                    controller_str += '\t\t\t\t' + 'datas: data' + "\n";
                    controller_str += '\t\t\t' + '});' + "\n";
                    controller_str += '\t\t' + '} catch (error) {' + "\n";
                    controller_str += '\t\t\t' + 'console.log(error);' + "\n";
                    controller_str += '\t\t' + '}' + "\n";
                    controller_str += '\t' + '} else {' + "\n";
                    controller_str += '\t\t' + 'res.redirect("/");' + "\n";
                    controller_str += '\t' + '}' + "\n";
                    controller_str += '};' + "\n\n";

                    // Add Action
                    controller_str += 'exports.add_action = async (req, res) => {' + "\n";
                    controller_str += '\t' + 'if (req.session.email) {' + "\n";
                    controller_str += '\t\t' + 'if (req.body.iDataId) {' + "\n";
                    controller_str += '\t\t\t' + 'try {' + "\n";
                    controller_str += '\t\t\t\t' + '' + modelName + '.findOneAndUpdate({_id: req.body.iDataId}, {' + "\n";
                    controller_str += '\t\t\t\t\t' + '"$set": {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + '"iUserId": req.session.userid,' + "\n";
                    controller_str += '\t\t\t\t\t\t' + '"eFillUpStatus": "Yes",' + "\n";
                    vFormData.forEach(element => {
                        if (element.type != 'button') {
                            controller_str += '\t\t\t\t\t\t' + '"' + element.name + '": req.body.' + element.name + ',' + "\n";
                        }
                    });
                    controller_str += '\t\t\t\t\t' + '}' + "\n";
                    controller_str += '\t\t\t\t' + '}).exec(function (error, result) {' + "\n";
                    controller_str += '\t\t\t\t\t' + 'if (!error) {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'res.redirect("/" + res.locals.route);' + "\n";
                    controller_str += '\t\t\t\t\t' + '} else {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'console.log(error);' + "\n";
                    controller_str += '\t\t\t\t\t' + '}' + "\n";
                    controller_str += '\t\t\t\t' + '});' + "\n";
                    controller_str += '\t\t\t' + '} catch (error) {' + "\n";
                    controller_str += '\t\t\t\t' + 'console.log(error);' + "\n";
                    controller_str += '\t\t\t' + '}' + "\n";
                    controller_str += '\t\t' + '} else {' + "\n";
                    controller_str += '\t\t\t' + 'const ' + pageName + 'Json = new ' + modelName + '({' + "\n";
                    controller_str += '\t\t\t' + 'iUserId: req.session.userid,' + "\n";
                    controller_str += '\t\t\t' + 'eFillUpStatus: "Yes",' + "\n";
                    vFormData.forEach(element => {
                        if (element.type != 'button') {
                            controller_str += '\t\t\t' + '' + element.name + ': req.body.' + element.name + ',' + "\n";
                        }
                    });
                    controller_str += '\t\t\t' + '});' + "\n";
                    controller_str += '\t\t\t' + 'try {' + "\n";
                    controller_str += '\t\t\t\t' + '' + pageName + 'Json.save(function (error, result) {' + "\n";
                    controller_str += '\t\t\t\t\t' + 'if (error) {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'console.log(error);' + "\n";
                    controller_str += '\t\t\t\t\t' + '} else {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'res.redirect("/" + res.locals.route);' + "\n";
                    controller_str += '\t\t\t\t\t' + '}' + "\n";
                    controller_str += '\t\t\t\t' + '})' + "\n";
                    controller_str += '\t\t\t' + '} catch (error) {' + "\n";
                    controller_str += '\t\t\t\t' + 'console.log(error);' + "\n";
                    controller_str += '\t\t\t' + '}' + "\n";
                    controller_str += '\t\t' + '}' + "\n";
                    controller_str += '\t' + '}' + "\n";
                    controller_str += '};' + "\n\n";

                    // Edit
                    controller_str += 'exports.edit = async (req, res) => {' + "\n";
                    controller_str += '\t' + 'var iDataId = req.params.iDataId;' + "\n";
                    controller_str += '\t' + 'if (req.session.email) {' + "\n";
                    controller_str += '\t\t' + 'try {' + "\n";
                    controller_str += '\t\t\t' + 'var ' + pageName + 'Data = await ' + modelName + '.findOne({' + "\n";
                    controller_str += '\t\t\t\t' + '_id: iDataId' + "\n";
                    controller_str += '\t\t\t' + '});' + "\n";
                    controller_str += '\t\t\t' + 'var data = await FormBuilderModel.findOne({ vSlug: res.locals.route }).exec();' + "\n";
                    controller_str += '\t\t\t' + 'is_layout = true;' + "\n";
                    controller_str += '\t\t\t' + 'res.render("../view/formBuilder/add", {' + "\n";
                    controller_str += '\t\t\t\t' + 'is_layout: is_layout,' + "\n";
                    controller_str += '\t\t\t\t' + 'fbData: ' + pageName + 'Data,' + "\n";
                    controller_str += '\t\t\t\t' + 'fields: data.vFormBuilder,' + "\n";
                    controller_str += '\t\t\t\t' + 'datas: data' + "\n";
                    controller_str += '\t\t\t' + '});' + "\n";
                    controller_str += '\t\t' + '} catch (error) {' + "\n";
                    controller_str += '\t\t\t' + 'console.log(error);' + "\n";
                    controller_str += '\t\t' + '}' + "\n";
                    controller_str += '\t' + '} else {' + "\n";
                    controller_str += '\t\t' + 'res.redirect("/");' + "\n";
                    controller_str += '\t' + '}' + "\n";
                    controller_str += '};' + "\n\n";

                    // Delete
                    controller_str += 'exports.delete = async (req, res) => {' + "\n";
                    controller_str += '\t' + 'if (req.session.email) {' + "\n";
                    controller_str += '\t' + 'var vAction = req.body.vAction;' + "\n";
                    controller_str += '\t' + 'var iDataId = req.body.iDataId;' + "\n";
                    controller_str += '\t\t' + 'try {' + "\n";
                    controller_str += '\t\t\t' + 'if (iDataId) {' + "\n";
                    controller_str += '\t\t\t\t' + '//single record deleted' + "\n";
                    controller_str += '\t\t\t\t' + 'if (vAction === "delete") {' + "\n";
                    controller_str += '\t\t\t\t\t' + 'await ' + modelName + '.deleteOne({' + "\n";
                    controller_str += '\t\t\t\t\t\t' + '_id: iDataId' + "\n";
                    controller_str += '\t\t\t\t\t' + '}).then(function (result) {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'req.flash("success", "Successfully deleted");' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'res.send({ status: 200 })' + "\n";
                    controller_str += '\t\t\t\t\t' + '});' + "\n";
                    controller_str += '\t\t\t\t' + '}' + "\n";
                    controller_str += '\t\t\t\t' + '//miltiple records deleted' + "\n";
                    controller_str += '\t\t\t\t' + 'if (vAction === "multiple_delete") {' + "\n";
                    controller_str += '\t\t\t\t\t' + 'let fbDataId = iDataId.split(",");' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'await ' + modelName + '.deleteMany({' + "\n";
                    controller_str += '\t\t\t\t\t\t\t' + '_id: {' + "\n";
                    controller_str += '\t\t\t\t\t\t\t\t' + '$in: fbDataId' + "\n";
                    controller_str += '\t\t\t\t\t\t\t' + '}' + "\n";
                    controller_str += '\t\t\t\t\t\t' + '}).then(function (result) {' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'req.flash("success", "Successfully deleted");' + "\n";
                    controller_str += '\t\t\t\t\t\t' + 'res.send({ status: 200 })' + "\n";
                    controller_str += '\t\t\t\t\t' + '});' + "\n";
                    controller_str += '\t\t\t\t' + '}' + "\n";
                    controller_str += '\t\t\t' + '}' + "\n";
                    controller_str += '\t\t' + '} catch (error) {' + "\n";
                    controller_str += '\t\t\t' + 'req.flash("error", "someting want wrong.")' + "\n";
                    controller_str += '\t\t\t' + 'res.send({ status: 500, message: error.message })' + "\n";
                    controller_str += '\t\t' + '}' + "\n";
                    controller_str += '\t' + '} else {' + "\n";
                    controller_str += '\t\t' + 'res.redirect("/");' + "\n";
                    controller_str += '\t' + '}' + "\n";
                    controller_str += '}' + "\n";

                    var controllerName = "fb_" + pageName + "_Controller";
                    const filePath2 = path.join(process.cwd(), '/app/controller/formBuilder', controllerName + '.js');
                    fs.writeFile(filePath2, controller_str, function (err) {
                        if (err) throw err;
                        console.log('Controller data written to file');
                    });

                    // Route
                    var route_str = 'module.exports = app => {' + "\n\n";
                    route_str += '\t' + 'const express = require("express");' + "\n";
                    route_str += '\t' + 'const router = express.Router();' + "\n";
                    route_str += '\t' + 'const ' + controllerName + ' = require("../../controller/formBuilder/' + controllerName + '");' + "\n\n";
                    route_str += '\t' + 'router.get("/", ' + controllerName + '.index);' + "\n";
                    route_str += '\t' + 'router.get("/add", ' + controllerName + '.add);' + "\n";
                    route_str += '\t' + 'router.post("/add_action", ' + controllerName + '.add_action);' + "\n";
                    route_str += '\t' + 'router.get("/edit/:iDataId", ' + controllerName + '.edit);' + "\n";
                    route_str += '\t' + 'router.post("/delete", ' + controllerName + '.delete);' + "\n\n";
                    route_str += '\t' + 'app.use("/' + result.vSlug + '", router);' + "\n";
                    route_str += '}';

                    var routeName = "fb_" + pageName + "_Route";
                    const filePath3 = path.join(process.cwd(), '/app/routes/formBuilder', routeName + '.js');
                    fs.writeFile(filePath3, route_str, function (err) {
                        if (err) throw err;
                        console.log('Route data written to file');
                    });
                    // end

                    res.redirect("/form-builder");
                } else {
                    console.log(error);
                }
            });
        } catch (error) {
            console.log(error);
        }
    }
};

exports.view = async (req, res) => {
    if (req.session.email) {
        var iFormBuilderId = req.params.iFormBuilderId;
        try {
            var formBuilderData = await FormBuilderModel.findOne({
                _id: iFormBuilderId
            });

            is_layout = true;
            res.render("../view/form_builder/view", {
                is_layout: is_layout,
                formBuilderData: formBuilderData,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.ajax__ = async (req, res) => {
    if (req.session.email) {
        var iFormBuilderId = req.body.iFormBuilderId;
        try {
            var formBuilderData = await FormBuilderModel.findOne({
                _id: iFormBuilderId
            });

            res.send(formBuilderData.vFormBuilder);
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.downloadTemplate = async (req, res) => {

    if (req.session.email) {
        try {
            var iFormBuilderId = req.params.iFormBuilderId;
            var formBuilderData = await FormBuilderModel.findOne({
                _id: iFormBuilderId
            });

            if (formBuilderData) {

                const formFields = formBuilderData.vFormBuilder;
                const columns = parseInt(formBuilderData.vLayout);
                const rows = formFields.length / columns;
                const totalFields = rows * columns;

                let tableHTML = '<table style="width: 100%;"><body>';

                for (let i = 0; i < rows; i++) {
                    tableHTML += '<tr>';

                    for (let j = 0; j < columns; j++) {
                        const index = i * columns + j;

                        if (index < formFields.length) {
                            const field = formFields[index] || '';

                            if (field.type === 'textarea') {
                                tableHTML += `<td style="padding: 20px; padding-top: 0;">
                            <table style="width: 100%;">
                                <body style=" font-size: 14px;color: rgb(158, 157,157);font-family:sans-serif;">
                                    <td style="width: 50%;" colspan="2">
                                    
                                        <div class="ivu-form-item" style="display: flex; flex-direction: column;">
                                            <label style="font-weight: 600; margin-bottom: 15px; color: #515a6e;">
                                            ${field.label}</label>
                                            <div style="width: 100%;">
                                                <div>
                                                    <textarea id="floatingTextarea" style="width: 96%;
                                                    height: 60px;     border: 1px solid #bfc9d4;     border-radius: 6px; padding: 10px;"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </body>
                            </table>
                            </td>`
                            } else if (field.type === 'radio-group') {
                                tableHTML += `<td style="padding: 20px; padding-top: 0;">
                            <table style="width: 100%;">
                                <body style=" font-size: 14px;color: rgb(158, 157,157);font-family:sans-serif;">
                                    <div class="ivu-form-item" style="display: flex; flex-direction: column;">
                                        <label class="ivu-form-item-label" style="    color: #515a6e; font-weight: 600; margin-bottom: 15px;">${field.label}</label>

                                        <div style="display: flex; align-items: center;gap: 10px;">`

                                field.values.forEach(subField => {
                                    tableHTML += `<div class="form-check">
                                                <input type="radio" name="${field.name}">
                                                <label style="color: #515a6e;"> ${subField.label}  </label>
                                            </div>`
                                });
                                tableHTML += `</div>
                                    </div>
                                </body>
                            </table>
                        </td>`
                            } else if (field.type === 'checkbox-group' || field.type === 'select') {
                                tableHTML += `<td style="padding: 20px; padding-top: 0;">
                            <table style="width: 100%;">
                                <body style=" font-size: 14px;color: rgb(158, 157,157);font-family:sans-serif;">
                                    <td style="width: 50%;">
                                        <div class="ivu-form-item" style="display: flex; flex-direction: column;">
                                                        
                                        <label class="ivu-form-item-label" style="    color: #515a6e; font-weight: 600; margin-bottom: 15px;">${field.label}</label>
                                        <div style="display: flex; align-items: center; gap: 10px;">`
                                field.values.forEach(subField => {
                                    if (subField.value != '') {
                                        tableHTML += `<div class="form-check">
                                                            <input type="checkbox" name="${field.name}">
                                                            <label class="form-check-label" style="color: #515a6e;">
                                                                ${subField.label}
                                                            </label>
                                                        </div> `
                                    }
                                });
                                tableHTML += `</div>
                                        </div>
                                    </td>
                                </body>
                            </table>
                            </td>`
                            } else if (field.type === 'paragraph' || field.type === 'header') {
                                tableHTML += `<td style="padding: 20px; ">
                            <table style="width: 100%;">
                                <body style=" font-size: 14px;color: rgb(158, 157,157);font-family:sans-serif;">
                                    <td style="width: 50%;">
                                        <div class="ivu-form-item" style="display: flex; flex-direction: column;">
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <div class="ivu-form-item">
                                                    <${field.subtype}>${field.label}</${field.subtype}>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </body>
                            </table>
                            </td>`
                            } else {
                                if (field.type != 'button') {
                                    tableHTML += `<td style="padding: 20px; ">
                            <table style="width: 100%;">
                                <body style=" font-size: 14px;color: rgb(158, 157,157);font-family:sans-serif;">
                                <td style="width: 50%;">
                                    <div class="ivu-form-item" style="display: flex; flex-direction: column;">
                                        <label style="font-weight: 600; color: #515a6e;">${field.label}</label>
                                                            
                                        <input style="border: 1px solid #bfc9d4; margin-top: 10px; color: #3b3f5c;font-size: 15px; padding: 8px 10px; letter-spacing: 1px; height: 10px;
                                        padding: 0.75rem 1.25rem; border-radius: 6px; width: 70%;" 
                                        type="${field.type}" class="${field.className}" name="${field.name}" id="${field.name}">
                                    </div>
                                    </td>
                                </body>
                            </table>
                        </td>`
                                }
                            }
                        } else {
                            tableHTML += "<td></td>";
                        }
                    }

                    tableHTML += '</tr>';
                }
                tableHTML += '</body></table>';


                const html = fs.readFileSync('./app/view/form_builder/download.handlebars', 'utf8');

                var data = html.replace("#body#", tableHTML);

                var milis = new Date();
                milis = milis.getTime();

                const browser = await puppeteer.launch({
                    headless: true,
                    args: ['--no-sandbox'],
                });

                const page = await browser.newPage();
                await page.setContent(data)

                // create a pdf buffer
                const pdfBuffer = await page.pdf({
                    format: 'A4',
                    headerTemplate: '<div style="text-align: left; margin-left:40px; width: 100%;"><img style="height:30px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABdwAAAHACAYAAAC4bqqXAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAABkVpJREFUeNrsvQl4HMd55v/VDA4C4AGQlCheEkiRFG3rgC5LcmITjC0ntuUYymZX6zihyU28STa2RW7i/J8niZdSfCTrbEzJthIfiUnRku3EiQhZlCiZEgndtwSJunkIJEFSPAGeOKfr39VnVXV1zwDTMzj4/uzm9HRXV1cfM9Dz9jvvRwQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgnMBwCgAYW2ze2NpovzTKy6bse6dp4uG99fKyqlNdNH3HS3n7O7LgCuqf2KAs67Pf28vbtKbd19/Q0o4rAAAAAAAAAAAAAACAGQjuAIwCNm9sbT5v4Cix7mONDXvfaqx6bw+drJ26pJJZVEGcemomN1ceO+S0rT22n7L9vSMyzpPnzXderQk1lKmqas8O9HX38ixNPNrZPjBv8fEz1ZO6acGi9o7qWXT9DS1tuLIAAAAAAAAAAAAA4GwCgjsAZWDzxtYm+6W+/uSB5rqdb1D1me4lVSe6xKrmCj5AFdYgVR4+MG6ONzdxMg3WTqL+TLX9JWO199dM7u5rOLe9f8as44dmLG4juOUBAAAAAAAAAAAAwDgEgjsAKSKc6rNea2tkg7nG6v6TSzJ9vfXZ3jNN1cePUvWpLpwgj1zVBDozbRblJtR25KondHDKPJoZ6OvY+eGbOuCMBwAAAAAAAAAAAABjFQjuAAyDzRtb6ydVVzZNe2Fzc/WBdy/jFZWN2f7eponv7cLJKZLBmjo6PW1OB+NWe5Zbr3S9/5r2A3Mubr/+hpYOnB0AAAAAAAAAAAAAMJqB4A5AAQjn+vT3djRP6nznstqDHU2ssqqxZj/E9XLRe+4csizePVhd035i/mWPHl1wRVs/Z0KE78bZAQAAAAAAAAAAAACjBQjuAGhs3tjaWHd0X1P9gR1LqnpONtceeLep9tgBnJhRxpmpM6lv0tT2vqkz2k9PmfHoscZL2uCCBwAAAAAAAAAAAAAjCQR3cNYjBPYp3Xub63dsW5IZ6GuuOXm0sQ7u9TFH36Sp1DN9dsdAZU3b8QsvebRrxoUQ4AEAAAAAAAAAAABAWYHgDs5KHnng3pY5zz2wpO5oZ3Nl7+mm6uNHiCwLJ2YcfbMNTJ5K/dUTOwYm1bftf/9H7j09bVYbImgAAAAAAAAAAAAAQCmB4A7OCp7f/MvGia8+2ZLp6/3MxAM7m+sOwMF+tjE4oY56p89qP3Vu473W+Y2t7//9Ve04KwAAAAAAAAAAAAAgTSC4g3HLcxt/3jz19ac/w06fapmy983GipMwNwMXq2oC9cxs7Bhg2bbDV/zGvVf/3p+24qwAAAAAAAAAAAAAgGKB4A7GFY9v+GnLOe8895maQ50tE4/src+ePomTAhKxqidQ36Rp3admzms7+oHr7j05oaEV0TMAAAAAAAAAAAAAYDhAcAdjnid/+sOWhr1vfabu8J6WKXvfqafcIE4KGDanZ5xPPefMaX1vwQfv7ak/F+I7AAAAAAAAAAAAACgYCO5gTLJ5Y2tz41Otn68Y6Glp2P16PQqegvS/HRl1zX0fDU6YuO7wRVff+6HP/xliZwAAAAAAAAAAAABAIhDcwZhh88bWxllvPHHzxH07WiYe3tOY7e89ez6odZxoojdfyYlN5eHKSntZAzdv12ARVRW4k34i3pUxruKH1K8KfowRH3CX8S7mbDveOTFrQXdv/TmtJ89tvP3KL34VBVcBAAAAAAAAAAAAQAQI7mBUs3lja/35O59tqX5vz811B95tqjw1/tI9HLG8yhXHmRDH6zixOvu1iscK6fno3DdIZ3p4UeOaO7uCamqG8BUhCfa+QG8d9N4fHEdfNZkMHW+8uGNw4qTbd13xKRE504FPKgAAAAAAAAAAAAAQQHAHo5JH7r+3eeEL936++uDelqquQ/VsYGBsf9AauONSd5zpnqBeiPv8nR3ucR89atGxY25sztFjOXsKI3T2dg5ST5HieiFMrc7StPMyzkOB6ppamlOfpWxdr7t8aoZoIqeFk6qJzYiJ9/EEeeGI56fD+bHsju+fOoNyk6e07rv8Y3de9bkvInIGAAAAAAAAAAAA4CwHgjsYNQg3+5xXt7TUnjy8esqu1xoplxt7HyhZWPfnY1zqQiTfu28wENN9If3MGe441Mcys2srqfYcojn1FVQ7ndHC6VVUO8t9r58Pftr+GjrFHFe8E1XjCPJj7KtJPDeYNqPj+IwL7+y4+obbUGgVAAAAAAAAAAAA4OwEgjsYcUQ2e+NTratrTne1TNy3vX7MfHiEmD7DFdSdKcbZ3emJ6p37co7ALoT2d7YPnLXXe2pNBU1ryNDCWVU0bZ492Vd84fwaytRJmfz9RNahDFEXc2JpxlIkTd+UadQ7+dx1+6/62J3Xfe5P2/AJBwAAAAAAAAAAADh7gOAORoxH7/uPltnP3n/zxMN7miccPTD6PywzOGVmWMTO5cY4GN+xvn37oONW39uZG/NO9XIiomnmNNbQnNlZWnRBJc21z7dwxfvwg14czUHmivGjPIomVzWBTsy6sP3g5R+7/drP/fE6XGEAAAAAAAAAAACA8Q8Ed1B2Xvze15Y3vLdj9ZRd2xqz/b2jdpyZOVboYNfc67K4Ll47OweVXHWQDvVT6+mCc8/QnNkVtGhhJS1YOJEyle4944jvnZkx4YA/Pndxx7F5l955ZH4T4mYAAAAAAAAAAAAAxjEQ3EFZEPnsM995ZmXtob03N3Rsq2eDo8/5LYT1zFzPwa4J7EJMFzEw23cMwLk+wsw5t4IWLaikRQsraKH9WjuFBRE0fK8rvo/WDPjT58zpPjbvsnXvvf/Xbr/+hpYOXE0AAAAAAAAAAACA8QUEd1BSRD77zDeeFCL78rojnfVkjSIXeJXnYp9rUeZcNSJGCOyvvNpP7+wYcIR24WgHoxNHgH+/L8JXUk0NC93ve90YmtFGX/05dHz2wnW7r77hVgjvAAAAAAAAAAAAAOMHCO6gJAihfc4rW1bX7359ec2x0ZPP7sfDZOZbzryPENTbX+13HOxCYEc8zNhFiO6XXVJFl11aRdOmZhy3u3C+W7tGn/jeN7GBemZesO7U7Pm3XvzFr3fg6gEAAAAAAAAAAACMbSC4g1R57r6fNU5tf2x13aE9y2sOvDs6bnIRFTPfdbKzulBkF7Ewr7w64AjtiIgZnwjBXQjv111T7eTAj1bxfbB2Ip2Yd0lb94LLbr3iD77chisHAAAAAAAAAAAAMDaB4A5SQTjaz9v/xuqprzy+vO7g7pG/sWNEdhET88q2fkdkR0zM2YWImmm6tCpwvwfi+1uZUZP5nptUT73nzVl34OpP3HrV76zowFUDAAAAAAAAAAAAGFtAcAdFIYqhLnhuw+qaw/tXTji8b2Rv5jpOmQstYvNDkd2PihEiO7LYgU9EfO9ijuvd2pV1CrCONFZNLfXUz1h39Iolt1664i87cMUAAAAAAAAAAAAAxgYQ3MGwEEL7ebteXjn9zadvrjncWT9iA/EKn2YW55RM9qef7XNEduFoByAJPXbGiZsRzvfOzIiPTUTNnJrRuG77kt9bdf0NLd24WgAAAAAAAAAAAACjGwjuYMi8+MNvrTz3tSdWT9q/Y8SEdicyZnHOEduF6C54Z8cAPfNs39kUF9NuT92GZceL6HOKPTVpy+oNy8YlQnAXwruYJlgZ4sL1vnPkI2f6Jk3tPnLR1bfvv3jJbRDeAQAAAAAAAAAAAEYvENxBwTx15x0tM19/fE1959uNrL+v/AMwuNmPHrMckf3pZ3ud+XFAm/fqC+fd3ryge/3ervaRHuCyuQ2N9kuj91aeX+K9CnG+fqxfCMf1/sFq59WPm+EHR/Yr88Tshd2HF1616sovfnUdvpEAAAAAAAAAAAAARh8Q3EFeNm9sbZr7ysNrpr/zQnPF6RPlv0lFNvtiizLzc4GbXUTGPPNcn5PLPsbwXemPUiimjwohPW2WzW3whXf/dQmNQbe8iJy57poJdO011TS1OkvWq1k3bmaE0op4ZRUdnXdp+5GFV666dsXNbfiGAgAAAAAAAAAAABg9QHAHsYic9hlvPbV6xlvPrKzuPlL+m3MGd0T2zHzXue672R9p6xkLkTFCQO+wp1f8+fEoqg8XySXfbE8XkCvCj3ohPnC9L65yHO/WWyMXN5OrrqHDC69q3XvVJ0S+ewfuKgAAAAAAAAAAAICRB4I7MPLUz/515dyn7x2RnHYhtGcvydmvrtDuZ7MLV/sopY1cUV2I60JYb8MdNDw8V7yYLvNem0fjOH3X+280T6DqA0J4zxLvGpmv09PTZncfn7v49vf/zXdvwR0EAAAAAAAAAAAAMLJAcAcKT9z/n03nPf/g2qnvvNiU6e8t782oCe2vvNpPWx7tHW2xMR3kCuyOcx3ieumRRPglNAqd8KLA6qc+UUtTByoot23kct6Pn7+449TMeSs+8JV/xD0JAAAAAAAAAAAAMEJAcAcOIj5m7qtbVk/d9crKqu7DZd23iIwRky+0Cyf7/ZvOjJYiqMK53kZu5roQ2Dtwt4wsy+Y2iF9dNJMrwIvXUSHAL1pYSZ/6RA0tnFblON5FodVyk6uaQCfmX9zadcmHVl1x05/gXgUAAAAAAAAAAAAoMxDcAbX/67daGna0r52yvb2s8TGO0H5pzimKKhglQnsHuQL7veJ1/d6ubtwho5vRJsCLuBnheL/24glugdUREN4HGqZ3n2h8360X/vUPbsMdAgAAAAAAAAAAAFA+ILifxTy88d7681/ctLb+3W0tVae6yrPTKqLs4hwx4Wiv407x0y1tvfT0s70jKbS3kutgb4WDfezjFWRtJleAb7Gn+pEYh5/zvvTqGqreXlF+4T2ToZ5pM9v2XffpVVct+zIK9gIAAAAAAAAAAACUAQjuZykv/+hbK8997YnVdfvKVBTVE9ozF+WceV9of6Stx5kvMx3kiezr93a14m4Y33gZ8J+nEXK/19Qw+mhzzYgJ74N1k+jIRdfcuvivvnML7gYAAAAAAAAAAACA0gLB/Sxj88bWxvPffHztOS9vbS5LUVRNaBcudhEbI+Jjyoxw+IqYGOFih9v3LMVzvwvXuxDgyyq+j7TwfrLx/e0HF39wxVV/9Je4/wEAAAAAAAAAAABKBAT3s4iX/vUfVp7z9vOrJ+16rSyu9sziHGUvGVGhXQiLdxKiYoCBkRLfZeG9qr2S+MHyfQ33TZlGRy68/NYPwO0OAAAAAAAAAAAAUBIguJ8FbN7YWn/+Sw+tnf7Wsy3ZMrja5WKoIyC0d5AbF3M7RHZQKCMhvvvCe/P7aqh6R3mF9yOLrmp/90O/s+L6G1rgdgcAAAAAAAAAAABIEQju45yn7ryjZc6LD62dvL/0We1sBqfslYPEGsJiqBs3nSnHYXZTKLJDQARF4YnvN5MrwDeWen+K8C4c76fL87XcN2lq9+5rf/vWD/7xX96Gqw4AAAAAAAAAAACQDhDcxynC1X7e60+snvXKlpWldrULJ3v2uhyxGVa5i6EKkf3e9Xu71uGKg1KwbG6DEN0/Y0/LS72vQHg/v5aq3qgk6i/98eWqJtDhRVe37r3qE8Lt3o0rDgAAAAAAAAAAAFAcENzHIZs3tjZd+MQv1k7d8XJpozFEQdRLck5Wu0DExvzintOlFto7yM1lX4fIGFAuls1tEL8QWU6u872xlPsSwvsNH62l5pl1lHsrWxbh/cSsBd37r7z+xms//6U2XG0AAAAAAAAAAACA4QPBfZzx8g//buW57Y+uqTu0p6T7kQuivrNjgNbfdcrJay8hws1+5/q9Xa24ymAkWTa3oZncrPflpdzPtKkZ+t2lE+kSqiGrM1Py4+qbNJXea/qN2y67+W9X4SoDAAAAAAAAAAAADA8I7uMEESEz76n/XNvQ8XpJC6PKOe1CYF9/9yl6Z/tAqXYnIi7WEQqgktVzImu/ZIg7vx6otF/dzy63hBJbYc8w4uLz7LyKFVV2m6zzGXe2Ect5Jnwv2lqifcZt7ywftN8Pun1z8vvjnJ929+Xs07L32eP1kcvOuujM2XpNPNf7SnLF98ZS7WfRwkr6/esmU8Mh+7J3lf4r+8iiq9vf/dCNN15/Q8tZ/ZkDAAAAAAAAAAAAGA4Q3McBjz2woanxV+s3TO58u7FkO5HiY8pQELXDnm61p9b1e7vGfa407+/1hG9PECdHKM9w933WbSTE8UAsz4SCO3dFdlcgz0iCu98+466TBXlvW+IZb1t/HTntw74znuAu7Uvs2/L78PchthUifE4cjb2sz57vs+ftV+rLnn9J73i/hsvmNiwnV3hvLtU+PnlNPS1dUEnVeypKHjPTN2V69/7LP7ri8i+uxi9KAAAAAAAAAAAAAIYABPcxzkt33bF85rP3r6050FGyfWTmWJS9btAR3Uuc095Grpt9XIp8fLDfzQVxhXEx74vs5Anh3mfSFbN58D4QtaU2yjJXLA8EdqJAEA9erVCQj3W566J86Ip3xsINIn4g/nNJhA+2z0hCvng6k7Nfu+1JCPC92XmXd423a+zFzYic95ZS9D+1Oku//9FptOBotuTHMjihlk4suPTW+bf8+BZ80wIAAAAAAAAAAAAUBgT3MYqIkJm5+5U1s5/YsJwNlMjuKlzt1w06gnuJ42OEwC6E9rbxcG24JbLsue4C94X1IKqFFOGbQhd7KLoTD0VsShTTA6FbiZXRYmSC9RlVcJf74prg7rb3xH8K+7OYKsDL+zOI7+F+mdqPEN/5KXv+pD1/zH7tyV54Vc9YvweWzW1otF9WU4ly3i+eMZk+d2ED1eVKX1H15IJL2vovv/bGC29a1U0AAAAAAAAAAAAAIBEI7mOQrZvua2x8dsOG+rdeaGKDgyXZh+xqv39TT6niY9bZ061jPZ+dh2K29JkyudGd5Z7gHrRhhm0yvmNddblbqkivCPD5YmXIFfSjsTIsiJYJ3OjOvvy+M4rjXo6VCdZHhHp1W066Y15zxzsLJKc8iSz5Lvv9CXv+aHbRNUfG6r0hCe/C8V6fZt812Qy1LDqXrptc+q/x/mnndnQvvPzGRX+xph3fwAAAAAAAAAAAAADxQHAfY2x94JfNF933vQ21hzvrS7IDydX+zo4BWn/XKcfdnjLraIwK7UJ0ZoxFlhkEd1Ic7m5DP/pFd7kbRPpQiOe6+zzoS3e56wK7808m4oRPLJ5aYKxM4FzXhHNXqFcjZsIImlCcj3XHk+quD4/5hD1/1J4X4vvh7EUfGhhL941UYFXEzaT62V04ucpxu0+typT23q+s7N533adWXfzlb67DNzEAAAAAAAAAAACAGQjuY4j2O762fPbzD66tPFWa6Gvf1d6T446jXRRGTZl1NMaEdjcexvucMP+FKQH2PHSca58p2Uke63LPqNtpcSxh/xktVsZdNpRYGbV4qr9fG0sSuvXiqWF/PBIHk1qsDIsWeaVoP2q7E/b8YfsQDttvDmff9+H+sXA/lUp4r6mqoE/MmUxLZk4kNljCZxGZDB1ddMVtF359/Sp8IwMAAAAAAAAAAABEgeA+Rnj9e6vXnvfC5uXZUyWIUS69q30djSGhnecGGbGM/vngFDrbucHlzlQh3W2mxMq4DVlJYmXk4qlhcVSKCOJy8dTiYmW09bGxMmHxVFUwZ3liZUJ3ve7Ul86ZPZ4Mcx8GHLfbddrrOis+sGTUF2P1hPc1lHLG+4KpdY7bfVomV9LxH3r/h9p2f/CGG6+/oQW57gAAAAAAAAAAAAASENxHOaI46pz2h7fObN/SVJIbYAZ3xPbejFUKV3ubPa1av7drVOc+85yTg59RPhMsw6MnK3S2m13ueqwMyYVQ3eV5Y2XkbUYqViZP8dSiYmUkl7taVJYZ3PgmoV6Np/G24Wq7QXt+rz1vT3Sw4uKlo9b9XoriqiLb/b9fdgFdXtlb0rGfOveC9jc/+ccrrr+hBbnuAAAAAAAAAAAAAB4Q3Ecxmze2Ni3ecufaSXveLonYnr00R5lLctS5b5C+/6OTabra28h1tLeN1nPLB/sZuTZ2JoWyc/LVcEdQj3w8+AjEyjjLw1gZIs3lLke0+MVThxEr47vYZZe7Hv+ixMqExVNjhXN1W2m/cvHUAmNlgvOkPaQIxPakdp326x77dXfFpR8dleK7J7yvtafmtPq8tGECfe7CKY4AXypOT5/d/d7FS5Ze+cW/gegOAAAAAAAAAAAAQBDcRy2PPtDaNO/BH2+dvH9H6sVRWR2n7JJBYg2c7t/U4zjbU6KDXKF93Wg8p3ygzxfZs+6d793+oXgeCu6O3BsRKguNlZE6T4iV8XosSayM4nI3OOHD2BmvHyVWhsKipVakqKkaK2MoiJoUK5O/eGomktnO5YKuPOKel2Jl4trJ+9tjz++2Xzsqmq4fdeL7srkNzeRGzaTykG1qdZY+t2gaLazNlmzMZ6bOpF0fuWnFr//+/1yHb24AAAAAAAAAAACc7UBwH4U8cdcPl89/7N/W1h47kHrffmHUo6csWn/3KXpneyoFFkWO8+32dNv6vV2jKtOZ9/e6TmxHZBcKOgtCU6SPgC+ky8L7MF3uhcbKeLvJGyujFE9Vi5q6CxNiZRSXuyxkkyZEe+MdQqyMu++MFisjZ8WnGytj6oeUwqqu093cLuO59fUHA7vt+Y6Kpo+/Pdq+A5bNbVhOrvCeygO337pwOn1iZi2xgdIUVM1VTaB9l1+/6tKv/N/b8A0OAAAAAAAAAACAsxkI7qOMbbf99fIZrz+5tqrrUOp9Z6/MUWZxjl55tZ/uvPsU9fTwNLpdR6OwICrv78nat7ew9Vb4izwBnUt3P9cEd3e+QMGdhls8tTyxMlJxVK8/1ZHONCGbShArI7vcw6KsqrBOaga87m4vNlZGLr6qFGyVHfVChX7Xnn+14vLfOjJa7mGvsOpKcjPei+bCaRPpDy+eRRN7T5ZkvEJ033vVb61r+t/fXIFvcgAAAAAAAAAAAJytQHAfRbzw/b9f2/jUhuUVZ1IWxKqIKj42QL0TUi2MKjKbV42mnHbed0aIqBXO5CrhsmDOw+gY2eWurHdfQ6d7vuKp6cbKqGK8tE0QnRL2EYrWpmXDjJXRXe66KJ83VkYVzvXiqoXHyrBoRE2CA16JlUlsl9H257fLSvs9ZTd/xZ5/s+LKT/aNhvs6zXx3kef+h5fMpEXVVmkGa38eDi+6el3HdS2rrr+hpRvf6gAAAAAAAAAAADjbgOA+SnjjmzevndX+yHKy0hXC2AxOFR8ZoM7Dg3TnXaedAqlFIkQ04WgfNdERVu+pCsZYFTliO/MFc6451GXXeoljZYZTPFUXvmkosTLi34waK0PJxVPD+BumuustTZgvJFbGy5WPFc4N4rscK6P0YyqeWlSsjN9fGCvjb6Ou0x314v9v2f+8UXHVpzpHw32+bG5DC7kxM43F9vWJOZOcKSyamy6H33dNe8c1n1kK0R0AAAAAAAAAAABnGxDcR5jNG1vr57zx+NqZz21qSbtvER8jYmRSjJBpJdfV3jHS583qOcGIZart2UpydO9Ay/YP0nJFcUUQT3a5m4qnljdWJp3iqQXFypAr6EdjZeTiqeFx+MVTfXFbdblnvLHkK55aYKyMsyCjifxy8dSMyT3PnauiFE8dSqyMaYy+kC9+cvK0Pb+j4upPj6jr3YuZEREzK4vt65JzJtLn5k+hWiqN2/3kzPntb/3mH0F0BwAAAAAAAAAAwFkFBPcRRIjtF2zbsnX6tieaMv29qfYtCqNm5lv0i3tOpxEh02FPK0ZDfIx15rgofjqBnKAcJgRx186tOM2Z5HAveaxMgcVTpQiWImJlvP5193ae4qm+mOz8E3XCJxZPLTBWRi2eKjnXk4qnphsro7jc45zy7thMjvqM0rcs8oft+u1jft1+/2LlB3/7+Eh+DpbNbWgiN2amqZh+ptZV0x9dNI3mVJbG6T4wcXL7wcuX3njxzX/XgW98AAAAAAAAAAAAnA1AcB8hHnvogfrGR+7cOnnXtqZUO5by2r//Lyfpne0DxfYoomNEhMyIulStM8eFk72G3Hx2sciTszNBhIzmcvdFd5Jc7qM8VsaZTyqeyqT+M0XHyqjFUzNhvIglicx68dSwPx6JgxlWrAxT3eVxYjlF+4m2Yzx/u2xBsTLhGDPyNeE8ODev29O2ymtb9ozk52LZ3IZbqMiiqhNqJtDvNNbTtZNL8+egt+Gc7u7Lfn3p4i9+ox3f/AAAAAAAAAAAABjvQHAfAR7/1QP18zav3Trx3ddTFdtZA3fEdpHX/v0fnaSjx4qKiuigUeBqt053Czd7HQlB1HOzK9EvoXjO42NlhuByL2esjNuQlSRWRi6eGhZHpYggLhdPLS5WRlsfGysTFk9VhXBWfKyMXDxVduirDnhlf/ljZZi2rwxXt91jL3ui8tobd4/UZyStoqpOrvvsiSUZ48Dkhm5rztylM/725xDdAQAAAAAAAAAAMK6B4F5mnnt4Y/2ce/95a+2Bd1MV2zNzLCdG5umX+5wYmSLz2kfc1W6dOlpj3551xFgmFMZ157onlCfFyqRWPDWNWBmSC6F6e84XKyNvM1KxMnmKpxYVKyMVT5Ujd+KiY5LjYlKMlVGLsYbnxjnmjOaeF+tP2POPVV73O6+M1Gdm2dwGkesu3O71w+3j0qkTnFz3mmwm9fEN1kzsPrHosqXzv/ojiO4AAAAAAAAAAAAYt0BwLyNbHriv/gP337G1+uCedMX2+a7Yfv+mHtq46UwxXXXQCLvarZNCaKeJ9p2Z8cR0LgnhuuDuLS9JrAzREFzuZYyVcQ8ucL0TaS53SegOiqcOI1bGd7HLLncu953RYmXC4qmxwrm6bXGxMsF50h5SBGJ7UruMIvyrjvrMEGJl5PPo93Pcfn208kP/ZUSE9zSy3WfX19GXFjdQLc+lPr5c3eTu0/Pfv/T81T+G6A4AAAAAAAAAAIBxCQT3MiEKpL7v8Z9vnbjz1VTFdiG0983MOa72p5/tK6ardfa0aqRc7daJI7X2yyTX0U5+zIsnmDuLfOE7KVZGEr719UXGyoTjkCk0Vkb6rCXEyng9liRWRnG5G5zwYeyM7+SWY2UodH1bkaKmaqyMoSCqKb+dgsKk+YqnZiJZ7PFxMcOLlSElOkY9Pq642CVRP+LW138lcMIR3n/td18eic9TsdnuwuH+Z1ddQOdTb/qf9Qm13X2NC5fO/PrPILoDAAAAAAAAAABg3AHBvQwIsX3x5h9vnbRvR3piexVR9kpXbP/2d05Q577B4fYkBHbham8diXNjHT8kYmMmkyMmM8/j7bz6IrnvUCcyu9yVWBmKd7mnECszHJd7obEy3m7UWJlwu3AbuXiqWtTUXZgQK6O43GUhmwyC8dBiZdx9Z7RYGTkrPt1YGVM/mvjN49tlPGd7obEy5BRb1WNl/GPWBfkwUifjfL4431D56//13XJ/tjy3+wZ7ahzO9hNqauiz7zuPLq8sgeheV9d9+PKlSy9a+S2I7gAAAAAAAAAAABhXQHAvMY7Y/tCPtk5KM7O9ipziqPvODBRbHLXNnm4cCVe71X1wAjE21Z7NuneiEMwlwd29O62EWBnN5R4bK+PsTouVkfqSloUu92HFytBwi6eWJ1ZGKo7q9ac60pkmZFMJYmXk4ql6YVLdgW+Ki2HFx8rIxVeVgq0JxVODWBnJRZ8YK6Pm2LvzHfb6rZW//t92lfNztmxug8hzX2NPy4fbhxDdr5uc/p+KwYlTuvk505ae8w8bIboDAAAAAAAAAABg3ADBvYQIsX3Rlp9snbLnzZKI7cLZXkRxVFEU9ZZynxOr68AE+7art++8Kk9MF4t98dsK70rf5c6iOeySm13bPr1YGdXlLonyicVT042VUcV4aZsgOiXsIxStTcuGGSuju9x1UT5vrIzmGB92rAyLRtQkOOCVWJnEdhlDrAzz3OykxMoYxsD1LHg1I54odLn74r2Yf8Oev7/ywzd1lfNzt2xuw3JyhfdhFVT9+Pxz6Ybz7I9sLt1cd1FI9dSiy5Y2opAqAAAAAAAAAAAAxgkQ3EuEENsv2rJ+6+Q9b6UmtrMGTtklg/TMa71OZvswxfYOGoHCqNax/RXEWAOJgqiymM505/pwY2WICiieOkKxMsMpnlpUrIz4N6PGylBy8dQw/oap7npLE+YLiZXxcuVjhXOD+M4Vh3ie4qlFxcrI0TEZJfc+b6yMXDy1oFiZULyPrn/Kfv9w5Uc+21Ouz2CxBVWvbjyXfn9ODbGB/lTH1T/9vO49V39y6VV/+BcQ3QEAAAAAAAAAADDmgeBeAoTYft5rj22d+8KDqYrtwtn+9Mt9tP7uU8Ptpo3KHCFjHevMOI52YlPIVcFllzoPI2MojJVx59VYGbl4auqxMlK7fMVTSxUr4zbU2qVUPNUUKxMsV3LWTbEycvHUcHx+8VTJ2S253DPeWGKKpw41VsZZkNFEfrl4asbknufOVVGKpxYRK0OmhwVh8dS4oqyqi17dnlt9dr8PVy757OPl+jwWGzFz5cK5tOy8LLH+vlTH1dtwbnc165vX8MOnu/EXBAAAAAAAAAAAAGMZCO4p44jtbz65dc6Lv2pigwPpXCRPbN/ylOtsHyZlj5CxjnVOtF/OsY8gq0bDBE52q4hYmcTiqarTnEkO92HHyoSvybEyBRZPlSJYioiV8frX3dt5iqf6YrLzT9QJn1g8tcBYGbV4quRcTyqemm6sjOJyj3PKu2PLUzw19ViZ0BXvLjtg939vZfPv7SzXZ7OYiJnZtZX05fdPpZpsJtUx5eqntvdc9aGlc/7kWxDdAQAAAAAAAAAAMGbJ4BSky+xXtqyZ++z9qYvt639xarhiuxCvlpZTbLeO7Km0ju6ZS5zPst9WShHo+gMeJseju3DDQyAljiXhIVHYLog4UVbz6PY8dn+UZz+mvpOuZPQ9Y4YO5XZMFfrDCB69Py0SJ26QEde/2o4lrWdh3A7T+zb94oBRzD4MDyuUfWi/cGDRbcOHMNIhGX/1IHRybjj98X1Hj58i8UZBH8qDHIqu94dg+KWEes3E5+R/DbTd/Vl7qinHZ9T+PlgnvhfIjZgaEqJ+xHfeOEY9VrpjynYfa6p9pm3r1k331RMAAAAAAAAAAADAGAWCe4q8tuav1s56+eHlafUni+1PPzusCAeRiXx5OfParSO7p9sv8+ypVj6U0Lkd1hcN1ob539Ky4FV2gJO0ja48M3XD6OmkSOdaxEu4PzL3XRi62B9EzJh7SRajzQfEDet4EL1jamcS95m+XxbTt0m0ln4Z4AnmTN1HvHCurosK9IzFj0Edv14cl9SMfr+IrkHQj/6KIebcRfpWiuiypG0ZM1w/k8gfnMOr7emrA213LSnHZ9X+XnC+H+ypdajbOqL7m0fpTMp/QjKnTjUtePjHW/HXBAAAAAAAAAAAAGMVRMqkRPv3bl0z96lfrsz2p1MDMQWxfZ09rSpXXrt1+N06e9QziLFqKfbFi4fxhc6MKVZmOMVThx8rk1rx1DRiZUguhOrtOV+sjLzNSMXK5CmeWlSsjFQ8VY7ciYuOSY6LSTFWRi3GGp4br3iqev4zNPRYmbBwrDu/0369u3LpHxwrx+d32dwGES+zcqjbzZpSSzcvnJR6vEzf+Reum/Ht+1bgLwsAAAAAAAAAAADGGnC4p8CrP/jG8jnPbxpNYrsQ2leUQ2y3Du3KWofenUmcGu23E9SIFm5yiCdHuJgiXjjFuNmjpy5vrEzy9vr+ErZJI1aG5FiZmHYmZ7dyGrQHASwaWxLMK9r+EGNlvAcd0qOB2DFGs/K9s5ovJkYbjzF+hsU471nseeAFtiPzAxr5suixMhStD6DtQ32QRGSKnWHGKJoF9nTLwJb1nyzHd5j9XbHKfhEC95C+M/YfP0O37zhNp7PVqY6nes/O5bu+unwt/roAAAAAAAAAAABgrAGHe5E88/Mftcx7+K4N1d2H07kgxYntQiwTQntrOY49d3DnRHvEcxgTOe3MCh3pzOxeJ2bIvVaKp8puc9kdT2rx1IzXH+kuegr79uNEmEkEHVrx1LgMbhZ5XqXEkzCmx8V77nW1eKrkIk8onhq6veVtMv4DBi734Tvg/WVq4dHwoUTouo464TnJBUDdbUP3NYWubytS1FRxuZsKoirHw0NXONeKnpqLp+pucr+oKWkO+GBb4UBnQYHTSDuxv6xhf77TXj0+HnGx+85+Fimeqv5KIEOyE179VUG0KGuwP2fc++1lP6n66Oc7S/2ZXja3ocl+EZEuQ8pRF4VUv9g0m+pyfSn+dWI0MOeCFeeseWAd/tIAAAAAAAAAAABgrADBvQi2/dPXm6Zve3xrzeG9qRT5S0FsX+rlMhfM6Q0LhLjWjKs5vslMXnxj9bV3PaXGyvjCrmihCNlkEIy9WBliBsE7GivjdBkIz1HhPO1YGVM/mvjN49tlgviXwmJlZJGeglgZ/5h1QT6M1JEecCixM0wR9EmKmHEfnnj98/urPrr8/lLfK8vmNojvM/G90DSU7WY2TKIvXTKDJvadTm0suaoa6jln1o1zbr+vFZ9iAAAAAAAAAAAAjAUQKTNM3tq4vr7hredGi9guRPZ5QxXbUxg1ppJO6XGis/ZDpMbK6HEtUuRLcH21eBqt+CvLU9Q0GiujF1HVCqtSTLa+Hu1ijG9JGIMUKxNppxVCjfZt+OWDcZ22b6bH9HAtsiZ63skQO8Pk8bFP9T+87q/7H147rZSfai+KaikNsZjqga6T9N1tB+l05YTUxiJiuuoO7V67+9tfbiIAAAAAAAAAAACAMQAc7sNg88bW+ose//nWyTtfTUUEYnWcKj45bLFdiGLDzmvf/Zef3Trl3Veb3UQX/45g0bT0IB6Fq7cNS/HuYunenoyV4OMi9xmVrJWFnAxtgzbcEAPPyJgN768zZcSbktildm4NVHGPTf6HSXds+ofiYmX04qmcqe7yvLEymmN82LEyLBpRk+CAV2JlEttlDLEyzHOzk+ZCj4xBK56aUQq0xhVPlfelR9GEsTIijigrLeu1p41V1/+PR0r9XbdsboPIUV8+lG1m1dfRl983jWqtgdTG0TftvO6uG/5g3uJPr+gmAAAAAAAAAAAAgFFMBU7B0Jn38kNr0xLbqYoou2RwuGL7OlEctegxCB05Y//jTOROjIX6MpOczflEbKUtC/oJ5p1ZWbhm0T7TUspN/SR1rQnmsScraJ9UOJUHvmdPfHYny5s4FVBkVRfl5Wh3bcy+x1qeRDJ+hhO3uDZ+sWPmdSZGKURfxuWCs+51EUqv0163qfvbGM6Xv45ct7gQkv3XcMQsKMCqFMll3BsGy9tPsMxu7Zdy9YdjaMeYF6FPzjZyO3dLFjzQ4N48k+4jafzBuLg6T0xrKz+Z4hQ9WVzalzcG/1jka+HuQ9QiCIsAsxp72U39m398kb3tuqqP/+GZUn3Xie+XZXMbHrVnCy5gur/7NN22q4ZWzc1STTadH1FVH32vfurG9Vv3/Oivl57/hW9AdAcAAAAAAAAAAMCoBYL7EHnzmzffMr39kZZUOqsiJ0bmJw+cHI7Yfuv6vV23FD0GX0QXYnvWfXXEd3+ZwBfg5W2CeRZd5iy2N8oKAT/j9sP8VyYJ7pIYr/TNIkb64R1byhZ3bligi/S+kC4J7cxxiwuh3XLF9pxFPGepgrsuvvt1SuVzK/XNTOPinqPdE9udGcs/D1yOK2HSwM1itSsEy0KxLzKTKlqTJMqHArSnEGv7SBDgVVGbIuNT5nWh2yjUG4/TFd41QT94CGHaH/OelEjbuMes9c2427c2LtIEfbN4Ly1XxuXtT74W3ljd9uKh39/3/+pf76j6+B++XarvPPt7Zt2yuQ1itmDR/b2DR+gHuSn0pfdPo2xfTzpfl0cPNlU827bGnl2Bv0QAAAAAAAAAAAAYrUBwHwIvfu9ry899asNqRzhN4+R/ZJD+41enhyO2iwiZdWkdl69xO2J71nOiZ8gTyCl0qXu6YbgRRUVtvzNHwM/aU8buM+u55yXxnfl9MUm0Z0kjLPIAi8DoRJet5uE856F7nZEnsAdiuye053LEBsldpjjWmTEWxhxdI+1b6LOWfyq5V2eT+wK6PB8V02WHuNGx7a0PnNiBOG12div9SSMOXeMG4VwRySkUtSnGHS/Py8Zxw/gVQdwgyqvj4uR54Ul9CMBjHPXauTE63XXXe/QBgy62yw8xwv1xz+XO1ENmNfa2f9n/0L/8suo3/+jeUn33eaJ7hz27wZ4Kqlux68hxumvPBPr8rErnnk+DTHfX8t1/9dndF3zzZ7fgLxIAAAAAAAAAAABGIxDcC+SpX/6sac6GO9Zk+3tT6S973SA9/Y5FW54csvszVbHdwXO5BxnuntPdj4EJlstRMBFnu5b7nskSq7A7EpMjuHvCO/OEdya53RXRXR9bCcoM8CE05Pk35lI8DOOevdzytFUhsjsxMjlXaHeTxD0reibYB+PR4+c8YdeB293bIOu52WVfNyvkwsuxMkHPLDj33LQ+UPFZGD1DajRM6Oz2xWTSgnCiwnQ4rGi0St5YmcS4mGAbHoraetw+k7YxOdFJOb5waPrDAk3EDyJ5wocUJgE+GIPywEKO32GSqE9SJI+/7DP9D/3LYnv2u1W/+YWSRMzY3ztty+Y2iGKqW6lA0f3FjoP2V8kM+oNz06vPPXHP9tU7//YL7Rf+nx+14i8TAAAAAAAAAAAARhsZnIL8iCKpsx7bsKH65LH6NPrLXpqjZw/30E9+cXQom4nc4stTF9sdWDRz3VvGGIVud1+Ed5zqJL3XJj9ORuQ3C4d7hRDfKxzxnVVWeFOlM5GYqux19jLxKq9jVZVSe2n5sKcKZxxqn+pE/uSM1xufM/nrpGUVbrtw+6w7+cv89eK4s96DhywL8/L9c+78GsD7VYH0AMP5H/Nf5aK2pD4AYVxbxsOooKA/JfuGSw8yuHwbuK9yh6qfXplXZHImC8GuFK3ugyt9B/uQ+2NhO0W/Z4YxyMuChxVcWqf3w/1tWL52pnNDyjaknR8eDjVhW8b05Wp/LHoO1f2R6br4YxbvL7Kn/9f/0I/OL9V3of39026/LPW+jwri+XcP0i+Pp/dsN9t7hqbsfnPtjr//0yb8dQIAAAAAAAAAAMBoA4J7Acx684kN9R2vNaZywudb1FnJaf3dp4aymRC3lnpiVxnwxXZGEaXXF3SJaUVRDdsGMTKZMFommHwh3hOoA1HeFead12xFIGqHy4c5OfvU+4v2HYypUp8qwnVZbwrE9PBYwn35cTqZMEpH+tWAVvVUOfWKqK7FxzB5uRw7HjGPM2P3RjGdhWK0KmQH45AFY4oK8UzrW+nHsG+TcG7YVhkXhQfIEsZoFLBlQTxvO3UbeR0zjEv9xQdXx8S4oR8yLmMFnQcnViY6BqWoQo39z9f6H/zhh0v17SCJ7gV/Hz381l56+gRPbQwVx4/VN+zYtlY8DMVfKAAAAAAAAAAAAIwmILjn4eXb/s8ts5/b1JxGX2wGpwPn99JtPxiys72MYnuIq/FySdjlUowJ1zJPNOOvEkMjXN0ZRYBnXsyMiJ7xBXlZnA+mjDZlhzDJQnc2ZspIDwaYPLHoFBmDt8wT150YnYzvZs8EfTL/mFmG1Pwe+dzxyGxoSvfPvXfKLb09Vy8BVzthxotEXHtSQsaLGYrspm259HAgxn1OkvCslXo1ud0VQVkbD0sU8UkVwePG4MfK5G9n6FvdP/MvjKGd6tbXf1mgPTTQx2BYFl6LOFFff2jwhf4Hf/iFUn03DEd0/9mb79EbvC61MWS7jzVduumONfgrBQAAAAAAAAAAgNEEBPcEnrrzjpZZ7Y+sNhfNHBqsgVP/xYP0/X86RT25gouullls51I2OPeEXfKywXn4Ks87xUBJdVz770lzyTNXAFfFd9UJz4KJmScmTxnzJMexxLTxMnFUgT2jCfARIV4X4TNqIVjJxc7k9fovBIgUHd05XznvNTjHljOJ+p3OZLmvzjXx21lc7ce7NlyRYJluco9GyLiqvO7YlkXy5CgaU9xKNFaGgv5MwrlJeHZ/LRF1mpuE7EisjNZPODAtVibSTneN8+h4lciXcEyMUeSYja5+4ubz57fXHzrI+5MuHjPGysjzH+7f9INv2FNtKb4t7O8l5/uJhiC639neQZ196Tndqw8fWL7nr35vJf5aAQAAAAAAAAAAYLQAwT2GzRtbG+e8+NDa6lNdxXdWRdTfNEBr/qWbjp7MFbpVycX2XEf7OZnq6kn++6D4ZyDmhkIvz3nLxJTzioLmpPc56b0nFKsxNLIQzRSR2xXBpeKsqkU+YZKRVOaEZrIhXFOrtYmk8VM0F93g4g+c6xmmFZglirjaffHcO2euqG65U84939xfL59v/xzrwrvl//pAOkj1YKXCtCwmC9wUa+KP3ZR9o0TRaH1r/USd6ybhnIzb5ounYUaBOrYdS24XJ5Kr+5LHxWLEdJbgnlf3xSN9GzPctVgZUmJl9Gx5v3+R535b/wPfv6AU3yFDFd17+gfpRzuO05lMSpnu9uelrnPHmp1f/Tzy3AEAAAAAAAAAADAqgOAew5xXt2yYvH9HOkVSrxukX2zooc5Dg4VuUnqx/d2XZ9ovl7CMCDbnqkvdEXcpFHu9V57zxWB7GvTei9dBe/2g5bYd1MR3H0WcDpcxY6QJH9rkuMF5/EQ8cIqr9m9dYJcHSjHrTcM1iPRKdAxT89gtWWTnznl0ztugf94s9SGGcy1IFddz2qvoR5yG4MEISedfK5gayV83FfNUhGz/mAzbKodsjJVRC4lq11uNldEKqyrt40Vreb1ZqI+4yXlsOy22JRoNo+6Hxa7jseOKxsqQVrTV5ODXC6+GY40+VND3Lxzuf9P/wPc/UorvkqGK7l2neug7b3YN5Zc+yd+vZ07RlM53Nrz7k28izx0AAAAAAAAAAAAjDgR3A+3fveWWmS89nIpjMntljh54rpeeeftMoZuUQ2z/gP3yAeKcqbEmkrPa8gTgYLLfD9ivA744bEnzOU94Dyfflc25LmaTGssyVCQhPYhboQQ5nic52uV+tQ3yifhccprHScDM8F6OhhmUhPZB0/tw4v6Uk16DiZxXp63sfE843KiYThSJlQnOjCbIq7nuUWHZnMOu9hcrnCcWbY2LsdFc9jHxNJr4zeKy0rWIGG3fJhd6pLAsi3PMm6JxjOfCcA4ZmcZgGqO+L7FOiO5/0n//P3+iFN8pQxXd93efov84MJDa/itOHm+seumZtfjrBQAAAAAAAAAAgJEGgrvGCz++rXnWS5tXp3Jy51v0yrEc3d92stBNSiq253a9WJHb9VITcT6bAqnOz/z2HNVCtB2wlw3wwL3OB3L2JOa9137LeXUE935/yoWvA+7E/SgUP94kiIsJ4UoxVoOYrk9UgOfcLyxq8fyiOY9rR8kKfqSQLNfV/Sj+frxfCIjz55znfv+c8vD9gDf5DzD8BxwD3CDIy254OdrHH45kr2eRGBnvvV4cVReBmS5kSzBDVrq+jshYJNUk1MvbMqNDvLBiqwlxMUJsV4unku6yD8enj4vFuuNNhU5jXfZkEuCVMcQ765niZDfm28eMl5b13//Pf1qK75ehiu7P7ztGj5xM709Q3d4dLfv//LeR5w4AAAAAAAAAAIARBYK7xOaNrfXntT+yoepk8bntokjq/ikD9JP/KLiv0ortO1+otF8+SMTPlYfp/Mv9OBIexsIoYjqXJm9Zn0Vcn3pz9mS500Docld8uQZNNingJWxUoHDu7FN3p1uaY51UAb2g6BrNNm4S3uV4m4iDXnK25zyxvU+cx1x4Tv2HGN5Dj8hkcL77kTN8kDynu3SocpHb8FTr89HYFqKoWM204qnhtpporeS6x8TKRPqOjitv3AolRMcYhXrjcTJzVjpRbGHTuFgZFmnHGOOx27ICH06Yom+Ua2GIp4nmwutu+iX99//TV+ypLu3vGUl07y6k/b1v7KNXzrDU9l+zr2P1zv+zHHnuAAAAAAAAAAAAGDEqcApCFjzbunbi/p3F5wBXEfUtytH3152gnsGCcopLLLY/P9l+uYQ4n0SMuQMScTKM8bsefYn6Dx4NG6egfc2ZWEm/+4HprnAvi9/ufikUpwvprcB2PO8C7eAKyGYPxpunW540Jq4UMOV+lMyARbe/fHjoJ5cP75pUT+ynr/hOdyHYcu6Kr+59QEGVW+b86MDPZhED9+3xXlaKt97vw+0zmPE6l/qW+5OOwt1e7TvYn7xf5yW+HzVwXwvLN4w/bMcd7ZmTuj91XNyp5hucG++YfCFbP77IuZHPW7AtKf34bYzrvXE5/UbOgyPoc3cM0iFL/ejHraxjV9mLVvff/0+3Vn3qf51O8/tGiO7L5jYI0X2rPeX9Pr17+1GavnASza6tLHrfbHCwfmrHNhEtczn+ogEAAAAAAAAAAGAkgODuse1Hf7ey4aGftKTRV/aSHK1/4BQdO1FwkdQVJRPbdzw/mThdS4zb19oR2xV5bs/hbjp5oq8EO44T2ynW6R5SjMieJLTzuA0NfRcitPP8A5SP3QrPifg1wfbjfWW7v2t5FWmiteEqBNq0L0Cr7UKRXVvPvA25KjyH/foXVBKW5dswRpgOh8UlYVztxyRUqwK11zvjQb6OtA0PRW35GssiOEXHxbQblHFpaPrDAk3E98YVHjM3CvDBGEzXzHHwM0nU98ZAmtDPePAwJHwIIB9Loz3d0r/xjluqbviztEX39kJF997ePlrfWUUrL8xSTbb4H12xMz1Nh7/88TXnfOdXq/CXDQAAAAAAAAAAAOUGkTI2T25+oHHmY/ekltv+wEun6dUdPYVuIsT21lIcV277c5OJ+HX2rG8dDXLbS01gZLfcbBPOtfqaxlgYS4tkiZkiUS5y3Is8CHK1zdhomYSJKH8WvPnIteMjab9uf9zi5bgE+qiixU/VLHF9mSED3RgrE84rMjmLxLtosTJqdEx8rExSJnlclrq/SXxhVSkOh+VrZzo3lCeKJiyeKvfDteMzfBhjYmWM0TemgrCRfVEkVkadv8Ce/rl/4/ca077nvIeIBcXLHOg6Sb8QRVRTSpepOHRw5ZEvXd+Mv24AAAAAAAAAAAAoNxDcbWa1/XxDRc/poqNkRG77DquP7n+mYLOoENvXleKYcu88O52I/xpxEXBDktTmOFtZZD5tlGKkFH312yhieAEZ7RYvQECXjlfOXU8aa1wh1cIPWNk3lx8KcE0f5iNwk4ep5d5L8BoVbWNy2MMbXc8h14V4vW+lH2lRknBuyi43Fm0157onFVvNl+HOTEVII9nyXC3UygwPHYw3XmG59ayg8xDmxCtjkAfBePy5CuZr7elr/fd9b17at50nuq8opO0L+47R1q5cKvtlgwNEJ0+tffrhB+oJAAAAAAAAAAAAoIyc9YL7G1//0i0Nb7+QSpE9kdv+g/88UWjz20ontj9zviO2B852PwpDjuooseobiOtesdAg6ULXMgvIUo80idlGLoRaiJs9EPDzDCfxQYBl/9/ydsW9Sd0vNxZYLTdSnEisSz32+YssBPszpm25WjyV9Laac52p+zC53RVBWRsPSxTxSRXB48bgx8rkb2foW90/8y+uoR0zFFsNnfBxQrjpQYN+LThFC9Ga3ewspmiru1yI7n/bf993SyG6i1/wFCS6b3j7EO2pSKeWa8Wp440XPPCDNfgzDwAAAAAAAAAAgHJyVgvuWx74ZdP0jlfSiZJZ7IrtBRZJXbd+b1dJ8oVzbz99AXF+pfPGlRMVG7LzqmdklwwecXrzvJEswaaalGgQqyMieaECPk+OrckbHcMlcd1fZpFeIFXdnofNiEYiUiZur9EIGbeUqOrYVkXy5CgaU9xKNFaGgv5MwrlJeHZrokad5iYhOxIro/UTDkyLlYm00wVqHh2vEvkSjikaKxPj6o/ctJqArj90kPcnXTxG8W72vBEzzjKhdH+t/5clEd3X2S+3FtL2xy+8S6dqJqey3wmHOpd33Po/WggAAAAAAAAAAACgTJzVgvu8x/9tbVX3kaL7YTM4bXr7NG3fV1ARzPb1e7tWlOJ4cm8/JfKYr/SHFazwfce6H7ekcFUI113eSa5zv+CqMvHoNNRIGt/17g3MEcwtw8TjJt/NHhNrY+k59P6p0B3u0YtRVpgWPWLOaCdzvIvXziT4KiJ5nliZqHPdJJyTcdt88TTMKFDHtmPJ7eJEcnVf8riijnRu7IeZXPKSGM5YckSOHitDSqwMN37UI1E28vkgX3T/ev8vv1MK0f0W+2VdvnbH+nL007cOpvMHrreHJnTuWrvv9i8jWgYAAAAAAAAAAABl4awV3J0omR3txUfJVBHtrOgtNLfdLyKYOrm3nmy0X6723oYRMqFDXI6VKX3xVNnc7WjcuvOb1OKkxtz0vFkvlOxoN2wr6eTmmJo417tWzFWPrLH0/Hh5R7rWPmJie1zxU80ZbirmqQjZ/m1m2JYoX6yMWjxVE+bVV22ZEkMTL1qrxxofbSPN89h2WmxLNBpG3Q+Ldfrz2HFFY2VIK9pqcvDrhVfDsUYfKiRFzMhjIMnp/o0Sie7iYWNbvnavvddNbYd6UtnnhK5D9RVvvr6aAAAAAAAAAAAAAMrAWSm4b97Y2li/543VaQiffRfk6AebjhfStJvcIqndaR+PKrZzuSAqKcsU13sZYmX0aBXvfPPA5U6qUB1uRMkiukVqMdQ8kTCeIO671xOF88h2nsM+rjBroW57ZWw0kgZ3Q6wKUVRMJ4rEygTba4K8KtZGhWVzDrveH5nPSmLR1jzithwrY4in0cRvFpeVrkXEaPs2udAjhWVZnGOeseSsdf08MNP+jLFA0fMT69pnava8ex5Epvs3++69fX4J7sEbyX34mMjGg/3U2ZfOB6X62MGVHauXNeNPPgAAAAAAAAAAAErNWSm4z3+mdW3tsQNF98MaON318knq6SlIFFq6fm9Xe9rHknvz8Xmc8w+SOZudhQK8HCtjEOBLgR4ho2ecR0y3BhFdeZs/OkaNhFE3ifaVIOrzGLc91wR2fTtNp+XyebD4CCfJyIIx6Q5p+ZaQRHBmcmlTKConxb+Ybi9TrEyMcJ6clZ7cDzMK1BTXTojtavFUiisyGh0Xi3XHmwqdxrrsySTAK2OId9YzxcluzLePGa+e+65sL0T3b6QtunsPHYXTPfHhY/+ZM3RXZw/xyurid2pZVHewc+3mja2IlgEAAAAAAAAAAEBJOesE92fXf29l/a5Xm9Po65nOAXrl9YJiD1aUQmwffOPxBvvlGtJ9yeorRWNltPWlQhO3uSRWc6OATjGFUGVHe8yuIvo4z1McNWbAstteyWinMBon0pfBcW90uZPm5i8jXH+6EYmNiUbDUIJYzSKxNP62mmit5LrHxMpE+o6OK2/cCiVExxiFeuNxMnNWenjeIuOOi5WJtmOM8dhtWYEPJ0zRN8q1MMTTRHPhzW56wwMZe8x19tJv9qcvuovvwxvztdt/5Dj9+8HBVPZZeeS9xivu+/ZK/NkHAAAAAAAAAABAKTmrBHfhbpzzzH2rs/29Rfd1dJDTPW92FdJ03fq9XevSPpbB1x9rIOIf5Z5z3Y1pITmn3Y+NUZcpQRScsVLJ7oFYzlXh3V/JEyJhlE544j5ihfZI2zzFVP3CrJEYGX8YcaK6ZS78yr1rosfqBGMp993va6qGZy4skqMubUDJGehMF2t1IV7v2+iKzyOcm7LLk3Lb5XnleBPc5UIQN4jypsgXUxFUlhD5kvRwQe/HnK9uEvQjRWeZMRYoyc0eLcBqejjB3Uz3v+trvS1t0b2NXKd7Ik9u76S3+9L5U5U5eWL1jq99oYkAAAAAAAAAAAAASsRZJbifs6t9bd2hvcVHCjBGd+86Tj05K1/LNq9IYKoMvv6ocLZ/jLgo2epobnmy2bVYGXlZKZFc4TwSL2OIPQ8my5t48mQUz0kVvi0pysUUKeOsJ3PMjCmzPS6ORlmuZcDr4xrhEHcyiunG/HT/1dyOxa1n6rIkcZwZ42fMwnSkP1Nh1bhYmcS4GKmCAIvJrac4kZyiDxAC0ZobHhZwQ366fMx5RH6Ki98hdRtmKJga52aPzcKXxyVE91V9rbfVpXkjeg8j1+Vr97O3DtIZli3+q7u/n6Ztf3kN/vQDAAAAAAAAAACgVFScLQf6+H2/aJ77s79rSaOvtsM9tKMrb5RMBxUQmTBUBl9rEyL7x4l4JRGzXOHcEccY54FlXfzjLHMEXiGocfLNx0xyIbNP/t5NP+g+2lXr9889gllHLZe86Za6niINubW459Ai6tv/56rA7KnastCd8YfqDycfyW53JQqmYCQXvrKYD2F7kp8jhNvLsTicR7q84o/+rGz3f1VVVXDNw4GLm4Vry7x7h7mp5so2wV0l7if/PuJcud/kfph8ycQ96t2rzqzYA/N/meGLvMwRlEXfwT7k/ph/EhnJnYdtpTH4+wuO0xWrnYdOzL/gcj/egwLuDyu+nencBPPBfkk6P8E2yjG7p59Jxxf8VCVE6i8878E5VPcXjIEi11U+n+F55OEtIJ9jcvv0zpdwznvDEg73/9u3Yc3/V33jqtNp3ZvioeSyuQ3CdR7rPD/WO0gbD+fov00vfn/szJnm91Z9evl5a+5bh/8EAAAAAAAAAAAAQNqcNYL7rMc3rE0nSoZoU8fxQpre6BUHTI3B17Z6YrvjbA+ETkccDJ2vnoAXCPHqMs584dFZ+ls3/dddrsM1cAi7yri/jDnzVtCNsl520oaO2t67v0uDL+5XxOZAgLak+HD3MUH+A48TvyMBIUMU2rk8DhazH564iMvxMLJ2K2XUK+5+yW3fMG9B+T7oFeFHXRJ9w0H7x28S0537xRWjVSE7EOJdIVvR72Uh3he8ZRFdEYpJ6cconCtCtiQI++OSro4uZCt9mwTssB2T2zHO1AcMinAtPwRQ18njUh46BI9gmHY3JT1giIrt8eeBPHHcX+atl+9tk0Dvz8vnJnq+vL6F6P4te0r7adFSe3rXnmJ/gfTEzgN0WcMFdFG2v+idVXUdXvP2hh+2XnTj/+wmAAAAAAAAAAAAgBQ5KyJlXvuHr9wydc9rjcWfrQzdvbOrkCiZkhRJJU6/Zf87VclmDwI55GWm4qjcECejLNPaS+s5Ny2jyP78cTFJw5Od3pYhWiZpio2DITUGJimiJTazXS6QqmXKx0TJyFE2/rwi9ivbSBk5FpcKx3pjHxEMhU7laJVItjmjmNq6hcfKRCsHUCSuJHx4o+4jb0yMNh5j/IypyGjSGKRYGdNY43LWleKpyjFzQ6FTrp53JQtePXZT7AwzRtHI2fvmgqnm8SdEzLBoPrzbfn7fhjV/nuad6T2czPuLoJ+8e7qQ79/8X+WnTtTXP3D3avwnAAAAAAAAAAAAANJm3AvuolDq1J3tN7PBwaL7att/knZ053XJl6ZI6rYtHyYhtucRzqVEimiuO1faSlJoJM+dSdsUJsqTQeSXhGpVHJfFbYrPVzetVAR7K79gr/fpb0MJojo3T965VMemu/e1LHmuPzwYofx2blnnmxYnLpPuoLAAqL9OEcl1wVjvWxdwA8Fcq9rLvV9RxIxLF5mZXJjUkHvODMVbCyusyuLbRQuhstgMd3mb4Nyw+IKpFD0G/ZowQ8FUeX/SxWOxRVK54SGGJspHH2xwVaj/WN893/6TNO9Rr4jqqqQ2J44eofsP51LZX9XJ7pW7/uImFFAFAAAAAAAAAABAqox7wf38V7esqT2yr+hCqcf6Ldp04Ey+ZsLVvirtYxh89ZFriNNCMhdHdRU9R87TxXST051CEV3346r9quuiAjwzRrDo20vRKqrjXXKXkyygW3nc7txctFTZl5Ydr4jsmlPdJKqb+tIF84jwn+CQD9zwsqu+vFicn0+S8MxMjmZ/mVrI1OD2ZvnEXWlbrShqXLHRqHPdJJyTcVtKdLbzaN+y8z7ajiW3ixPJ1X3J44p3pCf92oC0ZZpIbjo3bltmcserjnrTedKuF3FTcVrtYUFL3z3f/nia9+n6vV232S+tSW0e232E3hqoLHpfbHCA6o92oIAqAAAAAAAAAAAAUmVcC+5P3/vTpumvPbE8jb7+s/MU9fQnuuRFJMKK1HPbX3l4kf1ysVSc0+wkjyzjch5IKMCHflVZlPfn5T5Moj0ZlkWFel+4lR3e3nuvtKoh6zxOQKeoMB/nfNdNwVyNfPFF9mS0vvTYmohjXgl0V9zz3Oi897T5kSXqFDdFjqixMvHicnDlmWFb5S40RrUwxRWvCfOMmeJPZCFbPSaTaK0ea0xcjNqPE+DOYp3y5lgWMsTEsFinP48dVzRWhpTxGSJfJGFdGp+hn/wRM+pnWBoXMzvk/7zvnm9flvL9uYLcotOxtO7uplxVTfF/AE+ebN77Vze1EAAAAAAAAAAAAEBKjGvBffYzG9ekUSh12/F+2nb4dL5mq9LObR9s3zzNflkSdZebY2U4993uisYXn8Nuip2RUfdbgChv7Fd9DbowieTSNgmxL/niZ7goVGr58/5k6N+U7R5xtFt5ompIFd4Dgd60Haku/fJjOuGGWBWiqJhOFImVCbb3hV09biVGWDbnsOv9kfnmMDnFjRnuJhFcah8b5UKUnOEenkdj7jmZXOikn5vYWBlTNI7xIYLhHDIyjcE0Rn1fZMiM5yaHPjdH3dzSd88/XpjWTeo9tFyR1Gb/0RO06XB/KvubuHc7XO4AAAAAAAAAAABIjXEruG/756+31HXuaC62n54cp3v2nMzXrDXt3PbB9l9V2S+flhZFxXYeFbiVWBlTNjtp4r0p1111x1Nkf9wgqBvg/nrf2c3l2BiKOsUVMTshx52iWekkFTL1B8ktk6BuxfflCOKk7ldprue36xE4XgfOWCh6TNaICe3e1VUvsxorQ7pDWm4rieDM5NKm6Hplx2ZRX29vKpJqEurlbZnRIV5YsdWEuBjmBCaxmNx6Ss5wZ7Eu+vyxMup8VIBXxhDvrGeKk92Ybx8zXj33PTpuFv21QZ09faXvnn+cmNat6uW535rUpm3PMTrWV3yeO+vpbdzzv3/nFvznAAAAAAAAAAAAANKgYrwe2KTdb67J9pwqup+2w710rGcgqUkH5XFjDpXB9oeqyRXbqxyBNhBBhbbFXBnM0bmYt8BNaREpGMoyF38+XOa/o6C91C5YKS9zdXzFkewuUPoWgryuqcoZ7pJTnXPPIMzlodIQ88396BZpyLqb3Vtm2jQyL5/mxG0THjLIjn7PJc8jGfZ8JD8a3HhPOFdDPZPhemeds6z3yf9yE+87fJPWZ5gdw+T3wSwzNJdvMi8sPXLvef8ww4Mjpu07eMtixxZV/Fn+7SLrWEzfMesSx8C0e9C0TVyfyfvjQxwDL2T8pNZwCNeK+as4nVlyesOC99K8Wd/ZkfjdS2yS/Sdsi717q7icpkmHOm4+9e+LL2CVg41j5E/cqrobd7QTAAAAAAAAAAAARh3jUnB/9Z++vrx+y08bi+3naI7owT3H8zVLPbedOP0aMZruCqCeIO6I2Yy7YrsuS4fiN/fWu4I2CwVz0Y/bBynLXHHO0zu9fSj7k5blE+V1d7McHWNwswsNmrHCToiqUXPlJSKSB4vziOU8rj8qsD8eXawcq6U43LkcRVMgy5evYK6IzyMi6s//7efzcrlco6dHM1mXZjLig15RcVwbeKBlhxFEsr7OSbr+/pMERrnTc8jquy7d231468Coo8GeFqfZ4aIF+Yqj9tGZuhlUebKrqP2w3t56OmYtpxlj5lzX43YDAAAAAAAAAABGJ+NOcN+8sbV+ypP/nkom70+3H8vX5FYv+iA1Bl9+UAhWi41iu8lxrojk5MdFMx7dVsIX4GXxPtEdT5pozxWXfejCl+Jo5ExzMmem+0M2qe5+cVXFcE2xorc+q/dl3sS0PLmtUfgnphZo1eNuSC8cO5w7g0XG9t9vuqnD7m930CCIEOJy3n/G27fvPWehkV05OCY9epHuJnUMh/ZcQQ2vPRYOKVjDzIkxQz3EssPM+2asgHExir/54jbihS/m+W7sBFJ5UsHLvL/8x2/6+BUrtvvkXswSVWXxXwUAAAAAAAAAAAAoinEnuM98+6mVU7a3F+3+e/XEAO04kViUr3393q5b0hz74EubphOnjxELxHaD+K2I0JJI7kfMhMuEzd13P5O/YeBcJjlWhkiJEDE52InM7no5C8Qfg1yf1Y99cR8BuK+Wm42eyXhD4tIQSEs28aIiksRHHrM86KuA5Vof0c1inPFcH7e/yjJk08sifOEELnRmPMzwZPmO9Lj10WXaE42EWBl3ENR3poH4oYz7YIcxdxOnMoBBoDYlrRhDUbwfXjAt9UReFp4Mcz/DxThuJr2wyF2eiN6GxzTiUo2D4HMS8xq3H/KzpOJvVfNCZlauE7cvZH2B0VB8uNvJn0+e/7M/RHjXiDzxAQAAAAAAAAAAwDhjXAnuwt3esPGOm9Poa8PuE/mapJvb/uID1cTphiCHPRC3pagWk7tcxQ2GUYVxU+SLwSkfbB3GzuSLlREqK6OIXVqRa/3JqSfqCc8ZqQ3pwhmTuuNRYbCQfPZ86xPERh6JleHKcCLbKOPWOuF+fjtXzwUN27Osyb7+AxaKDkJ2qwc6urLN0GJlvJvDb8Iyntie8SYmRY4zJYCdIvdF8N5rKB68OP1k7H7F8oy3jkVEcGMGUVyceyEYI+bDfbGiNViuiuM8vGeYf+9Y/qsV5v6L+ZyV99cZzHwbFy5E5xG4eb62ygMnFnvsyZ8/Fr3DTR9KFrYLb2n/Wd84Dh/CcwAAAAAAAAAAAGBMMa4E99ntj6ysO7KvaHf7pv2n8xVKFVEy6Ras43Q9MZpMRMliu8lxrojkJMfKeMuMgrkkwPvzyjKS1EhzrIy6zM+GUYXfSJyM73L3BEbH5S5jUawqXYioxpPcugkO9qTtdPEyyKbXx+3P+gVTpXnJ6T8ctT0Uxbm+wnAwyg8jIgK7amTnqpDPGJciaAwOeGm/WeaK4xnmCfAUEd5Jd4kTqaK5ENmzdh/+JO4HlnH784V8vc/EWqWFnlDDtZad9VIjxelO3HybJLm+fb1YjhaypAdKluXVNOCuyG6/Z+J1MGcvtvJ8DpLqFzDjcIz3NTefJJb0meNhO25sx9SYJeMxMHN5Z+W4mLqNV2zZ/6FO2M84Fd358G9zAAAAAAAAAAAAlJ9xI7gLd/vkX/1r0e72MyxDjx44ndQk/SiZF+6/3H650FXdmC62S1EtJnd5snDuFU8lUoR6Sa1ikez2MHYmRI6ikXPfySzKe3DZpa7GqzgdCa3d4gk2XcojoEsL8lhxuVEkj9mH0k1C3EVkvRwPwl2x1NIjQoq8WdwzbQouicbKSOVwKT5WJv8yV6BnSuwP89R0IbZnJbc7Mwjv8iv5UTTeGyGwV2SdiWWzoejuud7D2BpvfzGieDpipO5oT4izJ3235gsbPmDh0XvE8h7AWJ7ILu6VrEV8MGd3nHMfcVjq1eFyelPw8TeMwZgsZBgqZ/oO1PtZPiFce5W+IlikHVc1f8a0zwszf4aDbxLpRxXBHcw00V376hnvcILoDgAAAAAAAAAAjAHGjeA+8+2nV07cv7Nod/uGnV3Uk7OSmqQbJfP8xnPsl2alSKqxqKmH7GZPcMC7Ojv3gysSYmVIcsqTGkoSdceH66ICPPP2qrb0xWaLwizzjC++i7aWtDui2Kx0bVXMgshqLouIBedWc3PbWLeu1sZ3K1thA66L7sPTCE2FTHlYCFWJm1F34oqV/u8fJNe7q4QqsTJhjJB2rL6m7+0icLZr0TJaHIxbUNUgljMWCO6sosIV3rNZ1/Huie6MZTwBP6MJ+fI9Y7h3CqlZGmnDhnE5klZxLfSJK7/44L7I7nwucsQddzsLPsBCfHfzl2SXvHqVuekBQVDOIem4ueE20c+jJqKbEqxM0TWSA52I5PtK+rFFZIPk85ukq8sPX/hZIr4DAAAAAAAAAABg1DIuBHcnu/2+4rPbjw1weu5IT1KTVKNkBp+/r9p++YyWzc49IUt2sIvmepQLqe0cTP5nWTiXNgwy3LlUPFXdn+pgJ4qK9v5iXcjXjN9uoVRuiagQrjjAWcShbHKCJ+Ssx7SJLzjJ87QxCP6RIqnyQPQHDDxaLDVHodjuufuHQ1g8lScXT41uySOWZ1VM1/LhI1EzRCbJk8lTVGwPhXY9i1265o5o70fK+C53VXSX+2WRIqcjZflNjgbicREq0r0hHiaIj5/jcPc/Rr6rPOIY1368wtWPTVT3ZhSNZeHR9cYryyjeEW+aN6jhcveMaR8zltBpwr54Ae3KdOlHBLjbAQAAAAAAAACAMcG4ENxnvbp1Zd3R4rPb7+k4nrQ69SgZmw8R8SlapIsqtvtFTeWImYi73Bwr49W65FKsDCXmsJtiZ+R55cGAN1KjKK8RuLr9iBkxiWzqTOCGDezgcTERnBcutBe0LE9UTcS1rh9MfD9cF939orHFudtNBx6TcxITKxNVPuPyPZjBIe/3aRg9i7xVXe3eVhlZbGeqWC8J70zOc3cKqWaiIrTySwqWfE+kTQEFSZl+f0j1C9wCqW5eTHBC/R8UyOdMud24FNUS8yApVsMu8N7n3LDfmG1M+wyaFvLQyvCrlbhxxHw+OS/TJR8tpnlkuQMAAAAAAAAAAGOCMS+4p5XdvuPkAG3r6o1dzxhblea4B5775ULO+VV2v1J+jVFgJ7Or3BATYyis6lnY47PZ/awIJVbGIMCbVHBVgPfHoAU6U+j0DoR2RqHx3tNOlYhr+ZRQfqd63GJeQCa8aZ1JJEzaVtff5QcMctFYi6ch3nEpENu/NIXFyoSjZeo2sh7vLfCFeiWKxtCTvkv/mYrnwmYklyLgWi9+LrucAS9nuMtiuxYpo0TTkMGhnTKySBwpW5C0kfSAgXufRPExz7jxMSIxhvGM/d77KFvaMflCPUkPb0w7jSvsG/dx4bywz0xStBKZnPyG/fOEz1jSeE3HKufDmzpiJYiVOYti4gEAAAAAAAAAAFA8Y15wn93+yMpJKWS3bzpwJmn1bXfuOdaW1pgHnr13gv3ySQo9vglxMVrBVDXDPaEd5RPOZUc9qbEycjtFa5WjaEjNl5ejaCQCodl1tIsXN1bGExydWA2SUkzkbU1nL0ZoLDifPWE/RoFPd7THdKOLiv5mlpfnLjtyhyDeqb9OkGNlKDlWRna5uwu49PMB0zbyheaSchmpHKrMCCyvez9y3ArvbLeKgBQXz/QRcCnbnbmFdIP4mEyY8x4UaTUdAqWksSc8EGGmeyauYKv0EWIUfSjhP6Hyz0eGSQ+hpKvh3DNe4V2/i3wPkXie+38on7HIvc7zf55iH1blOc+Ku51R0gOFxLGUCpbvnJVx/wAAAAAAAAAAABjVjGnBXbjbJ25ZX7S7fXuPRTu6Y7Pbu+3p1pSH/inifELoRpeFcc1xLjBFx5jc5QGh+M19H7Mr3KqueHMBVimEJCFWxiVOlPeGoU9etorj5PXEU11oL8SdGiu4JQmNpm1iBFZZGTdFWSTsUnG1y452SyocOzRM0rpBJDfktIc9+Fc0zgEc5gLJxVNJrqsq/TpCOkbxMIEFIrt/G6kFUjmTXN6eaT5wtFtMe6ihOdjliJU4sZ0oBVczT77PhtWlQYjWY3GYepOGqT9etntOiiYy3YA8j/Ac6xI33dcJnyme57zEPkyKi7pJGrf5+6Awob0MajiLfNOZh2EK2Up63JX0iacC9gkAAAAAAAAAAIBRwZgW3Oe8sqVl8p63ina3P7i7O2n1qjv3HOtOa8wDz7QuIs4XkRcloziYIyJ6TMRMJDpGaqeI5OTplkGsDGmCuYcvwMtOeW7KApFzNNQseNVlr+EVSBWZ7YH72RdtrWhCirRd5G0+YdXkeM8rFiYJfHHDiRFo4+Jk9IcPRWMqVMkoOVYmEOVjY2WkI2GKUK9nxnPvAYL9j2Ok515BUyZFwEuiezC+IKdciO0ZdxshLOvnRRc1GYu/J4YNT1jEi+iSFzA+rhyrn98e5P9bosiwENwt79chFOMk53lu0QKKn8oLhivc5/sVCQ2h38jmCWPL55xPC/l+JhqS8M0Kv/vyH18q9z0AAAAAAAAAAABKyZgW3OuO7F1dbB/bT+dox4n+uNVtd+45ti6t8Q48s2ECEb8hUG6CAHNT5ItikUyIholrFxXOhc2dqfuTC7DKylA0doYi68jsrldDQ7hbGzIsHCoiVjKe+O4vi8SMaPBC8toN7ZIUc84Li4eJbEfJY1HEdi3T3ZKyp4cg2KUXK6Otjy7LGyvD5QcKVnhcoimXM9YVwZxJ0euScz3rxQkN2vdDpRVehIjIXuD9MBSK7SNfdEvcDRLjmud6UeGcJ7YP5rxIIh4Tn0QFCNExC/kQD5jnP7x8Czkf5ph4Iee11LDg4chQP8MxvRV4n4VbcE7pZ9QDAAAAAAAAAAAgVcas4P7M3d9fPvH+HzYW28+D+08lrV6V8rA/bU8TvHm9eGm0SKrsGs8bK2N0l7uBLaowHi22anTKB1uHsTP5YmWERZfFqGyWF5HhxMnwcNcZaajMsJ1+phKdr7wA5y7XarIOQUw3vTet41yd/Bx354FDnn2ZGU6sjP4IJSZWRne954mV8SpWcj9CxrnkUtMYGZEz7TqLKZtxW1dVejnlebI3rDIIjQVFGiUV3i3g5om9n6TiwuI1lyPen3OFd6VtGg8LhtaID6uPIgdZaMxK/G1XsluExT2KLNF1Ueo/QG8HAAAAAAAAAABGNWNWcJ/10ubPZ/p7i+rDyW7vis1uF4VS29Ma78DT9zQS8cWB31sSspNjZSg5w93kOFdEcpJjZYgiBVpNArw/ryyjvLEy4bJwEy6l0viCYsbL/mYkab0acmSGUWTKY+uVI699E7/ST4GueeN8wraBm52CYqnh+IevlMW63ONjZeSFFB8rQ5qRnatCfnDi3GX/sWkLWfuPDv+DIImSs+sq6Xc/cA6xCTnvYYyeVc7D24mnlKWeRrvIpeSF98XD6xker+pgd6JkvDgZ3j9I33ntCP5SFcFHFpxHTdUDRfVxxr4P/+V1XAcAAAAAAAAAAADkZ0wK7ttu/+vmupcebi62nwf3noxblWqh1IGn/lO42n87lDidGVfUDMVxKQbG5Bo3xcUktiOTcC4Jt1KB1kDkF2/1+BqmFMx0kaNoJFd7oO6rhVN987yIa89QIDS68e3qYYTbUGEidUyMDNcEcp4vAiQxUkZanrSOkepsF1hy9AqFwmpajlzvLtJ6NMfKSOVwKT5WJnFZ54FDdPpEX3of5hwPxHb/AQVLK5d7qIU1h9I+6cFNjNjOC3K/q7+MEGxP83yfhRzZeZQ+cM0FVHmya9h91No35eTpDfTirvdwQgEAAAAAAAAAAJBIZiwOuvbogc9nT58sqg8nu7071t1+a5qFUm2uJc4bKIiO8VDztTWxU17nzXODTOv2wSLb/P/svWuQJUd2Hnaye2bwWGAx2CeWr+3VPiQvRWJ2SYq0aBq9Eh0M2bQwa8MK8w/RE5J/+QeAUIRCtsIawBFLM6xwAFCEFTZNCT2wpeUDIQzkJUUJotGwl5KW4nIHuytwCQKLBkBg8ZqZBjDvma7jW7cys87JPFl17626VXXvPR9Qc6vrkZmVmfX68qvvlGlbGb132RbyCpa5fFBYH3uCF+tQKiPh4UMiMQwq6tTgGdk+NmmHiJSk6VuPdMyABy+NygJyEFOJUPfe8xAPCIT7ZLyInmzPkHu7T4OSpZXqXUjNIPE+x2T3MoasN+TIHE9v6LZI9mkXSILngmu8jCi/YcopUI6HEwtgi9XbowtYivK2QR9Gax0kbY4o9V/pvAA7QNSRhc4KYO+98/CH332tcTo/fYvRylQoFAqFQqFQKBQKhUJRi4Uj3P/Vbz2xcfMrf7zVNJ3ffztJtu+eePnMQ22V9+rvPXbb6GeTLApDhDpfmXI5I689UUxJdYH8Drcr064gzgMCHokDO8b5AQrHgGnynqrKM4FoB+dtjgHJ7n03ODnpyWvkJCf5LdMIyFJKmvv5FIlKCNCMLM+QW+NkFfuTgQNHvjelTzGQSBekuEnapqdTMvE6TqYHQwuEnG+bA7Zke8GzZwERDTAV2444IQlfQaSHgzll5QvEupvo5mHmJC+pz5ByYUjiK1rDkxeua5zGp683cNsHD2tlKhQKhUKhUCgUCoVCoajEwhHuH3n9T7YONbAGyHF6H+D330gGS32g5SL/p4zc5lYUEnEeq9CREeIm2i5Sl0vpoUH05D5E62vLIKjneb6ElE8oQRNKd3SydEpGZplMrLtFmRNFC0r3KK2EkjnF3VJynqruAdK2MgHv64OzSkr66TCprDadOrUDMuRIYsV6SuMP84tKaQOwjhsUYh93qKm3pEI9ExoSJ//KIOirMQ9f8akEG7ABod8Fn2FgiojXG1RbePP1N+CPzE2N09n8/pu1MhUKhUKhUCgUCoVCoVBUYuEI9w/+wZN3N03jd15Nku07J14+s91WWa9+9Tc/B4gbEKnKZZU6RnYtIsEOlaryCluZYkYg6lkaAXmPLM0KCxyaHw9eWgqLs1LR7tTqGSU007YcsgVIoBzOMu6ZntpPmvL99ivIecnOJlQtZ1RNHw4EwOyke62tTNSMk9nKsPV0H2Yrg663pJNoAO/xD4HViqv+RL1LKnRW2ZMtltKMBndEkh3kPsGCvwaEP+tT4I8VUV6Pyri3in/x/JuN0/jJD98Ah268UStToVAoFAqFQqFQKBQKRRILRbj/m0f/wdEbz35vo0kaF/cRvnXmUmp1e4FSv/ob1wPgX/YLRL/2iEidwC4mIL9RsHwRtxv/U0WcmzC7gNiPyXSQlPcQFZsLj600PaO2HhmRrAfTePn+aH4/2C8jATez0LgdOIEJkyvYpfLT/cO/Q7KW2oEgtczBJip30o0kWxmot5UxoY2MwZqjDo7WAMxLc50JRDT1cRc7UmogBJinf8rGBceDPxmxhSkn2cc9CwZ2smCwJSNke2hnlFCwZ4KivXIwQdEE3337HfjTD368URoHLpyDn/zo+7QyFQqFQqFQKBQKhUKhUCSxUIT7R5/9vbvNtWuN0th56yJcvCKmkavbd1os7k+PJhso1YN6s0vEuf1bmMeExYxfVhFYlSjqA1sZno4cgJWYkFTYytBjgcAVxKl4XWBTqggfE5VQQ0gCJ8WdTUegpI9I9SpyvSpgahgIlaVVHoNT4ntelhKm+yg4msxMolZ/WSCS5KbCYoYR8VCRHg+eOv4oYl6ya1tf1Mvc2c1kGBDe1Pdf6jtZ4LFOp2w81X65kLQiyuT9oMKqCEBIu0gHRS/5VvqMQsDTf/Ctxmnc8fEPakUqFAqFQqFQKBQKhUKhSGJhCPcnv3Jy432v7x5tlIgx8PtvdaBu//9+/VZAzAn3gsDGlF+7aCtTrpW82XES6xiyHSPJ3UYoe7OXC4WBATYIwMvP9onz86RhJil+gaiBIbbejsj4jLLbMTGaVSubk+RqKq2gvF5MT44xHFzgnu+WOHbrZrVkqSVejdjfg5YItjNhcNTIVoatH6vk50AAe2I6CJialXUn8tJR8NLMThKZnslkdlJ9LgVWzSoIeZDJ/yyMR1B+jYEZ/TIi+EJCVe5zwdfePA/v3NAs8OlHL78Ln/jIrVqZCoVCoVAoFAqFQqFQKEQsDOH+oef/cOu6c82Cpf7+WxfgzMUr0qrtltXtPzuabihmZb92Qpy7JYYozGusXKqsYRDEwKoCcY4oWNqUJD/EaUOFdzwI6noMVONOxUwU76GVRhRYMgwuSdKLiHmoDmoqqdtTKvgsUTyaXnhszMLGkahAFPnQCl/dnq1MskQJW5k5IWmpkvDRTwZFhdjqJ0ukHQ7aCIR42q4G5ADAWSYP5PhzwXq2Z5xsTw/66A1qHvjqq2cap/EXP3KdVqRCoVAoFAqFQqFQKBQKEQtDuH/gxW82Dpb6tbNXUqvaU7f/v1/+PkD88fEfXK3u5mTrmEj5LgRJRcliJqVWTwdWRRSJenkZC54K3HYmYStjuPI99jcXSVBIW2tQXhUgbQtDsqzcFyBtKUOKApNaiPv6QW4DEtqCSDT25GhqKxMOoSRsZUyUDrOVmQcJTNofawOjBo2EiSC3tUFvIfHVQ0XfijzZQSbWg0EjzIjFDbXCgaoAvKCE+5zwb85ca5zGT916CK6/4XqtTIVCoVAoFAqFQqFQKBQRFoJwf/IrJzdveuvljSZpnLm8D8+fvSitytXtuy0W96/a34QnO4QWMBGRWm0rAzCZrQzZjpHkbqOE8l4k4FFaBlF+IcmfIqu9PYtAsFep1iNPdJIwVpCXkS0MtfCwE5CgmSk/+ZQdDQbe3eHxgj3mpiD9Qla5V9rKxE1czMdEPbeVCdbPy8Kd1i+tQywV5xiOhghppKxY2C6CIl4ulGwnUxUQgAwqoWgRkxgUCI+zjQi7ChHvnj4NX792Q7NErl2DH/3wjVqZCoVCoVAoFAqFQqFQKCIsBOG+8a9P3r1+5VKjNP75q+dSq9pTtz/9Tz4JCJ8ULF9StjJ0JiTHY6K+1i6mcjuQiHNuKxMo1jHMx/7Gvu+mMmgrHT/w/upQkJL7NsDovuCZXkVwI00HSv5zH4htDedmSwIU0mrqLOH1XmlvEljHOMuQLPBvbw4z4RaYbAFqK2MqNfeTLmuG0E/de95XqNWTgyuQtn0JR0AkAj70cA892nmBeScjAzl0QEcm/1N9GmKVvWIu+NpbFxun8dM/8CGtSIVCoVAoFAqFQqFQKBQRDgy9gE9+5eTh6/7lP2oULPVihvCts5elVe2q2xF+DkxOPBscE9DGFGyr8+Qo5vM5U87b7Yv1hhCi45Uk8XI7N5+T2+NFhkqfjc0DE2mP081tZUyRBMZ5BfmP8xmLwCnlW6znZbZrBFk1lkUfl2qfFBlCUTWxra9xE68SJ7s8MSJXg1np6LO6DAQ4gXKk0G+ROaV9SCbWg7o3tvUwWB/snqdZsNy2z6DdYLxi3KT56j/7F37qn7zx0u5X19bWjG1qs7a+bgqsFXuPZ0f/r42Xg99ubbzQ7/fj5p0fBnjvf/DtNO6N4VcFUAwLYqIxKjvAhNthYt+oz2GcFNaVCaP9+fkQfukB9kQr8fm/8d+u5M0pu3p199SJXznWQlJ3jqZ76YLvvPw9+N5nboePvffmzIn+mbWL8Of+3KeOfec7z+/2UD2n9PFFoVAoFAqFQqFQKBSKYWLwhPsHdr919P2vPX+4SRrffPcaXNzPpFUn2irn1Z1//KnRzyfB8oZWPUxYTU9+O7Kd0r2U/bSEOBrLVhLyfkywYyFG93n4fTj5bbMXt4MUcU52tIQ9GqtZR0LEekIfOEPuSH6aCsnMkeA0NGxB9JaHhoalRkuOSAtMtpGY80nDflYFWZ0UkVIauOp+TmplO2pSNqQlxYtmEkuZUMgbjOyAjKFksm/Ne3/lkZdHvy8DZsb35eJ3zRbKfikxLsOajQMAPnjweLsxuWwuPPi3zP5zp2Q/9tz3fNzj1os6XAsHSnCKdpuMFEexv6Q/FuAxGoRl4j6sAeXgqVTpPsKtn/jUqt6fNv7ylx7c3do6ttskkV/8wVvz/e8Nl//BK6fhP29yZ9nfh7+xcePGR393b1sfJRQKhUKhUCgUCoVCoVA4DN5S5vAr37mzaRq//7ZoH7Bz4uUzOy0W9edYkFEeELX4lQJfpm1lIEqn3DQOaioFYK2xlXHBU7kfOE5YBsE/nvrIU94fYxsOhNBCA0sRNrEBQYjtXJD+LcUFnYSIrSLfpyXHTZguVe8HgVSbw0y4XTrDlK1M/GECpmvOyGnSNFzakbO/CdIO2px64tP2z4KAu2GbYU3xIy/1bDSbld79GHi209GSsYd7lg6aKtnHJAO0Fmkh9YWPyqA3J4vjTRN49JWzu6Of7XD510b3has339rsBmrwbm0ihUKhUCgUCoVCoVAoFBSDJtxzO5mb3n65kZ3MmWsAz5+5IK1qz7v9qf/rUwBIZKiYCkZKyXfi1y4HMMXIK10k2CFSJ5f5QV1gVSMR9SyNgLyXfd3DecuREp/rzPBYk2OvdeLDbpez+JiBTbYYVHWfLNsnf4fzqf0r4l8mp0z4O3OBMiEO7Fmnsq9EwgNcqnO/Q9SMlEzHVJPx9XQfbwtTrvdkupEKScpgkPzyvDEYlCEBdSMyO6sIVhv56Wc8yGlObmclsV5OwDsYpv3Z446I3G9dyp92uFQeUVpYji8p655ja3v7kY0W0omu93nw1H//+l6jRA++c3rjuYf+5hFtJoVCoVAoFAqFQqFQKBQOgybcv//U7x697p3TjdJ4+rX3pMWn2lW341+BkOBGIWAqV9sKZHVEpIZEPN8OhYCpE29HyioT51Jw16oyk2XCIQdBRYvAosDI6vGCjNhsZAHRGgZSzWiAy6wkOum8ywRrJhCmcJssSI/mS0laRuQHhG3LQEzY58s6eDl4arGgzlwnGDYwwjLX2wRy3wT5BkQ9+rqCIBBpVl9vpD+gHfRAhJhYB6gOwisGYUVZqY4pcj89IRJFfUYCs7IJVOUeY24q9z/Ibmp4AgLc8PILqnJXKBQKhUKhUCgUCoVC4TFowv3WN19sbCfzzfeuSYsfbquMV/6f//PTo59PE4I9sHzBMmBq2lYmRZzbvwU1eyo/v0zYjqdh7WSYrUwwaMDIe6KED/Jg+SEn6qNgpchU4ZiFBCdUK8737bb7NfuFZchmmCS7koR9ScG7clIWw7LNAqz6eiGaFyrbb1ZhMcOIeKhIL4iHa/g+nsQXlPC8DEz6j/78kNTsQKxW4kEQplyHCUhwgHpyPKGSL/+uG8CJyXWkavgssK7JiI0O0OCpCou2VO5RzI5vPvscnFu/rlGiN55946g2kUKhUCgUCoVCoVAoFAqHwRLuuZ3Mobdfa0RkPH/+Gpy9cCVcvHvi5TPbbZXTAP4VFBXgmFKNS7YyfDtuK0OSFLzZcRLrGLJdSIgDs5VJELwoDAywAYHwOOOAkmgJabdOIrSpupftG3h3S67i4fI2ApdOmg8mtkGQvb2nRqWtTLJnxovqbGVCQjy2lWHrjakxywltZRJ5C0R0GEzUKdVLnloIOOq/OKhRr2OgKqftFKWReVKcDQJQNX1G/qbkeiZY4YjlC5TuqnCX0IbKfWf0sxMu/7q5pVG66xfObbz9t/4ztZVRKBQKhUKhUCgUCoVCMcZgCfcPv/nC0fUrlxql8bWzV6TF7anbf/fEpxHhM8VfghpcUq4n/NoJcQ5++1JhXmPlUmUNgyAGVhWIc0TB0qYk+SFOG1LqazP+NxIOGy8SLklP+jdwgj0k2SWVeVazfBaSHcmRpBTqzOo79uC2K5qp2yctcmu2MkJL82U48bJJbGVcHZFflAKRij7qrAKEZZAg2zPZ+z2pgAcesNcFO4VQET/hFKafSccLUfBdxfxU7v/2G882S3F/H9befFttZRQKhUKhUCgUCoVCoVCMMVjC/Qee/s1mdjLr6/Dtt86FS/MIedstFvM/zP8xEsHOLF+YrQwE85J1jImV7ylbGUH1HuYbLmfplrYyvDwoL2PBU4HbzoTqeEpY02KgIUFQA5Ux83WHtNWLm3cIl+N0k7dyJ+lRRxGJl/XNGFHOyAOCNiVO52MrEw6hJGxlTJTObLYyLHhqnL/vCxkfaHHrKgOnQvxVAYQBWRM+7DTAKQt26gh2DBT1ICvXARLWNCjb0pC4BTRmAaIq3CvQhso9v/7v0mWvXrgKr1/XTOW+f+CQ2sooFAqFQqFQKBQKhUKhGOPAEAuV28lc/he/evTA+XdnTuOb716Fi/sRY3XyxMtn9too45Xf3f4gAP4UFAEkxzyZMTkxSslILLj4gsx09LMlyU25LCdUjf/bbQduPk/ck5xuWxyryJ0Jtp336UKQJt+uWFcssVvkBXV27mRfKPcnaft5tsxuatXtAJwwpGR2uAdYfnJt9PcaEsJXslKhOwUk+SygWZh0emFoWJFoJ57tSIn/VkAPnFSacWMdyIjwfB5RkNcbI9jRsC5n25Vk6PcxQGeKtjZILJKC1jEm0Y7FduxLgJBkLjzZzbgi14IyBklhoqUw6GxiWajYHuPFKeseTPRNwPR2klUStZ9hSvzpesfNh9ZevPW69d3yJGSDdgbKnsIG+Az6r1HCr3GMKb+OcbZRbrnxQyl2nb3Ksa94+EAkSQPLeVMOJlKrrVG5aMyLIp2X969/oaWTKf/K6UG64N+/9R7c9v7ZEzz47tmNF44fO/LJBx45pY8VCoVCoVAoFAqFQqGYBHt3fTa3J6UCrvy9fufwY8/uau303jYbo5/N0bRBFudtszPJ/oMk3G/bfebo+7733UZpfPv0BWlxa3YygPiXHMOJdeR3SLBTYp2S256uHM/Q9QCesKLpjLMJ85t0O5CIc0LclumUJH/+Z3wsdrSB1I5kQxNrrjOIOeR80Rqj7z2n6jfN5uS4kbHSA28T4Tjo3ynvdquQLsc+Ziw3ohTQ1NSmZntR8qiMbd9iW7TkaOqoJ1tWEPQmGOpx54br3hiR46H9i8lGi9eLQRi5UuJ5kSjHeJG0j5+vIM2T22Giv2OizyAPGjz2rXcBVGFq3/9br1t/8We+78YdKInrtdHvms0rP6PW/NcoOP7EZM1+amJHM+z8aB36ZeCX2X1tGmTebVumWeYZ5IHi9uO83bb58a+RfNi2nz/w7vMtnenbEBDuXzt9Cf7Sh24Gc+XyjNeODG4+/af5jVgJd4VCoVAoFAqFQqFQTIrHgRO6Y+zd9dn8/f7hw489e1KrqFuM6j4fAMltY6Uv2e8ZTbdOks4gCfcP/MnX72iyPx48CN/ai4iTnRMvn2mFDLny5D+6cfTzF21uYPXElPAu5scEY0haY+nJUZKoppwXVerFNpyhjIlzmp/fjKjZKxTwBc9eaIpBJnIFpTxQurUcIMhnvVI8JELJfE4umpCxtQkb4/lKY8pxiMZEO61NqDxKeTuJcmYe9DXBXc3UJaZfRmCFyt1MlqPVNCMG64PdC9Lcff9AVO+mUDR7BT2UpH2knmeKeOCDO2iYpNwGSjVZJljHVLWZbN2DGHa6RHvjLOQ6cMV8sjypPMAHgi3mM1nhPm1/4V8bxL3UDYSUpxltW9c+4wsBom/vYn2Rtv8epoyrSwZs6PbFVYLmUWxdpk33LY+2zIf0T5/fL4ymX2t6/X70lbN7v/iDt26PZrfcstffPA1v/NkPwW1weeZ096/s5zfkh/TxRKFQ9PyCsDn6OTyaXDDn2+3fddgdTS+5Z+Y8qdELng4iKhQKhaKv+9kGFCTkEXsfu4Xc2+rwNLm35dOp0T1tT2tVMVBsJJbnz3Sbo3Mh77snbb8+qX15Lteb/BqTk+t32N+qZ+fDk6Y7SMI9W1tv5Iebe7dfvHItXHyixSLmF/obLU2NgdDbeHW6Q6mAJ8sJQc8V4s5GAanGOyepjDFcFV8Q7OhdHUwQ7pMR9l78LmwHMnFuDNnREval8wxVx9vtmC69zDbkPQOrGV8TNOApsZZBwY6meScj8yYoq6nIT1S3I0uP0oXeyhskgXq7SNrKGNGipILOpSSsT4wSx0Ff5p4zrB+VtjJRzWLYR7zteU68Y9EHxkFK16gjvNAQKKvX68h1aRkmGrlWBT9hHkjK633dIQiiClMr3IW2Qk6GmzJzT7yzgRSwxDqUXkS2XdlXEJ7Up+0d2gvRLoElj08I9pL8J/t6qxnaD5Hk+7FLX/7SHdf/wt95uoXTJf/aaYsueOa8gdsOzp7gDe+8deSb//CXN370r//tXX1k6fTh6H77YERfrnLkRCF9GB3Ui5YlRcMHagdHkO6OynyshbzyZ5YHyYvnS8FLqN90SASrLffh4NnL/f1x+3KSl/mLK/xicMT2nTtsfWy0lPxxm4c7l3bt+XVq0k9oV4gMeiRYTM8x6TxrfC0Krh/heSINsKg6jp8zvm26/Fy/r3Yb5ftI0CelfplmFVo65wl5WvV+PSmJIRGv+Xn1TLBsp81jUMz9erppz4UjwXPRLNgU8tiz97RTtq+stGWHvTZsCOdOeL4M5hlauI6Hbe3eCZ4YlfmhBWmHSQaRDtt313x6ZLTPLunH9HquYonJ3y3cPenjtk8dmTKtzUnuLYMj3P/trz64efPv/B+Hm6TxzfOREffeiZfPbLdYzL8aKMTBKcSt0hw8Ke093BnB7pjRmKBMk98pexpuYyPnB9z3XbaVseJ07hnv1fN1ZWBqfA6s+TVQcn8Z4WhT1i5NSfdQMR1ayeAM6XnSNPiViNlZiXe5js2EJU7nnLKVKRj7qppLF0NSvbu0CffLkk4FRh2T7yCI+xP+7JP6rldazFQEva1Ur2OyvyEl7jFxnBmWtjJTdUE7JMGJ7FA1TmrQKdd9e7O290p0doUK1OycQCdqdc+jh+p5g/GZzFX2xpn3k08oyitjXq6fB06qzoRHXzl76hd/8NZT9Mb67147Cz/38QZxxPf34YN/8gf5A1+b9xpFPe4mL/CbNQ9G0uKJSYe2XvimxLEWynB00nIk6si9oM4L05AsMz/wLtGLQt6ed077YtCwfby3qO0jVGW1smSFvfZsztCOk55jjc6NoJz6OXrRhx/p8L6wAc0GwQ43bTdL8m81TGPR2piCDh668+xp+7ujKtFe72dUTboJ7Q0Y151Tm/S6bYnL/BniiRXsE1uJc+f4hNeBUNwypGfovD8typfHh2c8vg3hmrdo1+xFxkTtNjjC/f2nX7mzaRqCf3trBMiVf/kP/+zo54PAld/gVO3W4cPIli+RRQwl34mqnKjGyT5W5c5tZWKCHfi8Rxz8VAisaiXsFd7sflCB2MoIBDyATLCL9hqj//ftWIAJbOUN4fCmDpBqICoLXdyUaAfkZHv+4+xAslnTTB6IEGrW5VFnK2Ni5fAktjIsQ7oP5ePtAk+mUysaqQykcdH2H6v4HtvJEG9zs2/V+aPKxGy0y5rh9e7rdwLlutQPAKe0hhESSQRmxWQ+ZL/MKtozai+DXPE+8eAMohDwFiPblnIAxEiWL8HXDej7CwQkPv1sQv76oZgf5+Guac5/y9hu4wcDeFm5fQ35ima8zx2Xvvylj13/C3/ney2cVycoYfXm62/Am5/8FHzk2vmZEzz0vVfvBCXc+yC8+iRE5v1C2gaZ/PGWXlCX6UVlkUiJ/Dp1T4eExKSkVj49aMmK/Hq6vYLk+5EW+/A8z7GNVRqYqsCklqVDuS8cGVAfXRZyZBM42Tom3kfT0/oVSCf3M0ey3wmyL3Jfz5FbdsrLmPeDnHxfatsO4UvLZbu+bCxQc2zq1WFhnwFr7xtrQyv1jW+81KjDPX9hf952Mj/tgw/m8IrPXCHuo2KW6x155dcBXVemiszCI1wf74sYp4OO6Cf7TLwdKSun+eJlJbdm4vUyoe6Vy3SVI6b3i22QeldngWF7BongpFUTUQ0jWRH+jbNOpKwAEWGKkp3O9JhKD49BHRvpawGIWzJWzhtMlFo6IoTY9KXcrrS5QX5Ukr8/qd+wrbwCHPg2YaeiyvdEIFtgXvGZoDaHwJM/bHe+L1rSPA98iikPelcoKc8MeZ+fvb84CxapKwntTdvd0H2RNVTZP6R5JPmiWAbjTdylPodBP6L9pkzfpj1K4xdaupZvhwueO7ffKEGDmT4wKfSlYzFJqaG9COcE6f2j6cXRn9+wRMDGgPtXroZ7cVTeb4ymLUuqrAIW6TiP66Vw8UiNFkixw9rstfePe0fT46O6PptbbNhAeYp2+/FRa19yFoqvTIZcx+5LmBdtf9BBK8W8cYtWwUJiIkHToAj3J79y8vChc3uNLmrffOdquOhUa8FS/8Wv5r7t/5FfgDFBbWKCncyPA0WWAVNLlTuwdNLEuf2bkvM1+YVlRYl6LYlzN2iALPgiSuR9TMBj4PudoGwpJxoSnJgF6uEMZiMfqR+7ZN7TBoLjQ0rC4wQ7TAcj1nF8RCaRMfDMDVbkRElREPYpO6wRsvYkv+GkbZg2C5oKQdBQ5IM0WcieQ5qwpnY+jtyuJNiBe6dXkuvFhFaFj67PIgqqepRJ/gyFIKno/etn82+ndWziEMWmgmw3Ltgpax/05DhrYiP1K3tlich7tF9GsPyQps1U9I5YjwZukA4LjvDzbZy+efBUCEj3b5+92CjNAxfeO/zSf/dfL9wLvUKhGAwpsWlJiZxozwnSjQUkrxxR8aD15FUMA5stKRoX9dw6Aos5+KjnUHdwHsk5+f6iHfTUAYvZz7nDdgA2v589Dg2tjXrsD99wg8naqoo5PjsplvT+PCjC/Ya9N4/eeKaZU8C33z7HF5hxcLy28NM+1YhsL8SX4zmuABfmJbW7SKhGy5iCOSLRc0K/gmwHYTuMiXMj7wvRsbCBAVEd79LwPxgER3U8JiVLS6U7URpPM+3zPMZ/Z+TvmdTywRQGt4yWIVe+t4opLFSiLkcXCcpzqCbYy30E4pWr5DFdBsNHVYiqGwlBjZSQBoj93cVORYj2jCrMKQFPSsi89glhH379YH3VeXbhiAvEgwBsHngUXXacEJD/s/YdQTUuEfBsAEQgv3nfsMR5oGaHlJpd6gcm8WUEyKp4uoz2m2L5TXnw1JZOpifoH8++/Dpc+uD3NUrw/S99Z2UJDcVw33uX/Ph2l4CYyMnQp0azTy0gKZEiKnLVqFMIbuhpOAjcvcLHvqj3Zj13+qt39+WOEu/T3c8O24D2OdH+yJL04fFgsh2I2dJWVsyhfymW9LliUIT7R5/9141IlNPmIJy5HFkCtOjHhj8XLIjU5/W2MgL5DSL5HdvKlArzKvsZqLaGQUkBLxLniIKlTUnyx/Ugku2Jvylxzf5GS8SHROYUE9T8TYn3WaeAbMeM28xgnZXMdERqT7YyyZKmydNpbGXCFJm6Hby/eaEMz6LAop7EZstG2+1n8bL8dx9jhbkLUrpvp/F8Zv/OvEUMI/+zjKvV913adFlKzQ7CMWDCz326i1Nk0cItWfxpLba3AcFiBuIvFOK8gBHxosUMV8+PPdzFQRqvioeg32B0gSuCpzbGo6+cze8Pu3TZcy++1CzRgwfvAEWX2F3mg2vJc/npJe8Dpxa14AHRvrmk7bMFy0u8L9r1Z2uFBz/uWNE+tguKRrdhIMS7Vkfl/YwS7cdhOe2M8uvnshDvp5a8Sy7StU8H9Bb4ule3zaCCph46v9foZePbb50LF53c3j3dirLryu/8yg+Nfj4M4wiOWJJZLPBoEeXQlEEhjQ8o6oKMjvfygQFLRWcc+JIGXg0DmCaCso7zA56fGCQVqgKrIi2rFGyVLnNBW32YxjwPiEh5lKxhfPDS0e5redBMKAJjhoSjGegZ5kXWkv0IiPbiZtpj4TuZoM9FbSfM09LSgJogBMg0JDX0AzMsKGYUmNMFT+VZu318UExjD8YF65Q/hDCMoM5PtTUXN7UYHqTnC2uHOHBp2Acjb3REMQ6qvG+QZ8pnPSpXnCxT9GcoKN1BjI07Ra+kwUhJDyHLynZBG/+0DFBaBjoFG+DULbPrxf4Q9iFk63iAVtdvirTLYK4kuKu7zJGhK2NjOhf7txk8NSfd73V/fOfKQfjRRjeLK5u5Pdp/8vNHl11VPBTcB6Uy5A7yQrRopNIeefnJf99p8WVhmzzM327nD8NiKmp2yItUPjq2d/ixZxfupdHaWzwI8yPZ9ypepvs6P3JyIid8Hxr9PrAkwehO2rq8xZ5PRxbgxTknwo6t4L1ikXy56fl7soU++qASOq0QYsdH16882OexRbzvzPmedi/Ml2Q/Bemv9TZ7OOT8up8T7/kx37eIAXfze/Co/MeCexgsyH1skmfonQU5dzb1CrLQOFLX1wZDuD/5lZMb1z/29xq9ADx/LvJvf6LFIv5M8UM4JUa2AyOyLddoOGtGCOoUsR6R+DGROmbETVAGRugzsh2ENPl2aEk0KInzgtmK9gUQCXgkRJoj98lRhyRiSjPtCUfjVeLG0MMYIlIEu0C8Gmhi427iRINKIeQ86yNASfEwNqoR7GhYl4sI9nIfQqJzohcgIveBji6RjmGROX62IJ5NrhZfWyvrNt8zg3iMCEGu0Cj4KAZ8eUUjSGS+zMzXEPyQCIKKsWe9s5HJiIXOTJ2RkdroG74k21Fs03Q7Y9l2fgDFXj1836Ft7+bLeqTkvs3DcIKdE/bSYIEdFQCnoi/W5w8oX27hJM6DanvC/Zt7l+Gvfei6mRMzV6/Cf/Cv/uEmtPp1laLiheFkqq7tQ2z+IHS7JVuG9AKRlzlXnp9qScVe+VI1+rk/UUdHgjraGFAd7dg62rH1tPAErVXCHKfXnL76E/G0vsNeT7sagMmPPSfec5Jie8GvP9G5ZRXkeX3eaX+HRlwctXW/MoPCAyY09uz17Rn7uztql9059NFbheuQO9/zuvk4lANGQ7r+uzp6Rljf1yBXnt837Dn00Ko/g9lzq23bmN3gnrY7RVm6fubLjzv3/M/767G2z98O7mHbE9TnHQO7l+3Z/vFMF8/QXTSDvs0tNGqvfYMh3D+w+63N686dnXl/PHgQvvX2eXYybu+ebvNB/mcs8SyR4pS0BkyT36G6nBJOsXrZU1HjmTAfY5eTdIpwhPX51W1XLiPEbZlfSfKDV/DTY6EhDlEgKBPK4HGygdVMYaJTVGIrpHuLfupIPbcJYYrUdoZaW8+WjTxEUa9yr0otZOspyUpU0F7lLumtJ1/myFU+1BO3ia9LQkS7bpbvsIayYY37I0muCzuI+2J6e6nv1CrrK/Yf9/cgeKvrQ036KCPWGZEdk+J+e3umURW6wTjAMv3qIUw/Vs/TbVBQzxsUB2nICJsZf4LgBg3o1bQYlitsZRoT7o++cvbUL/7grbvuhvnu6TPw9qd+CD4EV2dO88Zzp4+AEu5DeJHYIS/tx+znv30r/R4YTQ8NheyyKj2nCrrPvmA92DPxkpMYDy/ai+sEZMBRS0y01f92bX86OUt/Im1/kpBweRnv6aD987xydeDdi0hS1NRrfizbdnLtfjcMR2Ht/PXvX6HbwZ1DuxxAoYrd7qmP7pF74w65Rm3Y8//eRaoXO3iYl3urw7LmQaFvH5V1Fb8WcfeLB1uu87zdT8xKoArPfEfstXerg+e+/Nkpt5nJv95aimsrqc+HbJvP+yuGQV8354gjc6yvU3Po511hZw71PI++u1G3wWAI9/e/9kIjb70XzlwIF7VGdlz55//7jwHi+wTVuWDxUZDjhpHckuULs5Up0izYbYDSQgbSljCR4rnaYsZvRtTsctreVsYK51O6dEEpDzKrXBNK0y+nKvc8oZxwXSeK7QHJ3KNAl6ljox9EwIyku2Qrk0oxtiZKlMYnZ1sPg/XB7qXamSuorfKZ2coYUw4MMeabKeIDktpwP/Z9y9H6saeMDLpQobwdP6ojy+miWmuYFLGPzW1mAIKAwCRmgQ3MWkZmnbK3hGQ7/9ogPttKlblo+eJU5cz6xRPnlLgnfYWr07maXiDsSdph+cMBAtJRfH6fbtFW5mH74jDGv4eb4Q44M3Ni+1dRfdyH+fKwPXphyB8+v9FTEQaviMtfsEZ19AUoPFj7eKlampfVgJjIifa2CNddW0/bLbf9niU8tjsksBxJsbRqUfcVjiUzH4FhePXnJNT9sDrYHFBZ8vP3c0P8wsAOFuUDr/mffZDuX5yFbLWDhznB+oB9lutqcGvLXiu/sGJfjLQ9eHzSPh/tttyf3aDyfVZwcRzm/xXf0toO5ffo0bHt2bbvA19YUiunj7d5ekI5cHVqDuc+dtjfvjCH8rtBuDa/6L29boPBBE1t6t/+/MUoWGqbdjI/Doy0osFNMQ4o6q3a/bac8EIpyGgUHBXINoatH6ctBFtFsh0KAVNZulXbsWOk+8YBXzGsk1LpHxzzRMrdyPWEENpo7UYc/0b/7nqKg7WSNm+qUk6jheCppj54apxtvI63cRCa1gjL3LzvRmjdkcgWZcRZpMpvuxG6rwfQziMNaArcuygKkutU5FRVnpX7sPyCklPrF7ZdFuwb9lsSPTc4nuj4MpSD/06POPipG2DjAUrJ1ciE+5BApyZu11TA1LC9TVQGsAM8PnhqXAYWbFXqhxhs/wstPux7fOel1xoltnb18pBe7hXyC1jn76iLQigS4rUPbC9Tf7PExIstEkAPWbJue97niVVufgK68UHN1aJPLXNAz5xMsi+QQ/j6aWMJAv5Neg7mfWpIVilfXABy9uEe8txtag1hz7EvQrcxCvK+9dQkgfOW4FzKg6LmZOvj0A7ZvmfPhy/O+yun/J45mvL72X2Q9oJvs098wyrCl+0+tt1B/Uk4tcRxEzZaOpdyIcato+k+jTFR+Wx7X8vXgtpr4SAI97F/+ztvNepsf3KBEe65nUx7D7SIPwGOKcQ6sr3YrtCqY6jkLe1WgO3r9yO5RoFHQSbOSb6UeK/ML94uTq+wxoGQuMWaMrBfEI5BIKuBWIkA97UmKmC6PUoEZVeTLwsvF9Jypo53NhjRcxzRiNtO2KuTLcSIUqCkKCSOrL4YhhG4voOxeV+HAsGd2ezGZHvGyfKQZM8CIjsLSPEwsK1ErGeWnPd5heQ6xvtJ5HlW/s0HbII0qIc7ztRRUGi/kMimRHho5o9Be7O29+S4SfSTMC8TkffIB26M5G4vDBYQNb4vf3k8o3K1oiR/9JWz+YO+fzh5Ye8C4KHZfdzXr1yCF3/pv9nUR5vB4kQPeS6axdATPeR5apmsRUYv2w+2TEwcsy8Knb3sEpL4ix28ZG9akuIoLDeGYkFxz4pc74d0Lz65CESIvQ53fS3ebbH826Cke9v3s/ExQntfPeXnwSe6DjZqhQ+f6OiZLB9IfnwJ+0Ufz7NPwPKi6YDwjj2X7tfXu16uBbXPGIMg3G++8u6RJv7teXDFF94+N5cLwZXf/t9ydfuN4z+QqsxRIpepwt2QXYxIaktKdmoB4VXlgpodvMqdo4ZEF8oqDyD4GXFQIa4HFAcIQChf9TqkPCpyf/S+CHaRmAVOvEsa32Yke1WFmcr1iMJ2dAehGY0REjQVBTKEgHeOIIwUDRXSQhkQozrzAyxYku+O/N4XlOAZ3yci25GQ81lIcjv7Gkuss/wCUpx6y1NSft9awmRYTnZQYDx5kj2LSXl3nPvh4ECDjmIElTpAmoCn5LthhHdMfpdKd+HrBUnNDgIpH5fLsDIIpL2RyuPz+NilL3/pM20/zF26dBm+u/7+Rond+NLzQ3rJV8QPp13jmQV7EN1ZkXaZBzGRqwBz26K21G050f2FPn1LLSnyOZj/1yE5MZETFEv74hj4Z/f6kj/gYKJtYkj+7Yt0H+i6jz7d8nmWXy+7Jt0fX8YTyH4N8xS096VIrjbvzVYpz7fDLyHyAeRv2AGLZcHTPeS5A8uLJgMyeUyolbK0mtO14IGmz/1V6wdBuH/gm7/XSKUYqNtztDcKVqjbQ0sVYPOSvUseFJCT35zg5rYyzIqGp0PziojUMF++HQoDAxNvR8oqHzc5fj8T1NEM5CEGASypN/f8rFqmKF9YTvI3TFLGmQ9A/nqgtjolWxmot5WJArKaZLhb4cBCWxm+nTFyVUiq9IzbykSEObNzmVLFTi1fIMgn6nfygA/a/6JtywUQW9wE6QHxcZ9Z4B5YtIT1bsQxN6G9YxU6HzSJbGWk+ZTFDO3Noa0MgBEGCLj1Da9Um5cNntoG2EDt7qtvNErs0DunbwfFUB+w+lAYLuLnnV2X+Z0lICbyl+oXWyQmHNl+agDnTa52z0n37Q6yO76kysChYRVU7kP6YmKR7gPPLHrDW9K9Syu3zWUbLLRfarXp1749lECztn98Dub/9dYGFF9AbC1Jt9Bn6PbOr80Gu4+/etTHmFauBfl1u8l1qfKZfxCE+/tffa7Ri8nz566Gi3ZaLN5PhHYxhLROW8z4mYhgDyxfvNqdpg8snTRxbv+W1POJ/MKyYri/384p9KmtTDBoIAwkMBgewzKlAA+tWoiDR6RkpsrkWSZsYXJlce2GGPG0cwFnYqWvJCDsf1AdxjYmVwEqm5ETtzHBa4yQtSf5DSdtXRIhiZ3Res24gh2RW7xAoGKnbRTZuoTkPQTbYyXpn/byD9XzoTI+1ZdtGvQLjsoIvFUIBjhSqnHZX52T7VSFLtvKJPoDpCxmgNnKcCV9Eb05ItupXzvEKnv+Pc3n2zi9Hn3lbP4wt+v+/u7e+WYJHjiwCQp9YSixu4B11HWZdxa5Q1krlKdaJCYGQ7YHLybHOiKxjsLy2jQ8PZByHF1m3/wB2hMtkgpxWQiuBzqu9+PL8OUI8Wtv04d8MGQ7uZ/l/byrr7ceWQZf9z6eSZZYwT3r882xPr96XMqGaPZVVOVz1CAI92vX39ToxvTCaUaMnNzePd3KSXnlt/5Brm5/H1soBR6NPdzBWQwXu/jtUzYvgtodUrYzjOhGFFTx1R7u4sBASWFhpFw38r4QHQsyixlZqjup5QxCEAyzIs1pMAdrGZx0+0aFnnJ9bT1J4yN1tjKCgtqIwvuY1K08CEKAZ6Fdiwt0CvIAyn44H/iu+yCpgbVMRmxk9suYAcgmZxFj+f1w4IamUeXLjoJljFtPbXK8/z/ClB8yWJW9CQOKwkS2MqzNJCsaah/ElgW92yTU7KbKpx0T/Yanw21lICjDZy59+Usfa+l25FXuu3gD4MFDMydkrlw5/ObfvWtpiYxl4GI6fpDbXcA6eka7ycTkxBa059fu8IWh+j1bVVUXxIkLPndkybrMHQMqy/ElPjXvBMVC3CPneK3Kj6NrFeiDC34/y+9jbfq159gZGtkePJ/lsUq6uN8+aAcyFJNjmQOAzvJs84CS7XO7FmzPeL+ofN/vnXD/6pO/feR9b78yewLr6/D8e1fokjaDKvxwyj9dsJgBYbsJbGUioj6RX9J2JmF1IwQ6TW0nBVYFjJYhCpY2iOngrrMgJNgBK4KWQk/BUiEuH0A9yT2LeBlr7HwmSaI1W5lk6dPkaaWtDAZblAMspX+7LT8ltqMvFqA+kGm4XxbGUSXBTL33O90/i9MD0u4pdbykcneK+aw8/iiQ6tR2TIa3bUysQ2DJ4k9rsb1N0sMdRLugWDEfesQTMp0r5XN1O4qDNF4VD7zfRENYrQVPpfeP995+C94w18+e0qjPHnruj5eNJFIoFDE5kZMSbb9A3zf04IoNXkxmeZF5aglJ96Hg6BJb9wxN4b4w9bwIwV2nQC6m6HIA4cii2ocQsr3N6+0uFIG3h9zfx1+UQTfk7tYSkO47XXbLJb7/fnzaa5kGR537tSD/gnN7yt0qbWR7J9w//Ae/s2muXpl5/z+9OscLAOJfsHNcwZ62mAmtXsa/RlaAC/YuAYlX5uHSSwUwrbCVEVTvYb7hcpZuaSvDy4PyMgxoXEnlPZFliwuc6cjIIPBl1sGUVKqXhCtSmxtpMCDqUzN3Roj6yHxtZcIhlPZtZbAImooQ1G1GbFYs0Y6ZQHQDaYuUPzoNvhvYqSf7Iki+7tG1odLbXe4DgZ2NcH7M/AGHYanF7SwNnqRsZUKyvVS9B7YyBiv6Q9iHkJP7MaFvUr7wzlaGk/f2SuO322zjkv/oK2d36IPd62ffa5bgtX0liIaLU1oFg3vI3Vm0Ms+JbD9pH/iX9cVkpqxASfd51u29y3ZQ1k5maAS39t9+rlN7HV2nKI4v4DkzD7I9x7FFsASxZfwidEPwbqnSXQE1yugAu9BtIOhVxn0wna3msIOmXv/Wq40Cy71w5gJ7gd7ePb3bRrmufOV/zU+AjxR/VQRMlQOdRuuRByEFny5GquUav/a4HNW2MhCS7amBgdBWhinXTUp5LxHwxqQV2BlObNXi88egrrsKnCoRsWSeWd5UKaublhehjlCHOlsZWeVeaSuTyKrWViZUVce2JeG888CnbPh+RlTulnTPN9rPiJVLxu1dMCMDNW7KrC1MZufdfvt2orYzoUodBJubzJZhijgBtKy2/2Mm+cRDE/shbuvCLVjCgLUBoW2qBl/Cduaq81o1e5XFjIn94dMWM2GfDMn5z1/+tV+6uaUzf8fNfOdys1vklQ98ZEi2AQqOLgN0Lqo6Z0+7SSU5sQXtk+17i/ZCZW0CTnaRFSjpPi/cvYTHpHYyCoonOs5vY4AxBKruZ/Mi2x9YpMF0Yi+jpLuiC0xzvh1bYi/7oV0Hpn0W36xa2Tvhbg4c2Giy/6vnLtM/27ugI/55iJXkIKjZJVKc2b1gXQDT2PIlkUeZiJSPYE8zaX4125XLuK0M8mMXBxVo3U1a9+SX+qSXC/vprCSoJaaU+1XH0k7JE0YwaCbeNt4Cky0l2ozAtFYzfBlTuZeL0VqrIPVkpxYv1k+dkdqOYI+CmWL5+QGi8HUFCCrzhEc7Vn31EAzEVHm2uz6QoTzA5H5nNWQy3lZGar848Gi5Ptg+DI5qwsClYX+I009ZzPhtY6U8imUifcV4Yj+8arntP9/Sme5fyl5ca8bhr128oMSQIscpLfdyYU5k+yK/UOUvJrtdvAeBku7zwMaiWmBU4Kg2q4IQKDlH0PW1dSEGsuZItuf3hIcWsK/kzz5d+f4r6b66z5GHYfKvsLYX8SvQJbhnPDRFe24k+YC+D8ZcOL/ZZP9XrzAeqMXRa/zJsKiCh3uxvDLQaWArA6HNS0haY5kmSn7tyQCmsXo+nMfQegYCL/p0YFXE8BjippygTsufumkfSu/rMfnKbbR78XD3pDApW52lCDnsFvokkD4i1Hulyr3KVoYmh/XBU4GqnYGroSMv8ZL0jdTzYX9ATnwjEsse8I2PThGPRbBQRIy/hBBtZaR4AKGiPWEVQ4ssbcPWV7S5YDGDAuneIDZwmmwPyfWw1LUe7sxWxindJZIchDIAhGp6gbCvGCwI85Hya0tN7h9ovvfHf9QocOqBi+cOf/vhv72hjy0KxVK9JG3BfMj2PKjcyUWsE/IpfifZjabHF9x3fIj3hbuX6Bwdop2Mon90TVgdXYBzZV5ke477FlWRa2OUbHeUXU66P6in58ph0nOuj8DPigIPwOQDtcnnul4J9ye/cvLITaf/dOb98frr4dV3L/m/t3dPt3kj/WGBjI5V5+U6IOsAGEEf2cqEPudJKxoQ/dqZNNjZysRqexTsYmg+8cBAYjt2jFIdUA93EI5versMyYqFqoR7YN0xZX1DvdtBKHMb8nacdZCDVmnCVqYueGqcreQHLu1L7Udi8pQNKAEjs8dE+75VuTPLlsz/IvN6D9ZHanUQgqdmgg0MyF78buAnNcCSwUTBfFmZ93k/wiYdBGm9SzY+JraG4QFKSU8z0kBKGHjVX3rq1eyCNUxpQ4Q0eGpcBhZsVeqHdLCgFYX7o6+c3QWi7H3lYtYovVuf/boqMRWKJYFVVs9LjfbAItdNx6rA/MXmqQUm3TcGWKbNUX1uLsmpuowWOYrmeKaHe8bQSfec6J3Hc+rCDiATTOvj3AT3LuFXRop2ngMeViuZ3p5r83p/eMLNk9fRXgn3Dz/3+xtNAqa+8Na79M/WLupX/tnf//OMBE/bdaRV74ECninESyWuEaxcDFO58/ziUJY8X56OFDBVzi8dWJXZyQie8aFCfxJMynFTEtMRlWQZZtWxV6dCFUlK3ElEMtZbi9SkPRMSfU8O5NmElK8ImJqwlYn93iuONFLIMw93HjCXL0OJQPdEdRbYukDsjZ4FwXirgp2mfNgTX2d4r3icbIpqmPanrEk/4V7pSaV4tWqcNlBgK8PbngRPlds0zCv2jEdBPQ8o9sOo/yEj/ssr48cu/9ovfaytFwU38+J6M1uZ9QNGCXeFYglgPxd9ak7J7yzD58I2iGpXxzHPwY9Vxd1LcJ7mgzBqJ9Pys5AeT6Pr1FDPlftHP1tzSv6BRe8sM/g4N8UjSzTo2SaW1d5wY5LTFBbQlmnJ8BBMpnJPCkB6JdwPXXiv0U3o1cuMI3q6xaL9CDgVeagu5wR7uVy2W6EKd0N2MbLlS6Qap+R7aCsTEd0oEa41JLpQ1qStjK2AqoEIwbKG/E5NPGNJRLrKS+lnRTXxFFMVcS/mhTUEfHCsJqiPmSEm0NBWRmjGSWxl2HpqRcNsZThxyxTS5NQI63UfAffBq9zRq92hDF7qBmHcunCbJOkOMbEe+K1H/WPfTv5vWyY6CDTJhFyxLwfenaFbGNsGYZtVB0eNCXgW6FRSzBvBQEcg8UFSs4NAysflMjzYalzWlMd7gR9r6R7g7yevnr/aKKG1yxdvB4VCsdCwJN7jMD+bigeWqLq6/PT5qCWNFO1gq8qHdFGOYcBlu0W7WK/Y7SHPO4ZYEVZ5f3xOye8si9+0PY7tDrN8fEGuwV2eS+8s6fVokvfDbVW3934NmFTlnrzW90q4X/femUZExOl9fnFvsWh/PmC9JrCVgZCoDghxJMFTBd90TBL1wNMR11cHW2V2MVXWMFXbkbLKx12jpJ6BRUwFJp3FoqYJUmp24u0+0b6zF6Cij7RhKwP1tjLGSAroqkCpoXd3vAyJtYy3fYHAQqb0V0dKjtP96vzbo7bCpNe+C9waBUKFJp9PVPQHS/5j3VcS06U+bh+TDpgq+On7Ng3amwZMnchWRppPWczQ3lzn4U77kUub12iLtjL0fvL2915rlNDaxYuLTl4oFIr5fXaf49QyBcOy1jJdKrKOL4BtwyLhngUv/5BV+vrFW7/Xpt0esh3cM+CcrdFynFiyrpMPIndFerrB/aHjJb2itNLWdXhYq2kQ2G7Snr0S7utXLzW6Cb22d8HfO7Z3T7f3uQlahXvpgW6q7GIIaV0VWNXtHJOnzPKlMniqFPzUxHmlAqYK+YVllQKrkgGCwFYmGDQg9cDrM7bQmHjCMoiq87ym9jJTpzfDhInySNslBwOQk8Kt9FNhUKjeVqYqeGpk8SGC2YywpKN0jBGyNoHcP/Bv99O+tZPxfuvWA73K+iVLpEXJ+fCLBpsuBtu0PrDD1OwkP7quCUzUJpK3elUwUuDzseVLwlYm0R8gZTEDzFYmIPSRph370kvBVpEMC7bl454/WI/vKS9fGrXVoetmTuvA1Uv6gq1QLDD27vrsvTBf1ewyvlBNE2iqDTyyBMrsoWBrUb3xbR/Qe66iCrsd57cxsHMkP7cfgfl9rbVrA44uDab0cW4DRzSI6kpgs2b9yZ4GCRXxNSBvhzr78mF6uK9fudzooeiFc/5T/522ynT5iYd/xM5WqdlLSIFHY1LerkNqKwMVNi+xYl0mVKPycZ/oiTzcxYGBksKi2/mDM/Lx16itZyEURUU5xMTovMDIV6EclEiv86SfsJrS/WvSiqy0lUlAyKbWVka0MJHSi0ndIgotsjYmAVPHnvk5ye5I9/3SRsZbuTjiPbKNwYQVTLEvhup5Zg8kWNG0NYVEfmp+tnNFsHcxVPmdajPZ153ZygjkN1eXu2UIsa2MoGY3VT7tmOg3PB1uKwOkDLe17eN+5cIF2LvxA7OnMurHzz30N5UAUCgWEFYJeHyeWSwbOdETQeFIJEU7dbm1oGWfVp3/ACyXnZOiHp0TWAMbDDwO8x2UOrGMnWZ0T7u/475zr365tdTPlpMMeD2hNTUonJigXcVrfW+E+5NfOXnk0LmzM+9/5hr7szX/dgPwIwgoENQOsn96gpSP1OdYRX5LyvVkfknbmYTVjRDoNLWdFFiV1ok/FsEbPCToQ+KwEemOMekOhBzFlqeM2IlQwjQDkm9XZ8zAbGVMJTWcJk95h2XtWganJcFSnS+7a499R6hnljyX/PgxoYCHtJqeebjX9SeYccJA3Q7BfINzpGxAGlSUfmGAkUVLRJoXp3V1e0tfPgh2QbFiPvSIJ2Q6V8rn6nYUB2m8Kp7mQ47Zb9iWrYy/r7x87UCjhD743NcXUi2oUCjmqgTMcXKJ627SQFNtYdN+jbBs2O4hz0W1lZmGoHLB6Jb5HFQMAxtDKIQNyDnva+T2Erdj14Nzjyzq10aKWkwy6KX3pgHh8GPPTtIe4rW+T4X74euaEO7cv701OxlE/FGAwLSgzlYmbTETWr2Mf41EZKNAWnO1OpA8+L5xANMKWxlB9R7mGy5n6Za2Mrw8yCSsvtZYFc7aKCAHK6Xrou1amGj+ZsaDaRQ0VmqXIOHubGUqLGZmtJVhWwqq8v2CWPdBU8ly2JdU6hjZtqQV5yBYAeFk1i4zD+AIfVla1gRpMyfejkYgzkXbGcnDvVTPc1sZgxX9AeKzqprQNylfeGcrw8l7qrT/TEu3gx038/bLzawKr8KBTX1UUSgWCzYY57y/Tllaf84eVO45HrRfJQy5X21MucszPbx4byyautKWd5q6zT/X37MxBxSrg90VvZ918RXQqWW2wLBfo3V5fPrl1vKi7l61o8FSB4mZbGV6I9w/+sLXN5vs/+p7l/389u7pnRaL9kkWyi/2Jwce9NRt51Bj1ULiRIpe6Riplmv82uNyVNvKQEi2pwYGQlsZEjzV28qkbW7YYQm/007MT90SldRHfR+qvdRnzS/yase4PHWkPaYqZCJI9Wnqt6u2lZFV7pW2Momsam1lQlV1Gb40L4TramFdMmIcOekeKtAjMj2b7quHbErleiOEivZ2grG6IUGgVjJl+2BgwUKJbIB625mqduaq81o1e5XFjIn94dMWM2GfpNu36eM+frB+Cw42Smvt/Plb9BlFoVgocmLeVjJj4mcFyL6uVe45hu59uzHl9nkf6cOmYdFU7ndOuf0JUKwiVjXY43GYv9J+Fc6pro/xqFrLLCXqzsWntYoGibp2Eb9I6Y1wP3TmjUYExNmLc/Bvf/zBPzP6eR+M2RuqHoeUkhwSavaYbCd2L1gXwDS2fJH95HkQTiFwqeAjP1l+NduhCYKnAkAcbDUyf59FXc7qGgI/9YTavQnpHhLpALH6eRrytVV/eZyAaI/6X/228RZpI6DpbWXqawODQKhjBTsU0zVLtO+XQVS9hzu1m0FBvS61LfY0ZRD0H2uNE5Zt9j5D/cwZqW3EgKhC4NFyfbB9GBzVhIFLw/4Qp5+ymPHbxkp5FMtkyuM03D6HXLU+1eJJN76/vHb+SqNE1vavqYe7QrFY6IK0XfrPha1Cq+vjXDprGfsp824P9bgQ9y6r3t2aYpd8sIu+P6qSULG06MhKZiXuadDTILJayywdbp/k/VMxONSJZO4QeYC+Snvo/DuNHuJevXht0gOfBp/M/0FHWjsKiKq+S5gKgj1FtkO9rQxy8h6xTBMlv/ZJA5jWWMxE6Qb7CGp9I3vB53/FPu7Tkoh1JHxowSHZwMxqI5MkTGdIG6AdZfS0tjJ0PU8n0T/cDlEYAqwPngqBixAL3GlXJ4Nmcqsgp2in1jH7WPqyX3Me7si/dIi+RBAm7HGCMnBrVJ7mZDuwehcDnQIIHu6pnhkHWxU93JmtDA/aKgY1BSFgKlXbx4R9xWBBmA9SMv7yr/9Pbfm455/yw+krCNmhG2ZO5Nqh6zf0GUWhWBhyYmv0s9lBVqsSEKuPoJTHl5Cg6MN+aFFU7tMqQE9M+RLdFpQ0U/SBLgaQl9pOxp/A/Qwi5+8Q92o3Xq6uVNPPdrSKBnn+78zSrr0R7jeefb3RQ8eZa56HeabFYn3SEZtGIi5TtjJRwNQECe7nI1uZ0Hk5aUUDol87GxBwyvNYeY+CXQzNh3u4V2zHjjFRBxAXvSnxHBKU8yJSAdK+8VmD45j92Ge0lakpzqzBU+NsJT/wih4QW/zHNjJ2usbV7ZSQR0e84/CncTmDeLHsC41JvOMn6ymCytwIBkeRvUtoK0N6mpEGUsLAq/7SU69mF6xhShsipMFT4zKwYKupK0Q+fbqle8L4xvre22/BxQ9938yJXHft4oY+pigUw4claY93kdWqvFBZEqbrY+2qHbvENnSvrNxakIGLe2aoyz6gX7ut6K2lx3vaVkf97okVas8+Bj+PzxD/Q7GY2NUqGDROTXut741wv3rDzbNf/NfX4cwcLGWgULgTpglLlbtsKxMT3ynVe6CARyQq99IaxghWLoap3Hl+IUknEecVAVqT+aUDq5J5wVaGr6cla0N97gjvML0ocCZMr6yP9qH+3kGe0xCjrfp/h21B2h1FNXsTUj4dMDVlK2OSBOhkxUjVFRtYwUARjovAtweDNujLzQYZEBoo3Q1VoZPvcyptZepU4zT9wFaGtz0Jnir3k5A0NxF5j4J6HhCSAwi0DMiI/2LItBXC/dFXzvqb6ukGgVPNhQvw5FdO6oOyQjF85CqyLs7VnRWr1z68fe9dJoKiJ2WlOycGC9vG07xT7qyCElcxqHP3VE/nRj5Y1lVMi50Va88+2vRBPZuWpg99YfTzidF0DIoB4B0yPaA1NGg8ELTXtm3HT9h2jXCgr5IeMNnM+56+Vs5v755u76EJ8ZOOoMoV4qaQ/OYkjlOR45iEpsRiTmo68ihn0IwTyOdkp3H7OoLIzo8XG7BctTFse/DzRdrAyoBjqr5Ix69H49WlpDz2GEIlsSlIKiE/DldWY5OnaftjyleiO87iYGxdhHVLfpsSz7l9yBqplTAP07gfJJbb32m6bmgw1EjhT/oaK5QBue0S61k/lVrKBBZKtnshBgmKB2TXG9vTyu6Sd1tEu8DY86Cqn9DcLOle9DZTkNRQdns5uOtAQG2PMpS/1KDN1NhWBm3D0Hnv7YN2IMYqxNl65Ostke0G/MbXANu2Pm6Ez4OUnOWLZDu72pR1Uubnrk42fEZZrvwahr4MpHzGlNck16/KvD/dYgvmN9PNC2t54NRrMyfy+d96MCcF9CVfoRgoLDnRlYXGSgXEGr2EbI/qNycLulZLH7cvQsv0orfVcZ75OXH/gOtk2nNWg6UqukSfz333dnXNXUELjFzl/kjHeeYBVDfVbmRpnovya8M29PfFlWK2dsuFD1OJH3pRuO/8zv+9ecP3Zr//nD13ySfVVpku/9P/5aOjn5sgUKOXwVMln+yErUxoryIGVkUSPFXwTZesXCDycE+trw62yuxisM5vHli5qmxlwjxD0GCnTacMY//rsm4aypGFcntydBZ/GmDU43SDDpLnelUf6chWJiL+K/3Ao85kx68QIEHZY9COGf16Abl/fxttPld5O3B1fmb5YUwo22ceO2Ae7sBTNmjSAVMTAxaSEp4GTJ3IVkaaT1nM0N5c4+FuqD2RS5um1ybhPr5Zvbp3vlEil7JD6t2qUAwbnZETsJoBsfpQZ28NUOU+c3n6suexthRDxdEF6IeK1UWf6vauBpD1ftYdjusppVAsFnoh3D/2R19ttP+ZK/vzuIndNv6XEM6IzPLFEA90U2UXw+ZxwsCjyAKNEsuXyuCpUvBTE+eF1WQtVgRJlQKrAq0jZiszEVXYnk0HzNfLPQpoibNbiZR1PT9IwVPrbWUSwVPD0pp0yZnNCEs6SsdUKdEDSx9fh659x4FRw4Cp1PM9tBYKCPveJiC2MY5sD8sMTa2HJMsX2iY8ZWMkL/WUv3oqYGrKVibRHyBlMQPMViYg9JGmLZlTGXEZXP71X26LdB/HCdk7fFujRN535jX1blUoBoqOyYneLAZ6Rl8ev0MjKDam3D7sKw9rHfrz9uiU9bltrXkUq4s7VuS61+UA8tOr1ol6tPjazFXuehorFIuDXgj3A++cbUQ8nC0J9/YCpiLeLvljm1q1eJROXaBToh5HR1q77eXAq1IZZEI1Kh+ioIqv9nAXBwb8dmwAwR+cHCSWbmcnbJFwRsq6p9TuMycO3DfceW5PXcbGJTHp/hX1s4qDmbZgQjZGsuevJthJMMy4QEz9XXEYTO1OFmJ4LM75SZigxyk8zvCYsbraZ+gy6L3NQSC1jcGKNkORgGeEvokDsJpYUR+r2U1CzW6qfNolNX6cDvOd92l/rKWrwZjseOOtt5tesPSJQ6EYLlTdPn+CIicn+iA7txbZyz0kiG097nZcjI2Bkjx3Trn9E6BQdIvOSdmuB5BX9Z4GOoisUCgmQC+E+42nX2v0UnNhzVvPt/nASeWLKVsZE9h4gGArA2Q+pM8i9TlWkd+Scj2ZX9J2Rg62mrSLEQYGpMCqtE78sXi+KyLl2R8TurBMAgz3YUQ5TK5iD6cgzUZcWWPf+gWwlTGV4T7T5GkiCz5OhNFXDBgGUB26pUxGOisJnBrZ4zTmZE04GEKDitIvDEJiXVbCg6lpb+nLBxO3dayYp+R/oKznSvmxh7ukvK+zlTHQVuDU8YvEu9e/v1E6Vw5/6HZ95FAohoceyImnV7i69TP8dtCHD/k9Azxvt6bYZdcOVihWG11+bXiypy8q8i8/urQxPLWifamv68nmMgUEVyiWHb0Q7uvvnr6lyf6vvXNx/PvIi2/vtFis24h9S2grYwTiOG0rE1rMiB7upT2NkYhsBMHeJfCsLvPg+4ZWNpW2MiJxC0FZQVL/Q2ArUy6TbO9tErMS4BUTuolymNkUUxVfareBWaeo20wJ0VO+QuU+ja0MSkR+VNqUrUyFxUwDWxmo6hNoLWQKop35oM/LUqg1WyIbI5aW1fXfkGyf1VKGWbr4ealm43Y2AnEu2s5IHu6lp7oRyyD2h/BIuRrfxKp4k/KFd7YynLxv3cf98vkLjRI4cO2SergrFMNE1+TE7grXdV+KwC1L0C4LHurjPBkYybPVIjm2yoNgKwN7DejyOvBwT4fa5QDj3qraNNnjPrUCbaxQKBqgF8L93Id/qNHocnbjDfN4YcnLJJLWLJQfoBRcNKEi9+uA7BOut6uCbd2+GKmWa/za43IgprzZfRGq1POGB1kNjp3byrj6our4QJU8p05FA2tCm4FZGxY48CWf8firPdcR6nzaayteVrlX2sqkildnK1Oup5mSvVAYmEEaJJcp3alKfKCTLXtJrGM8aIRTNZfYzQAkIpwGGPXrMLBgoUQ2QL3tTFU7c9V5rZq9ymLG0EGaOouZsE/my25q8Qqze/qVl5rdbM+d0ycOhWKY6Fq5u7vCdb3TY95bS0b0bK84yXP3lNs/PIRCq/9yr+hS3b4zOk93eupfGx1meWrF+9SJnvI9umSDyArF0mJtEQv9vbPnW31hufzY37uJBSWNQWxlgJPgIVNGbVyqAqaS7bAugGlk85Lwk6f+1tXBU6vsYqT8arZDEwRP5Uj5Vs9LTbwvTFUq9P3E1FKZECGuusmR+FwgmeAEsQVqto23wESLzmIrQ8sUb1Hlc87aEQV7GRwm2Q5kHIgFeIVWbJUS7Ub9zBmpbcSAqELg0XJ9sH0YHNWEgUvD/hCnn7KY8dvGSnkUy2TK4zTcPmeEz7V42xmr367e/IGZE7jw4R/Ql2yFYmAYvbAe6ZiEgT5ImKHAEsV9Hf9QLFFuaSmdPgjkQZA8M5y3p0Z9bxcUq44un8OO9XSMd3ec36oT7n3dz/Lr8FE9pRWK4aMXwv26i+81eli7eOVa/tPm53+fgpKgBvubspVxkRDDNEKCvVwuBzqlROrYVgZ9+pLlC7OVcYQ+kPwMhNYxiBXK6FrPeBDKWqnWd9Y4Il+IyTXl+qZTFVryjJ8aocJ9pjRasJWBRP61tjJRGAKsD54KVO0MXA1NvcRJeUPLJoHiR2otw5TuxPN9gP7tfkCA/lqbGWQe9In2nKa3Scp0MdApgODhHgdJpb241sOd2crwoK1yUFMhYCpV28eEfcVgQZjPOK3Lv/HLbancx5/Lnr58beYEbnzrT/WJQ6EYHlTd3j36su/YsERt32ilDIcfe/ZUD2RP/v527wDqcFpS8YSedooR7ugonwf6GOCZIa5BG3hnlTuUvQ73Zalzj57SCsXw0Qvhnpm1Nh4227u4Id5kf/N/U7YyNHiq20+2lYkCpiZIcMRUANPQeTlpRQOiXzsbEOCDBmEZQrsYms/E27FjTARMBZ7UPEhvHMhZFR1Xo4CYUiDUOluZboOnxkWW/MDjfZGSv1DvYe680DNgvuiUdMfEmEKvnDstZ+ThHrjqNOrDgvJbVJlLxLpg72KMNKCCguVL6NfO1fW1avYqi5lSPV/h4e6U91IfbcvHfazg2bt4RZ8aFKuE3RU4xqNap51jp8e8716yc/DEitbh1pTbb+tpt9qwZPRmB1nlX1PcvyL3s76v56t+TzuiwVMViuFj4SxlzmSeVGnzE6ZPiz7oKJLfxTayrUxMfKdsZYL8EEnw1JKoNYKVi0nY30jBTyXivCJAazK/eLs4vTJ4Kga10ZWaPMyrrylVLvs7gy38tLYyU6jiJybl0wFTU7YyxqRaJy5BlbEJsWdBbyuD1mIGOXlt/d7p1B/bDtzmxivzXdDXmqrGqXu+q/fQ8oV8n1NpK1OnGqeNFhP6pO1J8FS5n4SkeazMR0E9H377EKvsua1MnkZbCvfx/ebyLR9ulMiTXzk5BHWlQjERlt2CYfSi2nWw1LafXRe1X/VJ0CzaJ/i7NXW5Dd0P4uRfCmz1eN5uTXnenlzVoI4Khi76bN7PjvV4jHf2cUpq14Jn9J6mUChSWDzC/fyViR5Cp0ROyiQU4GAXoTGRhzpCwsPdzUO5PVYolUvP9chWplgvDQBQW5mg3LKaHSXCtYZEF8pqguCpxToaPNVUqNxxxc6wlEnH1OlMaSsjtl1i/fS2MjCRrQxbT61ojFw/EMS8TdYnpgc52FhUz1Nl7ALJwx3ncI4IKvSkrUy0HiMCnqnQJcW8EYZMBBKfkeQVFjOCHY7hSnhhoIENELSicH/0lbPjF4rLF843Sucntv97DXCkUAwHd/SQ5zta7WPs9JTvUGxl2kQfKvc+rQzuXID6UQwPXfTZ+6zFSOewCv7Oyde+jlfvZzNfDxUKRcfonHB/8isnWyEcHnnx7d3WCoWYkzLcwx1kNXpkK+O2KfcRgpkGSnnR3gVJ8FTBNx2TRD3wdMT1hgVzjcpE7WKwzm8+rp94mVDHZPdshabw2BvbykjkeFUf6chWxhhJAV0VPBWDjDnpTOstIKXRq8TJxDzSh+LfHpSJqO0xg5k+dZigqyRsZUzQEw2adMBUEL5OgIAkFwKmTmQrI82nLGZob67xcPeqeCBpz37GyTj16oVroFAolgZ9KMN2tdqL6+mKtfs88VAPeeZWBptdZ2rtE6Zpv73Djz17Uk+31Yb9KmJjztncZ7846Qub2tL9oOevtjaHEMhaoVCkcaCPh7SbX//uzDvj+24c/Xt2Pg/qOblckDjGzxe/Y9MLLOYLCsdYW5lxqFNjxH1LgrucLzbHKD+vzUVrF0904mhzK9TlYMvl2FAzZkmNs5oHu69Nhxt22G38OgCeH0b5QVDWIm3gSlY0pVV8QM2GQuhVVLgbWzFIGvQjAP/VLV+q3f3Cb3/p3a6LfP7xT/EynPz0XPJZ/1mAbPtQTME7ixgJGcRicoc1TBDFPbV7cUYU884Kx1vikOOkV4km54e7Xjkie3zO2usB0CuRcSMrCGUUW/Rfy9B0/NXOLyP5GCwH2ex8md+Y0MeyDK6MZKCB7k/KXK4rfotZnx8WabsvdhBC8yoDdPDpc6Ppkbbe2fYP3TD6eRcUCsXCEzAbHRAwEna19sfo8xP8O3o+9lbJkdwuZdSft6H7YIm5YrhromnaYzw5wPNyQ0//Tq/1+fl2fM7ZbI/Ow4d6PtQ+rms72sM8cm6qr6+nNqe41ikUio6xcJYyr505N75/tpzsZwRfdCMptmNbGZC93h2kwKOi7/uYK6K2MlBh8xKXQbbDicrHFMw4kYc7iB71KOSHQR0RApHxiEPxVZ8F0+aXXI969aE9I5uwvUiA1NKWBQWrljl7s0/S/mG56Bce9O9Un5mpIsdnIEZ/G0j5okv+6fYMF9Tp4fCIkaxoDFWX02UTqtlNlU+7pMZPBXxt+yR7umkCZzZ+RF+0FYphYFOroFfsrnDbz4OU6cM25WgPAfumDdj68AD7oz4HdIvjc67zBw4/9uyxARyn3tP6RZ9fbd2h1a9QDBcHFq3Al65em8eDUe7hXmpNC8U4J66JGj23lTFOnTmehVDtbZXmfh+gSnmWLt+usJXxanbDleZcXW7TSuXn04GS07cKfa56d+kAEYoSdXyUH0TlYtuhqYwF2RfPjD3m54NmgrUVGVXeG2tw9R8f0isQ8TLHDCcj3TMQhrdsd1yDUhwtwbTRd3Dy/Viw1NG07+xkgOrH42TNzL0u2JOp0MF73RQScftjR4AK9bmsGvd/k/SY6t0p25lqHYJ8ZTV7qa5HooAnynpg6nmrnAcIlfdF3vYzB5/ebe1212YXklvOv64v2grFMNDXC6r63ULxCf7eXZ/tLf/cDqVnG4B51GcfCsuczDzWVZvBdMTprvpLrzaslcy9c8ziWM82Mu44D0M/6moNmFripR7z3tTqVyiGi4VTuOPBg3O4qDFPdbIYJYW4JeXZ/gChaj01z/3UA/W4tSlm/uiCP3dZLlpWCPIzCR96gGRQ1kR+Yb50Hw45KmaoaZ3GAx2XYLLHgo54zW1F9rH4xSWaov5SXR9wDQHH9YCx1UoV7MCF93LHRJ+BKcrU9tcR9FhJWigq4aGdwLrF1UMIIkpU6EYsZXQxiz35IVSSV3i4l57qRiwDuxQJanY3T/3a4wCtJuUL74KtFss+2iZR9vpzf6RPDQrFcqCXT79z+w+t+vKaqgRFq4TWwz2U72iH/sHzUrcrlhA2OPIjczwvvzAEsr3P+xn0aw02NOysYPsrFIoJsICE+6FpH0Arcfk3fvnzQP3V07YynrRmofwApeCiocWMTFQjygQ1eJFnQGpTy5dEGTnEciAKZUDBLgYqBwZCWxlDip9oPCB2KhNOuAwTEJIZCqL5aj5lgFdGC0fz4+nanKf9Fqaq9K8GU2qba/bY/XZQEO/TBqQdq+KhKBcNpLpfsc9+R5P3ay/Lg35gYcoBhklhTMJWhpLeNMAo2c4I28u2MiDbuwjLmK1MZPUSW8xI1jDsuMoy1AR8ndf3LM3vO6hWUgrFihMUijavqbPj9gWpo4kJLUv+dV2nOdl+79w7SkHqb025m3oar+qFpVC2PzWn5POBws8N7AuZTW313rHbc5/XPqBQDBSdE+6f3PnyRks3u3bgCRDkoRhj8p1ibCvj5hkJjgFJjkjTTJPtZDtMk98QzFf7yZde8GE+9m/BR36y/Kq3YzWQEsKv0OQI931Lul4dzV8Z/V6BgmzOCWg67VdMWYNpv4Vp1vTp8V0ldZD/XiMEtMhHSgMx4O1o0Nm2eEU5zt/Lvw6hT7srH6Aw6JQo26y2MqI6nURYCEhtQ7dPE9kxKe7/ZoQ+cNW7VAYh/bgMge4/VsqjWCZDI0mMl1/+zf/5pqE8TK+//bY+dSgU/ZMx+mI6DDzdY97LOuDSh6r7ng7y2Jpy+5OHH3t2V0+xlbu2Hx5ND0KhbJ/Hlxe5X/vnBti3btfW7xcD6BMqIlAoBorOCfdLhz+yMbhaSJPkJSGftpUhrCpDSLCXy+VApyR4amErgz59yfKF2crQQYKgvCLJDxDReULAVBQCx8ZK/jBtI+YwSSDRZZ+swh2vWaI5J5496WynffIrTdeCbYcyXZugvO44yXFjPtF9xboz0XgYU4dnVulO/va+6X208z5EXzagnzckPjI5laQwyMJZmoYJzzJ7BgvKdDHQKcRkOyfX4zPYJC1faH7OVoYHbeUkOQhlAICgnAJhXzFYQPP5VBu3iUdfOasv7wrFcqCv51C9hgynPjaWtE63++CarKJ4npjWTuYJPb1WC7YPfgPm88XFzmj6xOHHnr1f72mKCvRpk/ZxrX6FYpg4sKDlbvOC9jH7SwOmFkuK+HuGBQe1gUkND55a7M4Co0bBTEmgUp8eCIFVpQCmPNiqD4hKAxv6P40tN7LjKha6v/Ogg3nwwTjYqj8WoMFgp9xOqGUpMOQqwRGuOa4VwRcNDfK5BlwY7yrNJOqRNMEgjk0aXOE6ZO5pnhEbmUp1u5CuQ8YzJ2dA+tuUiRNvoU7ccfrAsMjrQGpTecEk2ZUEsx+Ec0FESTBTH9yUVhndji0r0nGBTHmAUiCBSe2Amw+Yaq816PbFsjNjOmBqmSgJmIrIyjBWzyO44Kk+yGsY8NXYINMtt+vVm2/VpwaFYvHRFzmxq1U/nPrIPZ67Dqo5768rcpXlKI9tmF4R3hR58NTtebUTTKfezG111E5myWH7RX4tv3M0HYX5KNp3oFC17wy8OlTdPJBuqX1AoVCEWDjC/btv7MEjL77d3gUN8WMCSU6JcEfdCcQ5eHqHkeB8X4dShR4R7Gy5VdE78abBkknEIp6q8WUtDd+LctGyOfLbBJrzkDgvBxPA52d8HnF+ZVnpdkDSC3MM+cNVJt3ZRwijP9ZNOZwSutEQ6+uJYeZc/lmPN1D6I/Ux34fZLF8yfhIWXXs0Nx68sF3S9NjOaAO7poK6hu29FvxOC0puczLanvOObDeFcRWWRHZ+sUFOgofzMhFeZk4IfUe2e3IfffrsCkUJfECxzGXaQAh9Px+OTgIdXCjOh1Y7AX7kB04BvKYPtYppsVFFtPX9Ml9Rts0lbQ/9/H4Y2O05/8NLWq8noHvCfXyNm9O1bFrLmpMDD058h576Io6P+tDxgZQlH4h7eEBBUavu3xt9Zq/dluHpHp+bDmv1KxTDxAGtApAIdoloZzsxhThVkTsSq9yQKtgdn89V8yFJXiRplejjOROR2mBoucoyFAr9gFhH4xlcUh57DJzmTJHoEPlCl4MUjpT3aRu+CZ1P2cwsbs+ZHhmpmtz7fM1AaKSRtBjpE7MS4oHCPbJ/mdVfnVm5IHNbMn3Xk9OMU4ublLrdkuxmzQRfOsx4FJzIDldSFTqZB4Fgt8Q5HdEICXivPIcq9XzwfQvLN1TUA0lTIOUJoe8ulAWZb8sgkvZtebjD+oED+nKhmImMgooAbqMX5roX2qYq3E1tAn0xHVwjFGrsPouQD57uLGG97ozqdRe6/5LjeNv1aYOlHp1ytxMz1pmelIr8y4iHF0DRHj5f9IVT2mUGAxUDKRQDReeE+4ED6432v37dtHpxL4xWArK9VIwTkjy2lQHJVqZAta1MqJSnxDkhyXGcdJCOJ9sFKxdaBqZij9YbTv5DQN5TuxinLK3YrsiQDiCUudrJrJPQrdZZIrSHnxsBPzRi39Qsn5RsNwMpd6qOzYR1T6lXbNjOa0XfwrWyq5tJBi2wvevJOLFMOCYM6nHNTvkl0ZHtazDrIIsjwA23aEnZynhW268b74kBAV+q2oHFiCgOhCjJHbldWr6U+VXayoTzUGExQ4j4gtw3STW+KS/eBj85+uOrertXLChywmlTq2FpCAoFxx70NwCyzAMvD0AROLJLbOZq25aDB05rFbK7YGSpYljI+1v+tUbONeRq5R0NvquYAjoAoVAoIiycwv2AmdvnSymSnJPshCxHRz4zWxlL8kj7UtV8ncWM56cpwR6S7WhNbAwyW5nSh57YyoiKeruNpJ4nhL6kevf1wUj5Mk1bi2Pi86Dl4XJScR2LIJkZIc+yllsyGGbw811D8ummKmanag5tRCIf95D/DA5orjYyWH+MKdsgWv9O3b1u+wW1fGFKd6w/S0N4stoOIq3ZM9iYbi2NkIQudVcT5zOPwNvZtb8j29fBTsbWkZmVdA98zFO2MnRLT4iHSneI0onT5mR7YPmSsJVBYitD2jxQw6dsZQJCH4u0ITFY0GprX3fj+3bh0uz7n7myr08dCkX/2NAqGBRBsanV0DqBkyt1H4TuBxVylfuxFtObNljqCe0yioZwMQO28j8s+f4wDNeq6EjPee9ol/HotX/0EZdEoVDUQy1lCl59zNcEJDkGvuhGslYxaQsaWd8rBR6NPdzLgIU5g4+SuwQrS1wG0a89VqmXhwkBiZ70cBcHBvw+noSz0zrJOZ/PRrMHwNpslGQbo/tCRXB/XaM9ULW/UzJHZHtAEFNClDY7QIcDCGa6epGC47p5EjjUZPZY9i2v69a57aZpH6IMLxyNXD0iV5PPu4owPm4XesEHTEVaXiMq3MfnIx2YmbgcLMhoSFYTItqT1iCQ5DQAKTD7F8tqB0pybksjWL4U+dlrKl82oZo9sJXhVjWSGp8OFrR6Ht94+NaX4PXZ9/93py+BQqFQKAaBRfDSnonAyYlBGzz13o7LuzXK9742iEnrTb055W7b2q0VLSMnlfOvRR6x59TDAyM1D69o3ktzvdb2UCiWG0q4A/zY+F+JRI590cEvl21lDHgbGAdRMQ8JUj5S1iMnv7nSXLJ8Sebn04HAdgYg7SNPj4EHVqXbheWyCvecNCxSt3xZZjehntZoPG3nCTaJrHXzkxKQ8ybrXTmyKbb1JCohWkMlc0S2L/CZhcLfjjd1BHs26iMZ+oCinnxPVaVUH6ZUtvsBjPLsLd2Y5tUHTLgASoKdfKsSDRDYvuDV+GvAVfmzDhLEtixBZdFRG2RWNI5Th9LeJaUaN9FZyVTvPkCr4B3l85XV7CVhHlvMFAOCTD2f8HAHMtCgUCgUQyFuFByrpnDf6DCvXJV7bw/HmOd5fwvpTBss9ZTafyjmjC0oBpV2Rr8PqH2RgiIfiNFYEAqFIsTCEe633XDg6VYTLCwOTAVpXbJ8aVsZHi7QyZJTtjKhxUyZB1fZe58YRngLli9oiK2MK2vp0S770ANXvqdsZdyYAgQDCRCq9cv9KWG4Rgl1p/S1nJgPdgmclHT8WpXSvQsaTbKEmbUcvk4MV7cbaqUPi02wS3UjKN4RyWCLC6CaWfIdcba2XaOku513anL6UYqZstwQ94GSXzecNpY2DhIeX2oiYp1/2WDslw7jUmc4XY1z+5cKWxl7PeBjESgcvaCY98ePsQpd8nD3+QG3lWEe7y7RMMgqmaee8YzQL0rg0g4HC4xR0l2hUAzifVyrIMI7K3a8Gx2SP3lQ2txa5mjHx3jPKN+HWlC5b025/cN6Oik6wiYUMQt2Rr/HdKBHoVAoFCl0Trjv72eDrAgn7mSqb2or4+fj4KncTduR1VTBToOeuu0gJqo5KW8JcfZnIIemli+i9Y2koI/KgdZTh5Uh7eFeLIkHBoDtwxS6hrhHACdWKREPtraAcG4R6Y71ljNY87crX1cI/MTHvu1uMj2Up2f4MyZv82tY+Jy7LyDAlF8/TF3HgZ2Mw1pZwd5hpM4KX7LzYb764NObvP2MJ9gNVd+vBWnbgSgzXTXQs6LGVob5qwMlrQNbGYDQviW2lQHZ3kVYxrzibUvUqtmxwmKmJOBNONBQ1sUKnVkKhUKhUDDkJHTXhPthm+f2rAns3fXZLZh+gOrkgrTJhnZLEbt2mqaf9f3V0OZoenHUX/MgxQ8N1ONdoVAoFD2ic8IdG37h/7M/OnpO+b2X2i4UVa5TIlwy+JD8KYitjAueCqGaneYFMEHAVHDsdl0A05hgD/3kiz09/eSZxJA4hwnsYkAk9Fm5iuQNDRDpf5zKvSQW0SXp/L1D6tBWgs+atV1ZgMZKeCnYahOEATuNIf7cBv7+H51e6YvP97/vAPyXP/h+MPv2urBm630dZgt3SUl3+7f09UBkS5P8WwhcavggiRETpPuT04Sq2g3x76eRIBB5fANjpqwAbhEDXO1NN3UKcfBEvCW1iVIcAoU8JIKnBrYy9kSlKnSvepfK4M++eouZsjJD9Xw5Okk93KcdtqjBFz9wdQdeHweFUygUCkVzKEE1R+SWF3t3fXYXuid58/vkdoP9pw2Wur1AZOeG9kwRJ0ZteP/UF5C7PuuI93zK4zJsQj/Bgu8eleWLGrRy5bEDGghcoVAQqIe7R9JuhfutOxIsbStjKTXESDPLPdwpGygFOgUovdTHtjKjxQX9LVq+MFsZqsqnxwAJRT1ARDMLAVO5Z3yQLiflvYJ3PbDQcJsRWxFDLUaYup2o3pHGkgW/3LN4oSI+2CZeBullpmbbaRASvmT6k3cv62nnLIdoWwVtLY3RVXHcUV1Lyym9a2SZuxHXB18lGCO3d0Tam9KvPexjrqtl6XW1V6+wcmQPd1mZTu1dZE92STUenomc2Bc93C3rXnqukzJQ4p4kHwVMpWr7mLBPDha0hPc/++/0nFUoFIr2oOTU/JGrbx/pOM+Nvbs+e/TwY89OrTqfMVjqEy2UeReUDF842IGWHTs9ZPvQFhSDNptd9vnR9I1c7T7LwMGC4uPaAxUKhaIanRPuL/ybr8L33TDMyvC2MgUk0hoqbGVo8FSwnLtsKxMFTE2Q4H4+CmAaOi8bYisDwIOeCn7tbECA2srEwVb9sTAP9+rtqIWEJSVLtfsaL6LlBdHbyxCWlRDvxnu8h4FVDWNkUSDkeTBL3uCTdgz+d832okLaCMsUxaBMol4d54qp+pXSMwHZTuxb3PowjZSiPSTU2b5BumQ5J/SFGMSQOE5/+uCsfQQJ255Se0u2MgGZTrdjy8oDRwzIcFOecJ54pyS+84z3tjKlur5WzV5lMVOq501Sja8nnGIwyImBUwmi56UOy3HHBNvkakH1HG+7A9z12U0NtKeY8prRFDnp/WAP5/M9MJvNy9aU2+/OQuwnrsMb2uUWH6P+sD362c6vt1AMNnXZrsdH+eb32C+ugMWMni8KhUJRg84J93NnTgN8/03DqwluKzNewmxlwkCnInEOnhpK2MpEdJ4YlJWT5N5WxpQBTMHLukMVut3Yq9xDoxSnwWfLxOCnpcUMtYuJ8yvLKgVWBYhtNNxCMs8Uzu4YXCswkh3TNjMYxK0FchhseWIdyMS9CQg79HUMCX94wwJruiryB4ymm6CviwCBDDeWp0Xp64RkIhBYxhhGusvzEH+BwMoh9FVKupuURw3wPoo1gywm6E8zk+1svvRbrwyeag/OoD1/K21lQtW4bCtTbBhZvhByH0nwVHKFCtTsUpnLtEFQz0ejk6Bsu2IYyEn2Lyzay7dVCT6izadQ9IM27Cny644NnrrVcfHzoJJHZjiGae1kTmpPUST6/s7o5xOjfpgPON3bZd+HQu2uFjMKhUKx4lhbuAK/994cUyc+xyXxE5pRxH7I40UYmU0QLxQQ0g3l2JLZBPVJMWQXw83MkZaLl4EyvT5vFI8FheMq1a9Cfhxx2uOlhhCgNdPY23ytmNbXi+nAGpgD68V0cK38HU/rdrJ/Hyp+ITkZMq2N0/Z/HzDF3/nv+qjMB8rJLSt+R0XNp3yd2y7YPl8/Vm2vG2+pM95mDSJfe4XtI+tFvZl12w/GdSnUc1jX4/7B2wHC6SCUbTz6NQft9gfJ/gfj9GDd9pHURNoXwnaVrG0qJ0rym9n6SDFohiQhujy1E4mYIMyb1Hp3dQgiLrAySENdJh4YEKM2kO3Sx1LYygTlMrweWh3aOrPxIxt6wipmwN6CKt12tekUy3xe9pDnHT0d6wM95XvPVA1y12fzYKvT3mdPaFdWVGF0/71v9HOs42zzfvxUPui0xFWrz8QKhUJRg4Uj3M2lS3NNfyySDsn2krQ2JUlOtiHbI6BAUEOK6JaIcmndeF9EidR2ZHuSqAeejrjekDyFMnmFuYkGBqTtaC7StDbNZEpi0xOgjoxfL/8my0pSnhL1jqwnfzuinsyPifhDAll/yJLzyXXS8pLkHX9Lsh74eCsKbtl5/K/ZejoYEubkl82HgyjGk+FjAt23O5koib6+Tgh0oT/RZet2sgFvxY4d2tnQ46qa2MATCDZEM1Qq9VVnJ6eJSW3DXPTLi4KJCPZyXuzEjCQv8zBhfoaWx15ZjEzil+R5TMpTMt0wsj0oQ3vfk1z9wG2NXi4u33SrnvMKxWrjiFbBYPDMqhzo4cee3YXC47prbNmglpNiWnX7KVUQKyY8B7ahe9I97/vzJt37HMzf0J41KOxqFSgUw0PnhPveZ3680f54/fVtB+goJfNYS5KX6zBWlXuFOKGLvDodZYIeYkV6lcoeDCPYQ7IdablomvyYJOKcHbeknqcDAoLqHYPtgnixnIysUrrDZJNEyHsilSvkS3Je+Hudk6vmwAEw6+ulqp5O6xXrxuvX+frxvCV5PVkLoIw76Rmuzf0XAUV9FYMg6+kpbyc/uBKuWxdIczsddH3jAGn/A2V/ySf3lcXaBF9lmFRfn6IvM5W7afIFRKw4B3JFkkhtqkLntjmMfAdZNZ4gv42gZo/yQyOWgV2CMXFsZF1M6CNNmw86NO+va83Seuq5P9WTXqHoH30SdOqLr+gLD/eU70RWHjZY6tEp01Z1u2Lyi+9yku464KRw/XtXa0GhGB46J9ybKvzw4MGNlov0XJBDqSTnJHnxywjy2FrFpC1oJOV7QPILhD5TjyO1lQlrRgoDGZtcSIMKfNAgLhsj9CvI9jAvI5SklvwyzSdGiK4RAjWfCLG6HqjjHTF+ICDk/bapdesxib/O0zVrhHSfqB5W6SpUWAmZdTI4sb6enqQvG8LljEC3PkDu1wRk+iygnvCuz/ForTCRjZIxLVoMGYlgF6xVDCfTS9IaBZI8QXR7IhsrbWeMZEVjMCb1TZ2anRTBVFnVoJmDsl2hUCwV+lQEflyrX/tBT2RM7nXeByEzqa3M1gxpby9kxyuCeSr6OQ/yPtO1xVIXSnftywqFQjFAdE64/+E/+6fNCjxXD3cCTHqqBxEw7fKUrQxC2u88VrObmu24rQxKtjIRUS97z2PSdsYA1NjTsDykgQG6MyUVzWQWG51NloQ3hORdoxOxElkPyHc6HViLl62vkf0d6U4sRBSkkwRtItWxSMIL9b1GFOosXQisXqB6nEaYkl9aSFZJqe1Txx+UowEDj9FlK7KVYZlThbjbBktPebJv2mJGDEssWb4EqnUI8pXyAkbEh6T8+O9YKY9z8nC//p239HxVKBYfuz3mvaHVPxisojK0D5X7YRuAuQ5TB0td0NgYip4x6jf3Q/cWSznp/viUFkt6HVNMih2tAoVimOjFw/3azQP+onZMOleS1sG2INnK8HCBdbYyocWM6OFe2tMYifBGEOxdWFkhyM8kfOgBKm1lRM94CMrKUpKV7jC7aL2OBJ12SpGmdUR9RKyTeaZoN5GiWil32h9MQexSK5e1NWEyE9i8TNDeMAEXLixMJpeyh0kGBg7822kGQGxlprxyEUuYVFDT1DogpHV44cIoH54OSUJS1Ese7l71HtjKGMEf3shkfqjGN7FH/TyU7rif3aEnrUKx8Hipx7w3tPoHg1Uka7d7Ou7jlQ0xW7DUJ7QLKxrgWA/nQt7HH28zwQEMOm1qV1IoFIo0eiHcL930wdl3Xl/v5HOswFYmVLaT+Zi05hQapmxlJAsYSFq1lMFTXfkgtrRhivoKFTrLNyoHYoU3O5tPDQy4uZCxNKVFtSVZJ51SBCbfDtqboN5uW5zW+G9ItpvZyNTlR1S5Lbajger+VBXINLWOFJj/Jx0PpBl+d8UIz5WZKeKIZJ7AVoYpxFFYh4xMr7aVkcogL2O2Mm4kpE7NXmUxY+SAr8Z8awhd/NUL1/Q8VyiGgd0e897Q6h8Gegq2Oc07zKk5HHNOzp3so9/XWE/cM2V6e9YapE08rWfFSp3/+X3ggR6y3hydC/e2nOaOtujKQ69fCsVA0QvhfvnC+dl33t+fvzyeK9fdwhRdJlrGICPiIaUkDwn9yoCpbjusC2AKoqd6TPxTAwZZUR/bxUyWXzxLld2MCYUZmW2JGW+Rca+zo5kwHR8UFNr06l4iiO0NlYMsU09QE7B3ls8toj4BPiBu5eCRJeXZyJxBWUs+fWWGXuluHkG2hqHtEO4DxFZGsonBiuCpwfah5QtVvUtl4FcnqLKY8YMDsVIew6CxLeB933th5n0v7qulvEIxEPT6Cf4y+vhqH5gY07zDzEu52lfw1OOJ82EDplfJntQurGh8Mj727EPQzwDsgy3fB3Z7rEb98nMY2NUqUCiGiV4I99euDIp4eE5ejJOQ5KVVS9pWxtm3hBmEBHu5XA50ShXuY1sZ9OlLli/MVoaq8oPyiiQ/QEQN13rGh+mSZSBTXmMLl3lJzQcyrTm7FOADDYqyE1CVNyXb11ocCIEpLGdmmoIypgKi0rOY6bcd6Y7FaYo4c3UWvyZUktsz1qSJbIG0BtmTXSLGY1/3Wg93ZisTEOdMUQ9CGeI8BcLetOzhfvWmw0qUKRSLT7L07Xm7oa3QO06teP/f6SHrTUuuh7hnhrQe1i6saAkP9JTvgy2mpTZpCvXyVygGigN9ZJqtH2q0/5t/964jH/kfH2vlwvLO3un9Ww7LFje5ktyUJI4jrUu1aE6KjdnBnGQeEzsFiW6MXYC5ptUyaQbsunxntPsU82z5OGHjiSebHp+36ZXbkn2hoJ4M34jsY2y5+XEVC93fmA8aGMPKhz6pgk83wOsm2M7Wj2AKHbmYNwsSOXAgQMLQ5y9/qf5Z6xf/2n/xYRD9t9c8SVkGtvTrM0syC+RqSIIyKw/XRDQgZXxAhMA1JhxvQcO/hLB9lA/8jNe9+9d/5j+GK+eepHVjqJf+LN3CzG1juW2rkkbkm9m/OdmOxZSRZRmSdZMXxtirk5XM22uCsed2eD3w29H54pzGqu3YsuJIXRZ5f/GDdqY8UmOvfbxcRb8o1rk03VW3TKeYp5eO8liKdcjKMFbPF/3PsPK0dMO8dGHmL6zOXNnXJw6FYlgvp30NoOX5qkK3rIs+8MyK1/sJ6Md7OVe5HwuWbU2Zxu4ABs0US4Lcmmjvrs/mL2RdB5jLB6C2WrJG2oGaOAlzxIb2okH0Y70mKhQDRS8K94vnzzW+rrRVlmvXrqyLK7DSez0OdBpt69Kx28i2MpFdTDJgapBfQftjaA1jRMsXSnby/MLgpykrHbsu9HMX8yvV/UioPkIsoptnynfktSkGMV3AiVnLENXzpJC/gkhYDgkWQHGCJpFPdd5hH4nng4ZM2oNEATfL8QJJkT7Prw+maUdIq9qlasBwwuInJ9bHE5ZTzsPuF/PolwNh5ifoJmVtVinOyRVJsIahtjJmIlsZmMhWhpbLgGAxw4KnpvoJ1gR8RUE9X4xsGvPdtu4XB987M/O+Z5VwVyiGhJ0e875dq7/95/kpsdLkhCX5dnvI+ujeXZ/1bf7/s/cuUHYc53ngX3fwfhBDkKIoQhSHkmzLlB1B8SPr7K44XMfyI9oIsund+GQjgru245zIAOEkZ9c5uyKZbBKv1w4ImYwsxSccQLbP2qZF0JKoByVxSNGyRVEiQEoUJVLk4EWCIICZAeZ5H/3vre7q7nr81ffRfe/te+f/yMb07a6uqq6q7r73q6+/XxKOXYyBQzzuGQVjakDl3lnQ9TzI5xm0iM+w1jCItpjmZmcwyotBEO7Ts6dP5cpg/UsvFflFBavV1e2eXVme6hYxbpKToULcSYvgsaeJ16myfYYUQjtEkJ7q5gSAHfhVK5sk+c3gqcnGTA93O61JKgZgku4UuZjFL7ZrK9Lp0lNiF1wrk87hYXQTCyNid7sEOrYor828M6srTFsTKr1NYvdlMgToiR19W1bA08T6BU1yPR7XGBHo0QLqGsCIXJdLXS3NdWzuw+RzlA479HVHsEjrlkFGyb7TCHRiXfj2x1WwCHijDtQkjHAnBoAg8aGtc0HbDqdRXdm24f37Fwt5cH3uU/yDgsEYHQxS4TzBzT9YDJqgKgkOD6Lpm4seMPK2LvIYhbdD2J6Or4XwWaAmnYrAICcR+Zk2WHDAVAajxBiIwr2646pcx+PGjUV+Ucn80RVaNNhku88XPU6jpU+Dp1L+6OgJZmop5UnVO2rBUx0VPq1cb+nXjvY5eOqUWGW4gVWjiYaULJTEYQNTorGeEo4pGakR7xpZSamEk+JNjXS+pWv/eGjT2xtc3/Z8xLtoPx0SJWaNkc5qZpP9icWMfwomTohO9wEMzpsf9JAGagnAHYOxQl23ggnS8Y1BkI7rhravHqgFk78YL7Xm53ipR9dLck100BVRsxIEtS94qptOV6E7V5rI8nAXntjRTn/rAVOFfqxZV5+a3fB7t+4GhGd8bWnh6qIeFOvGxnIdf2apzt84GIzyYJCkHZNtKW5YK31fwmC59wyo3NtUe0xA52rQ6fEHnpvhy4ZRJJQdx9yAit9fUD7TA2xCfmtrsJjmJmAwyouBEO6vns/3TKvMzxdWl1377pur16rbyJ3YkiRP9xEWNIlCXKOOEmVyhl2MsY6ZKnsQZMBU3fIFqbqa50QR58Z5YzZZS9nKoKnixZoiFmMVbw0NAjIkHhtBRM43UpWvQcDbhLxhu0ELp/PZ0xSpnIfuLcORHHNt2spkjumkA528SXsh+1rwlkMQs62CVwpLuaxty0uit1yIWgdokuzJNtMGBtWEUrwk4zsm2OU4r0bjPbwGqkG0rDb3Nf/CqvwcXRtY1yanugj1KajApJTlS1u2MvqdyCXfLfV8C/Jb+AKm+mxlqICvQMckiMulCf2gUS/MTuZNf300F1my3ChVsHAGY60TLPKL6MAUgfwKfoKJAZQ5KDXgeAmvgalB9Hlz/EuVezd2Gof5kmH0CIOahN3tCSbcKR4aYNvxJPIAn+v8xhaDUW4MhHBf2HxFruNx/fqbC65SBhOiKcnRIpQdgtwlJ4XXgoayZgHbPx208gh/btRtZZx6m9UAor5ATyqYkwZu3bI93JPqJWRkSLAHIfGYEI61iGhMlb1BQsJHxLskICMSMrHgCLS/iT1NvA1S1XEbver14y7cToYYDZ0R78K/vS1bGdGTy6NjWxlwbWUQ6dolHu5gTZSIznzW7W3O+WhvUti+6mh91sZfRK6rySFdtV5TY1j9TcZ6tRH+BUWyJwR7LchFsNM9pdvKCIpgdwlxsD3XE9Ka8kWn/NNV0YQ63akDZUUj0CX1BbZQs1PjysyzifrqUmHPODFWyUWWXFxlD3cGo2QY5I9UJigUVzCAMofFkqQfEwOD8kOXQSr3dspn9bjvZvhyXNMYpC3Hntw30oh0HZRKf5KHz8DAAdgZjJJjIIT7YiMfByhqtULrU6/XLrRMhEgR664verzdZyuD4FcMu2p20SKdaSuDlK2MQ9QLujwi0KZtK2PmozeOf9JBs5UJCXRJMq42AFckAdkIlb64itFfRVBCvCgiPibeMSbj1XpiTxMSoYFGxgemGtmxA9H+GhUlWtsi5aVlitDU63JdX5IAqQApEVgA5e0JtAsZTK0AyirI7rN2bGVakOtd28pAu0Qz6Tlj5kg4gTt2RHqw3iCwJnEC019dEeqYjL90gZq5pMR6oMazUrDH6/G+evEEu9VOlq2M3t4kQY1kHsJvRSP0Y0VGgFaqvwnLF/rNB+H6vftsZWxSPvwcpW3Uq5swCBaLat0NF87memX2YhQ0dZq/djAYpcEg1bI3c/OH6PfEwwxbkqRQVhrD8lw6qlT5PRsbPCLWNAZ5Hby/qGtkUCfAb20N5Hkm8RA3O4NRbgyCcJ+7cOoELF73tq4zwK1bC76p42n/LhSZAUxJBbhjKyMIctdvK4NI2cq4KvvUttklvBEEUS+9rmCVJyxLGOtcfbYyJHGb0dSQkvB1TBXAkoBfaYQLLje3rSgyXm5bjch5g4gPFcKNVClct3yyG2q9EZPxFgkfk+9xMNdEIU/UVY8zq5IJYQtrY0ISaM92o+s7Gpv6uCP6EbTJFzT7x05rjweqYpStjEeH7lgT0Wfqs5VBpy1Qr7M9VuPgpGj6p1Me65RiXSfX48mZhhnANJnUURNDBrEeK9Q1Qj0co+G4jZaIXG+kb3D0lFz3DBSdyDaDp6rRIlrbyuh9Jcir1+01KhArqainPNwT1TsKQQZqBe8YMtZNQr+xurKtufWZohp404VXulZisp0Mg1E+KLJxZg39MC8V5m69aRDqdrYkGd424b5j9PJ5IJ8Fg1KIF/U8YFuZAQ+jAZTJCncGo+ToO+F+5NRs6Jm5uphDeFitFlqnRr22sVUay1bGVrZr6y5pbTpTo89WxuOL7fFwT4OnxvUD19LGUNT7FPpglevUAykfeb+He+d6bsVix/YzkQpeke2SyFyOiM3kb0jGKyK+GhPxGtGpk/C6ZY1NyOvEa6D5xjeQ8I8PDIU0BrZoGtP1wFZTQ4ugr11DWIQ8sd/pZ0G0fyuf9pYVplXughhhws3MUKGD+TZC1iLT6X3WQHOCRZ940SZk4ngCyRsXauImHUvK+iUh1tO3MjCeFOqrcr3NoZBENPBYvtijz2srY3ipI7EPDTI921aGqgO9zbCVSd4SaaFmJy1mYjuZnc1sRFEtXN26o+sfExwwlcEoLQZF4k0U5Ns7zBgEQTPFQ95iiB54TrbJXMmrOTNiPsU7eOSVEoOK6zFexPOgeY0cHeC1zG9t9b8Nev3WD4PBKACVQRV88dTJro+VljLn9//MZIHV+UqjUd/g3Wsq1+ONVORFH+Gs2cqASYIj6eHeVsDUOB1mBzAFgmCn/eQxzQRoRb3rI097uBdDcuk+8LF1R0h8KhX8svbXJkWVKj5RHFcVsVo1lfFpcEv5t0EQ85j+bej2I0FqQ6LbkzgqejSJ5E4JWV3t7cYQ0PvL1+6EBZAzLqBl32EHacme9J6cOkVM+jud7LBsX/SJkrpvaai3H3RCXev7qkasx2NkJVWrR2NLX9S4Wk0ncTAYNLmeeXfUlO6OVzqAZREDPmsY11YGNFsZOiirP3iqld4Ojqqr3o3R5Vez+yxmkskBgfXV5Suba48X1bK4YVMOhXvA3zYYjHJiaoBl71njbT/R5/KmB2wnM1nivjhU8rEyaipOVgOXE8cGWPbEkF8rkzx8+q5w57d+GIwhwMAI9ws7rs11/NiFC0Xe1BpBo7EhOwm2Q5JrKmavrUxs32IXYBPs6XY60KlOpIa2MpjkT1m+GLYyuirfqi9J8gO4iudWnvG9gW5JoxPxGlEK8u9Svbm45GlsWxMuIZFajwjVqr1IEr4RkbOJrUjDUswjoZgPXKW1rKfuK98dV+u3dyGD3BZuKwMtgqd6bGUciXtK9tr2MI00KKlBoifWLo3E3iVVpmuTKKuNxKIotCDS+pok0+0xsmIS68ZkyTBAEB7urq0MeMh2N51r+WIOAr+HO7UNMzzcjXXhEugWcS889dWmDIPGuqBWu6KornvikYcnt77y/a6Pf2W5XoYfcwwGw/51HBGw0wMqfq0rAif6XB6TE35Mlbx+h7iLGH3A/ADL3j3k18o4+7j3dSJtRr3RwGAwSo5BEe4zr81dzpWBWF0t8qb2TL1e3dYqEboBJgXhi+6zldGDp8bpaVsZJ2CqhwRH9KnZbedlQoHveLib52WpphE9wVaTc4HiFe6dwgjQalrTJGTrUiMlWcP1QCPqg5CsD8naVY3ATdYbplJ6NSbk1V+dkK81VMBXDAO9pmQydKqOFp6gpaLNtu7GVkZ004+dB0/FxBYGEy/1IFWoJ28l6G8qNDRCXZtAWdYJ9cCaaCHU6tW4b8qsVu/6KghtZYQgLF90lbtzEftsZURGOmKbMOxg7KGEhp97ul8/1lTXe9XsYJ6LRsrXlhZ2ypU3fugPvlJEo+78zt/mmuBdrkdVvP/l8/zqJ4NRPgyKoFjr5EQ/JxzmlHUKg4CaeCpr+xzjQLeMPmF6kJdhQdfyIAMhv3+tDpy5W2/q91srPIHMYAwJBka4n6/le8UeN268oajKIOLFlgp3zPRedwOdOmnjfFQa2laGIvTpgKnWMYha8NRU5ylIyxdd5W6WRxOv6LOnsf3cDc/4ciEm470WNQ0IlqIFF+Wi1M/h31gJHYR/YaWufLwbKZkfq6MNol5XZ2NKKmO3JwDgeQvCYzlEWAC5+fosZPzXgIlWEy1ZKuiomEAFLa1pvuixf/pyYKrT9QkT9TnsN0Olrvn5G6T6SBHrGYOklX+6YSvjD54qCIuZbFsZaMtWRq+Xrp5Py0uDpxqjS/hsZRzP+NrK4k4osLfHX3w61xfpM8vs4c5glBVKJTYziKKbP9LXsq1MPwmKYVRI93uCtqwEDqvbGYzhuGYm+XnWN9zDw5zBGA4MzFLm3Prt+TIIgokCq/NsEDTWt06W6aluEeMmOSkV4sJJi+Cxp4nXqbIJgjP1XHdsZfRjzQkAO/CrrZh3yHakCFfaw324oAft1BXyOvkbEr4R+R5IQn6hri0aSb+o1g2rkoiID33DYwIYsYPKUYFv/ekQM61f6LFm5mOjXVsZD4jYvPJqkBVV7R1OSsgJihX1loFOoMt21y1fEk9+LVhpkMMnfzRgEOcivLIt0toMMkoT5m7f6ep4d1349sd3B4uAN+rgVdlbVyZB4kOrgKkrVzVXniisdasruSZ4L8rrfzCEHoPBaA93D6jcNakIVAEC++V3Ozek5ERfLchUUNIy2p6xbQKjb7emUTiJAU4i717DwcDf1ceypjhYKoMxPBicwv3EiXw5iOJmUa/bd998o17b3E7a0FbGJtt9vuhxGi29ZitDEeeeYKaWUp5UvaMWPNVR4XssQ1r5tTu2M5466bYy6AscO/zQ/eNjYj4k5wNTMU8t1TSQZ0gOd1x2ppq9A1sZynM9a4z00FYmThu3p7U4RDojc2i61i+6nzvpgW4GT3XT2bYyaOad4eEuPLGj04OJgKlt2cq0tJgJGvV1jVr1iiJ/OAXbduT6AXGxyoQ7g1FygmJqQNfoWlW491XdXhJy4l1D0C9lU5Mf7VffqQkHxtp+DoxSnJ1BTSLzM210+5bBYHSBQRHuJ6rLS3Bp846uMxCrVbjwz2+eKLBOx+qtSHdsSZKn+wgLmkQhrlFHiTLZbxdjK9KzVPYgyICpuuULUnU1zwktH3qr6Wn1/IiS7N0CPUsXQ512ZXHGXJu2Mplj2iwHyfIKspURTKH3buSF7Ut4uNOWL+3bysR5gDOiafV8C0W9L0CrGrhkHYyhRlrMVBfm36hq+2xRjTq2dHmy22NfvFzjUclgMEHhw1q1lemXf3uZ1O3jZe8UNfFUJuXkqPoU7wYGo/fX8swAir5tjTb5ZJ/KmeKYFgzGcKEyyMJPL+Xzta1cvlzcFxaE2fZU7pqS3CTJo7+mmtghH4XXgsZjzUIFKCV93zE5lnb4QJ962Vbr+zzcXbsS9Hq4M/lePFopzkWbtjKiiIvFk3cb1c/MhVFEx2jktmkrIyiC3SXEwfZcT1ToSJDkSByriibU6U4dKCsagS6pL7BFwNSkBrWVxavUmDxeRKP+9SMP79587lTXxyt1u8QxHqIMBhMUBNairUy/yIlD/Op9x5gqST3mlDXGSN5ueJgxLPTi2TOISeTdAwggOtgb1a03TfaxOFa3j9bYkaKLO5rLXdqyt89jitFjDIpwD4mHVzddmSsT3LixwBs6Pt6o1za1nxwpYt31RY+3+2xlaGW4S34jadViE+KmrQxStjIOUS/o8ohAm7atjJkPX029AAL1BoVHzZ7ZDwIoq6D0uPZtZVqQ623byiCPmUJHitnICZEtaK90vYMyBo9h+RKnQS14qjZSMwK0UgPUUL2THu7p8LPPwWcr01yvLy+9Se05UUTDTjxwcCLP8bMp4T7Pw5TBKD0G8WN2j/zRtZZ+YEJ/FL7D6t0+aJTFVmaKu4KxhjBTdIZqEnl6AOey1lTuk/36fsLq9pHD/c3lYHO5U1vktkeb35WwudzPTTT8GBThHqpNzl7IJzoRq6uF+SEi4suNRn1zGwlFZgBTUgHu2MqY9FErWxlEylaGUKQn/Jo7GYAgiHrpdQWrPPM8HeW7z1YGWd3eE6A5VrxjLt2XEt++QLzWeKDKo2xlwDNJhBSRT59AfNFxvxYIwhLGsJUR/nSQZSsTH+/2LGWU5KrnzfpZ5VAe7slkAQpBBmoFp14qTW15cQdisEF+uPY37y3EUmbxxnfmIoY0SxlWWTIYJceACApJQK8lW5l+khN83+38GpCEThmU5Ye5NxhrCDO9ug8O4Fz2rrG+68dbajyBPGJQAYZbfffjt6FGAAMLmir/Of/aa7kyQTFWpELmZLuBU8Oy0VWXE8FTSdJa15amZLVjK+PxxfZ4uKfBU+P6gUt+G4p6n0IfrHKdeiDlI48tfbwZXQ/0ToKjZtrG+CeK6PLAn64blbvgkdGP0WKtI3gsX+xAqF5bGcNLHYl9aBLswvZc99vOZG0zbGXityRMNbtd/+rCfKxuP15Ug65fnM/lNXxxlS1lGIwhw4EBlLmfyYlCcWz8gefKRk5MdvNbaUAYtMr92IgFsGQMAQb4ptFcr5TLKhjwVJ/PR9pk7F1DY6Yfb2wd4AnkkUM718hD3EzDj4EQ7kdOzYYPlVOLdQg2bu46H4GNiVfu+VAhD8c37f/PXxUAjbZId1O5Hm/U7VYoj3Sz6gYRDz4luU3oZwZMBcdWBsCxlTHXs/3kNSdoAMK3HggfeQ6eWjCQULOjOwaQdO3IIM4xqywivecaaCctcTT3a28Gi9YNBvktSK/0ZB2hlTWMIPJObWXooKz+4KlWejs4qq56N0YXesZQUlZteeFNanwWFjB10+xrub5Iax7uDAZjCKCIvn6TtbvXkGdnP9T8B0ZgHM4MsOxpGCzhz+p2xiAwKO/x6T7cD/tN1q4VW5l+PM+m1dt3jNFCO0KLo9xMw49BBk2dqS4vwavrr+g6A7GyAhtOfb+whyMifr9eW93aZup2SPLUksNvKxPbtzinZxHs6XaXhLX9uUNbGUQrPTqKeiBU+VZ9SZI/LVtvj5hsZ8q9R2jT3oUMclu4rQy0CJ7qsZXhwdFTEOS34eEuyHRp/wiB3vxcyxdzEPg93KltmOHhbqwLQ8kuCOI+qn+jurIlqFXH1YZCFO7Tn/vUxOazM11P6r54uZqs3//y+WkeoAzG0OBuJiiKx9ytN0lyotcq0nsUYczIfw0MCkfXwLWwm4cYQ+GxXmaulNH9vp4n18gkcj/e2DrAl8jI3f/3tvFd6Ci/1TAaGCjhLv85h+tzZTJ2/nyRN/Pj9Vpta7uJ0bVREYQvus9WRg+eGqenbWUww2KGVL2TynttE6XAdzzczfOyVNOY5SPPKvfigESf0dbnHdjOOOla2cq0m7d1fbQZPJVREBy7mGQESVsZ4QYmdW1ljFHns5URTjrhkO2GX3s8AJAYcmj4udvnIgy1vpUeDIuZ1ctz12n33WcK+ZFy8UyuH8Rnluo8LBmMIYT6kXN7n4vdqzw9mZzI99vibh7BhUCS3oP4sX90gOr+ftrYsDdv+TCo++90H55p90D/45OMtFWaspPptcL9brbXGkm0c22wncyIYOCE+5mZfN+pGhs2vavAOh1vNKrtEe6Y6b3uBjp10sb5qDS0rQxF6FNEt3MMohY8NbWGEaTFjK5yN8ujiVf02dNwwNTegBoPjq0MHbCX9v/v1FamA1V8Sy9/SvHMKGCQEO3qJ7L9tjL+4KmCsJgRNvkOpMoesmxl9Hrp6vm0vJTQN0aXGTC1vrz0hvj+eO1v3vuVIhr1qmcfz0e4L9f79mOKwWAUTlBIwnGqz8XeyeRELtxeRkXYME6kqHacGkDRgyQZWE24tjGI63Smj4Tq7X0e43tGfBJ5b4/zl7Es7uLLcuS+C01Ca/uqObYRGh0MknA/If85WV+XK5Ox1eXJ4qoUPCFVuUGjvqG99Jme6hYxbpKTUiEunLQIHnuaeJ0qmyA4U891x1ZGP9acALADv9qKeYdsR4pwZXV7j4H2GLPHALhjINP6hR5rZj422rWV8YCHSE9g2sUYxLkIr2yLtCbSOetuIbo63l0Xvv3x3cEi4I06tJossGxlNBK/ec9eX19Z3KWOKeyHy/r5C7kCpp5ZqvGPeAZjuCFf5Z7p5w/4ESYoem0nU2YrmWHt034HT11LJAMr3MuHm0f5GlNvjvTbnuTOER4vvVTwy98NH+BLciTRzjWx5r3bR8mSauAK99OLNahtv7LrTMTy8vi5D99a1BfZ5wCxWq9VO7OVQSqwJWExE6fR0mu2MhRx7glmainlSdU7asFTHRW+xzKklV+7YzuTrd5nFAPXVqaVmr0DWxnKcz1rjBRoK4Msci8eHtuWpP1Ju5g0nXDtYsBvK4Nm3hke7kJk11W3lbGtb7JtZcL11fmLu7Tx+kxRrVkfW5/rYa9ZyhznsclgDB+UyrffP3pHlaDoJTkhlYCj5HM7U5LxP9PnH/5riWRgD/fyYWIAZU71+Zqe6nOZe0fRy12dUy/Hy4FBBs5m9GzcyDHTzvXAdjIjhIET7gsXzsOF1Ub3uSBC4+JCITfya/d99FLzjwycur3NsluR5Ok+woImUYgbhCqa6X32NK51DWkjIsiAqVpdzeCpcXnmOSERqNU47xae8YwccCZkhMeVxe7HNm1lMse0WQ6S5XVvK4MBd29RoyRqap+SHID2cKctX1rayjhKeft4n3q+haLeF6BVDVzS2iaqRG3x8pujrJsbBDxeRJs+8cjDuzdfeLXr4/WAqWUhTxgMRlcEhXxrpp9+7iNHULT5CnXX2UP5lYCd9meZnhmH+1jWIWAwBnOPmoD+E+5TA7LAkpOT/fQFH8VJ5P09HhdTJT//G/iu0bNrYU5ZGjJGBAMj3I+cmp2O11+9vJwrr7EKFvgKGB5r1NsPnGooyU2SPPprqokJQhxaeL1nWMzQgU41NTrqtjJOvbUPPkKfnlQwJw08dWP0GK0U56JNW5kC+oxtZUoAJJrVIbdNWxlBEewuIQ6E57pw7F3SdMLjGS8IdbpTB8qKRqBL6ovEjiao16SdzJvju2mAWIia/IaH/2hyrNr9s+nFy7WykicMBqNDDEAVeHAN/sjsFh9gJWBPx/7RPj3DZjg4IGOAmBxAmXcP6JqOJyn7RfZPzt16095RGShqcqZX8Ujk21q3D0EzTPAto+NxI0UH7VwHUyWtP9ugdYnKoPtO/vN8NZ+P+/rzrxWmmkHErzSXSqNe29ThgT5iXZDEps9WhvZAd8lv2sLFJsRNWxmkbGUcol7Q5RGBNm1bGSMfRkEg3oJoV81u9Zebr2sVlB7Xvq1MC3LdayvDKHqkZNvFQOrn7g4QoXdQxuAxLF/iNIkVjRCGPY0/QCs1QA3VO+nhng4/7RxW5y/sCsdgNMU0d91v3vtsIc2JQVH+7eEXaB6gDMZwQ/0I7te1vLv54+aOEfmROQm9I7NuL7Fvu453DTmZ0Q/l+WFYW7h5COo4uYb64/19Lm9qkBOFqux+vhl0cIQIu15NIMs+uYW/bTnYMSLn0a6QoqzPQrZB6xKDJtzDHy4zjY25MhHV6u5zH761qJv4cwi4WquuXNFWajMgKqUGt9ICZSsjTH1qC1sZRMpWhlCkJ/yaOxmAIIh66XUFqzzzPB3lOzLp3jNQPv8Glyn8Yy7dlxLfvkC81ngwN1LpKS94fdxkjQc2cO8JBFpBUdUosMn27HSQZSsTH+/2LBL96qrnwSjXKofycE8mC1AQ6vjV+QvviCX7zeXxwppybCzXD03NUmbu/pfPc9BUBmM0cAv0j3S/UymimJygMTVEATY7/Y0yUbL6y3ae60MZDEbf0WPFMlkk9D94qXtTiiYr+6WmlvfA+0dgrLSrUu5mTHxgQBZDZcfuERg38jdlO78r+U2vEUQpCPdXv/sdqG67MldG1Zoo5EF57b6PPolBsNS2j7uCZStjK9u1dZe0tjgrn62Mh9D2eLinwVPj+oFjT2Mq6jOtb7TPTj0QvcFWGXlBtSd2Ehw10zbGP1FElweZ6TJPg1S5M+leKDxWLnpPgmYr46SzbFuEEzCVIMJTMtxIJ4j0UZ4AWbYzWdsMW5lo+DVWl7cGtdrO5sfoAMRCCPdnP/Yfd2879d2uJ3EvVhuw3EDjOcdgMIYf6sfw7dCfV/GHnqBo/sjcA71RyU4NyWv3I0EYqHHfS0/ZabYFYgwQe/tc3t1lIVbVpGW/rG32jIC1TK/s3m5honU0od7saPe7HHu3jyAGTbifiFdeWGzkymjL2ZOFvZonAB4LGvVNiMFYWweYyvV4o263QnmkG0WiQcSDT0luE/qZAVPBsZUBcGxlzPVsP3nNCRqA8K1vFZCTkWNIxs2OhJod3TGApGtHBnFOcd/YPtGO2Dkpj8y392CkUB7o+rqmdM+wfBGGxQxBfhN5p7YydFBWf/BUK70dHFVXvZvnujp/8Xrr5Aoh3De/8tIkBN0H9bUCpvIXaMbEkNabXx+lCQp5TUulez8IE2ktM5Sku/qR2QtyYnrIyPZw2IzA0O8lKbfW7GT4/lqu+9T+PhYp71/3lOyZdhf07w2Tg8P65paaLJjsQda3M9k+0rizg98BHDh8BFEKhbvEy+cu5soIG1jYDVD6uDf/NGqrKx2o3LEdkjwlGbNtZXwWHZQViCBIWNufO7SVQbTS6x7upq2Mrsq36kur2ZGV7T1Ghs2L3vekQp0Kclu4rQy0CJ4qMvYxioAwCHH11yW/Ret0aYcKQdnAxB8yAp0CZHi4U9sww8PdWBcpSS8J9x+W6naVzex1v3nvM0U05aaVy7kmcPWAqVhQEFfGUGNiSOvNAZL8BEU/Sfe9Q6oKvLMHY1+2+weGqRHUq+TdHDdRsjE/0/wz3aPs16Kqj++v5cAdfeyLubLev9Qk5lSfxv2Dw+bn3sMJ5NuHyBqN0d3zv914PMf4Ta/RxEAJ9yOnZpMvbt9bycfXVqorE+c+fGtRX06/DojLjW5tZSJQpLXaTtrK6MFT4/S0rQxmWMyQqndSea9tohT4joe7eV6uatqn3u8LdJks5lj0/EoDM3it22c0cd2B7YyTrrvgqS1Pgwn2Hl8CQHqgaxYsoNnKCH86z+Xgs5URTjrhkO2GX7vaZJD32t1IEAFThX5suF5buLwTg/p2TWpfmH/7prMzk3mOtxTu/OWpnLiZm6B0P0yGSvXWZ9L9fmXPMix9uaeDH5ntImzvIfS47ZZYmijhufRCfTe1Vn2LRyiI5DA/c+7sY5Gl9ujuI+ku722PDtn4fxCKn5hhsr09TA5jpdWk+YMdHHKYu3o0USlBHWbCf145BytXXdd1JqJeh3VnThfj477/o19HxIVGo7al7YMw03vdDXTqpI3zUWloWxmK0KcCpjrHIGrBU1NrGEFazOgqd7M8mnhFpD3le4xeEOUUcT9gEH1PjQdikoYK2JuZTwdn27YqHjiYbt+HSrieoS73+Kfbli/CsItx0wnCYkYI91L0WcxkBVSl1PNpeSGhv3Lh7DtTsh3l/58qohUf//xnJtctXsrl335xNbVIm5q5MM1jk8Fo7/f+0FW4/6R76Scl1I/Mom1wJClxy5ASsyNjHdJs/6NQ/CTyQ2v4nre75NfxqKOfdl0HVJDSsl/j/SLd5dh/cBgGSfNauAuKJ32HnWxnS6zsMTMOnU/SlP1Nr34/EyZHZTyUgXBPbGVOnTyZK6O6WF+kYu0ziFipVbuylaEITosYN8lJackinLQIHnuaeJ0qmyA4U891x1ZGP9acALADv9qKeYds74etDEWyj2KZLceXUbt2FOf6GMi0fqHHmpmPjXZtZRi9hklQq23CJrfD2Te00+kdSJPkVIG6Ot5dF7796q9L8mt19U8WBPXahvrywg36rua4fqyIJtz1tU+9P8/x7N/OyPjyPWx4F/dcWwRFTLr3+nqXY+jRMpPuXf7IbIUwQOoQq6C7vY7K2s9FqvBmFIlfmst5xAmMUalbEfeqg328xqbK5tve4pkmSfcDfShqUsYoKfP3I2XnVuRbEPI59u4RULaP97kfhobgV+P50Q7vL8NgJzPSz4ReogyEe+Jv+5365lwZrV+8NFlUpRDxb5r/rNa7sZVBKrAlYTETp9HSa7YyFHHuCWZqKeVJ1TtqwVMdFb7HMgQ9VjVe2xk7SGxhyCK8UTVx0Uun9egJEKn2B6fPstXsHdjKUJ7rbCtTdqB1QRK2MuBav+h+7oZdjHalu3Yx4LeVQTPvDJW98MSOTg/2W+Q0j12ZPfeWAIMN2pkfv27ffSeLaMvtJ76T6zmi+7cDE+5lxmSfyxtGNRCTT+0TFP0m3feOyI/MVrh7CAOkFnXtl5WEKpI4PDoifdUtyjypObLkirp/3tGn4qaG8R6mJgj6Ue+9UFJ7GTVOinwLQpLttwx7gNQBvf2ye0japtvvQcNgJ3MDPx+7QxkI9+l45fnVsVwZiVpt/PQd/2NRHpePIuLlRm11W9tHYEuSPN1HWNAkCnGDUEUzvc+exrWuIW1EBBkwVaurGTw1Ls86JyJQq3Pe/UFCkEMPlvJww0S7I0GSE8FMO7aVyRzTZjlIlse2MoNBSpZT3uyEp7qgSHnK8qWlrYyjqLePJ1X24NrKWHXwBWgFWJ07/yPWPbAQdfsTjzw8UWlUc32pe3Z2Rf/IAVP5x8IwkxeT3EYdERThj2no/ev48gedVAXeUaJrqmiyXbalVLXfNeT3mskc4/qGEo/zosb4oRL11SAInckSD99+kx0396mf90L/rGSmhnnCUKmw3w29t0yT116p3t7qwTiRJPuNw062a/3Vb7y/7I2ixm+334OOcr+P1m8CHaWylDn98suw3AhyZbZl4XwhD+xr93/0FcTgu9JWpt6hrUyiJDdJ8uhvC9JaeC1ogFK+2/7poJVH+HOjbivj1Fv74CP06UkFc9KgUErVZv0CNJdecuJ6eTqxb9ert8BWu9tVs4s2bWVET+rMyvbeQ5AEtdsxGvlt2soI9KUj8kZrdKltRDrh9YwHcCYD7Dq422oL8zsbterV6WRkOLw+UUQTvvXhj02uW5jv+vgzS/XmMwzJ5xujVBgEyTFUSo0B/fAd+kC2kozs4+v4B8vwOr6awCqSbJ+BSAU4NcxjQfXLwRxZ7Cmx1UIRRHnZXqEfxHNhd4n7uN/tMdGHa1JOUjLZ3tkzLSSK+/B9NibdBx4cvAdkuxwL7x6h4NCD+K62p+R2enc1/zzd5feg0tvJqOdUv9t/96gEFh844X7k1OwcaAF4nl/JqXJfXinsRo2InxOAtXonKvf0YB+xLkhi02crg+BXDLtqdpGRLlwMWxmkbGUcol7Q5RGBNk1bmfztn2RL27zoSvSeLRllWWffOyAVEJXsZ8/4o1qVHFOuVVB6XPu2Mi3IdbaV6cUY0ddE3CtZdjHxds1WRs9JS0cFQtXzEMYxesBVaSuj3TG8FjN03GND9Z6ey9L5V38UzAJnr9t37zNFNGMjEEX6t3PA1PJiEEqZPUPWRpODKHNUvlyr1/FvgeIDTNqQxMDTSkk9iB9hkzl+ZFKQKq93j8Ar90Uo/kM//DJeE6p/8j7fyvYK/f4BlbunpOO372rGXt3H5Pk0Fxlb4mCfzmUkyHbtepcTyVLp3msf+vied3AQ9z01TiTRXhTZHr+pNTJjYcD3rNIFjpeTM83lZcjn8z8MvxUn19hYKxSVktQjGWjHlvJxtWJ1deLFQ/+qkIuxWZNvYICX6tWVHYhBe21lBkSl1OBWWqBsZQQY+tQWtjKIlK0MoUhP+DV3MgBBEPWi6q3lT9bBPu+uYJDt4CHf+yEzR5pk1y1nelyFDBsY6u0GIoiqMebSfSnx7QvEa40HqocoWxnTC14fN4WND0bmZWOS7WbwVDUKbLI9Ox1k2crExxvlGpeKW0dBqeQpRb3p4R7UqjJY6o1JSdHQ/FQRjffIp4+Or19ZyPVgf3ZuVf/I6vYSQv2IG8QXuIlBkaJdYlDk094RIinkd1tJUvT6VeEJiJSBD/bLLkkRE5K8ehSK8RqXxMSBZpt9YNhVgEodKX+AF/FbRN4zni6jZz/kJ8ynStRnd8DgXl+/rYR9Oyi7qkKJVnWfuktdj/167h8YQYI1fqbJN7dugd5bzMjx93I/bdPU97NHC/wOIn8DjEJwVKqdBnWv3K2ehwMl3mXZalJoFqLJmbztMQz+7YP6TXDbKFw360pSj8Tn9qXX5wGvuwpErdp1ZjuPP3FbEWTHG/f/4WPnPvLPTwqo7JS2Mus3bmnba0AqyUMyS5KVkcpThNLeSPKprUvGKCSXhEpre8ZE+6N9cheqbWodorxTBlQk5JfKD2KyM1w3PgqT+8SIixcqkZunXmZcrlOPvAR0fHxAcrpmmn5BgFUXkVZECEgarDfQck/7VN8t3D7znQaiNXbs00SqTz3lUce2eX0gzP/yO4FR9EAN7xWqPwSmkynJOpqjN2S0PemE0Ka2BJE3GuWE97vks5kuycqqV7QdVd4ZdY22LZ1/5YcwSIKlRhcC4l8V0XQ7L5/bs+nCK10fL61kLIX7NA/I0v1QkF+KHxxgFSQheqDMP8BUGx0c4A8q+SMmVoiPAkEhiYkPqNfk5Q+zXqr2ZBnytWs5vg4rwr/o8RFPWB0s8Fzk9/Xbh1nVrrXLnT24dmR+kmC4U/0wnyrD6+fyPqbq1M35Hi3DxIq630kiYZDxECaVqvZASdrkjgGSKzGhJtviaI5z2K3OYQ/0L/hweK/vxX23ZM+06Wb73qieZ72cxBhX3wdkPx5S9725Ht0D5H1sb4HZ3j3s8Uc8bTU54O/QMWRfSWX5jPqtdVx9jzhW9BjR3vaRi7SGnCz4GT9T5u8+6vzvhMEp3CfV20kHym67k4WyEO7Jw2n+8gK8VJmAt8GF7nOr1QsbFNJWpvnvTfXqyhVtE+4xKWqQowlJLqGTSRRRGdrKiISI1whVm3A1Cf1UGaqT7SZ5GtJfCWGarGtEvUuwp/VMy8OUDgtX0v152lv9pch22nei/4hPGbWe7BnpHvdP1LcmIR7RpWZfJR76wp5wwXT4JTUlx156dvbZ6GPETu+5BtpJyyhqZOrEuRqYoByqdAI7mRdLyG8h4pdrSFJeTR/GQvOEHDcV9WjlrUj25qGYBITWjxUCtHx0Uj8tP/ysxl6zjOrc+b8TD89Iko/z1+27rxDC/apnvpLLZuTZuRV7EwdMLceXxd3qi+JtMJhAT/YPyJg4k2TCQ2X4ca5+cMo2ej+U4/XNg6qNpiAKiDw97KpnSR41z2m6DySF/WNUErRH8/6gU9fRbSrvogisOUVM3DOk95aYZL8Z+vNmRkwM3akRDY8pkmFQP9gPQ3ev0h8eYL/Fz4SboTyvq8vxE0+W9fW5oEi03ao9JqF/BHXWOJeT0zE/IMf6CbXvGPjV1bLuN/SAEGuXx/jACHl0t3qe6RPJvZ6gn1BlyO8F4fcmyDlhp927b4NiiUQ5Dg6MSGDU+F6p3xsmSlbFCfvZq+4bM5Dfzm93n+6F0yXs9wnrGTnoZ0IsJomv/+lhI99LY+3wweuvTMib973zBnjvtu4V7jA2Bo1r3nDjVX/w5dyd8dqh3/jvK5Wxw6JSecOWHVe/1FyvtdeyQrJYQWKTINfB+hytN/9WAnVMEKWL1kWYPvwg/8p0Kr2IAwlG64JYB7Ue5WeuQ0Wy+YFUtopwu0of2zdEddWPjepglqHyhOi8IAmAGNSO/e2N1Uce+medtrVOtqO2wW/yPJiLxfmraEYhOrugjr7vX7e+Lt7/8zcmgSPNIJOY2oAkthtaOjutFqQyPKSSbE99to20evRbzebDtg5JytG7Rk/vdrMqb/6X3ylv5I8CI/c4uXr7lk/8/Lt/4I+TmAuIFeWHVEliM6C1L10P/6I/XSXKRzsmtc5K11EvV9qVxcfIvDGy5EqPrUA8gZiWN6bS6PuT/Jdff+Vti6/O/MM4vRpch6/7zXt/tYh2fuU33ju75fzprr9U/OnMJXjy/LK+6capmQszPIL79qNgXPuCfIP6Ij45JKcwrUiE4/oX8CJJF/UFekJrpx2QqnaGwTf9mPoBddwiXo4NG8mhCK6D0N8JoDnVZo/pP0btMWZ5N8t6vqtHJNwURGT7zJD124TWJmXyko37Vy4n4uul1+2rxstsh4fJet3Yh3rpz4VheybEzwHZj/MaKdNVn2qTDPrzcaKEBNrQff2AIZ40LPBak29F3NnnovX7nff60L4f9vLeHVuiTQ3p9+fdqn12WPdNRu/xgTxv8xT4jJS4eYieC/H32PgZWerfBGUi3B+NvwhdO74d/s07tucyJ1ndec3db/z49F1F1O3cR35jujK2/u+u37T1wsbN29qT3ityUlQ0wtskrS0i3CTOQ1Voc12k2yJSWyflwSToQSf4aXI/LkuqThsR4a6OBS2fmGzXiXWAeMIAjDwFxOcVk71Ye/pvb6x+8aFf77Sdw0IIr3SA8smi9ciwOtneCeneHuH+c29Nx0olbYq4n5KxFlZEm/ioAJiEu+W/7SXcQY03sNTsaPhqR+VpATqFHV+WCfeC0CXhnpLgilBXs1gVIwaDIrej9x9QoD9dRSPGFbGepEvJdJN41+M9VDAm11GR6aCto0bex+sG+R9UZr/79C81Vpd2STJfqdvl/l8qQuH+7Y98eM+ur/zlg3meOb/99LnQVib+IjA1c+FGHr19+dIoXzXcM6KnVwgxpfyf7x/VIdBcbhxGZaHql15YkJQZ04qgmh6iPirSPqffuEf5LveyjeS9ZW9ZCIZmfdbCm5Q3tku8K0Ltaf62UDimoCQ2QCW5V05A8dYsw/D945C6z84Nab/NDvHzbejRHDdiQNfqyyPcrO8u21smlRLV5bF45ezcZTi7fnuuzNZXFwsz2UfEzzf/rdary1d0cJATEDUhw6hAp07aOB+VRgtpaB0LRj50wFTnmJhg08qIy3MDcqbBU4kgsFBIsFQf2a5vL8MSN4eagTACqPYshiuCG8SWDKIKbhBVczy44zMzH+zkIvFfAybSbaICjKKhByMV9nb3ajf3mensSRTjDQkinSCCtgrrLQnz2IyJGrOc+vLi9pBsTy1x5D9zRdnJbHnlpdvykO0yWKpGtsekEqM/2DPC5zZRUBDMd43y7xUYLtWq/kNLEjYyqOrd0PsgdIPGTHO5pXnOtwyZx/FtQ05G9EOl2Ik9zNEek+0TsDbQyXNvEhhFYlrdy25nst14ns2oYLFSJDC1Bk45fH5Lr/YhJtsngcn2Qd9L+HfTGji/SlkH3bcvLOY7sYXFiQv/YrKoL5pfwSA4D0Gwvl5d2db+YQZpTTmSaMS4SU5KNahw0qJLyqNDjNtlE4R4StxiTJUZ5VOTBQmhD+AQ+iTJ334rgUZqQ0RkB9r2OE1ALNjnRfeW10l2+xx6AJE5vozWpEh5fzqkyXJqm6+8jLpm5C3K84bNSIAismM7ImNUuIR25NBupdM7kCbJqUpoBDqxLnz71V+KgG/+XXz15b8X34O0wo8U0WxPPPLw+MbFuVwPZ0m4W3iMBySjIBRBII36q8FDe37yh7oKriZJCqlEnhnBH5SSnLpx1IMJlhSTfRjDsl/bUZPJsX37ENwvhwE3d9JFfBkUei+7he9lmfeDUSbeJbEu7YNuVBMuM9zjjBwY1G9Ffib0GaUh3I+cmjUeXt9cGsudJ2KlEJX7G/d/7AnEILypdqRyBxWg1CbbdcW4S2wa6RHQVZynudPqeJqUd0hyRHAJ81T1ThH1HqLfUUR3RKTaCvH4c+zlnqVwD/q+YPQ3o97Fk+6Yti8i1f7g9Fm2mr2Vgl1LR/Vp1hgZDhI9z9sIPXuTId/50Cp17a5i7QPHox8i4l1Lh046R7FupLOthtDMO0NlL8iXISJ1+9LCFbWF+ZvSoZxMCBZCuO/8zt/s2fLqS7nyePFylfphxmAwGO2SFJJ4v0dZCN0+AvcQSbQwObV2cKjF/jDIIiuCmTAZMsype9mNfC/r+JmmE+/D/haX5IAOqHFwgIl2RkHg+8kawboSDrxJuXJq5gQsXDcB2xqrXWc2Nndxj7pB5gfilxHxRxr12nYMGutFO8FTJTuUkkwhUxSRRSJal6RoRDSJJG30N9yP0XpESUVHND+rg9K8zfWkHI3Qj+tgposjIIqI1w/z1tZVXcPUwq4rJutJWerYosh2tRMVH1cughON1qtYZy0J+YoojHlWYyXRg6PV7hC1u97+iOYw0PPBlNxM+jkdD1Y/YosxrfViXKxTHpHek3f/ei9pFeygE0pR+VanFt4vUOv8sDuj8ZOMEzIdht1kpoPknpRenukx+r7k3gVqjhBFmlLEr1GkTR/fU+x83Lxh6bUT74boLECb9Dt+3b77jhfRaJtfP/X+PMefWarDxdWG8cWcg6UyGH3/MTxKRIUkeKaUPcZ+iF6PnRiCqkuVs7QXmWJidW1BjtnmePV53cuxcEufPFX52cvICzlepe3RQ4MIZjiC9wZ5Td4lFxUT4/0wPJYW8ll8mCdaGD26NnhcrRGUjXB/CLTXH59qbG1+6J5wF9XqxMm7/umet9z1iSIemJ9HDH6lAmJ7bWVxfMOWK15v7zCpJBeRctQkyVER65mktbAJ9JTE1Al8rTid4Eyy0on3+Lg4ezR50rTemhrWrQNNqDosb2bLQKpeD7TPAZokewnVxCkLHtZdJA0TE+3o9kzBpXt32yS6jx/WGXJMh6OeV7i7AH6ZqDMOrlftwayPMdHiLOw0Hc8w9fVEU9I6IdHN3tTJbZVIT6cR9Gk6hxBXeZtNIeK5Mo28j9OlZD9YkwDxfTEh4INadUNtYe6dGBvfpFZIHymiiaY/96mJK47cleuL/5MXlp1s+atF338cj7LarwiS6jEYbR/fmREmKqRo5IAKfHgblI98P6a+u0+x6q/lGO20fYbtmpUq9zuJ+3O/yPbwmmleK70upptn/ETB1+1ch/W9ky/BzHvYjHpOTpct2N6IPdOmIJpMHlfPsrKR73PqepHPtKNrYOKYx/rabPtR/65Wuuu2jAr3BE+fOAuTN2zIleH2F56VN/PchPs1+z92/Nyhf/YsjI29pVFb2Y647YIQlaDtDAx1uUaEm+Q7mV5KRkVMTIWrQCuGdfLbIOXJdCqZrmYX6XqqegdT8Wwo9LXyknzabxJIyfbUPgbjCI20t3hJkDSIUBFtQ1W7iDzeleo9FvfmKyjhxEVSWLwt7UcEm+501Ozu2w3mmTjQuGhjvJpK+JTg1/dbtC4W0BDF9FnWZ982XxqENlpxMKdoqtljolqArnSP0+mHhcfEtLd5alo6IeIJE4s8V3mkSnnV+QmZnnDqQKrs09tC/HnxlZf+LgbBJlVonGa+eVwhwVKvPv3cnrHqcq482L+9FN8b9mSQW/KL1/E2CJOZoghDpUymyJVJ67MMZhpPFsi/u4k6FfHFccbzhfSY9eV/3vo818M2os4X1DZ9AuUGqy0n1+KPRkUCySUm32U73Kz+9nPCaUZdPzE5Neo/3I6Bn/ieJu77yb2nSPWadb3o9xf9HjI54LaaUvWZ1MbJ3QMgrWbAT27PWPdD+76n99lcr8hX4v6n993NGffDGMc7HMNrkcyas553x61n3xyT6wN7nsVWPTH5rj/P+h2TZVqNh4fWmuJY9kOz/bPul4ze3qPKVva09aw8kfHshB58z6G+w9i/p3ZY9wjqGVm667hUhPuRU7PHPnj9lYla7eWzF+Dc238Qrqld7jrPxtiGvec+fOuBa/7tAwV84cMvYND4Caise1Ojurpt3cbNl1ofotSirk2HnxR1bWWEYSoSU7k+WxnbYiYtAwkFvBCmHYlLpKb1gmRiwLSYSUlhEG2x5JSVTEy2B4qPw2G4XWq9KetfUSbYyfkVo3J3xw1pA6O3vyFyt99KIG1lwsFm9yNFlndqK2PY3Bjn1JfuEX0qozRqd9OiRVm4aGS7MKyiDIsZ0TodEiQ5WPtAU9Sjpah3j7NU9vGOoFbdWJ0//2PWvJvc/9B1++4r5Af8+Le+uj/P8ZJst+xkJPgV5P5CKoClsnKmLOSfqsdMiy+z7ZAxcwXVZ0r9qCrNK6zqB/d0EW201uxLNPL9HtUWu1VbTCjSYqKgH9Cx8vO4Wj+2BlXs0nv4oXjYDYqgy7hefNdH/CN0ro91lGPjAyXos1u08T9TxjFL9Od0G32qX9fHOixLDOsFSJAwTJSP1vMstvA5avW5XOIJ98kihlL8HIOISDzGlh5h+9/Io3DN9fmx5jV2S5l+ExRRl/hZUcbnw7oSjgN5w92bfNufXYX3bstxgksLgDMvS/XbVAF1eyQIGrePVda9qba6eGVbhLtCLO7UVMmmrUyyrpHfiti0pO+KEHdsZShSFizyVCfllcWM8dGihzVVdXs+36LdL3VJEFLQgpGCScKDxdb1E+2chEG0Kt/2wPIdEfnF3RSxnhZAkd+kN7sv73S2JKNsz9hyyvPUO08rdwf0rPeyPLuR+j9oXdLa3ObYu1Dq8tRWxkmXSNsJkly3lTHIdltRr9vKuPWK/d6beS++8v0fS9Xtyb1FJv63RTTXNx74r5Ob/+pjuUgpQt1+bGrmAnsX9/cLmiRTZkbsnDoi1/rxRXYttNEw/3gCgoCzCDrfGwUx9LY8xh7swz3O1jIhOYrPhFE+r7X27GK03efTxDNNV7P63v4AsN5y5AkaBmP076tlPqcyEu7ytcy98YenzlyE9/7QtlwZ1tdvkSrGqbwVu2b/x187d+jXH0UM3g4BXNGormwd27BpseWBpnJdJ09joovySNeh2cpEDiYJoWoTriahnxkwVa1rtjIARsBUED6C3faTj45MqLtsitO2kgnJds3HHRNjmsFp3DspOfZuD7Q5DKECqgaKpsxFwFKKdNNfXaTtb/VVIiwXQMQQ0M7WtowxTg9p/Tb5RkP7wVb7bDODGmWct2gqrwG/kUE43BjEOaS2MvFbDIZdjHFM5Pxip0sv39giRvWjrnqPi0PLVkbEDdY8FBGJoKyaxUxQXZXq9h9XyURyR0B4bNe+ewv5wfmGr33utrGly7nyeHZ2JYu0YjAYjEH++NBJBwl++4bBYDAYw/pMO8bftxkMxrChUsI6GT8Izs5dhlc378yV4fqFud0Xb//JojzBvohBcFbSQPXq8o72D8OUXEd0/dpjQismsbRtGK/Hsm+NgNJg5pv6MAijbLs8iKMRyuCuVnrU11GvX2pV45xDorz3IvCQ7bqXO6K2QHmXuAWTCQO0PkNh8UGFZ90eX/o26ng3nTnW3PyoE8CM8hA99bby6RHh7vVmF9bfnL2B6M8LYUAEfDypkU5uYLrNIMTBSpesC386tPoNiUkU7bNernCt74Xw2eHDwpkXfzxoSHW7MW7lcqiIZnrikYfHx1aW9ubJQwZLXW44vXyYv1YwGAwGg8FgMBgMBoOxtlE6wv3IqVk7kBc8/mo+FSI0Gs0zHdtfRP2u2f/xvw2C+jMAuBLUa5sb9ermdo/VAhFKUKS12u6S88pHxtTSmsS5Ttrb1i46eUqs6+Spk6ci3rU663VwJxAyaVSdjHZtZZTyXSfg9XRlXuzzUuLy/IS7ZRFijwGnT5O/+hiiMvYT8K3TufVBcn9fJew2yW1M2hCzJdhiyerv5C0MPf/yICMerMCUeNcJb2GkCw3c3XSgvcKCZN5gHSPcvMPXMByyPT02qK1uqs6d/4nUSia6BpptPLNr370PFdFAVz/z2N6tr76UK49nZx07mbmpmQv82iqDwWAwGAwGg8FgMBhrHJWS1stQCR4/v5T/RC9d2vPCX/zBeBGVEwBfCoLGableX1m8sq2DECl1eepNjOiS2i5xrpubU4p5Nx90iHgqnXJVTshTvTyXlNdV7kazxOn97GNCSgcRKe2Q7aApw4dgievqTBYQVjndDzejzdM+Mf26CVKeUJdTb1PYY8ObTwdn0bYqvhhgxmed2e307YV2y7XfygCrzL7AVJtTJLieGP1NRajUTTV7bCuj3ZFaKupddTxV7+b6wqkXfyLybk+SxQn+bVFNtWn2bK4JWBkolSDc2a6BwWAwGAwGg8FgMBgMRmkJd4O4WLhwHr5eyWcrA4jjVz720N6C6vepoNF4tfm3FtRrm9pXuVPqcvAQ7SY5KW1lhJMWaVI+PYgqm1Aip8StYyujH2uWpSv0heecDEjyuUGo2htDqGi3ldCyFRpgKd4h3Rd0zboieNqUmNjw2ry0oTjXxwD6xhJlheSrL7jjzLu/UKBGMRvWPz2yJwL0Sr77CZfAJuxiDHsX2i4msZVBO51eDpU3iURRb9nKENY3zW1BdWVTdf78T2qhAGI7rbnm50LU7U/96R9Ojj//9Yk8eRBku8RDwGAwGAwGg8FgMBgMBmPNo5SE+5FTszNgRWL/+iuzufMdm58txFbmDfs/vgiAX8AgJN2lyr1t5XxoK4OOp3q8Ttl/GOk1Wxmfj7dLjtukPKl6Dz3cXXuaTMsQpK1qCFeZxEpGKb8bSv3d0Mn3IFoQy7/4SHhnwgDQtCDpHML1Rif81bNtZVqp2Vsp2CnCvDS2Mo7KHLS3DzT7mDRRsYttLaOr3Z1W620bIO2LTvmne+1iVDrbz51UyqOrWCdsZdJjjHkJQdT18szz78EAN+n3INWOH9m17765Itrq2q9/PvdzYPo1Mlb2NH+lYDAYDAaDwWAwGAwGg1Epcd0MlfvzJ16Bue1X58pQLC9PXPy1vz9ZUP3+LAjqJ+SKUrlvanmE5XWuNlIWH4KyoEG0LF9QCwOZZSujr2Omyh4EGTBVq6sZPDUuD+x83FNPVd8NW9lO+bYXtPTNWgZpT3p9W/c+3+ix7gGCOHcmZIRJSdv5GKxwm/Y03jFtloNkeb0j4NFUsw9KZo4aET9gb3eCgBdAergT5Dfp4W7byujHeG1lHEW9fXx4bG1hfkd1/sLf08eLar95KChY6mOf//TEuqVLe/Lk8eLlamgpYz+vpmYuzAGDwWAwGAwGg8FgMBiMNY8yE+6H7Q1Pvnopd6ZBULmziMq9Yf/HX0fEbyAGr8jPjXa93HUluUmSR39J8lQnxIn9SCjSk+KQVicj5c+NOsnl1Fv74CP0SUI1tFvBeMGUdAeXbO+Fx3pfFyKIaiNWuXc31DzBSp0+9Qw38KjZqXK6sJXp/BygSCbctouBPivLfWdlWM1gv+riWsOYuwlvdr9dTBI8NU0n0JeOyButEWDbyqTp1LGLp743qaxk7EmlQ7v23VsImf3WLx7ev+nCq7ny+Nrry9RmtpNhMBgMBoPBYDAYDAaDEaK0hPuRU7PHwLKV+crJC1DffmWufNddmp08/ZE7Jgqq5l8EjdqLUrAZNOqbgnZU7jEMwhv9vuhEetTTI0mAuuQ37fVuE/amrQxStjIOUS+ghXoZPZ7tjYAg3od90c8TXKuZHLYyliJd6x+kAqKS/ewZf0ZvAb1dn1zp2FbGHRAFgCTbtX09dJHxBlm1SXfsJ+kuCKU5EGr2uFtEe4FVhSADq2rJDIsZ6xQTD3cwbGXUMUL9XT53+obawvxuw0ommQTCw0U0z+Of/8x4pV7dmyeP5eYN68nzJOHOAVMZDAaDwWAwGAwGg8FghKiUvH4GiTF/eQGePZtT6IgIm7/1VFEq9+cQ8SkMgjMy5/rK0ngb5YvMAKaU5YtrK5NQayphtO6zlUGkbGUIkjzh1wgVPVD10uudnoc1CZAS7agp3VMSuhHEnu4jsgBFwmN3tjKYYQdjrhM2MNTbDUQQVdpWRlO5I+0Fb48HcyOVnrC5yXk5g0ls2y3Vb0cXm4AH6LetTKaynfZwF0Q6QXi4Z6Qjmho9inokmguWXz1xixUqIP5wZNe++2aKaJmrvv3E3q2nXhjPk8eTtLqd7WQYDAaDwWAwGAwGg8FgJCg74e4oG59Y3Zw7U1yt7X3uD++eKKKCAvCzQaP2Pbne/Nu2yt2ylbEDpmoqd5c4t6TvPlsZj9LY4+GeBk+N6wduYFZd9d6ez3egWckkRLuudG9u75cCuV2VctfjCtKJhcA4d5N872qYWZYvZruTynW9Yu0GMCXKcOoB0MrmJuuti4JgqNs19Tilau+npZBX+Y49J97RDHoKpuVL0gveoKbe+Qr0prO82N28zXJoRT0unXnpHY3V5Ru1+5ZI1e1wd1ENtPPFp3sVLJXtZBgMBoPBYDAYDAaDwWAkKDXhTtnKvPDC9+Hcui258l23eBne8M1H9xZRx6v3/xfp434Sg8Zp+bm2fLl1ZFdTuR5vTIl3yiPdCriKEfkdpdEJVSQ93NsKmAqOrQyAYytjrvvqmaSNPMzBULY3NLI93lcWS5hGzkVX7ds2OpGKP9dw8yvSkVKuE32lRxN1YwgkmRJjw9pGnQjS6dtWxXcG41S0mhc1gdJ1vcBtob7VS5Ae7hTBrqUnAqtaxwgynXZqwrCYyVLUp9ubxzRWlzctnT3xC+DsDP8tTN3+zB/+x72bz52cyJPHs7OrVLBUCbaTYTAYDAaDwWAwGAwGg5GgMgR1dFTuXz63mjvTdYvz+899+Nbxgur4yaBR+65cwSAYq68uXdH6EGyHJCctOdAktXX1s5kWSSsQQZCwtj93aCuDaKVHJ1ArGMFeLWI1DZZqqtt18lkPmDosS2aQVu2cGvpfa2kHY2OV5802bmkrQ40vq0ecYwiFumFh5ObXqa2MPS5zIss/XbeYGRQS33bos8pdEMR6TH5TNjDCIMTBSgeurYyTLiHOnbyByM9QukfrCyee//tYr1/pxLGI4kMXpm4ff+np3DZiHnU728kwGAwGg8FgMBgMBoPBMDAMhPuUveGp0xdhcWxjrkzF6up4fSW4o4gKXr3/vzyhVO4n5ef66tIORGzZtugGmBRWYEu1nbSV0YKnEkEz3WCmNEFLqt5J5T0Qfu6eQJrRuiSW6yowat1QgqNmKZMSkcOytK+Yx/RcwVTBt3VxCnHJIs47t5XRaFZtDJGXhDM2PJcOka7d4Klm3kJ0T75bVjLJRIi+fUDjA8AKmKr3Dpp+88VD2D1vrTtqdiSIeseeRtrKCOFVyiNdns9WJsq7dml2vDp3/r91AqVGjXj3rn33zhTRIscPH9q7/eR3J/LkIZXtL16qUrsOA4PBYDAYDAaDwWAwGAyGhnVlr+CRU7MzH7z+SmktszveVl1egqdqm+HmSj6l++YzL+//0mf+6p6f/of/qAiF4tGgXts1tmHsegCs1FcXdqzftH3Wm1qSolL9Gf0Nt0TEkxDG/pTUjtNG60k+4d7IVkaoT/qxdj6SzIp5TjMdaOmksFmojwjx1qg8VQ6KhESLjk3rqXbUFbkek+4R8Z6S7RHxiCN3UcUdETdbQzWInIGJCPhmip0Ik1v/OPvirNR+ePmv/98j9six6FrddN/drlbHdv7khze8+/e/BYYJiwBj/PnGRnpM3OcirUebnLmet9ZS3UAntgMwFeTWiQ8OGF9I9GmK3pQYXZcivKzS+4vR4fq6fitRx9LpMJ3Y0dIJEU/uxLYyQr0SoZeN6X1O1StqALz88rffh0GwGcCZDJpvrh4qqmGuevrLt4l6LVcenz2zQDQ4zh2euch2MgwGg8FgMBgMBoPBYDAMtCTcj/3nf79366XXJwZZyedenJk7f9HkxGs7tgNczseTS5X7TV/8o/tf+J07juet4+wywIZK/S1jjZB32gLLK1vGliutIryi4aEcElICNQduVLwYGp8Tgis8iHCusPMw0qX5oZ13cnz4N6bTRCLKddSsZP5jF+d2jCl1e12p2yPSXVnLBLEqGUfyohIqzGTYeBXVzc31iHCP2mL9eoA3rDvRKqvt0ICfLKJOWLu0I+0yoROr6BuacTo1+2LtTiZZtMmfhKS383GbSI4v0Tk5Tlr5ED7uogTjIJ29MiskUGvdAstK7yEWSZ5O7KUEvEGCR92RTpcJTN6uUekiKt1Kp59megxYRL/d9eHFsfzKzNvqSwvv1N/eSWX14tCuffcWYtPy4u/sn9z21COTefJYbl60T55fphqdyXYGg8FgMBgMBoPBYDAYDloS7le89MxtO2eenRxkJW+W/9gGLZfPFZL35lMv7pFLq3Q+YtAWFlss05ZetEdZaWrdx3w1AKg2V2qBpnRXZHsUTBR74mc9UKJVM3cJ5f6KmAzXm20hKiJiFSWBfb7ZFl9Yn6QTRZ2rJ6+xG8YAfgJEMhGTrWZvpWDXVe9xGlcJn751AW7eXY4xtKxaCAsXMeBrBJN5LZHqyBEc86UeTAzYynVbhe6q1CFVnDtEvZ5PPEcSadhFOm3glBer1HV1vKGUb6wsbV44+cL/5LGSmW8uhanbt53+Xm7v9sfOLvp2HQIGg8FgMBgMBoPBYDAYDAvruAlMGAEOwSL0PGnJPHCNtBOYtFxMpktyXZLttVDRnZLtDegd2T7wZrcKr6jWqccbgpTfxJVm27wifdplOuEQ5T4iVrSwJ0kiTgrzL15RsSpqu4cktkUaEa/ZFHltZew2yFC5m7YyopvmRd+6Zk+EpblGMHkRIGGVhXZ/6MnskG4roxTpepUcAl5EM0Am2U4S9SJLPQ+Gi1KmrczCy8/9d9io7ySGsMz7t4pSt3/96B9Pbvrj/zCZJw+pbp8+u0TtOnb45MVj/MRkMBgMBoPBYDAYDAaDYYMJdw2JajbQgjCCX1Eb/UWNeVScpZ5hB2WXHQLsc9fqr7VREhw1iDzbk4CpgElA0RGfj4i82xUJHNvshwiiERM0P46JiHCXPGZFHeS6nGujK1kVetTb5JNOsFfU5zGwSHqXRPcJwnWGvH1bmfabp+MhQKnb4zcqoCRku9kz6FW369sL490pr3SX/HbTRx+wDbsYgUY6SykfpSPKS1XuK6+/ct3qxXPvdaxkot0ndu27d6qo5rj+S39SiLp9uUGGOeZgqQwGg8FgMBgMBoPBYDBIMOFuA1OyPYjXNeLUIPwADGONdkOAYssNxaETn+hOVeeuwjj6K9usodnIBJp/e6ClHRVQbWxMusi2qKjYtPJjc2AFzYNiwr2i5ZN4bCTbjNinmjW/Zluj2NV4qahyZP4BkvUj1OxUkNS4LNsyJslDJejYVqbjwalfc/oYSidvROEBeEVXtw906i1UU0PstiLSa6VAH3frpRNNuW6q0NWp6WS50t2bCnYAT2DV1FbGKi8J5yAgsZVJlfIhFl769q9oTRuN7tgvHuD2ohrjm/f9+73rv/rJybz5fI3ybo8wxQ9LBoPBYDAYDAaDwWAwGBSYcNegU0sxsSftTwKLTNYJZgDCasaiKweKgitA+WS7imNFritluwyO2iAmL0YF1ERFzCgGqT1LeNZjKAArUnkejauKshup6H7vcR7aNtC3gc1Yxkr5iLzXNfUmoZvYwYDl/mGq3WlbGUhV7lo+FGvctq1MZ3SzPqlDKduLtikqIjuh5iFi0l2+2VBRVHehpLtRWcOHHS1bGdsGxvVwlxF+0UqniHPROp2bt6rhpe9+8z1BrbpL827XFvHYrn33ThfVd+PnXrpzbHU5Vx4yUOrF1Qa1a+rwyYtzwGAwGAwGg8FgMBgMBoNBgAl3C0KnJNEMBBqgaTUDbfi8A4yImlvTvDqqfsvmIzDaC0NrGV2hnDVZUUgflmYwRWMpiP+CUqwHETsuacdA2ckEiThYOAFXjfNCk5CP04bkvWrP1KZG0G1MerP7ziC1lfE0tZ2Pm9ZUuafHdtBRAVJjCJN9ZR1FiNH0klB9EnLUwjoX6JWtjOUYr1nDWOm10Yr+wKpqHb3pEmm7U1517sKVqxfO/qxO/mtWMvJjYer2Z47cs3fnX318Im8+nz2z4NvFdjIMBoPBYDAYDAaDwWAwvGDCnYAgTDWcBQl7mTg9oe31yW/b4gp7zSBjqzKFMatgJ7dtYgJ0g84a5WimFaKTdshxOj0bKy3rgonFCyiuXW/HmCRPyHPVYEIj4O1yhEZ1Jn7tIuqHmJBPxqVIJcyKLjWDniKmbjC6rUwkZ9dalLSV0bZRtLGuqNfSG3m315+Guj257oSyDO9Fn2OhY0S3kkkm6oTp715ASS5JHivSTQ93BJdgh9RWJgm2qtvFGMdEtjJ2Ou12KWL/nHBw4uXvPf1PMGhs0ZpEC5QKh3btu3emiBZ45NNHx69+5L/m9m7PULfPHD55cZqfkgwGg8FgMBgMBoPBYDB8YMLdQmzvUcHob4BEsFCN9NPD6SV2KRlcHbZVB9H5QYUhdQrxVUD/FCBdVZLD18lmiortcz/3MmMR96MwmUWbwta93lN1ur4djfQxj5lol7W+EsLuA9Qr5HGaF3rNadV7RMRHa0XYynTYC466XdrxgDnhNRQIJ1xQTbIIY/KuAFsZqxliIbpFrCcKeNcuxgi2mroNpUS9Whf+dGqkmR7ul5578pbG6tIPgOmEFKvb55v/3F1UE1///BN3bDn78kTefDLU7XcDg8FgMBgMBoPBYDAYDEYGmHAnEJPu4V9MSfUMea/2WcWKRH/eANkkYdkdzg3SWGR5mEdcXqziTghSxS5iEq8zi3rF0p039SnZKtwUyXhKgqJiwn87nu3t1EPkqH1Kqae2MrTi3E/At07XXvDUNk7ECchrBd5N3hwovH+LA4I72RRP5KEodITrVi7C6grHGkbtt1TxJFGvT/EktjKKY6eU8omtzMprJ9+88vqZXwKSbA+3Hdi1795C/NCf/sLR8e2fuX8/NBq58slQt8t6HuUnJIPBYDAYDAaDwWAwGIwsMOHugSRIxyLzBKhoFDgq5XsatFGjtDQxMbaiKeP8hqhN0GLW9fPWlcaJXUZzR0VXJws6wCVaymwrAOTAzrcbYtu2fkkJdWF4rifRIoXrr2GWTQU/tSJOinSCSAg9sKrm4W4o3C1bmSSVMNTsZvBUzW7GJtMzbWW8g6mtFtYDpNoBi/VxJEp/7aSV1O1lkvMR+c4hvWQMkjzqo0xbGX3dmDPxerh7vd5jpbxmK7Pw/W/9U82zyBrqYaDUqaLaeOvXv3Rw65kXxvPmk6Fu52CpDAaDwWAwGAwGg8FgMFqCCXcPJCs0JlKvaF2GjXHcS+0z6eE+SGeYgmBy38K7L7V4R4J4h0TQatugDHPbUGOGaiqdGAcwiXadCReCNlwR1gYyP/VZvk0QLsIl/Gkjn4T4bqFmT9OltjLWABBW9IO2bGVaj790zKBh5YQ4hBeT8tqXk3iRyl0kXu65bWVMQj0eHNiGrYwdbBUUUU/ZxSS2Mmin08+ymW7u+BO/ENSqb4bUvV4PlBqq24tq2unPfWr3+F/+/t68+Tx2dsmnbpc4xE9GBoPBYDAYDAaDwWAwGK3AhHsGQpU7KKqo+UEGtJRkWRCr3yEllSUSArCiZTLExLLw1NtWouvqftRI9bhNbJLdbgMcoTEjPJ91Ml0nyqm/IDz5aLplEK4djR5AdUy+maGr3XU217SVibZkq9lbKdhdpXx7tjKZiJXtusI9XvQ4CkX1Va8RT3dURKpqDyC1XCqqGEiJcW2kpJ7qmgrdYysDNFFv2NNEZLtQhkRpGOSovKWTz/9g9eJr70snehwrmbt27bv3WFEn/bYvfeLghtlzufJYbmArdfsMPxUZDAaDwWAwGAwGg8FgtAIT7i0gyTHZSJIhCmI1KqQxASOiTDi+6z6Vezse7mUCdpHesIvx5IctChiW9hEZG4UnjbE9w0qEDC9a8eSjW9aocRur3U0hvLLgNtTuNreepLeIeM333WsrY3dk9yp33U4mDkacWLBgdoDeXoztrsaCvj9x7THf/AhUkOa8tjJmv1qWL7pyXW8Ch4APD0fHw50g6oVHPd9YvLRl8eXnfkMj27XxF66egALV4s/84X/Yu+Xxv5zMm89jZxdhueGNBnCYn4YMBoPBYDAYDAaDwWAw2gET7i2QkJfCCtSY7I01n2JNtg961jOPwTU0fkT740z/203ePh94zO5Am0T32croxift28rkaBzUlkThrsj2oMMx1+9rwdfHscI9UAfETi4F2MqouT+fTYzlw07azxjBVjHLLiZWqqORLlqfe+av92IQbAHN7ciykrm9qECpTzzy8PgbHjx051h1OVc+Ut0+fXbJt3v68MmL0/w0ZDAYDAaDwWAwGAwGg9EOWhLu0996ETbPXR66E7v+h8bgRxa35M5nOWi2wSuXfQwkIwPITeCF6GNby7KuuWoJbjECohqVINTsVJDUuCTbMsY4JWzbViaDXU5I9nBBw0ZGD57acbuLwV8P8pwqqkVsa5kCi3IDosbK9ZRY197DMYKtithtHQi7GDuwamorAzD/7Fd/urG8sDvJw40JfGjXvnunizrRNz79xTs3nzs1kTefT564lKVuZ+92BoPBYDAYDAaDwWAwGG2jJeH+6LPfH8oT+8HN62HX9RvgyqV8Iv7NFYCTuAG+dfoCjxbG0OKm61bgluSTbjtC2sqYanfaVgZSlbuWDyXT9trK+KF7t+uBUnWyvZsJnUG/XVGxAi1HdjKYxD7oqa2Msx4fYhDwroe7QKEFTzUsZnRbmeVXX3rL6utn/rEi2yl1u7SSubuotpz+3Kd3jz/wu3fkzUcGSX3yvFchP3P45MWjfAdhMBgMBoPBYDAYDAaD0S4qo3piL7xQh+/vWCkkr1t3bYbNG9h9hzHESPTOhPcRtu1oI8wQud5SSPt5ojwvEFPvdt1KRifbEy/3IVkAtIkE0OxywLaq6raPBWp/0dyWhNsFax8Seaht+iSJoCzz40is2Fi6tHnhe09/yOp3gXFnRp8Ls5KRePsXpw6unzufO58/eXk+a/fdfPNgMBgMBoPBYDAYDAaD0Qkqo3xyn3l8EWavqeXO56pKAO/9gWt5tDCGHSYhbgYfUNs0vhW1fUi69Qt/GRR93F6gA5uITqxltHU9nsKwLIn/vK3e17blUOATRxIkubCIeGO/QbBr6e18hHPM7DenP4SN+tW2uj0l20WhVjLf/v3/447Nr7w8mTefFy9X4cVLVd9uqW6f4tsGg8FgMBgMBoPBYDAYjE4w0oT7hYsBPLm0BLAhf17/4EoBN0y8mUcMY0RAkd8GZ+tXvWNCqIa2Mk5+FGuM7UcVjsl1g4gGU92uq8IHsXTd6qCp3q08A8itck9JcMfCR2BCtuvEuqmKj/IwVPH2aHCPnfvmo/8oWF1+h3IgsseGKNpK5msP/8XEVS98I3egVIk/eYnV7QwGg8FgMBgMBoPBYDCKRWXUT/DLX12GV69dzZ9RowG//Ea2lWEMKzRfE5381mhWtSY8MusObGecdNo6Cqs8t6aJoh1NdXtQDrJd95PvZknU7aAr9jE57y57V7U0oVwXhHKdsIZR+201O0XUJ/Y0l7/7jXdXL762B8zgqKB9lssHirSSufrJRw5uOn9mPG8+j51dCv3bPWB1O4PBYDAYDAaDwWAwGIyuMPKE+/IywtdeX4KL6+u587qhUoWfvYlV7oxhhLBXXBLc3Kb+pIplg4hHbCOfztnjhMzWiXawlO04vAuAS7qjdY45VO4EcW6Q5KZynbSV0dcN/tyxp1k+/cJblk+/+GtRQitQajJWxN279t17rKhR/Owf/e6end/92p7cz4UGwmfPLGQlYXU7g8FgMBgMBoPBYDAYjK5QWQsn+eXpFZibqBaS189duxmuvWqcRw5jqCBEEvBUg9dWpg01e5oOaQsZ93jbVkYIQR2GKlhqbL+ik+/D6N1O+rhD+jcNpIp5fdyTjlHt6+bUnq2MHWwVjdHQ3FdfmN+y8MIzv4ZBsNUaLwJTZfvxXfvuLYy4fuKRh8evevrR+0Utf1yOT564BMuNwLeb1e0MBoPBYDAYDAaDwWAwukZlrZzoA48uQuUdjdz5rFtehA++bTuPHMaQIRadt7SVaUfN3krB7irlKSW8y7eH5HMDXJI99XTHoV5C65j4XEFT7lvBU7vrYuFXrkfbbOU6Gt7taUb+YKtxkNRvfGl/UFu9AUx1u0oZJplvrv5ikSN44jMfv3/Laydyz3bKQKlPns/0f2d1O4PBYDAYDAaDwWAwGIyusWYI99Nn6vDYhcVCAqheP9aA977zeh49jKGBiNTkBHEeE6VIkOREEFWDiG/XnkZDRvBU3VolVn43LLIdh7wfInI9Jt91D3fTaqbLrK1OE66aHQjlutGjjoe7Q7Zf+OtP/XqwunxTvJOwkpHrt+/ad+9MUe32rUP/154t507uKSKvFoFSWd3OYDAYDAaDwWAwGAwGIxcqa+lkP/2lJZj/wWKsZX7hDRvhbdfu5BHEGDJgq90eNbsD0ZWtTFbRCQGNprpdI9uLCFw6aFsZCd2fXvep188zVye3tImxBgRpP+MS9XPf/PLP1Rfm3wNmUFShMorXP7Jr330PFTVipz/3qYmrXnjq/kp1JXde0rc9I1CqBKvbGQwGg8FgMBgMBoPBYOTCmiLcZQDVB75+GSpvDnLnNba6BL/yjmtg86YNPIoYpUcocEdLpW4ERDWTG+nStOm+dAfQ2zV7mixbGQ0GCU34thvbcwQuDfq4kD7ukCrdU896K5hqV4y7oIlzX0BUN3iqbjujB1sNV+aPP/6e1dfPfBB8bzRElT6+a999B4ocu2958q/u33w2v5WMJNo/e/pyVpJpVrczGAwGg8FgMBgMBoPByIvKWjvh489U4Vvblgqxlnnj6iX4xbezyp0xFMhQrre0lTGPoW1lNJW77g9PKt/dYKngEu3pOjoBU4cFWaR/Yi8Drod7x+eIoCvXCeJcUB7tQNjKAOXrvnTiOzesnD0Zk+3Oolh56dVSqG/7t//Tb9+x4/lvThaR15+8NNcqCavbGQwGg8FgMBgMBoPBYOTGujbSHG4ujw3hub2ruZCev4f/fAH+3f+2ETZ8fX3uQv6b7RV46Yeun/6b754qrI02bli34W1vvupHr945/oObN216oxZHEbGyfknxkRBtT4MhWlwdorUujDQi0TlDypVSeUX7JJmHoB0THyJUGoyIOrXZKBuNfNU2VHXHuH56WrUt+WTXUatKeoxKn5aPoB0Wp0syUWn1f+KdiM7xaWVQX0dMD4jrk3xEarc6dzQqh2kFtHNIj8Y0azCzTMtIDnfrhTBx002noyEQt3kSRFU4qmhJooskEcX9xttF1HWijXTJ/nRdmLy8YSejfwY3mGg7hLQo+IbSLdHvq0egWi6ZYBAa6Y4dnwA63RVdj0L9jaqSbJNjRQjtsjTTaXksvfztiYXnn/owYLDVOiVb3f6Lu/bdN1NUe//1ww/uvuYvfu/gWHU5d17TZxfhhUuZdmJS3T7NXwkYDAaDwWAwGAwGg8Fg5EVLwv0TZ+amhvHEPnj9ldKCYLK5OFYE0lrmE49fgl9755UQvJRf5P8/v3nz7l/Y86u3T+z7vZmi6j+3/30/Kipjk2Nbxv8FgNjUXAIhAJvbamNbdsyCEIHcFpJk4ToEIXsXb5cQoiH3CxGS64GQZGsULDE+DtVx8bb4uDhPtPILrH2KNI+OjfhTLR9QaWWZopLaZ6flpZ+N8hR5qJUj0u3a8Vo6kdQVzXTJ9jSdvi/KJ62rZKWFsV/PI9Jzp+v2fkiP09onqZtqW9DO2zwnNNqEaIeIF3XqjUZ9RGKpbZeVkukRT24S4uFaTMrH22JHEWGS8wnPDloWJEGvzfM4rjOpOh5S4rmhrSfbu2C7y6KE99Wjosj2mPsOUJHuoaJfdFmUSPvXaXChzbupfdFEWkrEGx0qsH7pwtbF733zX2HgkO2VJNOorN/ate++wiYdpz/76fG3PPqJB9dfns2dV2Qls9Aq2e38dYDBYDAYDAaDwWAwGAxGERhZS5kjp2alf4DXIiC0ltmwBGJrflpubGlhfNtzTz1YZP3H75h6FoPGM42VhU80P66IODBh0FjXWL40LrXuSWLDi9s4nzigpeanHdtAOOctDKsP1H230bYUsf25w7mAWPJuWZPox4BRB9cjPD7WOQYdGxIUqZ1JXIZdttkOltWJY60iALMChQqznPjUs9oGwClPs2DJtFvBDEsXe93OOyFbEcj05PlR7UYe7zs3K3hqZ7Yyjdg6xrCUwdx+7WVeAo83fYDm6yntIHkxxHxbIbWTcbzd4wkatZ5sToOt1i9d3Dr7t5+9K6jXrwEiSCqkVjKHd+277yNF3v92PfWZg9tfenaiiLyklcxyIzNux9Thkxdn+OsAg8FgMBgMBoPBYDAYjCIw0h7uR07N3tP8M+PbL61lqj9ZK6SsdefP7T77oZ89WGT9x++Yegzrq09jbeXz4QbUSffLOyAlrW2iVbd6iKXHUsJqBrE0iXOb6M4md411h2AniPAkHzeQpkvGAxFokyTi03Mh6utODNikfLvEubCLswhqu85gtAGCO1ng+qK79bImNeJ1RCrQKdUXCelO+bXbwVPNU0DHLsRG1uREq3TGMTH53FBLuB5Y3uYwWguCHcAVnaCqXQROJYlzZ7+xTbjHNtfrly5smf3bh+8MatUJoP3/40mzY82//7LI+97Tf/S7e9/wrSf2FpFXG1YycmL2ADAYDAaDwWAwGAwGg8FgFIS1EDTVaxUgrWWOPHwZxv5Oo5CCNr3+yh2n//df3lNo7RG+2FhZ+FJQW/mc2hIFKZSk++ridnBJ6CziPKHcLEKVXk8/ixbpQhJYpAS7Xh6lLjfrqeVDE+KEYp5SnFOqd/14zFZ2x0Q2GnYcSJH3WjtYZRjldUDKuxBOG1L1oo+13lCwjkWS/Pa0sTlwvG9TILaRjy2yjjYnRDT6gqZqAQVGZNHP2Va5o3bOXd0xiKCnRPBUTNaFScRLG5nZv3n4bqxX3wqEhUySIaIMkvoPdu27b66o293jn/2r3bu++qmD0Mh/T27TSubQ4ZMX54DBYDAYDAaDwWAwGAwGoyCMPOF+5NTsdPPPtG+/tJb52tISiDcW4PjcaMDW0y/e/+JH/8+Jouo/fmBKyjO/3Fi5/BgG9ae1IKkC69VNinSXsIh2tFTEYYRE11bGZ4GS5JN8AO1Yaz2sUky8q+1eWxmb0AcgifV2bGWgfbKdUpyjS5wL+lhwz6UVQQ1+Wx19UoEirU1bGUql7pZBTyoIT7/Z+8BpN7OsNtTsaTqkLWTc4yMPeaVux0Th3kB0yXbE0VvA9KgPLXUgtdPp+I6UEO3C6RhzvzEKdCI+XJ//xpdvb95bbkTMVLZLkvqniyTbH/n00fG3fvHw/RsuXRgvIr82rGRmDp+8eBd/DWAwGAwGg8FgMBgMBoNRJCpr5DwzA+L9xScXYf4HVwE25C9IrK6Oj3/zKw+e+/Ct40VVfvzAlJRpfqG+OPtn2KgdU6RqGAAxIt0XYqU7Zf9hqNFRV0ZnWZ24im36s543EvY0gBmqal95XtsZj3of7XMA8E4MZKXLJM6FRmq77ZWpYAePur6lutwm2ZP+RdJHPtNWRveFd/3V/bYy7ajZaQU71Q7WBID81EBzCSC2lomI6SBAx+O8lwtqHuo9VbnLc4t968GtR6Mjxt2whjGtY/y2Mg5Bf+HLf/6hxuKlW4Ai2ePnRTQZ8C937bvveJE36bc+88j92058Z3cRebVhJdPyucBgMBgMBoPBYDAYDAaD0Q3EWjnRD15/5V3NP3f69r951zr47X9yJTQeW1dIeStvesvUtX/wuUIJnfmDt10pKmPvG9u689ehMnZts/eCyJ0dgsq6DcuVjdsugRCBsupo/q1E8s5oW7IuQH4OLSWiv6ISuVqEJJxI16MlEhlH++L1ICHskvVovxBJWlT1U+tJmb489fJAHavOL00jhKpfGghSHSOUUY4IjDJap8M0nVm/UICt56PXWah2NbZr5x63OxjtkZ6fdV5mXfV6Jcfr6eXxKNI+BOPYtGyNWK1geqxzvuDWozmGv3rrL0N98c1RfRMP/DicpxXWM5ZkR9ptYe9IZO/pMXgheEv10/C/LDeaZQUIq82/1ebfejNJPVCK70BnkEfvximbvtJcxpo5rqsArG9+2ND8u7G5bBoTsPkKhBd+/qda5jO+4fJTu7a8/lSaq9Cqqbx71BsF6nO6cds7jq2//lePXfji/7e/sXjpf2geVUE0SHb9r5z0+dXr9t13pMj2/PZ9d99x3Vc+eVDU88fUOLNUg//n2fMZbR6e+dHDJy9+gL8CMBgMBoPBYDAYDAaDwSgaa4lwl4rzp5vLhC/N+35+C/zcNdsgeH4sd3m4bj0svfnG23f93tGpIs9j/uBtN4rK2M1j2676X0FUrk2JbECxbuPy2KaQdNcJ5pjIDk2RRbRNkbQQpCS0RspDM62wiGof0S1Mclvmj6CIcYNwd0judGIA1CSBTkDrx6aEfEyEByaxHJ8j2OS+mU4nvkEn903iX4Q68oTcR3DKs0j0lPQONNIatPOkjoW0jgmJrupT0cnyICXEKwa5L4TVPsaxBpkeKMId3AmGMN8of6M9AZa//J4HIKj+VC+vy8VPbIClRky2A9Sao6cW28oEkfK7TMCCb64hk61I93UiJd1jwn3rdQFs/Jl6T8+psuWt918+8+NXNBYv/TREAyol103CXU6cfOK6fff9apHlf+P+j0xe/5U/f3TDpYu585IWMh957mJIunvbXAhpg/PuwycvzvBXAAaDwWAwGAwGg8FgMBhFQ6ylk/3g9VdONv88mpXmwL4r4K3f2wI4m79p6lu3w9Jbf/iWt9w5NV3keSjS/T0h6S6V7gkpDFhZt3G5smnrpZTA1shypRCXjhUiJV51NbuKVGgq4o11MLajQ0CrY4Ug8kmI94qt7CYU8+BTqSvC3VCExwQ7JGS7rj4HS62vpzNV7kG0K5o0gJjUTgl5AGMiwzovsJX0jko9mkgwrD4sUp1Subvq+YS0T9rCVrnb6nUgyPn0LQTLfiQ614t//I8f2HD+1E+pswbXlwRib6OWTDQSnyWhvnS8AqvNv6sx2R4oP/fADJY6yjdEyWSHKndFuK9rrm+QpPsYwOYrADa+LQj3d1RP4U8jrAzqm288v7rp7WNhVURiDeWS7ohHrtt3368Vee5f/eyDEzc+9NGnN58/XYgF1ydPXArtZDKbRogDh09evIcf/wwGg8FgMBgMBoPBYDB6AbHWTviD11/5YPPPHt/+zZsF/N//5kpY/4WNANX85VV3XjP3/T373v1Tv/CLM0Wex/zB2ybEug0/M7Zt54ea3bgxVoRLvkxUxqqVLVfMhwR6tq0MRGr2FrYyxrphAeNazEBFKsQD2laGsHyJlN0N0G1lTJV7kNh9q3IKt5WJ20cjqpXKPWhpK+NY5BRoK+McLz9XNII+sZVBc/KgGFuZ07/zWw9s/95TPxVYnuYArqmMjxS3TGeSbYEKkirV7DWNbK9rAVQRALCnbLtufSMKy7Oj26tIGe2YdF+vVO7hX6l8r8jtMfsdG8H4S3Ci42ppk0Wp6iXqb5lYqd/41hUw1e0VMIOkfqJosn36s58ef9vjf/ro9heOFeLb/szsCvzR92ZbJTt25NTsu/nRz2AwGAwGg8FgMBgMBqNXqKzBc5a+6nO+ncvLCB87chnWvacYG4cNF8+Nv/3Pf/fB7//Zfxov8iR2HDg8g/XqF4Kl+Y+DwGVIeTVAbKwLli9d0VypGEE4VbBLk5BDKgipFbCTDOppBs9M1pOgnlQ+OsfZKlhp/NmpB1JBW1EPDpoZlJVIp5WNTvlmQFE7GKyxTgZyJQLIEscitiiPDgJrBk+Nj3WCpwrtvKl6A9GnSbXCAJ4QkeF1jMlxhCqqv8SyqpZwvYGw0og+y7/L8nMA4RL7tteSoKmoBS5F6O1/KlgpKL/4QpbO8kS0gqY2t9XVBEQ1bLNmOzXSJW7X1Ya2aG1d1dYje57or8yzHqR9GAalNf16lM8R2IGA5bUtg6P+66JvxBNP/Nn9RZHtF5sD6U++P9/u/Z/BYDAYDAaDwWAwGAwGo2dYc4T7kVOzkmy/OyvN916owcPfXISxH2sUUub6hUu7dzz26fuLPpcdBw6fCKrLnw6WLn28+XE52qoI76CxLli5vKO5EnsvG4QtSrJV6aANEtwiXDUi3tyH6COCBf7/7J0JfBzVnef/r7pbrdvdvmRsyxYGGwIBFHIAuztBZMgByQTBDDuTEBR5N9dMZgCzc2V3srYnM0lmAhjChEDIREKBmclOAoKYGMJhQcIZDhmDAV8IJN+WJVl3q6ve1qvz1dEtWeqWWtbvmxRVXV316tWr6vLn86uffn9ZyHa0XM58Qrh9PGc/3/HkfYlCxeiQPvBsgrh0Djy7cG6fA+eycM79In+GPvCg8dh73PFFeeLhLzy4R6CnLPuEESLuh6yTBX0yxWBbDE/bRU2twqZjlkvdFImDU8q3nLIc7SnVnMa4KQarmiu224I1dwN5TtrJfrFgn7cxDmS92OCu8z/ln7g79mOad/z918W8XtyYjDEmco4nusFc67x/Ikts/8TS637Qm8tn19vfuW5T4u2X63PV3t07e4z89nG4VX/+t+OffQAAAAAAAAAAAOSTuehwF6K7yO9ty7bN5i1DtDs6Qsqq3JRtjB4+WH/wf/3BplyfiyG6jw49bDjdiY9YRmZTLNbUiDbcP4+0dNTaPOgQ97qfZfwCu7veLzLzgLNbJJ0wzgPbU7jL2xH0pc3sNkNd6vY5kKddv4jO/cf2HI8yuvXdAcj8IsIjwFNmAZ6HuPzl43leToTkpwQE9jBh3Lfsb9sR0zPms4SMEWf2roY4a8W82JPfLZ3msgPeFYBl8deNjjGd12lbbJdiZEyh3VSivQ706Z+mV3vnTmyPppkCvKrZwjt3x02TXnZovrH2XQdVejFij61qXU8nHsj6jXKe0dkuxPa+XD6z3v7O9Y0Ltz9zQ67aE7nt2YqkWnTQOC9aAQAAAAAAAAAAAHKBMofPPWu0jODOu/up99QUsWRugqSL391zw4G/uaox1ycyb9097wnRXR3qu4uIDztfCGFN04ToXkmaKooiyrEypsvd3tCYZYiV4VkiZkJd71xyufOQSBW/2C/1IShuh7nBvft6Xe/kOwZR5hcDflE+u3M9TOiWHfyZ+0yeMeAUfFngb9sTDRN0nlO2WBlPf/z9yhIrw3ngmjqCsPW1I9pac1UjSTS3hF6SBF/je9MZb4vqaUm4V23XtZ0HbwntvAAeENMltrvHkiJpuBk5o2r+ceXueEp/bSCPuR0XI8Zek6+VfQ2l8faNtd/Z/slci+1v/OSmukTH602R1HBO2nvhyPC4RVLt5731100AAAAAAAAAAAAAeWXOCu4tnT0dNI7jUeS533lPP0UuThMV5ea48X3vNXV++yv1uT6feevu6dRGB3+lDvXeZTrd3fqK+mdFGz4+j4+NximYqW5uEx4rw3zOb5YhYoZldYh70k9C3eVu+y5hLvVxYmVC4mJ4Fjc7z+zstrolx8qE5bCHROT4juE53gmI8kGCsTlh/coUK8N9LyzCYmW8x2FO72znNbnFUzVJgHdFYmudZjvivbnmjhgsueaDwrMcsT8+JypsFyrM9x7Ejdbh5niSNG6eMefmZLniHYGdvPvYL0208PEIi5HJudj+4kP/Vjv/1a0PxLsP5KQ94WoX7vYJIKJk2vDPPQAAAAAAAAAAAKaDuexwn1C0TNe+NP20tZ+il47lZsBHhqjsjZeb9n7jmtpcn8+8dS1d2ujgI+pQ7w/JzHS3Y0HMbPXUYDkfGyk213ER3RyMlckUgWLuFHSSZyjmKUeSUPZYGb+g77YTJlBnjZWhiYvtYY5zHhTOWfi+FDyXcYvAZo7V4Z58+OB4cx4WDZP9WnCe4YVF6HXzfxcyrNKV5a7w7r5fsIV4tyApz7C9s07qgtNBcVMy812RQmGh4oFOjzspGdoqhMnplzhnFva2wx14zuWx87vjrSKz8h8qZLlemaoUW2L7p3Ittj/96MOJxS9s2Vpy4J2cFI8Wee337umbSG57ByFKBgAAAAAAAAAAANOIgiEYP1rmuRdG6fmdIxS5KJ2bQR8dTiT279y6+x++mi/R/dF0f/cm0rT9JNRW2XedGi7jIwPl1mc5ViZ71EnQsR3+WRKtJxgrQ5Q5xoa82wZiZzLE3oQVFKVx3OUhTnmSzyUk0ob7Hfwh0TChDnbK4K4P00F5SPyO066Te89Dc+QzOeWd6BgKbOeJlclQg5WFLmYaZGfBuWDMnEyxmZEilq3PIvdILEf0/yj6ZMyZPenrJzEpE5rYCU40xUk/N7sthTnnr5B3O+PVgz1+LHzMJ3RtfPty688WPHEypth+Wa7F9sc2tyZOe+RHWxNvv5zIVZv37embSG678XxHlAwAAAAAAAAAAACmkzkvuE8kWsbY7r4B2q2NknKmmpuBHxxMLHj7paYjf/vZRK7Pad66ln18bPjX6eOHv0/cEN092htPj8W14eOVnGum5unN/iYaL1ZGXs4gtktZNSEubtldHursJukYdnuZTLkZ3PMZjuceY0KFVXnmFwOZ13ljZfwu9XDhnxPLcozg2HA+Xr/kS+ovvJohuodCYoG8F95xZjPfOlsglkRpJonIHuGb3GVTFDfF56glsEet9fY8ou8QNSbmmez9cjOdqJB/Am37+m1O0nlZ5+qcY4Z+2eMmj6vtjpfHnLHgtQo6/H3vUzh/mvIktq985dGtZZ27cvZyccu+AXqtZ2QimyJKBgAAAAAAAAAAANMOHO7kRMu0jredKKJ6YMkoKau0nByXDQ/Xsp6+ra8+1poP0f0YV8d+q/Yd+j7XuC2626nuTBRR5UPH51nFVM212fPJSfrO/hC2PiB+89CkEp4hNzxbXnvQzc5DI1jCnOTZYmWIfLEyHuc6y+S8D3Xth7nLw2JceOY8+rBYHR7mns9UdDb0Wo0TcxO8ptwyVbtiulcwF4KxIwiTNUkCcdQSmyOSOB5lzCcwE8X05ZjiTkX69mKKibm+fVwss+Bkb1fwU1jfrf7b5xFzJndcopIgH7Fc/1FrDOW/AIhKLy+i/uuguC9H7GsYEOT7+jYvve4Hl+dabBecuu3xTQvfeiFnYrsQ2rd09U9k03ZClAwAAAAAAAAAAABmgCiGwEFEy9TpU0bxWxRRvefeQbrxugjFeoqI97CpX4DuI7VLf/mjrTsfuPOSNVd+LafRB/PWtfQf33TtM+rxQ0ORykVfZpHoWWK9my7BiY8MVFJRyQCLxUfJLp5qSrZ+YZYbAi5jdoYJM6y13vXyZ2szuz3hZmfuspFmI74zdnP3dXVA3/GcdtyuucexHPVyO5wCx5v4duQuu+uEuM+c45E8DtxqlgXOhRuyNfeMpb3OHWvp1KW2Q/rg+946JVMaN7tnn1/ovtL1tLbjtqzOzfcKjJx1cvSLOM0I445w62SCW8v+BHg5290+E3c9c7LF7XU8UCiVO9uS1aXZiP8J4clQ98S8MM/2gQILIXEy8raez7795Ln5woS5f6EgPvf2bM/Hub990182LXz+V425ak9EyIgomYk+zxElAwAAAAAAAAAAgJkAgruFEGcaqpNCdH8g23aiiOqdP+6ndV+tpPTjsZyI7vH9HbWVbQ+J416S6/OqXPfTgeObrn0ufaxrKDp/2ZdYpOhDng04Zzw1VEZaOsLiZUPWSrKEXFdIz7TsFcq9Irm5mjGv4B0U2x3RmpOtaLtCsCNQk0c49wrZkghNFCqce8Rt99ytY3iFad8LBFvI5l6hPlwEt4/jfWURMja+7+R9A+fj6a8rknv679knuL/TL2csyZcc49/X0YSNP4NRzPcJtsDuCug8RCz3dzv4HafsAr09EFxa9lxhcoXmWQP3XXXyiuPyoGf484/gtr5xkK+oEx1jXXxZdLfn+WLHLX/btPjFLY25ak8UR717Z89EiqQK1unP83b8qwYAAAAAAAAAAICZgGEIvDRUJzfpsxvG2+6iC+J07eUVhuhOqdxcCW1eonn+j59dm4/zOr7pWvFyZVU0ufRaJVb8SUss1gwBl5FmLEeiqhIv6yclkjYVO6YZk9lBd9leb4rH3PrOXGa+ZSHT6nNuzK19yVxv6qhM3kez1EPNatPa3mjL3l6z+m6tM/vEnHadY5jbG+3ax7COR57zcNsitz3rs3Nsy0DOme/c3OPJbdhjYnzJne/NMZX2D+xL7jka68jtqyL1yxknfRtF6r95DOZp0zkXebzJuW5ynzz9Uajrn//yZxVvv3ShsbFPVLcFcFv4DnOgj2dK57458wnzcrs8jw8uXugPY5a5OK0tvstvvwK7+wT5EGf8xsTPd2zI1Xm8dts3m5a98KvGSGo4J+0Jkf37O45NtEhqW0tnzyUEAAAAAAAAAAAAMEPA4e6jpbNnXUN1sk5fzJo7/NwLo7T69BhdeCnlRnTnItqht3H/X15JS296IOeie+W6n6b12c7jm679SXReVbcSL/uczw9r5LprIwMVrKh0kEWLUuR1s8vYBVPJFzET5rSWYmVIsuZKESZe57qd2+KPp5Fc446/2e+o90e6yFE0IXExYfEu4bEy3OqrN1bG79CnsFgZCh1Ddx/mJsZ7jOX2uuyxMn7Xu6/fIXEy/uso/2UBl53wT72+h6KH+p3LNh3M1BtAPp0nWSgnnYfDb/vBtzYtf7a1UcmR2C64/93jExXbRYTMlfhXDAAAAAAAAAAAADMJBPdwhOC9lbLkuQta7hug0i8xOuciRupTuRnK0vd2Ne7/2z+mpd/9WV6c7pXrfvrO8Vu+8G+R0kRnpGLB14lYXMqnEEKrwkdFxIwaYfHSIWlX5stwJyk/3Ssye6NayI6VEek1Rlp6aOSLR5S2BX3uiMhOm5w5qru0j74pYx5RO0TQ92bGk+fcxomVsQaA8fBIG3scyBNfEybAe2NgpO56BHj/GIdsZ4vkgYyYLKK8J1bG3jfsNjH2bXt9D6VGRvA0ABNi39//j8aSZx64QUnl7p4RYvsLRyYs3l+J3HYAAAAAAAAAAADMNAqGIIiV/7tuItvec98A7WcpilyUzs3BNY1Kd29vPPLnH2/K1/lV3njvYXWod4vad/g7pKn73bBnV93l6VSRNtxfoX9vvUngbhoF5/70CneZ85BlIbY7lU6lfbkt83vqPFrfuW3LbYZ979/Xs72TScLc5PGw88i2ndTX7PHaUrey9pk8Y8D9qrdnfKW+hvaBnPcB1rIz1jzQZki/uK89HtZXALLS+c2GxtI3X2nKpdguhPa2g4MT3Xyj/txuw5UAAAAAAAAAAADATAPBPQMtnT3N+qx5vO2Ghznd8v3j1JNI505014kd3Nd49OuX5lN0H9JG+p9M9x74J66mt/u+thzmakQbHSrjY6NxZz3nXoHbFa293/GgOMxcgd2FU1AI5txt39unbMI5+dphIYJ+puMFtwu25wjZnHsE7jDxXhoH3zE8xzsBUT5I8JzC+hW+rxy6HtyXo7YDmDhvf+8vG0u6djexsVTO2nytZ4Tu2zths7rIbd+AKwEAAAAAAAAAAIBCAIJ7doTLvX28jYTofufd/TR6ikrKmWrODh49tL/x2NoL8ia6V9x4r1r+53e/lD78znd5amSLJc565XCuKXxspEQbHSzzCcThTvJQB7hR1tHOc7fWh4jkPODsZl4hOESg5rIgHiIu8zAneZgLP2Q7HhTOWfi+FDwXyvASgELGySfAyy8V5GqkfqHeGZuASz14jLC23X39fQv7DECAV3/0T00L29uaov25S3IRee337emb6ObIbQcAAAAAAAAAAEBBAcE9C1YesBBzxlWTuvalDad76qw0Kau03F2g/v7Gnmtqm/J5nhU33tuRPvrendrwwJ36RzsTwht1IgqqjvZXcHUsZnwOOrbDP0ui9QRjZcj3XVBs5xljZzLE3njeIWQRvzNtF3CLM85DIm2438EfEg0T6mCnTK7yrH2goMhuv6DwxcrwcZzydpwOpyzbAeBh253faVrZ9rPGyOhQztoUYvv3dxyjYXXCz1DktgMAAAAAAAAAAKCggOA+Di2dPR1kFlEdF0d0rx3LqejORlONh7/+8a1v/fKeRL7Os+LGe7vVnv33q32HvkVc2xfYQIjJnBNPjZTy0aHSgMAdjJjxucdta3uYi1t2l4c6u0k6ht1ehjzzTO75DMeTz8+/XaBdScj2HJtnX+eNlfG71MOFf06Zzs9/XShLOxniZMg3xjxDdA8A4Wz/wcamFU//v8ZcZrYfG1VPVGxfh9x2AAAAAAAAAAAAFBoQ3CdAS2dPqz7bOJFthej+n/cPGnnuuRTdiw7tq1vw6L1bO+75h3yK7sPaYO/z6Z793+Lq2AuSR5qZ9VSNxBlGWjrKRcRMeiyWIVaGwjLcfbEyPniGYqTZ8tqDbnYeGsEywQz38Fx0SSS3N8rgvA917Ye5y8NiXHjmPPqwWB0e5p7PVHQ2cJ40fsxNyEcAdN74/t81LX3moUaWGs1Zm0Jkv3tnz4mI7c36c/lWXA0AAAAAAAAAAAAUGhDcJ4hVlK91Its+98Iotdw3kHPRPXZwX23F01u2Ht7wx/kU3dXyP7vzrfTB3d/jI4P36auGfZvYlmrGx4aLeWqoROS8Z3a2kywOe2NleDaBOkREzh4rQ17XPQ9GufCQoqYT247ChPOQWBnyxsqEnEsw952FF23NIsqHv5wgfzQMDxX8M8bKBF3v0NuBxGObWxPv/dXVDyx7+v7GSGo4Z+0KkV0420WczAQRdTXW4YoAAAAAAAAAAACgEIHgfmKIaJn2iWyYL9E90tdTyzr3vbrt3ttr83miRsTM0Xf/TRvovpm41kU+cdoRmzU1ykcGy3g6FaNgxExYYVXGwgRvb6HQMGc3Ofs4wjrPJGT7pOJMETM+eEi2vPccnFgZRiHFVsOOz8mX655pbIgC58I9Qj1l6K8/VmYCznUKi5UJ9A96O7B54uGHEjWvPLq18p036nPZ7iTEdqOuBnLbAQAAAAAAAAAAUKhAcD8BTqSIqiBvTve+YzXLHrt3a9eGxryK7uU33jei9R78bbprxzf4WGqruZbZOeauHiuW0qkiGh0sFXEzznoeJog77mvyFRW1v84YRUOhee1BYZmHZqXLxUHHKcoa2E469vjC+Xju+GDkS9bM9YwROr7jhReB5X7B39g3UwFWnqEdMJd54uGHak59afPWRa89ldPnzSTEdsElVl0NAAAAAAAAAAAAgIIEgvsJYok9l0x0+3yJ7tHB/kT5my9v3b3pr+rzeb7lN96n6tNB9cDbt/PBvk1E3MiSYMyjxYqPQlRXKDVSTGMjcdI0+97yC7iMezPV7QUWGvniutwzZqp7hOLxcsqzxsWEucvDhXP7HLyxMn6HfqY+8Ezuevu4ExDlMznqQ4rOZo+ToUA7PCC8gznKi79oqn3fEz95df723xaC2L5Wf/6246oAAAAAAAAAAACgkIHgPgks0WftRLfPl+hOqppItP/mgUP/+7835vucy2+8b0Dtfu9J9ei7f0Naepv7jRX1Leu7ajpKIttdTRUZAjAPOLuNSBZLE2bhkS8BQdkbq+KNlQnswwOi9riZ8eQ7Ho1XWJVljrTxnpfHZZ4x397X3bCCr+PGyvhd6iw0C97ftndfAAzam2+uX7H1Z1tL39uZ05oRkxTbb9Wfu824KgAAAAAAAAAAACh04GKdAg3VyU367IaJbn/RBXFquKac1OeipO3N3bsOHo1Rz5oPNK/6++a103HeA7dcMy9SddpHlZLya4kpcWFaZ4xp+t2kESki4VwotxopTCyrFI2nKBIT6pr4rFn6uSaWDaO8uc58E2Eum9uZ7bjtmfuQ045YL2R0c19u7Wcdw7Ddh7Rrt+csm+vd7ez9vdvZbdvHYmb73Dx3qZ/y8eQ+Wf0009G59D1J52m9jTG2t86fkXN8Iuv8pb46/SJ5bOT1RhvMc07yvk4fydpWs/t01zf+5n39x45VaJrGuWXlN2b6f5x1Ro1ZEV1jrDRX2dtq9nrV2k1z2hDbWm04fyZg76upKre3ce5xTSN9D9GEvY+3PadfYj/VWK+37/kTBM+25vfc/xKDKYoYK2b8yYb1Vxz6OmauV/SZPlfM781hND6QEomYeymKu4+83tpefP7ImkWfWFgR/xRZF5MZLz6NvxERn+0Hg7NO/78iXtT09A08+vNHXmzKw0+646f7ejvkFW9/5/rG5M6Xm4r6j+X0QJMU25tbOnvW4l8cAAAAAAAAAAAAzAYguE+RhuqkEMAaJ7p9vkR3wdjiU1rfuWrd2o9c+pm8FxQcuOWaqFKWWB5ZsOJ/UiR6DnOEalvIVlTyCOP652jRqL6tagvcjEkitUdsNoRpsY8lSnu2cz/b+zBLjDeFYkdYZn7RWxb87WN4xe/xtrMF7IBwbgiqcjuePpMr+Luit/TygShU5Gc+YV3e130BwH0vJrhvHMh8IRLot1dw9xzHaY/cY5E9HkSBlwt2m+TbV26X/G2S9GJEUr1DzsXEfVngtBv43mqb3DlTgrZ96XjeY9v6vPHXEN7nI9fsv0SQ/jJBzvrn9l9huH8d4f7lgPHdsf+86Vy170gD19QPWAOpmJMQ1B2xXbHiigyF31zHu/S2vrXkujsem45n2s5v/0XTwjeebVRGh3Pa7iTFdvHXRJegSCoAAAAAAAAAAABmCxDcc0BDdfJVfTbhjON8iu7pymT77j9ad+WFl/9Rx3Sc+8At15RFT1lju92L9VtKdd3csuPc+qxE0obbXYmoHoe4KegGnecs4Ow2l2WBWt5XFvlNIVzzieWy45xLgromCcRaiAPc65R3RH2jLqnGZOd9UFj3CvumQK1JQjpJ5+n2wd2XPE5+z36K5tve6oficb2zUIHd3tfjcjf/SsHtk7Sf0S5RQPg3Nna3cVz7zLetTxQ3+2hfJ/uR5NuW+QR0+dxtxds5jrudd3v/bWtvm0Fw99QOsL9hGYsAu1FFinc/c5++R5uWpLrevparY58kR1i3xXSyxHbjc4TsGgeMRcy2ebP+n+8vue6Hx/P9W27bsjmx4qXNDyTefLFOSY3ktG2I7QAAAAAAAAAAAJgrQHDPAQ3VSZFxvJUmIboLwV0I77lErUz29p5+7pWn/e8ftk3H+Q/cco0SmbdoiZJc+gUWiX3YdYY7kyQ4WwK5ElEpUpQyhXeaaKyM3+Wu+eJYAo5vX6yM7fKeYqyM6xa3XxqQLWrLzm/yvThgFCbiTzJWZhyXe8AVL8fK2O2PEyvjjaDx95V8ETqy69wrzrv7y+5yTx99gjv5zpO8+0jbyS535hHnJyS4e49trbDd6zwguNvPTK/LnXsFdvv7geceLB9+49lreTp1FRmO9YDA7nG2G28guPP5Rb29f1xy3R1vTsdv+KWf/aj2lN892lTW8WZtrtuepNguRPZLUCQVAAAAAAAAAAAAsw0I7jnCEt3f0acJFxjMp+g+Nm8+UTK5dtFNv2yerjEYuOXz8ejSMz6kFFc2kKIk3DgZ5jrInax34vo2GinRNIvE0obb2uNyD4l88WSos4nGytj57yFxMR5BPEusTMBxHyqcjxsrEzgXv1s9S6yM77wok+DuEcmVicXKuMeWXO1KWKyMV5jP7nKfeqyM7PA3maTLPeexMtaRw2Nl+n/zi/KRt1+8ko+NXql/riRTbJeFdWmZPMv6oQf0+beXXHfHA9P1u932g431C3c831R26N1ErtuG2A4AAAAAAAAAAIC5BgT3HNJQnRTuUOF0n7BwtWZ1jL72pQqKH4iQ+nKUKJXDDkUiNHhKTfOey/503Uc/efm0xTIM3PL58ljNB65isXgdMVYcKrh7hHghvCsai0bHiEXUgJjtLQrqFk/NHisjC+HeuJipx8p4hH8rVkYunqplFdFd0Vvz5a6fSKwMue5yRRbLNV+sDJfidfxZ8FOLlfGMp/HBlqi1CcTKePs42VgZt+8UaHuqLvcTjJXpf/Le8tE97fVaekw42iuM70U0jFdsD4js5IrtP9D3aVly/Q/7p+u3+vrt6zcsfuWJ9bkujio4NqrS3Tt7TlRsFwixvQ3/ogAAAAAAAAAAAGA2AsE9x0xGdF++LEo3XldJxSMKpR+P5VZ01xldtLR9aMX7rjz1G7d3TNc4DNzyeRZdsnqlUp78HEWi7/cJ7q5DnURxVaewqGpEzChRfa6oUgFWv7N7ErEyHke45oi0bn/c7HXyuOf5eMVTJx4rQ+QpLEuBAq1+Z7cUK0Oy0DyFWJlxiqfK7frFeW/x1FzHykhOc//3U4yVcfvhe/q522aJlZGek+GxMscf+XFZ6t0d9Vwdu1L/uoJkIT0ouIeJ7Vv143y36ro79k3X7/O1R3+RKH/+0abE2y/V5zqvXSBEduFsFw73E2RtS2dPM/4lAQAAAAAAAAAAwGwFgnseaKhO1pEpuk8YWXRXn4oSH8ztpRlLLupNrVi1dtk3m1qncywGbvl8LFZz3rmsqPRqUiJLfdnkmuFwtt3pcqyMoviEd4+zWyU5Vsbrcpfzx7UcxMqEZL1TQDj3FE/NdayMx+Vut00+oT1sfyJv8VQnVsaXSZ/3WBnftuGxMpTZ5Z6DWJnJFE/NHivTt/mO8rGunfVcTdcTGUK7cKl7c9qZtcxEQVTH3R6x5i/p0w+r/uKO303nb/J3D95bW/Xbhx6ofOf1mny0D7EdAAAAAAAAAAAAcxkI7nmioTrZqM+aTmSfkhJmiO7LF0UNpzvvye3lUeMlNLLy9FuXfftn66Z7PAZu/nw8dtqHLmZFJVfpd13cExfDrFgZ2wHOFENQZ/Z3osCqEN/Hj5WRY2E8Ge8stCirR7R2HezydgHhfcKxMt52PM55Chfgw4qn5jhWxls81eecz1Q81RXKpf5MKVbG53LPGCtDJL0skG4nPm2xMiHFU7vv+T9V2uDxz5KmXqq3aWa0i7cTPFAElUmFUoXb3RbbD+jT96r+4o4np/t3uO3H/3RD1cuPbyo+kh8z/QtHhun+d49DbAcAAAAAAAAAAMCcBYJ7Hpmq6C4KqWpdSs77NbS0pr377IuuPOer3+yY7jEZ+bf1CaViwScpWvRRK99dcyJimO1yZ6q4MS0BWyNnUkzx3XGMB/PaAzEwU42V8Ua+ZIyV8RVP9Tq/px4r4xf6/SJ65liZQLHVScfKuOtcMZ0o8HLBbpN8+/pc9HKbuY6V8brcJVE+a/HUcWNljt51w6k8nbpC/+JScmJlmBDQFa+z3cpqZ76sdsYO6ffv3VVfv711un93j21uTax49ddNVdufridNy8sxhNh+395JlYq4taWzZx3+xQAAAAAAAAAAAMDJAAT3PDMZ0d3Y75pyuuiCuCm678296D6aWNQ7WHPW2lV/98NpF/8Gb/4ci646fwkrLv8DUiIfNqJLnEKqhlNbRLNYjnHiTqyM7V5XhFgcMYT5DLEyXiHbLRqq+cRy13HuFcG1KcfKkE/AD3fKc2/xVNlxTloOYmWsfige1zvzC+zGPorsqCdf8dSpxMqMVzxVjr7JVjx1hmJliB29/U8v0Zcu1decQxTiYieKBNztTC6OKoR2dvfiL3/vlyxeyqf79/bmnf9YW9Kx44HE7ldr8nUM4WpvOzg4mV2bWzp71uJfCgAAAAAAAAAAAJwsQHCfBiYrul99VRl9rK7YENyF8J5r1JIyGq4549Y3L/ijjR//TH3vdI+LIbyf/qEqFi//jCG8iygZV4DWnPgTQ4xXNDf/XeHONqYjXoqnkSJXvMVPJxArI4vkkqM8NFfdUxyVy8VTcx4r4829l4uSat5YGY/LXQs4yKcSK+Pd1t9X8r2I8MbKyBE0OY+VkbYbr3jqCcTKHLntK4v12SX69DF9quKmgG5nr7sudjO3PWJ+Nog47nbGDjMl+pNFX/7nh1hRMZ+JZ8/2f/3nDVW/eXB90UBPXtoX0TFCbBfu9kkAsR0AAAAAAAAAAAAnHRDcp4nJiu7C5S7c7vyQQumno0Sp3PdteMWajt6VZ135vuu/3T4TY2MI72suXMziZV8gRTnNK7hbYrshptpFVRVbSFe9GewsWDw1a6xMxriYiW4nO7u5L1Yme/HUcPe7JgnGQZE/N7EyJBVPzRwr4zlOzmNlshZP9TrNQ87F5ERiZdx59lgZOvr9r35In9Xp0wVkudm517XuFkS1v+ceIT6if3WYRSI/WfSVmzazWHxGhPZtP7+7pmJXe9P8l7fW5esYQmwXxVFFkdRJALEdAAAAAAAAAAAAJyUQ3KeRyYrua1bH6GtfqqDiEdPpnutiqoKx5EIaXFKzseZbLRtmanyE8B57/yVnkBK5RJ/OIiOeRc5oZ7K73RagpeKpkqPdK2hPLFbGWzzVGx0TdIB7nfLe4qk5jpWh8YqnZoqGmWSsjMflbsfKUPAFQ5hzPa+xMuTJdnf6NLVYme47r1vI02OX6VdOiO1VRN7s9RDB3Y6S8RZKZew1Fo01L/ryTS+zWBGfqd/RG7f/XeOCHS+IwqiJfB1DiOxCbJ9EcVQBxHYAAAAAAAAAAACctEBwn2YmK7ovXxalL36hLK/FVAVjiQXtx6vPWHva+h+3z9QYDd78Jyx21kcXUqzkU0xRPuREx5AdJ0N21ruZ504+lzvZue8Blzr3xcq4xVOnFCvjusXtWBnyFE/1FArVgiJ/wFk/iViZcVzuAVe8HCtjtz/lWBnvmJlPGOm4ljjv7u+LlXH7mDlW5gRc7lliZXrv3VCq9h35IGnqp/QVKymQzW4u82AuuxwlY35WlF+zWPwXi770zztZNDZjQvuLj29OLHziP5rm7361nnj+uiHiY0SMDMR2AAAAAAAAAAAAgCAQ3GeAyYruJSWMvvblClpzeoy07RFSX4vkpX9qWQUN1Jy9ceXGn2yYyXEavPlPqOicSxdSNPZJUiLnkyMq2+I7CxfcM8XKkJXjzgJFUTVHxHa/yxIr4xPOJxsrQ34hnzIVPw3GyvjiciiT4O4RyZWwWBl/Jn2YIM5zUDx16rEyUyye2vfz75WoRzo/wDX1fOL8g+QV1KVsdsfZLn82t2EsYgnug/rN18piRY8s+spNB1gkOqPPlO33/kt98ne/birftzuRz+Ns2TdAW7r6J7s7xHYAAAAAAAAAAACc9EBwnyEmK7oLnGKqXVYx1TzkupOi0EjVivajtRevff///Jv2mR6v1KN3lbKi0t8jpvxXfYqTENwVq2CoVyQXfbey3iVXt3G3i++5HadiifOKloNYGU+8ixUrIxdP1bKK6K4Ar/ly108kVoZcd7kii+WaL1aGS/E6/iz4qcXKOH2UBHezLW0CsTLePk42Vsbtu/F9/yN3l4699+Z5XE3XkqZ+gDIL6pkEd2+hVKa8rt9fjy/6s3951LiXlMiM/i6e/NVDNac9/8Cm8t2v1UdSw3k7jnCz37enj17rGZlsExtbOns24MkPAAAAAAAAAACAkx0I7jPIVER3UUxVCO/FmkLqU/nJdRdoxSU0sKZ244r/+68bCmHMhkTczAc/+0FS2O8TYwlLFFYdUVZRZNe442x3hWVyxVtH1BXFVxVuCvWK5o2VkXPePTEq2tRjZUgWqLknc16OlfEK9JovwmWKsTLjFE+V2/WL897iqbmOlZFc7v7vs8fKHL//pqR67MB5PD12Hte0M8g8UcWKWckmqDPy57ab64b06QVSIq2Lvv6DvUxReCH8FrbdvuGGU958Zn3R4X15dbWLvPZ79/RNtjiqYG1LZ08znvgAAAAAAAAAAACYC0Bwn2EaqpO1+myrPp2waCZy3UXEzIL5ptNd26vk6S5hNFxV3TG8fNXaVX97R1shjNvQzX9CsQ9fsUrv2/nElPPIFoUVycXtuNwNTGFaXnbjRrwZ30xRmRLRKBJRHZd7oOBpmHPdFao9xVNzHSvjcbk7LxV8QnvY/kTe4qkzFivj2zY8VoYyu9wDsTK9P/3m2Xx06HSups/VR34ByRnr1lsVfb3jZPcJ6v65/f0b+vTE4ut/9DgXV5MVxuPyjZv/qqa053BTYterdUxN5/VYU8xrF0BsBwAAAAAAAAAAwJwCgnsBMBXRXeS6f/Gacjrv3CJDcFdfzlPEjI4aL6HRRUub91z+p+t+7xOX9xbK+I395mdJYqyWmPIBfV5JruDOjf8ZAi2X4mXEjGkeZzXn/gKbrqjOopYAH1UNF7xX3J5orIzs/Pbmwwdd5pmLp+Y4VsZbPNXnnM9UPJX5Y3qmHCvjc7lnjJUh+1yOt966RO05eDYfS51Gmno6+QVzbkfAMHd9dsHdXj6iH1T8Fp9cfN1dhwrpOfHY5tbEKXtfvWHB68+sLzm2P+/HE0J728HBqTQBsR0AAAAAAAAAAABzDgjuBcJURHfBZy4rpU9fVkJ8kOU1YkagllX2Hv3g728847p/vLXQxnHsmZ+fSYyJ6VxrleYW0pTd7MzrbA8s2wI994rxwkGvRNIsEtNIiaoUjabDYmV8xVO9zu+px8r4hX6/iJ45ViZQbHXSsTLuOldMJwq8XLDbJN++Phe93GZIrEz/g7fXqMeP1vDUyKlcTa8irkkxMELt53IOu+1uZ5Lg7kTKhGS3H9W3e0m/plsX/Nnt75BH6C8M2ltuq1/06tZNZZ07a/J9rGOjKt29s2cqETLiZdwlLZ097XiyAwAAAAAAAAAAYK4Bwb2AaKhOCrFdiO61k9l/zeoYfe1LFYbrXX05QtpbeSzoqCg0vGRle8/5F687q/Gv2wptLMeeu7+YOBfC+0f0j4stt7vltDbwR8xIOeDGovsd12zxnXm3tZzySkSlaExlSiytz9OGE94fK0O+XPhARI0tghO57nvZcU5aDmJlrH4oHtc78wvsxj6K7KgnX/HUqcTKjFc8lQ+1/ftitXt/FR8ZrOKpkRrS1FPIzlzn3Jmb14NJwrktqBt57cyJknEFd7lQ6rAhsiuRlxb82b+8aF9rpigFdR+/dH9TTeWubU2Ltj1Vx1KjeT+eKIoqiqNOIUIGYjsAAAAAAAAAAADmNBDcC4ypiu5CbBe57mtOjxE/pFD66fxFzAi08koaXnZq846L/njjxz9T31GIYzr27M8TpuNdnxiZkTOGgV0uqioJ6aaj3ed2t+R6+bMxExquRqYAbEXTiPWxuBDex4QYz6LxNIsVj1EkouUkVsZTPJXJRUk1b6yMx+WuBRzkU4mV8W7r7yv58u29sTJSBM1Q233L+UBvhTbcv5hSI4u1dGqlU9yUu4VMrYuhzzlz1hsudybFwnC7YY/bnVw3+1HOlFdYNPbKgq/c8oZ9joUmsguM+Jh922+Yv+2Z9aWH38v/PwRJTo/09tPDz08pQkaI7Fe2dPZ04EkOAAAAAAAAAACAuQoE9wKloTrZpM8aJ7v/x+qK6eqrygyxPf20EN/ze6l5SWnv0VXn3fbeR6649fc//dneQh3XsWd/voQYO0dfXK1Pbt67cRIBod2ae8R4+zturzEFd2e1s2Tp8tz6pF8AjbNo8SiZefBChE+REtVYcWnKdsWT1/2uOT/RsOKpJxorE+aK9xZPzRwr4znOicXKjO1+qVw9uq9C7T20gI+livjIwDKupueRmExnuhUDYwvt3BLUpYgYczszDsYR3Hmg4Cn3Zbnr/ehikWg7Ky5/JfnFf3iXzL9c0PR1Bfvbb7/j243z976yqbJjR2I6jjd6eprufrGXdu4am1K3yXS29+LpDQAAAAAAAAAAgLkMBPcCpqE6uUGfrZ/s/suXRQ23+4L5ihEvI2Jm8s3owlM6jq85f+PqG7/XXOjjO/bM/1tCivJ+/WdQTSJ2xhTRpWKf3C2yysl2tftd7vJnZgru3JXdzUga84C2AG/MNHLb5sx0xReNEikaxeIpo0hrrGiMRYsNFZRVLhwiJ1aGxiuemikaZpKxMh6Xu+YYzi0hP7XzxQQfGYhp/T1lfGSwnKdTRXx0cAGpapG+vMAW0Lk5t587rrCeSXC3xHXrvYbCneKn5MbJuNva2ewjpER2sVh8WySx+O15f/i/jljjorFojBfy/bit+da6sr2vNy3c8WzNtDz8yzjtWTRCdz3UR8PDUxqa5pbOnrV4YgMAAAAAAAAAAABAcC94GqqTjfqsabL7i4iZL15TTuedW2QUUlWfy29BVQNFoaGlp7b3f+TSdWd8/vq22TDOY7/59wQpEeF6r9b7f5qx0nG82+K6vbVHiCeSRXWy5XbubqtJUfGc+9rRGGncVvTtbZl7CM38mepzbgn1LF46bB+DFZUOUySqme1qxjqlcmG/7a8nx2lPUvFXuw9WI9x6x2C9LBD/V/uOVFA6FXVqxorTGOxdaH/mYyNJ0tQiS/C2W7Kz1RXr/MW5GXNurGPMGjOngCk5RU6Nk/MXOLULoFr7kyzEW+K7vo8S2cOiRbtZSfmuxOe/uVNfrzoie6yIF/q99/jDD9ad/vwD68s6d9XFBnqm5ZjKco22iAiZJ4am2tTGls6eDXhSAwAAAAAAAAAAAJhAcJ8FNFQnRZ67yHWfdMSEiJj5zGWlhgBvuN23R/Ka7W7eXYwG3vfBtqGlKzeu+dq32mbLeA/e/Dml6IL600mI74yJebn1lS2+Owqzs96JjyFX17bEa+k7R1y32mKWUC41KRvrnTgaMt3hth4u9tOkiBtb7NZIdsxb7TGPi94Wtk2XuKTBa1ZZU8Pk70S2OG14BHAnYUextHtT+OZWO1aeOtkOdUdcl7LZ3fYUqU03HsYcLtv9rmiSQM8i0XcoWrRHKZ23Z97Vf73LFtf1Kc2KirXZcp+9+h8/rKno2LF+3u7XGmO9R6bnoEVEfWtSdNcjfdS1Lz2VlkR0zLqWzp5mPKEBAAAAAAAAAAAAXCC4zxIaqpM1+uwBmmQxVYGIlmn4QrlZUHXQcrsfyv8toMZLaGRJddt7539y40XX/GnbbBv7safuS1BEWU6cVhDx5cQUIcC7wrkrQHNHNHeVcnIiabgs0DviODnCt7ObZsXTaFyKhbdEao2kY5C1DbPEe5LEe1tgzyC4c2b1mNx9zWNwe/tAOwHB3W6HOSJ6QHAXor2xpHiKobrxMookyHuiZIz/EY2SEj3IY0XvRCoWvFN5xXV79a9VMlzslCbGVFZUwmfT/bR1yy9rVj997/rYkUON8d7D03Zc4Wp/anSANj8xNNUIGSG2i7z2djyZAQAAAAAAAAAAALxAcJ9FNFQnhcN9E02hmKpgRtzuxt3G6PiKM9sGlq/eeNa677bN1uuQ2npPgkUiy/XzWU4aryKFzSeSQ1q45D7XfGK7T6QPutvdXHdLmHcN8lwSuy2BXuzPnaKsjkPeEMHtKBtDBJdc8Kb+z6yuSIK8cRzGPWK50y9bDLfc9o5DnklRMkzKZSczf13KbefczmAnc5mbofJO8VPOWLToPYrGDirF5YeUZNXB0kuuOaDvIqzYacaMucbiZdpsvG+ee2xzTfJ3v14/f9crjUX9x6bvwJar/adPH59qYVQBiqMCAAAAAAAAAAAAZAGC+yxkqsVUBXK2uxDbhdtd61Kmpf9qSRkNL65u6/3IJza+/4+/1jbbr0fq0R+WsHjpIiHAc84XEedVxJRSIrlIqi2Ui5+cJkfNuCK9VwB3HPJueoxm/V590TOaJexbLnivSO6LhLFc8HZsu5Ehb78M0KzipaEOd0vQtzLVLTd6UHB3nPhS3AxZ5yVEdtsJz1iKRWNHKFZ8hJVUHI4klxwu+a9XHSTTuT4mnOtirpRUpmf7/fHsIw/WLP3tA+uLj3Q1Fnfvn9ZjK2eq9FT3YC5c7QIURwUAAAAAAAAAAAAYBwjus5SG6mQ9mcVUE1NpRwjuQng33O5diiG8T4vbXUcrLqXRRUvbjp1/ycazr13XdjJdn5FffLdMmbdwAUWiS0lTq/RVlcQMJzw50S/c8op7XeR2pIxQ2pnrbrdFcqfuKXf2MYulunEvcntyZIzlaufcFueNq+CJkzHb0Gy7PnPz3kly2ZMj5EtxMpbYrpnH0U/WODumpFisqJvF4kdJiaQiyaVdkQWnHC9630W9+jYpU1g3xHVx16WVsgQ/We6B3z10X01Ve9v6eNeexvixg9P7YC/j1HtGin76eH8uXO2CtchrBwAAAAAAAAAAABgfCO6zmFzkugsCbveXo6TtVabxLmQ0uuK0tr6qVfes+etbm0/mazb66B1VjCnzSInMJ01NcMbKSNOW6F9FjQ1MMdzIgmfEHcGd25nvpoOcywK6XCzV1NBdtztxeVmKpXHEe8fB7sTGcOd4nuKmbsSMGRnDnNx5JTrEotFBFi0aZLF4PytPdLNYyWjxhy47QKZbPW2I6+KtgX6phcCuVCxQT9ZrvK35e3Wl+zvWL3jrxTplaHB6D67/hCNnqtQmXO1bcuJq79CnK5HXDgAAAAAAAAAAADAxILjPcnKV6y6Q3e78kGJku09HUVX3bmQ0vGRlh1pRsfHN//K51o9/pn7O5ET33/KFSGzNBxfpi3GmKIs4i0RJTSeJKWWkqRHOaIn7e7VjZPS1mpsF77jS5bgZX5wMJ/l7cp3vmuVut1zq5ieWZrHiPmPJENOLh0QLkflLjUqfSmLxQHTpGYetayds1GnDrc7EaxumKYmq1Fz6Lb76o+82Jvdsu778UEdtdKBv2o8viqLuXzJKP//1YK5c7W1kiu3IawcAAAAAAAAAAACYIBDcTxIaqpONZArvU4qYEWK7KKgqCqsKhNNdey1CfHB6b5XRivm9fafXNh/+yOW3/bePX96BK2wyvHlTnIgvMgRyIZanx+ZTRCkjJwKeC8d5lMVLy5ysd7vAKnny3YlFY2mlclG/JeAbW0YWrzykJE8ZYcw0y5M5Fy71lPG40P+vLFgxhCth8tjm1sQp77TfMH/Hc1+M9x2tiaSGp/8hXsZptHaM2t4cNlztOWJjS2fPBlxhAAAAAAAAAAAAgBMDgvtJREN1UkTLiFz32qm2tWC+Qg1fKKc1p8eMmBnt7Qipb0WmLd/dRhRYHTj17Nb+xSvuef+f/30rrjIoBHbc8fe10Z4j1897b0djvPvAzHTCio/ZzofpP+8fpO5jWi5aFW524Wpvw1UGAAAAAAAAAAAAOHEguJ9kWBEz6/Xphly0t2Z1jBquKTcEeOFyF273ac13t9CKimlkyYoOqii/7Y0P/1HzXIqbAYWBcLMvO/xWfcm7O68v27+3tqjn8Iz1RVmlUe+pKWr5z4FcxccIxAuttYiQAQAAAAAAAAAAAJg8ENxPUhqqk/Vkut0TuWjPjpmZsXx3idHFy0gpLmp++6KrH/xvVzfC9Q7yypO/eqh22c7fXl+++/X60qP7Eiw9NmN9YVWcUrVjtPV3OY2PEaxr6ey5FVcbAAAAAAAAAAAAYGpAcD+JsdzuD+hTXS7aE2L71VeV0UUXxI3PM5XvLjO8eHnHcKKqdWjlmtve/9VvduCqg1zw+MMP1lS9+Wx9Wc+B6+fvaa+hdHpmH9RVnCLnqPR8hxkfMzzMc9V0O5mu9nZcdQAAAAAAAAAAAICpA8F9DtBQnRTxMiJmJidu9+XLonT1H5bOeL67jJH1fsqqdnXhknsOn/+J5gsv/TRiMcAJYRRA7dpRX9bxxhUlPYfqi7v3z/wDuoyT8iGVdo+MUsu9A7nKabe5taWzZx2uPAAAAAAAAAAAAEDugOA+R8hlQVWb884tMhzvIt+9UIR3wciiZcTLK1rHovEH+z/62dazL/scxHeQkTdv/2ZjybGDV0SPHqgv7d5PSmpk5h/MQmg/V6Xd2ig9vGU4lzntgg4yXe1tuPoAAAAAAAAAAAAAuQWC+xyjoTq5gUy3e84QETOfvqy04IR3weiSFaRFIq3a0mUP7jj78lYUWwXCyb6iq72+qGvvFSWDfXXFnbsShdI3W2jvSaTp4S1D9NwLo7k+hMhp34jCqAAAAAAAAAAAAAD5AYL7HCQfbndBIQvvgtSCKkqXlLePzF98z57z/0CI7x24G+YGj21urVlwdG/dspcfvSJN0frSI53EZjiX3fMgzr/QLu51uNoBAAAAAAAAAAAA8gwE9zlMPtzuorDq79eV0Mfqio3lQhTezTuf0ciCUzr6lp/RyiKxB9d849Y23BEnF2/c+e3aSPfB+vJ9u64oOX6kNjIyVHgP4CpOyqq8Cu0CuNoBAAAAAAAAAAAApgkI7nOchupkDZlu97pctptJeNf2KMQHC++2G16yktRovDV9yvKnDp53aduFn6pvx90xu3h2S2vNwne315W8+dIVPF5SV961M1EIeeyhD94qTpFzVDoWy6vQLu7hdXC1AwAAAAAAAAAAAEwfENyBQUN1slGfbdKnnOZZC7FdRM18rK7EjJrR0fYqpL0WKUjhXZCunE/peGkHTy5o61td+1TnojPaED9TeIiYmPnp43WLXvvNxZHug3XRkaGaePeBgu6zskozomOOjar5FNqFk/22ls6eDbhLAAAAAAAAAAAAAKYXCO7AoaE6KcR2Ibo35qN9T8a7Dj+kkLo9os8L+zbksSIaXbysN5VY2DZUXPnUSPXp7bVfuL4Nd8z08tJ/3FVb1rWzLp4aPi/ed6Su+Oj+mkhfT+F3vIgocqZKbJVGuw6k6PkXRvMltAvEfSmy2jtwxwAAAAAAAAAAAABMPxDcQYCG6mQdmcJ7bT7aX7M6Rp++rITWnB4zPgunu3C8a11KYeW8Z4AXxWlkQRWNRUva4iPH27tXnrNt3/vr2j/+GcTQ5ArhXq/a+0rt/He316Yi8YujpNZV7N9LbGR49jxck5yUM1XD1b5z9xg9vGWYdu4ay9fhOsiMj2nF3QMAAAAAAAAAAAAwc0BwBxlpqE7eQGZR1UQ+2l++LGpkvAvnu4HIed8bIe0tpWDjZjKRLimj4XlVVDLQ3Ta4uKb92NkXvTuw5LT23/vkp9twJ2Xn8c0P1s0b7K6Z/9aL50UHe2ujfd21sdGhRNFAz+w7mSIyiqAKkX2kWDOc7E+2DVP3MS2fR92oT7eiKCoAAAAAAAAAAADAzAPBHWQl3zEzArvA6oUXxD1xM0bW+15lVo/fWEWS1LLKjpFFyzrSZYl2ta/nXeWMs9q7kys6Lvz01R1z5T564lcP1SyhgRr2+su1sXQqoRC/uOj4sZp4z6GaogLPXZ/Qg1Rys3ftS9OTbSPU/lqKhod5Pg8r3OzrEB8DAAAAAAAAAAAAUDhAcAcToqE6KeJlhPBel8/jCLe7EN7tuJnZ7HrPRnr+YkpVzBfO+A4+MtoR42PtVFnZ133mhzsUxjr2ly7rnU0RNY9tbq0tU4cT8wcPJCLHjtbGew7PS/f110Zi0QRFo7Ulxw5S0aHOk+tHIdzsyzVDaBeCu+lmHzEE9zwj7gshtLfhyQQAAAAAAAAAAABQWEBwByeEle/epE81+TyOcLp/rK7EEOCFA17guN5nSdb7ZOFFRaSWz6PRWBlpTKFIhDrio4Mdw/OrSItEO6Lq2Lvdp55rb95x8JSzO0KaEZnyE44YeWxzq/hLhkBmf9lwb6Kid5+xvuLwu6SMDK1MlVTWJN97k9JFxYlRVlTLVJVKUv3ExlIU6e87uX8AlsjOqjVjLqJiRGSMENvz7GY3rrU+bWzp7GnGkwgAAAAAAAAAAACgMIHgDiZFQ3Wykcx895p8HyvU9d6lEO+0xHfgEo0Sj0Qmv7+mERsbwzj6kEX2YdV0s4tpGtzsAvHi5DZCTjsAAAAAAAAAAABAwQPBHUwaK99dFFa9nvJUWFXGdr2fd26Rk/XuRM7sVYj34HYGOXw4ilz2VZpRBFU424XAvm17ira9Nq1/XoGCqAAAAAAAAAAAAACzCCiUYMpMt/AuWLM6Rhd+JE615xa5kTODzHG980O4tcGJIzvZhci+c/cYPf/C6HQUQPXTTGZ8TAeuCgAAAAAAAAAAAMDsAaokyBkzIbwLhOP9vHOKPOI7YmfAhPBlsguEg1042WdAZBc0E4R2AAAAAAAAAAAAgFkLBHeQc2ZKeBeEiu/kiu/C+S6c8GAOP/REVEy1RkwI7UlTUJ9hkV3QTBDaAQAAAAAAAAAAAGY9UB5B3phJ4V0gxHdRaNWT+a4jst75IUTPzJmHXBknVsVNF/tiMyqm+5hGO3eNzUQmu59mgtAOAAAAAAAAAAAAcNIAtRHkHUl4/6I+1cxEH5Yvi9Ka1VG66IK4sSxjiu+mCI/CqyfBQ80W2Ks0c15mOtZFHrsQ13fuSlPXvvRMdlEUQL2NUAwVAAAAAAAAAAAA4KQD6iKYVhqqk436bD3NkPAuEFEzInJm9ekxo/iq7H43st8P6597GGmH4ICfFQ+xpCWuJ7lHYBcudkNg3z1muNlnKCpGpkOf7iEI7QAAAAAAAAAAAAAnLVATwYzQUJ2sI1N4r5vpvtju9zWWAC9nvwts57sZRYMM+Bl9YAn3uhDW5+vTYrFsRsQIhGtduNeFwN7VlTYE9wKhXZ9ua+nsacYVBAAAAAAAAAAAADi5gXIIZpSG6mQNmcJ7Pc1AznsYtgBvzn0OeEFK5MArxA8z4sdMAR5RNHl4OGUR1wWmsK4WkoPdT7M+3dPS2dOGqwkAAAAAAAAAAAAwN4BKCAoCK+e9kcwCqzWF1DchuC9fHqXqZVFavdqc+13wAsMJP6gvDJpxNDRAcMNP5CEkImDKiZQqjUiI7GVkRMTICPd6Z5fquNhnOIM9Gx1kxsY0oxAqAAAAAAAAAAAAwNwDaiAoOKy4GVFgtbFQ++gX4RfMjwSd8BaGED9GRi48H5Ac8ak59KBJcsOdLlzqTMwXi8+mg92PcKx3d2uGqN61TzXc67OAVjLd7K34BQMAAAAAAAAAAADMXSC4g4KlkF3vYQjXe/VyEUUTodISZVwhXmAK78xxxnMrrsZgljjkHTE9Zsa/GIgomBgFYmBkbGH92DHNWlYLKXd9InQQ3OwAAAAAAAAAAAAAQAKCO5gVNFQna8kU3gsm6/1EEHnwpaXMKMwqEGK8wP48HobwPuD+XA2h3mf8tt3zU34oyMK5jRX14myTRUi3EQ71oWFu5KyLfPVO/bMx70oXYt76idCsTw/CzQ4AAAAAAAAAAAAA/EBwB7OOhuqkEN1F5Ez9yXJOtiAv3PDCFW+ui1BJqfkTHc8pP13YwrnNrl3mcvcx150+SyJgThQhrj8o5i2dPb34FQIAAAAAAAAAAACAMCC4g1mLFTkjRPcr6CQS3yeCHV8TxkRd8zJDw5qRlx5YP8QLuUBpvmknMzKmFZExAAAAAAAAAAAAAGAiQHAHJwVzWXwHOQUiOwAAAAAAAAAAAACYNBDcwUmHT3yvo1mY+Q6mFTsupg0iOwAAAAAAAAAAAACYChDcwUmPlfl+MZkifA1GZM4jMthlkR2Z7AAAAAAAAAAAAAAgJ0BwB3OKhupkLZmud1uAB3ODNn16isyomHYMBwAAAAAAAAAAAADIBxDcwZymoTpZR6YAL+JnajEiJw1CVG8jU2SHix0AAAAAAAAAAAAATAsQ3AGwsLLf68gU3i+2lsHsAAI7AAAAAAAAAAAAAJhxILgDkAXLAW8L8GJeg1GZcYSYLgR2Q1wXyxDYAQAAAAAAAAAAAEAhAMEdgBOgoTpZQ6bwLovwCYxM3pDFdTEX4noHhgUAAAAAAAAAAAAAFCIQ3AGYIlYUjV2MdSWZLvg6jMwJIwT1Dn3aRqZzvQPiOgAAAAAAAAAAAACYTUBwByBPWG54MdXp0zxyI2lq5vCwdFiTENf7yBTWe1s6e9pxxwAAAAAAAAAAAACA2Q4EdwBmAEmMtydbkBfUzeJTa7PmHfr0LrkCO0R1AAAAAAAAAAAAAHDSA8EdgAJFiqqxqZOW5/m+C9tmsrSFrOsgU0C3EeK5XagURUsBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQG74/wIMACzLZmR7d1wqAAAAAElFTkSuQmCC"></div>',
                    footerTemplate: '<div style="font-size: 8px; text-align: left; margin-left:30px; width: 100%;"><span>Copyright © 2023 Hombolt inc. All Rights Reserved.</span></div><div style="font-size: 8px; text-align: right; margin-right:30px; width: 100%;"><span class="pageNumber"></span>/<span class="totalPages"></span></div>',
                    displayHeaderFooter: true,
                    margin: {
                        top: "80px",
                        bottom: "50px",
                        left: "20px",
                        right: "20px"
                    },
                });
                res.setHeader('Content-Type', 'application/pdf');
                res.setHeader('Content-Disposition', 'attachment; filename=' + milis + '.pdf');
                res.setHeader('Content-Length', pdfBuffer.length);
                return res.end(pdfBuffer);
            }

        } catch (error) {
            req.flash('error', 'Something went wrong');
            res.redirect('/form-builder/view/' + iFormBuilderId);
        }
    }
}

exports.assign_form = async (req, res) => {

    if (req.session.email) {
        is_layout = true;
        const iFormBuilderId = req.params.iFormBuilderId;
        const FormBuilderData = await FormBuilderModel.findOne({
            _id: iFormBuilderId
        })
        const departmentData = await DepartmentModel.find().sort({
            'iPosition': 'asc'
        });

        res.render("../view/form_builder/assign_form", {
            is_layout: is_layout,
            FormBuilderData: FormBuilderData,
            departmentData: departmentData
        });
    } else {
        res.redirect("/");
    }
}

exports.ajax_role = async (req, res) => {

    var iFormBuilderId = req.body.iFormBuilderId;
    var iDepartmentId = req.body.iDepartmentId;
    var vAction = req.body.vAction;
    try {
        var SQL = {};

        if (iDepartmentId.length > 0) {
            SQL.iDepartmentId = {
                $in: iDepartmentId
            }
        }

        var DepartmentRoleData = await DepartmentRoleModel.find(SQL);
        var assignFormData = await AssignFormBuilderModel.find({
            iFormBuilderId: iFormBuilderId
        });
        var assignFormIds = assignFormData.map((result) => {
            return result.iDepartmentRoleId
        })

        res.render("../view/form_builder/ajax_role", {
            layout: false,
            iFormBuilderId: iFormBuilderId,
            DepartmentRoleData: DepartmentRoleData,
            assignFormData: assignFormData,
            assignFormIds: assignFormIds
        });

    } catch (error) {
        res.render("../view/form_builder/ajax_role", {
            layout: false,
            DepartmentRoleData: error
        });
    }

}

exports.assign_action = async (req, res) => {

    const iFormBuilderId = req.body.iFormBuilderId;
    const iRoleId = req.body.iDepartmentRoleId;

    if (typeof (iRoleId) === "string") {
        var iDepartmentRoleId = [iRoleId]
        console.log(iDepartmentRoleId)
    } else {
        var iDepartmentRoleId = iRoleId;
    }

    if (iDepartmentRoleId.length > 0) {

        AssignFormBuilderModel.deleteMany({
            iFormBuilderId: iFormBuilderId
        }).exec();

        for (let index = 0; index < iDepartmentRoleId.length; index++) {
            const element = iDepartmentRoleId[index];

            console.log(element)

            var AssignFormData = new AssignFormBuilderModel({
                "iFormBuilderId": iFormBuilderId,
                "iDepartmentRoleId": element,
                "dtAddedDate": moment(new Date()).format("DD/MM/YYYY")
            })

            try {
                AssignFormData.save();
            } catch (error) {
                console.log(error);
            }
        }
        req.flash("success", "Successfully Assign Form.");
        res.redirect("/form-builder");
    }
}