const FormBuilderModel = require('../model/FormBuilderModel');
const Paginator = require("../library/PaginatorLibrary");
const CompanyDetailModel = require('../model/CompanyDetailModel');
const RoleModuleMasterModel = require('../model/RoleModuleMasterModel');
const DepartmentModel = require("../model/DepartmentModel");
const DepartmentRoleModel = require("../model/DepartmentRoleModel");
const AssignFormBuilderModel = require("../model/AssignFormBuilderModel");
const fs = require('fs');
const path = require('path');
const puppeteer = require('puppeteer');
const moment = require('moment');
const UserModel = require('../model/UserModel');

exports.index = async (req, res) => {
    if (req.session.email) {


        var SQL = {};
        const assignFormData = await AssignFormBuilderModel.find({ iDepartmentRoleId: req.session.role })
        const fbData = assignFormData.map((result) => result.iFormBuilderId)
        SQL._id = {
            $in: fbData
        }
        var formBuilderData = await FormBuilderModel.find(SQL);
        console.log(formBuilderData)
        is_layout = true;
        res.render("../view/form/index", {
            is_layout: is_layout,
            formBuilderData: formBuilderData
        });
    } else {
        res.redirect("/");
    }
};

exports.ajax_listing = async (req, res) => {
    // var iFormBuilderId = req.body.iFormBuilderId;
    var vAction = req.body.vAction;
    var vPage = req.body.vPage;

    if (vAction === "search") {

        var SQL = {};

        try {

            const assignFormData = await AssignFormBuilderModel.find({ iDepartmentRoleId: req.session.role })
            const fbData = assignFormData.map((result) => result.iFormBuilderId)
            SQL._id = {
                $in: fbData
            }

            // Pagination
            var companyData = await CompanyDetailModel.findOne().exec();

            var dataCount = await FormBuilderModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = companyData.vItemPerPage;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;
            // End

            var formBuilderData = await FormBuilderModel.find(SQL).skip(start).limit(limit);
            console.log(formBuilderData)

            res.render("../view/form/ajax_listing", {
                layout: false,
                formBuilderData: formBuilderData,
                paginator: paginator,
                datalimit: limit,
                dataCount: dataCount
            });
        } catch (error) {
            res.render("../view/form/ajax_listing", {
                layout: false,
                formBuilderData: error
            });
        }
    }
}

