const async = require("hbs/lib/async");
const HolidayModel = require("../model/HolidayModel");
const Paginator = require("../library/PaginatorLibrary");
const WeekendModel = require("../model/WeekendModel");
const CompanyDetailModel = require("../model/CompanyDetailModel");

exports.index = async (req, res) => {

    if (req.session.email) {
        is_layout = true;
        var weekendData = await WeekendModel.find();
       var weekData = weekendData.map(result =>{
            return result.vWeekend
        })
        var holidayData = await HolidayModel.find();

        res.render("../view/holiday/index", {
            is_layout: is_layout,
            weekendData: weekendData,
            weekData: weekData,
            holidayData:holidayData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/holiday/add", {
                is_layout: is_layout,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iHolidayId) {
            try {
                HolidayModel.findOneAndUpdate({
                    _id: req.body.iHolidayId
                }, {
                    "$set": {
                        "dtHolidayDate": req.body.dtHolidayDate,
                        "vTitle": req.body.vTitle,
                        "eStatus": req.body.eStatus
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/holiday");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const Holiday = new HolidayModel({
                dtHolidayDate: req.body.dtHolidayDate,
                vTitle: req.body.vTitle,
                eStatus: req.body.eStatus
            });
            try {
                Holiday.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/holiday");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async (req, res) => {

    var iHolidayId = req.params.iHolidayId;

    if (req.session.email) {
        try {
            var holidayData = await HolidayModel.findOne({
                _id: iHolidayId
            });
            is_layout = true;
            res.render("../view/holiday/add", {
                is_layout: is_layout,
                holidayData: holidayData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.weekend_action = async (req, res) => {

    if (req.session.email) {

        let vWeekend = req.body.vWeekend;

        await WeekendModel.deleteMany({}).then(function (error, result) {
            for (let index = 0; index < vWeekend.length; index++) {
                const element = vWeekend[index];
                const Weekend = new WeekendModel({
                    vWeekend: element,
                });
                Weekend.save()
            }
            res.redirect("/holiday");
        })
    }
};

exports.delete = async (req, res) => {

    if (req.session.email) {
        const vAction = req.body.vAction;
        const iHolidayId = req.body.iHolidayId;
        try {
            if (iHolidayId) {
                //single record deleted
                if (vAction === "delete") {
                    await HolidayModel.deleteOne({
                        _id: iHolidayId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let holidayID = iHolidayId.split(',');
                    await HolidayModel.deleteMany({
                        _id: {
                            $in: holidayID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}