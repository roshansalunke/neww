const async = require("hbs/lib/async");
const InFieldFormModel = require("../model/InFieldFormModel");
const Paginator = require("../library/PaginatorLibrary");
const GeneralLibrary = require("../library/GeneralLibrary");
const CompanyDetailModel = require("../model/CompanyDetailModel");
const moment = require("moment");

exports.index = async (req, res) => {

    if (req.session.email) {

        var businessData = await InFieldFormModel.find({vCategory:"new_business"}).sort({
            _id: 'desc'
        });
        var salesData = await InFieldFormModel.find({vCategory:"sales_presentations"}).sort({
            _id: 'desc'
        });
        var handoutsData = await InFieldFormModel.find({vCategory:"handouts"}).sort({
            _id: 'desc'
        });
        is_layout = true;
        res.render("../view/infields_form/index", {
            is_layout: is_layout,
            businessData:businessData,
            salesData:salesData,
            handoutsData:handoutsData
        });
    } else {
        res.redirect("/");
    }

};

exports.newBusiness = async (req, res) => {

    if (req.session.email) {
        is_layout = true;
        res.render("../view/infields_form/newBusiness", {
            is_layout: is_layout,
        });
    } else {
        res.redirect("/");
    }

};

exports.business_action = async (req, res) => {

    if (req.session.email) {

        let structure_doc = null;
        let added_doc = null

        if (req.body.iFormId) {

            try {

                if (req.files != null) {

                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vStructureDoc,
                        path: 'in_field_form'
                    };
                    structure_doc = GeneralLibrary.localFilesUpload(CRITERIA);

                    let CRITERIA2 = {
                        files: req.files.vAddedDoc,
                        path: 'in_field_form'
                    };
                    added_doc = GeneralLibrary.localFilesUpload(CRITERIA2);
                    /**** Local END ****/
                }

                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vService": req.body.vService,
                            "vPrice": req.body.vPrice,
                            "vStructureDoc": structure_doc,
                            "vAddedDoc": added_doc,
                            "vVideoLink": req.body.vVideoLink,
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vService": req.body.vService,
                            "vPrice": req.body.vPrice,
                            "vVideoLink": req.body.vVideoLink,
                        }
                    }
                }

                InFieldFormModel.findOneAndUpdate({
                    _id: req.body.iFormId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/in-field-form");
                    } else {
                        console.log(error);
                    }
                });

            } catch (error) {
                console.log(error);
            }
        } else {

            if (req.files != null) {

                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vStructureDoc,
                    path: 'in_field_form'
                };
                structure_doc = GeneralLibrary.localFilesUpload(CRITERIA);

                let CRITERIA2 = {
                    files: req.files.vAddedDoc,
                    path: 'in_field_form'
                };
                added_doc = GeneralLibrary.localFilesUpload(CRITERIA2);
                /**** Local END ****/
            }

            var formData = new InFieldFormModel({
                vCategory: 'new_business',
                vService: req.body.vService,
                vPrice: req.body.vPrice,
                vStructureDoc: structure_doc,
                vAddedDoc: added_doc,
                vVideoLink: req.body.vVideoLink,
                vAddedDate: moment().format('MM/DD/YYYY')
            })

            try {
                formData.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {

                        res.redirect("/in-field-form");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
}

exports.ajax_business = async (req, res) => {

    var iFormId = req.body.iFormId;
    var vAction = req.body.vAction;
    var vPage = req.body.vPage;

    if (vAction === "delete" && iFormId != null) {
        await InFieldFormModel.deleteOne({
            _id: iFormId
        });
    }

    if (vAction === "multiple_delete" && iFormId != null) {
        let formID = iFormId.split(',');

        await InFieldFormModel.deleteMany({
            _id: {
                $in: formID
            }
        });
    }
    if (vAction === "search") {
        var SQL = {};
        SQL.vCategory = 'new_business';

        try {
            // Pagination
            var companyData = await CompanyDetailModel.findOne().exec();

            var dataCount = await InFieldFormModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = companyData.vItemPerPage;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;

            var businessData = await InFieldFormModel.find({vCategory:new_business}).skip(start).limit(limit).sort({
                _id: 'desc'
            });

            res.render("../view/infields_form/ajax_business", {
                layout: false,
                businessData: businessData,
                paginator: paginator,
                datalimit: limit,
                dataCount: dataCount
            });
        } catch (error) {
            res.render("../view/infields_form/ajax_business", {
                layout: false,
                businessData: error
            });
        }
    } else {
        try {

            var SQL = {};
            SQL.vCategory = 'new_business';
            // Pagination
            var companyData = await CompanyDetailModel.findOne().exec();
            var dataCount = await InFieldFormModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = companyData.vItemPerPage;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;

            var businessData = await InFieldFormModel.find(SQL).skip(start).limit(limit).sort({
                _id: 'desc'
            });
            // End

            res.render("../view/infields_form/ajax_business", {
                layout: false,
                businessData: businessData,
                paginator: paginator,
                datalimit: limit,
                dataCount: dataCount
            });
        } catch (error) {
            res.render("../view/infields_form/ajax_business", {
                layout: false,
                businessData: error
            });
        }
    }
}

exports.business_edit = async (req, res) => {
    if (req.session.email) {
        is_layout = true;

        const iFormId = req.params.iFormId;
        const businessData = await InFieldFormModel.findOne({ _id: iFormId });
        res.render("../view/infields_form/newBusiness", {
            is_layout: is_layout,
            businessData: businessData
        });
    } else {
        res.redirect("/");
    }
}

exports.sales_action = async (req, res) => {

    if (req.session.email) {

        let structure_doc = null;

        if (req.body.iFormId) {

            try {

                if (req.files != null) {

                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vStructureDoc,
                        path: 'in_field_form'
                    };
                    structure_doc = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }

                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vService": req.body.vService,
                            "vStructureDoc": structure_doc,
                            "vVideoLink": req.body.vVideoLink,
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vService": req.body.vService,
                            "vVideoLink": req.body.vVideoLink,
                        }
                    }
                }

                InFieldFormModel.findOneAndUpdate({
                    _id: req.body.iFormId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/in-field-form");
                    } else {
                        console.log(error);
                    }
                });

            } catch (error) {
                console.log(error);
            }
        } else {

            if (req.files != null) {

                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vStructureDoc,
                    path: 'in_field_form'
                };
                structure_doc = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }

            var formData = new InFieldFormModel({
                vCategory: 'sales_presentations',
                vService: req.body.vService,
                vStructureDoc: structure_doc,
                vVideoLink: req.body.vVideoLink,
                vAddedDate: moment().format('MM/DD/YYYY')
            })

            try {
                formData.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/in-field-form");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }

}

exports.ajax_sales = async (req, res) => {

    var iFormId = req.body.iFormId;
    var vAction = req.body.vAction;
    var vPage = req.body.vPage;

    if (vAction === "delete" && iFormId != null) {
        await InFieldFormModel.deleteOne({
            _id: iFormId
        });
    }

    if (vAction === "multiple_delete" && iFormId != null) {
        let formID = iFormId.split(',');

        await InFieldFormModel.deleteMany({
            _id: {
                $in: formID
            }
        });
    }
    if (vAction === "search") {
        var SQL = {};
        SQL.vCategory = 'sales_presentations';

        try {
            // Pagination
            var companyData = await CompanyDetailModel.findOne().exec();

            var dataCount = await InFieldFormModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = companyData.vItemPerPage;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;

            var salesData = await InFieldFormModel.find({vCategory:"sales_presentations"}).skip(start).limit(limit).sort({
                _id: 'desc'
            });

            res.render("../view/infields_form/ajax_sales", {
                layout: false,
                salesData: salesData,
                paginator: paginator,
                datalimit: limit,
                dataCount: dataCount
            });
        } catch (error) {
            res.render("../view/infields_form/ajax_sales", {
                layout: false,
                salesData: error
            });
        }
    } else {
        try {

            var SQL = {};
            SQL.vCategory = 'sales_presentations';

            // Pagination
            var companyData = await CompanyDetailModel.findOne().exec();
            var dataCount = await InFieldFormModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = companyData.vItemPerPage;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;

            var salesData = await InFieldFormModel.find(SQL).skip(start).limit(limit).sort({
                _id: 'desc'
            });
            // End

            res.render("../view/infields_form/ajax_sales", {
                layout: false,
                salesData: salesData,
                paginator: paginator,
                datalimit: limit,
                dataCount: dataCount
            });
        } catch (error) {
            res.render("../view/infields_form/ajax_sales", {
                layout: false,
                salesData: error
            });
        }
    }
}

exports.sales_edit = async (req, res) => {
    if (req.session.email) {
        is_layout = true;

        const iFormId = req.params.iFormId;
        const salesData = await InFieldFormModel.findOne({ _id: iFormId });
        res.render("../view/infields_form/salesPresentations", {
            is_layout: is_layout,
            salesData: salesData
        });
    } else {
        res.redirect("/");
    }
}

exports.salesPresentations = async (req, res) => {

    if (req.session.email) {
        is_layout = true;
        res.render("../view/infields_form/salesPresentations", {
            is_layout: is_layout,
        });
    } else {
        res.redirect("/");
    }

};

exports.handouts = async (req, res) => {

    if (req.session.email) {
        is_layout = true;
        res.render("../view/infields_form/handouts", {
            is_layout: is_layout,
        });
    } else {
        res.redirect("/");
    }
};

exports.handouts_action = async (req, res) => {

    if (req.session.email) {

        let structure_doc = null;

        if (req.body.iFormId) {

            try {
                if (req.files != null) {

                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vStructureDoc,
                        path: 'in_field_form'
                    };
                    structure_doc = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }

                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vService": req.body.vService,
                            "vStructureDoc": structure_doc,
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vService": req.body.vService,
                        }
                    }
                }

                InFieldFormModel.findOneAndUpdate({
                    _id: req.body.iFormId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/in-field-form");
                    } else {
                        console.log(error);
                    }
                });

            } catch (error) {
                console.log(error);
            }
        } else {
            if (req.files != null) {

                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vStructureDoc,
                    path: 'in_field_form'
                };
                structure_doc = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }

            var formData = new InFieldFormModel({
                vCategory: 'handouts',
                vService: req.body.vService,
                vStructureDoc: structure_doc,
                vAddedDate: moment().format('MM/DD/YYYY')
            })

            try {
                formData.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/in-field-form");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }

}

exports.ajax_handouts = async (req, res) => {

    var iFormId = req.body.iFormId;
    var vAction = req.body.vAction;
    var vPage = req.body.vPage;

    if (vAction === "delete" && iFormId != null) {
        await InFieldFormModel.deleteOne({
            _id: iFormId
        });
    }

    if (vAction === "multiple_delete" && iFormId != null) {
        let formID = iFormId.split(',');

        await InFieldFormModel.deleteMany({
            _id: {
                $in: formID
            }
        });
    }
    if (vAction === "search") {
        var SQL = {};
        SQL.vCategory = 'handouts';

        try {
            // Pagination
            var companyData = await CompanyDetailModel.findOne().exec();

            var dataCount = await InFieldFormModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = companyData.vItemPerPage;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;

            var handoutsData = await InFieldFormModel.find({vCategory:"handouts"}).skip(start).limit(limit).sort({
                _id: 'desc'
            });

            res.render("../view/infields_form/ajax_handouts", {
                layout: false,
                handoutsData: handoutsData,
                paginator: paginator,
                datalimit: limit,
                dataCount: dataCount
            });
        } catch (error) {
            res.render("../view/infields_form/ajax_handouts", {
                layout: false,
                handoutsData: error
            });
        }
    } else {
        try {

            var SQL = {};
            SQL.vCategory = 'handouts';

            // Pagination
            var companyData = await CompanyDetailModel.findOne().exec();
            var dataCount = await InFieldFormModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = companyData.vItemPerPage;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;

            var handoutsData = await InFieldFormModel.find(SQL).skip(start).limit(limit).sort({
                _id: 'desc'
            });
            // End

            res.render("../view/infields_form/ajax_handouts", {
                layout: false,
                handoutsData: handoutsData,
                paginator: paginator,
                datalimit: limit,
                dataCount: dataCount
            });
        } catch (error) {
            res.render("../view/infields_form/ajax_handouts", {
                layout: false,
                handoutsData: error
            });
        }
    }
}

exports.handouts_edit = async (req, res) => {
    if (req.session.email) {
        is_layout = true;

        const iFormId = req.params.iFormId;
        const handoutsData = await InFieldFormModel.findOne({ _id: iFormId });
        res.render("../view/infields_form/handouts", {
            is_layout: is_layout,
            handoutsData: handoutsData
        });
    } else {
        res.redirect("/");
    }
}

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iFormId = req.body.iFormId;

        try {
            if (iFormId) {
                //single record deleted
                if (vAction === "delete") {
                    await InFieldFormModel.deleteOne({
                        _id: iFormId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let fmID = iFormId.split(',');
                    await InFieldFormModel.deleteMany({
                        _id: {
                            $in: fmID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}