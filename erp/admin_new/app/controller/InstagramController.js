const async = require("hbs/lib/async");
const axios = require("axios");
const GeneralLibrary = require("../library/GeneralLibrary");
const request = require('request');
const {
    from
} = require("form-data");
const {
    ReturnDocument
} = require("mongodb");
const ChannelModel = require("../model/ChannelModel.js");



//insta Login Api
exports.instaLogin = async(req,res) => {
    if (req.session.email) {
        try{
            let token = req.body.token
            let pageIdUrl = `https://graph.facebook.com/v15.0/me/accounts?fields=id%2Caccess_token&access_token=${token}`
            let pageIdApi = await axios.get(pageIdUrl);
            pageIdApi = pageIdApi.data.data

            let instagramId = []
            for (let i = 0; i < pageIdApi.length; i++) {

                let instagramIdUrl = `https://graph.facebook.com/v15.0/${pageIdApi[i].id}?fields=instagram_business_account&access_token=${pageIdApi[i].access_token}`
                let instagramIdApi = await axios.get(instagramIdUrl);
                instagramIdApi = instagramIdApi.data.instagram_business_account?.id
                if (instagramIdApi != undefined) {
                    let instaDetailsUrl = `https://graph.facebook.com/v15.0/${instagramIdApi}?fields=profile_picture_url%2Cusername&access_token=${pageIdApi[i].access_token}`
                    let instagramDetailApi = await axios.get(instaDetailsUrl);
                    instagramDetailApi = instagramDetailApi.data
                    instagramDetailApi.access_token = token
                    instagramDetailApi.fbPageId = pageIdApi[i].id
                    instagramId.push(instagramDetailApi) 
                }
            }
            res.send(instagramId)

        }catch (error){
            console.log(error)
            res.redirect('/')
        }
    }
}

exports.instaPageDetails = async(req,res) => {
    if(req.session.email){
        try {
            let instaId = req.body.pageId
            let token = req.body.accessToken
            let fbpageid = req.body.fbpageid
            var dtAddedDate = new Date().toLocaleDateString();
            const userData = await axios.get(
                `https://graph.facebook.com/v15.0/me?fields=id%2Cname%2Cpicture.type(large)&access_token=${token}`
                );
                
                let urlLongTokenUser = `https://graph.facebook.com/v15.0/oauth/access_token?  
                                        grant_type=fb_exchange_token&          
                                        client_id=${process.env.APP_ID}&
                                        client_secret=${process.env.API_SECRET}&
                                        fb_exchange_token=${token}`
    
                const longLiveUserTokenApi = await axios.get(urlLongTokenUser);
                let longLiveUserToken = longLiveUserTokenApi.data.access_token;
    
                const urlLongTokenPage = `https://graph.facebook.com/v15.0/${fbpageid}?fields=access_token%2Cid%2Cname%2Cpicture%7Burl%7D&access_token=${longLiveUserToken}`
                let longLivePageTokenApi = await axios.get(urlLongTokenPage);
                let pageData = longLivePageTokenApi.data;


            let instaDetailsUrl = `https://graph.facebook.com/v15.0/${instaId}?fields=profile_picture_url%2Cusername&access_token=${pageData.access_token}`
            let instagramDetailApi = await axios.get(instaDetailsUrl);
                instagramDetailApi = instagramDetailApi.data    

            //profile upload aws
            let url;
            try {
                const response = await new Promise((resolve, reject) => {
                  request.get({
                    method: 'GET',
                    url: instagramDetailApi.profile_picture_url,
                    encoding: null, // If null, the body is returned as a Buffer.
                  }, (error, response, body) => {
                    if (error) {
                      reject(error);
                    } else {
                      resolve(response);
                    }
                  });
                });
            
                if (response.statusCode == 200) {
                  const data = {
                    name: response.request.uri.pathname.substring(1),
                    data: response.body,
                    mimetype: response.headers['content-type']
                  };
                  url = await GeneralLibrary.awsFileUpload(data, "socialPost/");
                } else {
                  throw new Error(`Request failed with status code ${response.statusCode}`);
                }
              } catch (error) {
                console.error(error);
              }
              instagramDetailApi.profile_picture_url = url
              console.log(instagramDetailApi.profile_picture_url);
                
                //use fbpageid in iuserid 
                let profileData = new ChannelModel({
                    vTitle: "Instagram",
                    vUserEmail: req.session.email,
                    vUserData: {
                        iUserId: fbpageid,
                        vUserProfile: userData.data.picture.data.url,
                        vUserName: userData.data.name,
                        vUserToken: longLiveUserToken,
                    },
                    vPageData: {
                        vPageId: instagramDetailApi.id,
                        vPageName: instagramDetailApi.username,
                        vPageToken: pageData.access_token,
                        vPageProfile: instagramDetailApi.profile_picture_url
                    },
                    dtAddedDate:dtAddedDate
                })
                profileData.save();
                
                res.send({status:true});

        } catch (error) {
            console.log(error);
            res.redirect('/')
        }
    }
}

exports.instagramImageVideo = async(reqData,pageData) => {
    try{
    let id = [];
    let schedule = false
    let message = reqData.message
    reqData.message = message.replace(/#/gi,"%23")

    if(reqData.imageCount == 1){
        let singlePhotoSinglePostParams
                if (schedule) {
                    singlePhotoSinglePostParams = ""
                } else {
                    singlePhotoSinglePostParams = `?image_url=${reqData.image[0]}&caption=${reqData.message}&access_token=${pageData.vPageToken}`
                }

                const singlePhotoSinglePost = await axios.post(
                    `https://graph.facebook.com/v15.0/${pageData.vPageId}/media${singlePhotoSinglePostParams}`
                  );
                let singlePhotoSinglePostId = singlePhotoSinglePost.data.id
                const singlePhotoSinglePostApi = await axios.post(
                    `https://graph.facebook.com/v15.0/${pageData.vPageId}/media_publish?creation_id=${singlePhotoSinglePostId}&access_token=${pageData.vPageToken}`
                );
                return singlePhotoSinglePostApi
    }else if (reqData.imageCount > 1) {
        //image upload code here 

            for (let i = 0; i < reqData.image.length; i++) {
                let multiPhotoSinglePostParams
                if (schedule) {
                    multiPhotoSinglePostParams = ""
                } else {
                    multiPhotoSinglePostParams = `?image_url=${reqData.image[i]}&is_carousel_item=true&access_token=${pageData.vPageToken}`
                }

                const multiPhotoSinglePost = await axios.post(
                    `https://graph.facebook.com/v15.0/${pageData.vPageId}/media${multiPhotoSinglePostParams}`,
                    );

                id.push(multiPhotoSinglePost.data.id)
            }
            id = id.toString()
            id = id.replace(/,/gi,"%2C")
            console.log("id",id);
            //combine post 
            let combineMultiPostParams = ''
            if (schedule) {
                combineMultiPostParams = ""
            } else {
                combineMultiPostParams = `?caption=${reqData.message}&media_type=CAROUSEL&children=${id}&access_token=${pageData.vPageToken}`
            }

            let combineMultiPost = await axios.post(
                `https://graph.facebook.com/${pageData.vPageId}/media${combineMultiPostParams}`,
            );

            combineMultiPost = combineMultiPost.data.id

            const publishPost = await axios.post(`https://graph.facebook.com/v15.0/${pageData.vPageId}/media_publish?creation_id=${combineMultiPost}&access_token=${pageData.vPageToken}`);

            return publishPost

    } else {
            let singleVideoPostParams
            if (schedule) {
                singleVideoPostParams = ""
            }else{
                singleVideoPostParams = `?media_type=VIDEO
                &video_url=${reqData.image[0]}&caption=${reqData.message}&access_token=${pageData.vPageToken}`
            }
            const videoUpload = await axios.post(
                `https://graph.facebook.com/v15.0/${pageData.vPageId}/media${singleVideoPostParams}`
            );
            let videoUploadId = videoUpload.data.id

            // Upload happens asynchronously in the backend,
            // so you need to check upload status before you Publish
            const checkStatusUri = `https://graph.facebook.com/v15.0/${videoUploadId}?fields=status_code&access_token=${pageData.vPageToken}`;
            const isUploaded = await isUploadSuccessful(0, checkStatusUri);

            if(isUploaded){
                const videoUploadApi = await axios.post(
                    `https://graph.facebook.com/v15.0/${pageData.vPageId}/media_publish?creation_id=${videoUploadId}&access_token=${pageData.vPageToken}`
                )
                // .then((result) =>{console.log("video upload succesfull"); return result}).catch((err)=> {console.log("instagram catch"); throw err.response?.data})
    
                // console.log("video upload succesfull");
                return videoUploadApi
            }
        }
    }catch(error){
        throw error = error.response?.data || error
    }
}

exports.instaRefreshPage = async(reqData)=>{
    try {

        //you have to get page id and it's token to get insta details
        let bussinessFbPageId = await ChannelModel.findOne({'vTitle': "Instagram" , "vPageData.vPageId": reqData.pageId}).select('vUserData.iUserId').exec()
        const userData = await axios.get(
            `https://graph.facebook.com/v15.0/me?fields=id%2Cname%2Cpicture.type(large)&access_token=${reqData.token}`
          );
    
          let urlLongTokenUser = `https://graph.facebook.com/v15.0/oauth/access_token?  
                                        grant_type=fb_exchange_token&          
                                        client_id=${process.env.APP_ID}&
                                        client_secret=${process.env.API_SECRET}&
                                        fb_exchange_token=${reqData.token}`;
    
          const longLiveUserTokenApi = await axios.get(urlLongTokenUser);
          let longLiveUserToken = longLiveUserTokenApi.data.access_token;
    
          const urlLongTokenPage = `https://graph.facebook.com/v15.0/${bussinessFbPageId.vUserData.iUserId}?fields=access_token%2Cid%2Cname%2Cpicture%7Burl%7D&access_token=${longLiveUserToken}`
          let longLivePageTokenApi = await axios.get(urlLongTokenPage);
          let pageData = longLivePageTokenApi.data;


      let instaDetailsUrl = `https://graph.facebook.com/v15.0/${reqData.pageId}?fields=profile_picture_url%2Cusername&access_token=${pageData.access_token}`
      let instagramDetailApi = await axios.get(instaDetailsUrl);
          instagramDetailApi = instagramDetailApi.data

      //profile upload aws
      let url;
      try {
          const response = await new Promise((resolve, reject) => {
            request.get({
              method: 'GET',
              url: instagramDetailApi.profile_picture_url,
              encoding: null, // If null, the body is returned as a Buffer.
            }, (error, response, body) => {
              if (error) {
                reject(error);
              } else {
                resolve(response);
              }
            });
          });
      
          if (response.statusCode == 200) {
            const data = {
              name: response.request.uri.pathname.substring(1),
              data: response.body,
              mimetype: response.headers['content-type']
            };
            url = await GeneralLibrary.awsFileUpload(data, "socialPost/");
          } else {
            throw new Error(`Request failed with status code ${response.statusCode}`);
          }
        } catch (error) {
          console.error(error);
        }
        instagramDetailApi.profile_picture_url = url
    
          ChannelModel.findOneAndUpdate(
            { "vPageData.vPageId": reqData.pageId, vUserEmail: reqData.email ,vTitle : "Instagram"},
            {
              $set: {
                vTitle: "Instagram",
                vUserEmail: reqData.email,
                vUserData: {
                    iUserId: bussinessFbPageId.vUserData.iUserId,
                    vUserProfile: userData.data.picture.data.url,
                    vUserName: userData.data.name,
                    vUserToken: longLiveUserToken,
                },
                vPageData: {
                    vPageId: instagramDetailApi.id,
                    vPageName: instagramDetailApi.username,
                    vPageToken: pageData.access_token,
                    vPageProfile: instagramDetailApi.profile_picture_url
                },
                dtAddedDate: reqData.dtAddedDate,
              },
            }
          ).exec(function (error, result) {
            if (!error) {
            //   res.redirect("/social-accounts/channels");
            } else {
              console.log(error);
            }
          });
    } catch (error) {
        throw error
    }
}

exports.instaCalendarData = async(vUserEmail)=>{
  try {
    let pageData = await ChannelModel.find({ vTitle: 'Instagram', vUserEmail: vUserEmail }).select('vPageData vTitle').exec();
    // console.log(pageData);
    let instaSchedulePostDataArray = []
    let instaPostDataArray = []

    for (let i = 0; i < pageData.length; i++) {
      let pageAccessToken = pageData[i].vPageData.vPageToken
      let pageName = pageData[i].vPageData.vPageName
      let profileUrl = pageData[i].vPageData.vPageProfile
      let vTitle = pageData[i].vTitle
      // #E1306C

      let uri = `https://graph.facebook.com/v15.0/${pageData[i].vPageData.vPageId}/media?fields=media_url%2Ccaption%2Cpermalink%2Ctimestamp%2Clike_count%2Ccomments_count%2Cinsights.metric(shares%2Creach)%7Bvalues%2Cname%7D&limit(100)&access_token=${pageAccessToken}`
      const uriResponse = await axios.get(uri);
      let postData = uriResponse.data.data
      for (let j = 0; j < postData.length; j++) {
          postData[j].pageName = pageName
          postData[j].profileUrl = profileUrl
          postData[j].vTitle = vTitle
          postData[j].reach = postData[j].insights.data[0].name = "reach"  ? postData[j].insights.data[0].values[0].value : 0
          postData[j].shares = postData[j].insights.data[0].name = "shares"  ? postData[j].insights.data[1]?.values[0].value : 0
          if(postData[j].shares == undefined ) postData[j].shares = 0 
          delete postData[j].insights
      }
      // console.log(postData);
      instaPostDataArray.push(postData)
    }
    instaPostDataArray = instaPostDataArray[0]
    return instaPostDataArray
  } catch (error) {
      throw error
  }
}

/**
 * Setting retries with 3 seconds delay, as async video upload may take a while in the backed to return success
 * @param {*} n
 * @returns
 */
 function _wait(n) { return new Promise(resolve => setTimeout(resolve, n)); }

 /**
  * Retrieves container status for the uploaded video, while its uploading in the backend asynchronously
  * and checks if the upload is complete.
  * @param {*} retryCount
  * @param {*} checkStatusUri
  * @returns Promise<boolean>
  */

async function isUploadSuccessful(retryCount, checkStatusUri) {
    try {
        if (retryCount > 30) return false;
        const response = await axios.get(checkStatusUri);
        if(response.data.status_code != "FINISHED") {
            await _wait(3000);
            await isUploadSuccessful(retryCount+1, checkStatusUri);
        }
        return true;
    } catch(e) {
        throw e;
    }
}