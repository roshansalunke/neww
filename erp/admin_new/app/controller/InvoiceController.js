const async = require("hbs/lib/async");
const ProjectPaymentModel = require("../model/ProjectPaymentModel");
const Paginator = require("../library/PaginatorLibrary");
const ClientModel = require("../model/ClientModel");
const ProjectModel = require("../model/ProjectModel");
const fs = require('fs');
const puppeteer = require('puppeteer');

exports.index = async (req, res) => {

    if (req.session.email) {

        var clientdata = await ClientModel.findOne({
            _id: req.session.userid
        }).exec();

        var paymentReceipt = await ProjectPaymentModel.find({
            ePaymentStatus: 'succeeded'
        }).populate({
            path: 'iProjectId',
            select: 'vProject'
        }).exec();

        var paymentInvoice = await ProjectPaymentModel.find().populate({
            path: 'iProjectId',
            select: 'vProject'
        }).populate({
            path: 'iAgentId',
            select: ['vFirstName', 'vLastName']
        }).exec();

        is_layout = true;
        res.render("../view/invoice/index", {
            is_layout: is_layout,
            paymentReceipt: paymentReceipt,
            paymentInvoice: paymentInvoice
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/invoice/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.receipt_download = async (req, res) => {

    var iReceiptId = req.params.iReceiptId;

    if (req.session.email) {
        try {

            var paymentReceipt = await ProjectPaymentModel.findOne({
                _id: iReceiptId
            }).exec();

            const html = fs.readFileSync('./app/view/invoice/receipt.html', 'utf8');

            var data = html.replace("#id#", paymentReceipt._id)
                .replace("#stripe_trans_id#", paymentReceipt.charge_id)
                .replace(/#amount#/g, paymentReceipt.iSalesPrice)
                .replace(/#created_at#/g, paymentReceipt.dtPaymentDate)
                .replace(/#type#/g, paymentReceipt.payment_type);

            var milis = new Date();
            milis = milis.getTime();

            const browser = await puppeteer.launch({
                headless: true,
                args: ['--no-sandbox'],
            });

            const page = await browser.newPage();

            await page.setContent(data, {
                waitUntil: 'domcontentloaded'
            })

            // create a pdf buffer
            const pdfBuffer = await page.pdf({
                format: 'A4',
                displayHeaderFooter: false,
                margin: {
                    top: "50px",
                    bottom: "50px",
                    left: "20px",
                    right: "20px"
                },
            })

            res.setHeader('Content-Type', 'application/pdf');
            res.setHeader('Content-Disposition', 'attachment; filename=' + milis + 'receipt' + '.pdf');
            res.setHeader('Content-Length', pdfBuffer.length);
            return res.end(pdfBuffer);

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.invoice_download = async (req, res) => {

    var iInvoiceId = req.params.iInvoiceId;

    if (req.session.email) {
        try {

            var paymentInvoice = await ProjectPaymentModel.findOne({
                _id: iInvoiceId
            }).populate({
                path: 'iProjectId'
            }).populate('vClientName','vFirstName vLastName').exec();

            var paymentData = await ProjectPaymentModel.find({iProjectId:paymentInvoice.iProjectId._id});

            const totalRemainingAmount = paymentData.reduce((total, item) => {
                if(item.ePaymentStatus !== 'succeeded'){
                    return total + parseFloat(item.iSalesPrice);
                }
                return total;
            } ,0);

            const html = fs.readFileSync('./app/view/invoice/invoice.html', 'utf8');

            if (paymentInvoice.ePaymentPhases == "FullPayment") {
                if (paymentInvoice.ePaymentPhases == "FullPayment") {
                    var iSalesPrice = Number(paymentInvoice.iSalesPrice);
                    var discount = Number(7) / 100;
                    var totalPrice = iSalesPrice - (iSalesPrice * discount);
                    var discountAmount = iSalesPrice - totalPrice;
                }

                var TotalPrice = Number(totalPrice).toFixed(2);
                var TotalDiscount = Number(discountAmount).toFixed(2)
                var percentageDiscount = "(7%)";
            } else {
                var TotalPrice = paymentInvoice.iSalesPrice;
                var TotalDiscount = "-";
                var percentageDiscount = "-";
            }

            var data = html.replace("#INVOICE_NAME#", paymentInvoice.ePaymentPhases)
                .replace("#INVOICE_DATE#", paymentInvoice.dtPaymentDate)
                .replace("#STEP_PAY#", paymentInvoice.ePaymentStep)
                .replace("#CLIENT_ID#", paymentInvoice.iClientUserId)
                .replace("#CLIENT_NAME#",paymentInvoice.iClientUserId+ " - " + paymentInvoice.vClientName.vFirstName + " " + paymentInvoice.vClientName.vFirstName )
                .replace("#INVOICE_NO#", paymentInvoice._id)
                .replace("#SUB_TOTAL#", paymentInvoice.iSalesPrice)
                .replace("#vProject#", paymentInvoice.iProjectId.vProject)
                .replace("#iSalesPrice#", paymentInvoice.iSalesPrice)
                .replace("#SUBTOTAL#", paymentInvoice.iSalesPrice)
                .replace("#TOTAL#", TotalPrice)
                .replace("#REMAINING_PRICE#",totalRemainingAmount)
                .replace("#DISCOUNT#", TotalDiscount)
                .replace("#TOTAL_DISCOUNT#", percentageDiscount);

            var milis = new Date();
            milis = milis.getTime();

            const browser = await puppeteer.launch({
                headless: true,
                args: ["--no-sandbox"]
            });

            const page = await browser.newPage();

            await page.setContent(data, {
                waitUntil: 'domcontentloaded'
            })

            // create a pdf buffer
            const pdfBuffer = await page.pdf({
                format: 'A4',
                displayHeaderFooter: false,
                margin: {
                    top: "50px",
                    bottom: "50px",
                    left: "20px",
                    right: "20px"
                },
            })

            res.setHeader('Content-Type', 'application/pdf');
            res.setHeader('Content-Disposition', 'attachment; filename=' + milis + 'invoice' + '.pdf');
            res.setHeader('Content-Length', pdfBuffer.length);
            return res.end(pdfBuffer);

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};