const async = require("hbs/lib/async");
const LeadCenterModel = require("../model/LeadCenterModel");
const GeneralLibrary = require("../library/GeneralLibrary");

exports.index = async (req, res) => {

    if (req.session.email) {

        var leadCenterData = await LeadCenterModel.find();
        is_layout = true;
        res.render("../view/lead_center/index", {
            is_layout: is_layout,
            leadCenterData:leadCenterData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/lead_center/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iLeadCenterId) {
            try {
                let image_name = null;
                if (req.files != null) {
                   
                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vLogo,
                        path: 'lead-center'
                    };
                    image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }
                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vTitle": req.body.vTitle,
                            "vLoginLink": req.body.vLoginLink,
                            "vRegistrationLink": req.body.vRegistrationLink,
                            "vHomePageLink": req.body.vHomePageLink,
                            "vLogo": image_name,
                            "vLogoSVG":req.body.vLogoSVG
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vTitle": req.body.vTitle,
                            "vLoginLink": req.body.vLoginLink,
                            "vRegistrationLink": req.body.vRegistrationLink,
                            "vHomePageLink": req.body.vHomePageLink,
                            "vLogoSVG":req.body.vLogoSVG
                        }
                    }
                }

                LeadCenterModel.findOneAndUpdate({
                    _id: req.body.iLeadCenterId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/lead-center");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {

            let image_name = null;
            if (req.files != null) {
             
                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vLogo,
                    path: 'lead-center'
                };
                image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }

            const LeadCenter = new LeadCenterModel({
                vTitle: req.body.vTitle,
                vLoginLink: req.body.vLoginLink,
                vRegistrationLink: req.body.vRegistrationLink,
                vHomePageLink: req.body.vHomePageLink,
                vLogo: image_name,
                vLogoSVG:req.body.vLogoSVG,
                dtAddedDate:new Date().toLocaleDateString()
            });
            try {
                LeadCenter.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/lead-center");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iLeadCenterId = req.params.iLeadCenterId;

    if (req.session.email) {
        try {
            var leadCenterData = await LeadCenterModel.findOne({
                _id: iLeadCenterId
            });
            is_layout = true;
            res.render("../view/lead_center/add", {
                is_layout: is_layout,
                leadCenterData: leadCenterData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iLeadCenterId = req.body.iLeadCenterId;

        try {
            if (iLeadCenterId) {
                //single record deleted
                if (vAction === "delete") {
                    await LeadCenterModel.deleteOne({
                        _id: iLeadCenterId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let leadCenterID = iLeadCenterId.split(',');
                    await LeadCenterModel.deleteMany({
                        _id: {
                            $in: leadCenterID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}