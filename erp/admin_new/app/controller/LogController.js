const LogModel = require("../model/LogModel");

exports.index = async (req, res) => {

    if (req.session.email) {
        var logData = await LogModel.find().sort({
            _id: 'desc'
        });
        is_layout = true;
        res.render("../view/log/index", {
            is_layout: is_layout,
            logData: logData
        });
    } else {
        res.redirect("/");
    }
};

exports.clear_logs = async (req, res) => {
    if (req.session.email) {
        try {
            await LogModel.deleteMany({}).then(function (result) {
                req.flash("success", "Successfully deleted");
                res.send({ status: 200 })
            });;
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    }
};