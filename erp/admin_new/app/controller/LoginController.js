const bcrypt = require("bcrypt");
const UserModel = require("../model/UserModel");
const ClientModel = require("../model/ClientModel");
const SecurityQuestionModel = require("../model/SecurityQuestionModel");
const SecurityAnswerModel = require("../model/SecurityAnswerModel");
const ClientResetPwModel = require("../model/ClientResetPwModel");
const SystemEmailModel = require("../model/SystemEmailModel");
const EmailLibrary = require('../library/EmailLibrary');
const crypto = require("crypto");
const GeneralLibrary = require("../library/GeneralLibrary");
const BannerModel = require("../model/BannerModel");
const CompanyDetailModel = require("../model/CompanyDetailModel");

exports.index = async (req, res) => {
    if (req.session.email) {
        res.redirect("/dashboard");
    } else {
        var banner = await BannerModel.findOne({
            vPanelScreen: "Login",
            vPanel: "Admin"
        }).exec();

        var companyData = await CompanyDetailModel.findOne().exec();

        is_layout = false;
        res.render("../view/login/index", {
            is_layout: is_layout,
            banner: banner,
            companyData: companyData
        });
    }
};

exports.login_action = async (req, res) => {
    const vEmail = req.body.vEmail;
    const vPassword = req.body.vPassword;
    var vError = false;

    if (vEmail.lenght === 0) {
        vError = true;
    }
    if (vPassword.lenght === 0) {
        vError = true;
    }
    if (vError === false) {

        try {
            const userData = await UserModel.findOne({
                vEmail: vEmail,
                //eType: "Admin"
            });

            if (userData == null) {
                req.session.message = '';
                req.flash('error', 'Email Account Not Found');
                res.redirect("/");
            } else {
                if (userData.eVerify == 'Yes') {
                    if (await bcrypt.compare(vPassword, userData.vPassword)) {
                        req.session.firstname = userData.vFirstName;
                        req.session.lastname = userData.vLastName;
                        req.session.email = userData.vEmail;
                        req.session.userid = userData._id;
                        req.session.usertype = userData.eType;
                        req.session.department = userData.vDepartment;
                        req.session.role = userData.vRole;

                        req.flash('success', 'Login Successfully');
                        res.redirect("/dashboard");
                    } else {
                        req.flash('error', 'Invalid Password');
                        res.redirect("/");
                    }
                } else {
                    req.flash('error', 'Please check your email and verify code.');
                    res.redirect("/");
                }
            }
        } catch (error) {

            req.session.message = error;
            res.redirect("/");

        }

    } else {
        req.flash('error', 'Email or Password Empty');
        res.redirect("/");
    }

};

exports.logout = async (req, res) => {
    req.session.destroy();
    res.redirect("/dashboard");
};

exports.forget_password = (req, res) => {
    is_layout = false;
    res.render("../view/login/forget_password", {
        is_layout: is_layout
    });
};

exports.verifytoken = async (req, res) => {

    var iVerifyToken = req.params.iVerifyToken;
    // var eType = iVerifyToken.split("-")[0];

    try {

        var userData = await UserModel.findOne({
            vVerifyToken: iVerifyToken
        })

        if (userData != null) {
            is_layout = false;
            res.render("../view/login/verifytoken", {
                is_layout: is_layout,
                iVerifyToken: iVerifyToken
            });
        } else {
            req.flash('error', 'something to wrong');
            res.redirect('/verify/' + vToken);
        }
    } catch (error) {
        console.log(error);
    }

};

exports.verify_action = async (req, res) => {
    const vCode = req.body.vCode;
    const vToken = req.body.token;

    try {

        const adminData = await UserModel.findOne({
            vVerifyCode: vCode
        });

        if (adminData) {
            if (adminData.eVerify != 'Yes') {

                UserModel.findOneAndUpdate({
                    vVerifyCode: vCode
                }, {
                    "$set": {
                        "eVerify": "Yes",
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        req.flash('success', 'Verify successfully');
                        res.redirect('/security-question/' + vToken);
                    }
                });
            } else {
                req.flash('error', 'Already verify this code.');
                res.redirect('/verify/' + vToken);
            }

        } else {
            req.flash('error', 'Verify code invalid');
            res.redirect('/verify/' + vToken);
        }

    } catch (error) {
        console.log(error);
    }
};

exports.security_question = async (req, res) => {
    try {
        var iVerifyToken = req.params.iVerifyToken;

        let questionData = await SecurityQuestionModel.find().limit(6);

        is_layout = false;
        res.render("../view/login/security_question", {
            is_layout: is_layout,
            questionData: questionData,
            iVerifyToken: iVerifyToken
        });
    } catch (error) {
        console.log(error);
    }
};

exports.security_action = async (req, res) => {
    const vToken = req.body.token;

    try {
        const adminData = await UserModel.findOne({
            vVerifyToken: vToken
        });

        if (adminData == null) {
            req.flash('error', 'Admin not found.');
            res.redirect('/security-question/' + vToken);
        } else {
            if (adminData.eVerify == 'Yes') {
                var QA = req.body.vAnswer;
                var userID = adminData._id.toString();
                var allData = [];

                QA.forEach(element => {
                    allData.push({
                        vAnswer: Object.values(element).toString(),
                        iQuestionId: Object.keys(element).toString(),
                        iUserId: userID,
                        eType: 'admin'
                    });
                });

                try {
                    SecurityAnswerModel.insertMany(allData, function (error, result) {
                        if (error) {
                            console.log(error);
                        } else {
                            res.redirect('/reset/' + vToken);
                        }
                    })
                } catch (error) {
                    console.log(error);
                }
            } else {
                req.flash('error', 'Check your email and verify code.');
                res.redirect('/verify/' + vToken);
            }
        }

    } catch (error) {
        console.log(error);
    }
};

exports.reset = async (req, res) => {
    var iVerifyToken = req.params.iVerifyToken;
    try {
        is_layout = false;
        res.render("../view/login/reset_password", {
            is_layout: is_layout,
            iVerifyToken: iVerifyToken
        });
    } catch (error) {
        console.log(error);
    }
};

exports.reset_action = async (req, res) => {
    const vToken = req.body.token;

    try {
        const adminData = await UserModel.findOne({
            vVerifyToken: vToken
        });

        if (adminData == null) {
            req.flash('error', 'Admin not found');
            res.redirect('/reset/' + vToken);
        } else {
            if (adminData.eVerify == 'Yes') {
                const hash = await bcrypt.hash(req.body.vPassword, 10);
                UserModel.findOneAndUpdate({
                    vVerifyToken: vToken
                }, {
                    "$set": {
                        "vPassword": hash,
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        ClientResetPwModel.deleteOne({
                            iUserId: result._id
                        }).exec();
                        req.flash('success', 'New password set successfully.');
                        res.redirect('/login');
                    }
                });
            } else {
                req.flash('error', 'Check your email and verify code.');
                res.redirect('/reset/' + vToken);
            }
        }

    } catch (error) {
        console.log(error);
    }
};

//forget-password
exports.email_verification = (req, res) => {
    is_layout = false;
    res.render("../view/login/email_verification", {
        is_layout: is_layout
    });
};

exports.verifyEmail_action = async (req, res) => {
    const vEmail = req.body.vEmail;
    var vError = false;

    if (vEmail.lenght === 0) {
        vError = true;
    }

    if (vError === false) {

        try {
            const userData = await UserModel.findOne({
                vEmail: vEmail
            });

            if (userData == null) {
                req.flash('error', 'Email Account Not Found');
                res.redirect("/");
            } else {
                if (userData.eVerify == 'Yes') {
                    req.session.userid = userData._id;
                    res.redirect("/admin-security-question");
                } else {
                    req.flash('error', 'Your Email is not verified');
                    res.redirect("/");
                }
            }
        } catch (error) {
            req.flash('error', error);
            res.redirect("/");
        }

    } else {
        req.flash('error', 'Email is Empty');
        res.redirect("/");
    }

};

exports.admin_security_question = async (req, res) => {

    if (req.session.email) {
        res.redirect("/dashboard");
    } else {
        is_layout = false;

        var iUserId = req.session.userid;
        if (iUserId) {
            const userData = await UserModel.findOne({
                _id: iUserId
            });

            var securityData = await SecurityQuestionModel.aggregate([{
                $sample: {
                    size: 2
                }
            }]);

            res.render("../view/login/admin_security_question", {
                is_layout: is_layout,
                securityData: securityData,
                userData: userData
            });

        } else {
            res.redirect("/");

        }

    }
};

exports.admin_securityQuestion_action = async (req, res) => {


    var iUserId = req.session.userid;
    if (iUserId) {

        try {
            var iQuestionId = req.body.iQuestionId0;
            var vAnswer = req.body.vAnswer0;
            var iQuestionId1 = req.body.iQuestionId1;
            var vAnswer1 = req.body.vAnswer1;

            //3 attempt
            const answerData = await SecurityAnswerModel.findOne({
                iUserId: iUserId,
                iQuestionId: iQuestionId,
                vAnswer: vAnswer
            });

            //5 attempt
            const answerData2 = await SecurityAnswerModel.findOne({
                iUserId: iUserId,
                iQuestionId: iQuestionId1,
                vAnswer: vAnswer1
            });

            const userData = await UserModel.findOne({
                _id: iUserId
            });

            var passwordAttempt = 8;

            if (userData.iPasswordAttempt == undefined) {
                passwordAttempt = 8;
            } else {
                passwordAttempt = userData.iPasswordAttempt;
            }

            //attempt wrong
            if (answerData == null && answerData2 == null) {

                UserModel.findOneAndUpdate({
                    _id: iUserId
                }, {
                    "$set": {
                        "iPasswordAttempt": parseInt(passwordAttempt) - 1
                    }
                }).exec(function (error, result) {
                    if (result.iPasswordAttempt <= 1) {
                        res.redirect("/identity-verification");
                    } else {
                        req.flash('error', 'Attempts exceed');
                        res.redirect("/admin-security-question");
                    }
                });
            } else {

                //attempt right
                var verifyCode = Math.floor(1000000 + Math.random() * 9000000);
                var verifyToken = ('admin' + '-' + crypto.randomBytes(20).toString("hex"));

                let email_template = await SystemEmailModel.find({
                    'vEmailCode': 'USER_REGISTER'
                });

                try {

                    UserModel.findOneAndUpdate({
                        _id: iUserId
                    }, {
                        "$set": {
                            "vVerifyToken": verifyToken,
                            "vVerifyCode": verifyCode,
                            eVerify: '',
                            vResetPasswordBy: 'admin'
                        }
                    }).exec(function (error, result) {
                        if (error) {
                            console.log(error);
                        } else {

                            const vVerifyURL = process.env.APP_URL + "verify/" + verifyToken;

                            let criteria = {
                                user_name: result.vFirstName + ' ' + result.vLastName,
                                email_to: result.vEmail,
                                email_subject: "ERP",
                                email_template: email_template,
                                verifycode: verifyCode,
                                verifyurl: vVerifyURL
                            }

                            EmailLibrary.send_email(criteria);

                            req.flash('success', 'Please check your email and verify code.');
                            // res.redirect("/security-question");
                            res.redirect(vVerifyURL);
                        }
                    });

                } catch (error) {
                    console.log(error);
                }
            }
        } catch (error) {
            req.flash('error', "something to Wrong");
            res.redirect("/");
        }

    } else {
        req.flash('error', "something to Wrong");
        res.redirect("/");
    }

};

exports.identity_verification = async (req, res) => {
    is_layout = false;
    var iUserId = req.session.userid;
    if (iUserId) {
        const userData = await UserModel.findOne({
            _id: iUserId
        });
        res.render("../view/login/identity_verification", {
            is_layout: is_layout,
            userData: userData
        });

    } else {
        req.flash('error', "something to Wrong");
        res.redirect("/");
    }
};

exports.identity_verification_action = async (req, res) => {

    let file_name = null;

    if (req.files != null) {
        // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vFile, 'admin_reset_pw/');
        // file_name = awsURL;

        /**** Local Files Upload ****/
        let CRITERIA = {
            files: req.files.vFile,
            path: 'admin_reset_pw'
        };
        file_name = GeneralLibrary.localFilesUpload(CRITERIA);
        /**** Local END ****/
    }

    const resetPassword = new ClientResetPwModel({
        iUserId: req.body.iUserId,
        vFile: file_name,
        dtAddedDate: new Date().toLocaleDateString()
    });
    try {
        resetPassword.save(function (error, result) {
            if (error) {
                console.log(error);
            } else {
                UserModel.findOneAndUpdate({
                    _id: result.iUserId
                }, {
                    "$set": {
                        "iPasswordAttempt": 8
                    }
                }).exec();
                res.redirect("/thank-you");
            }
        })
    } catch (error) {
        console.log(error);
    }
}

exports.thank_you = (req, res) => {
    is_layout = false;
    res.render("../view/login/thank_you", {
        is_layout: is_layout
    });
};