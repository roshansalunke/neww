const async = require("hbs/lib/async");
const NotificationModel = require("../model/NotificationModel");

exports.view = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            const notificationData = await NotificationModel.find({
                eUserSend: {
                    $ne: 'admin'
                }
            }).sort({
                _id: 'desc'
            });
            res.render("../view/notification/view", {
                is_layout: is_layout,
                notificationData: notificationData
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.changeSeenStatus = async (req, res) => {
    if (req.session.email) {
        try {
            const iNotificationId = req.body.iNotificationId;
            NotificationModel.findOneAndUpdate({
                _id: iNotificationId
            }, {
                "$set": {
                    "eAdminSeen": 'Yes'
                }
            }).exec(function (error, result) {
                if (!error) {
                    res.redirect("/dashboard");
                } else {
                    res.send(result);
                }
            });
        } catch (error) {
            console.log(error);
        }
    }
};

exports.read_notification = async (req, res) => {

    if (req.session.email) {
        try {
            var iNotificationId = req.body.iNotificationId;

            var id = iNotificationId.split(',');

            if (iNotificationId) {
                NotificationModel.updateMany({
                    _id: {
                        $in: id
                    }
                }, {
                    $set: {
                        'eAdminSeen': 'Yes'
                    }
                }).exec(function (err, result) {
                    if (err) {
                        console.log(err);
                    } else {
                        res.send('')
                    }
                });
            }
        } catch (error) {
            console.log(error);
        }
    }
};