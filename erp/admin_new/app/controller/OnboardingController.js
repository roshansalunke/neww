const async = require("hbs/lib/async");
const AgentModel = require("../model/AgentModel");
const AgentTypeModel = require("../model/AgentTypeModel");
const Paginator = require("../library/PaginatorLibrary");
const ContractHistoryModel = require("../model/ContractHistoryModel");
const NotificationModel = require("../model/NotificationModel");
const crypto = require("crypto");
const pdf = require('html-pdf');
const fs = require("fs")
const puppeteer = require('puppeteer');
const ip = require('ip');
const CompanyDetailModel = require("../model/CompanyDetailModel");
const GeneralLibrary = require("../library/GeneralLibrary");

exports.index = async (req, res) => {

    if (req.session.email) {

        var agentData = await AgentModel.find().populate({
            path: 'iAgentType',
            select: 'vTitle'
        }).populate({
            path: "vReferrer",
            select: "vFirstName vLastName",
        }).sort({
            _id: 'desc'
        });

        var existContract = [];
        for (let index = 0; index < agentData.length; index++) {
            const element = agentData[index];
            if (element.vOnboardingContract ||
                element.vOnboardingApplication ||
                element.vTaxPayerContract ||
                element.vAuthorizationContract ||
                element.vDrivingLicenseContract !== undefined) {
                existContract.push(element._id)
            }
        }

        is_layout = true;
        res.render("../view/onboarding/index", {
            is_layout: is_layout,
            agentData: agentData,
            existContract: existContract,
        });
    } else {
        res.redirect("/");
    }

};
exports.contract = async (req, res) => {

    if (req.session.email) {
        iAgentId = req.body.iAgentId;
        // var IP_Address = req.socket.remoteAddress;
        var IP_Address = ip.address();
        var agentData = await AgentModel.findOne({
            _id: iAgentId
        });
        var contractData = agentData.vOnboardingContract;
        is_layout = false;
        res.render("../view/onboarding/onboarding_contract", {
            is_layout: is_layout,
            contractData: contractData,
            IP_Address: IP_Address
        });
    } else {
        res.redirect("/");
    }
};

exports.applicationContract = async (req, res) => {

    if (req.session.email) {
        iAgentId = req.body.iAgentId;
        var agentData = await AgentModel.findOne({
            _id: iAgentId
        });
        var applicationData = agentData.vOnboardingApplication;
        // var IP_Address = req.socket.remoteAddress;
        var IP_Address = ip.address();
        is_layout = false;
        res.render("../view/onboarding/application_contract", {
            is_layout: is_layout,
            applicationData: applicationData,
            IP_Address: IP_Address
        });
    } else {
        res.redirect("/");
    }

}

exports.depositContract = async (req, res) => {

    if (req.session.email) {
        iAgentId = req.body.iAgentId;
        var agentData = await AgentModel.findOne({
            _id: iAgentId
        });
        var authorizationData = agentData.vAuthorizationContract;
        // var IP_Address = req.socket.remoteAddress;
        var IP_Address = ip.address();
        is_layout = false;
        res.render("../view/onboarding/deposit_contract", {
            is_layout: is_layout,
            authorizationData: authorizationData,
            IP_Address: IP_Address
        });
    } else {
        res.redirect("/");
    }

}

exports.edit_contract_status = async (req, res) => {

    const iAgentId = req.body.iAgentId;

    if (iAgentId) {
        try {
            AgentModel.findOneAndUpdate({
                _id: iAgentId
            }, {
                "$set": {
                    "eContractStatus": req.body.eContractStatus,
                    "eClearmarket": req.body.eClearmarket,
                }
            }, {
                returnDocument: 'after'
            }).exec(function (error, result) {
                if (!error) {

                    //send Notification
                    if (result.eClearmarket == 'Yes') {
                        const CRITERIA = {
                            vNotification: `Admin ${result.eContractStatus} your account and you approved as clear to market`,
                            iUserId: iAgentId,
                            vLink: `/profile/edit/${iAgentId}`,
                            vTimezone: req.session.vTimezone,
                        }
                        GeneralLibrary.sendNotification(CRITERIA);
                    }
                    res.redirect("/onboarding");
                } else {
                    console.log(error);
                }
            });

        } catch (error) {
            console.log(error);
        }

    } else {
        console.log("error")
    }
}

exports.reset_contract = async (req, res) => {

    if (req.session.email) {

        var iAgentId = req.body.iAgentId;
        var contract = req.body.contract;
        var agentData = await AgentModel.findOne({
            _id: iAgentId
        });

        var insertData = {};
        var unsetData = {};
        if (contract == "allContract") {
            if (agentData.vOnboardingContract ||
                agentData.vOnboardingApplication ||
                agentData.vTaxPayerContract ||
                agentData.vAuthorizationContract ||
                agentData.vDrivingLicenseContract !== undefined) {

                insertData.iAgentId = agentData._id;
                insertData.vOnboardingContract = agentData.vOnboardingContract;
                insertData.vOnboardingApplication = agentData.vOnboardingApplication;
                insertData.vTaxPayerContract = agentData.vTaxPayerContract;
                insertData.vAuthorizationContract = agentData.vAuthorizationContract;
                insertData.vDrivingLicenseContract = agentData.vDrivingLicenseContract;

                unsetData.vOnboardingContract = 1;
                unsetData.vOnboardingApplication = 1;
                unsetData.vOnboardingApplication = 1;
                unsetData.vTaxPayerContract = 1;
                unsetData.vAuthorizationContract = 1;
                unsetData.vDrivingLicenseContract = 1;

                const contractData = new ContractHistoryModel(insertData);

                try {
                    contractData.save(function (error, result) {
                        if (error) {
                            console.log(error);
                        } else {
                            AgentModel.findOneAndUpdate({
                                _id: iAgentId
                            }, {
                                "$unset": unsetData
                            }).exec();
                            res.redirect("/onboarding");
                        }
                    })

                } catch (error) {
                    console.log(error);
                }
            } else {
                res.redirect("/not available any contract");
            }

        } else {
            insertData.iAgentId = agentData._id;
            if (contract == "vOnboardingContract") {
                insertData.vOnboardingContract = agentData.vOnboardingContract;
                unsetData.vOnboardingContract = 1;
            }
            if (contract == "vOnboardingApplication") {
                insertData.vOnboardingApplication = agentData.vOnboardingApplication;
                unsetData.vOnboardingApplication = 1;
            }
            if (contract == "vTaxPayerContract") {
                insertData.vTaxPayerContract = agentData.vTaxPayerContract;
                unsetData.vTaxPayerContract = 1;
            }
            if (contract == "vAuthorizationContract") {
                insertData.vAuthorizationContract = agentData.vAuthorizationContract;
                unsetData.vAuthorizationContract = 1;
            }
            if (contract == "vDrivingLicenseContract") {
                insertData.vDrivingLicenseContract = agentData.vDrivingLicenseContract;
                unsetData.vDrivingLicenseContract = 1;
            }

            const contractData = new ContractHistoryModel(insertData)

            try {
                contractData.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        AgentModel.findOneAndUpdate({
                            _id: iAgentId
                        }, {
                            "$unset": unsetData
                        }).exec();
                        res.redirect("/onboarding");
                    }
                })

            } catch (error) {
                console.log(error);
            }
        }
    }

}

exports.restore_contract = async (req, res) => {

    if (req.session.email) {

        var iAgentId = req.body.iAgentId;
        var contractHistoryData = await ContractHistoryModel.findOne({
            iAgentId: iAgentId
        }).sort({
            $natural: -1
        });

        if (contractHistoryData) {
            try {

                AgentModel.findOneAndUpdate({
                    _id: contractHistoryData.iAgentId
                }, {
                    "$set": {
                        "vOnboardingContract": contractHistoryData.vOnboardingContract,
                        "vOnboardingApplication": contractHistoryData.vOnboardingApplication,
                        "vTaxPayerContract": contractHistoryData.vTaxPayerContract,
                        "vAuthorizationContract": contractHistoryData.vAuthorizationContract,
                        "vDrivingLicenseContract": contractHistoryData.vDrivingLicenseContract,

                    }
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/onboarding");
                    } else {
                        console.log(error);
                    }
                });

            } catch (error) {
                console.log(error);
            }

        }
    }

}

exports.download_contract = async (req, res) => {

    if (req.session.email) {

        var contractData = await AgentModel.findOne({
            _id: req.params.iAgentId
        });

        var templateHtml = fs.readFileSync('./app/view/onboarding/onboarding_contract.html', 'utf8');

        // var IPAddress = req.socket.remoteAddress;
        var IPAddress = ip.address();

        var data = templateHtml.replace(/#vContractAgentName#/g, contractData.vOnboardingContract.vContractAgentName)
            .replace("#vSalesRepInitials#", contractData.vOnboardingContract.vSalesRepInitials)
            .replace("#dtSalesRepInitialsDate#", contractData.vOnboardingContract.dtSalesRepInitialsDate)
            .replace("#vSalesRepName#", contractData.vOnboardingContract.vSalesRepName)
            .replace("#vSalesRepSignature#", contractData.vOnboardingContract.vSalesRepSignature)
            .replace("#vSalesRep#", contractData.vOnboardingContract.vSalesRep)
            .replace("#vSalesRepPhone#", contractData.vOnboardingContract.vSalesRepPhone)
            .replace("#vSalesRepEmail#", contractData.vOnboardingContract.vSalesRepEmail)
            .replace(/#dtSalesRepDate#/g, contractData.vOnboardingContract.dtSalesRepDate)
            .replace("#eSignature#", contractData.vOnboardingContract.eSignature)
            .replace("#IPAddress#", IPAddress);

        var milis = new Date();
        milis = milis.getTime();

        const browser = await puppeteer.launch({
            headless: true
        });

        const page = await browser.newPage();

        await page.setContent(data, {
            waitUntil: 'domcontentloaded'
        })

        // create a pdf buffer
        const pdfBuffer = await page.pdf({
            format: 'A4',
            displayHeaderFooter: false,
            margin: {
                top: "50px",
                bottom: "50px",
                left: "20px",
                right: "20px"
            },
        })

        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader('Content-Disposition', 'attachment; filename=' + milis + '.pdf');
        res.setHeader('Content-Length', pdfBuffer.length);
        return res.end(pdfBuffer);
    }

}

exports.download_application = async (req, res) => {

    if (req.session.email) {

        var applicationData = await AgentModel.findOne({
            _id: req.params.iAgentId
        });

        var templateHtml = fs.readFileSync('./app/view/onboarding/onboarding_application.html', 'utf8');

        var IPAddress = req.socket.remoteAddress;
        var IPAddress = ip.address();

        var data = templateHtml.replace(/#vName#/g, applicationData.vOnboardingApplication.vName)
            .replace("#vAddress#", applicationData.vOnboardingApplication.vAddress)
            .replace("#vBirthDate#", applicationData.vOnboardingApplication.vBirthDate)
            .replace("#vSecurityNumber#", applicationData.vOnboardingApplication.vSecurityNumber)
            .replace("#eLicenseRevoked#", applicationData.vOnboardingApplication.eLicenseRevoked)
            .replace("#eConvicted#", applicationData.vOnboardingApplication.eConvicted)
            .replace("#eFelony#", applicationData.vOnboardingApplication.eFelony)
            .replace("#eTheft#", applicationData.vOnboardingApplication.eTheft)
            .replace("#vAuthorizedPersonFirstName#", applicationData.vOnboardingApplication.vAuthorizedPersonFirstName)
            .replace("#vAuthorizedPersonLastName#", applicationData.vOnboardingApplication.vAuthorizedPersonLastName)
            .replace("#vAuthorizedPersonMiddleName#", applicationData.vOnboardingApplication.vAuthorizedPersonMiddleName)
            .replace("#vAuthorizedPersonOtherName#", applicationData.vOnboardingApplication.vAuthorizedPersonOtherName)
            .replace("#vYearsUsed#", applicationData.vOnboardingApplication.vYearsUsed)
            .replace("#eWorkOfCorporation#", applicationData.vOnboardingApplication.eWorkOfCorporation)
            .replace("#vEmployeeSignature#", applicationData.vOnboardingApplication.vEmployeeSignature)
            .replace(/#dtApplicationDate#/g, applicationData.vOnboardingApplication.dtApplicationDate)
            .replace("#IPAddress#", IPAddress);

        var milis = new Date();
        milis = milis.getTime();

        const browser = await puppeteer.launch({
            headless: true
        });

        const page = await browser.newPage();

        await page.setContent(data, {
            waitUntil: 'domcontentloaded'
        })

        // create a pdf buffer
        const pdfBuffer = await page.pdf({
            format: 'A4',
            displayHeaderFooter: false,
            margin: {
                top: "50px",
                bottom: "50px",
                left: "20px",
                right: "20px"
            },
        })

        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader('Content-Disposition', 'attachment; filename=' + milis + '.pdf');
        res.setHeader('Content-Length', pdfBuffer.length);
        return res.end(pdfBuffer);
    }
}

exports.download_authorizationfrm = async (req, res) => {
    if (req.session.email) {

        var authorizationData = await AgentModel.findOne({
            _id: req.params.iAgentId
        });

        try {
            var templateHtml = fs.readFileSync('./app/view/onboarding/deposit_authorization.html', 'utf8');

            // var IPAddress = req.socket.remoteAddress;
            var IPAddress = ip.address();

            var data = templateHtml.replace(/#vName#/g, authorizationData.vAuthorizationContract.vName).
                replace("#vAddress#", authorizationData.vAuthorizationContract.vAddress).
                replace("#vCity#", authorizationData.vAuthorizationContract.vCity).
                replace("#vState#", authorizationData.vAuthorizationContract.vState).
                replace("#vZipCode#", authorizationData.vAuthorizationContract.vZipCode).
                replace("#vBankName#", authorizationData.vAuthorizationContract.vBankName).
                replace("#vAccount#", authorizationData.vAuthorizationContract.vAccount).
                replace("#vRouting#", authorizationData.vAuthorizationContract.vRouting).
                replace("#vAccount#", authorizationData.vAuthorizationContract.vAccount).
                replace("#eAccountType#", authorizationData.vAuthorizationContract.eAccountType).
                replace("#vEmpSignature#", authorizationData.vAuthorizationContract.vEmpSignature).
                replace(/#dtApplicationDate#/g, authorizationData.vAuthorizationContract.dtApplicationDate).
                replace("#IPAddress#", IPAddress);

            var milis = new Date();
            milis = milis.getTime();

            const browser = await puppeteer.launch({
                headless: true
            });

            const page = await browser.newPage();

            await page.setContent(data, {
                waitUntil: 'domcontentloaded'
            })

            // create a pdf buffer
            const pdfBuffer = await page.pdf({
                format: 'A4',
                displayHeaderFooter: false,
                margin: {
                    top: "50px",
                    bottom: "50px",
                    left: "20px",
                    right: "20px"
                },
            })

            res.setHeader('Content-Type', 'application/pdf');
            res.setHeader('Content-Disposition', 'attachment; filename=' + milis + '.pdf');
            res.setHeader('Content-Length', pdfBuffer.length);
            return res.end(pdfBuffer);

        } catch (error) {
            console.log(error);
        }

    }
}

exports.upload_contract = async (req, res) => {

    const iAgentId = req.body.iAgentId;
    const vAdminAddContract = req.body.vAdminAddContract;
    const vContractName = req.body.vContractName;

    if (iAgentId) {
        try {

            let filename = null;

            if (req.files != null) {
                // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vAdminAddContract, 'contract/');
                // filename = awsURL;

                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vAdminAddContract,
                    path: 'contract'
                };
                filename = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }

            var UPDATE_DATA = {};

            if (vContractName == 'vOnboardingContract') {
                var contractUpdatedData = {
                    "vAdminAddContract": filename,
                    "dtAddedDate": new Date().toLocaleDateString(),
                }

                UPDATE_DATA.$set = {
                    "vOnboardingContract": contractUpdatedData
                }
            }

            if (vContractName == 'vOnboardingApplication') {
                var contractUpdatedData = {
                    "vAdminAddContract": filename,
                    "dtAddedDate": new Date().toLocaleDateString(),
                }

                UPDATE_DATA.$set = {
                    "vOnboardingApplication": contractUpdatedData
                }
            }

            if (vContractName == 'vTaxPayerContract') {
                var contractUpdatedData = {
                    "vAdminAddContract": filename,
                    "dtAddedDate": new Date().toLocaleDateString(),
                }

                UPDATE_DATA.$set = {
                    "vTaxPayerContract": contractUpdatedData
                }
            }

            if (vContractName == 'vAuthorizationContract') {
                var contractUpdatedData = {
                    "vAdminAddContract": filename,
                    "dtAddedDate": new Date().toLocaleDateString(),
                }

                UPDATE_DATA.$set = {
                    "vAuthorizationContract": contractUpdatedData
                }
            }

            if (vContractName == 'vDrivingLicenseContract') {
                var contractUpdatedData = {
                    "vAdminAddContract": filename,
                    "dtAddedDate": new Date().toLocaleDateString(),
                }

                UPDATE_DATA.$set = {
                    "vDrivingLicenseContract": contractUpdatedData
                }
            }

            AgentModel.findOneAndUpdate({
                _id: iAgentId
            }, UPDATE_DATA).exec(function (error, result) {
                if (!error) {
                    res.redirect("/onboarding");
                } else {
                    console.log(error);
                }
            });

        } catch (error) {
            console.log(error);
        }

    } else {
        console.log("error")
    }
}