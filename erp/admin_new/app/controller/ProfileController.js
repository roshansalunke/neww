const async = require("hbs/lib/async");
const UserModel = require("../model/UserModel");
const bcrypt = require("bcrypt");
const GeneralLibrary = require("../library/GeneralLibrary");
const momentTimezone = require('moment-timezone');

exports.add_action = async (req, res) => {

    if (req.session.email) {
        if (req.body.iUserId) {
            try {
                let image_name = null;

                if (req.files != null) {
                    /**** AWS Files Upload ****/
                    // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'profile/');
                    // image_name = awsURL;
                    /**** AWS END ****/

                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vImage,
                        path: 'profile'
                    };
                    image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }

                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vEmail": req.body.vEmail,
                            "vImage": image_name,
                            "vPassword": req.body.vPassword,
                            "eType": req.body.eType,
                            "eStatus": req.body.eStatus,
                            "vTimezone": req.body.vTimezone
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vEmail": req.body.vEmail,
                            "vPassword": req.body.vPassword,
                            "eType": req.body.eType,
                            "eStatus": req.body.eStatus,
                            "vTimezone": req.body.vTimezone
                        }
                    }
                }

                UserModel.findOneAndUpdate({
                    _id: req.body.iUserId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/user");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async (req, res) => {

    var iUserId = req.params.iUserId;

    if (req.session.email) {
        try {
            var userData = await UserModel.findOne({
                _id: iUserId
            });

            var timeZones = momentTimezone.tz.names();

            var offsetTmz = [];

            for (let i in timeZones) {
                let newData = {
                    timeZoneName: timeZones[i] + " (GMT" + momentTimezone.tz(timeZones[i]).format("Z") + ") ",
                    name: timeZones[i],
                    value: momentTimezone.tz(timeZones[i]).format("Z"),
                };
                offsetTmz.push(newData);
            }

            is_layout = true;
            res.render("../view/profile/add", {
                is_layout: is_layout,
                userData: userData,
                offsetTmz: offsetTmz
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.changePassword = async (req, res) => {

    var iUserId = req.params.iUserId;

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/profile/changePassword", {
                iUserId: iUserId,
                is_layout: is_layout
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.changePasswordAction = async (req, res) => {

    if (req.session.email) {
        if (req.body.iUserId) {
            try {
                const hash = await bcrypt.hash(req.body.vPassword, 10);

                UserModel.findOneAndUpdate({
                    _id: req.body.iUserId
                }, {
                    "$set": {
                        "vPassword": hash
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/dashboard");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        }
    }

};