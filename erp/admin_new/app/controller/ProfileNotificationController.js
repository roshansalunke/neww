const async = require("hbs/lib/async");
const AgentModel = require("../model/AgentModel");
const ClientModel = require("../model/ClientModel");
const UpdateAgentModel = require("../model/UpdateAgentModel");
const UpdateClientModel = require("../model/UpdateClientModel");
const AgentTypeModel = require("../model/AgentTypeModel");
const Paginator = require("../library/PaginatorLibrary");
const GeneralLibrary = require("../library/GeneralLibrary");
const crypto = require("crypto");

exports.index = async(req, res) => {

    if (req.session.email) {

        var agentData = await UpdateAgentModel.find().populate({
            path: "iAgentId",
        }).populate({
            path: "iAgentType",
            select: 'vTitle'
        }).sort({
            _id: 'desc'
        });

        var clientData = await UpdateClientModel.find().populate({
            path: "iClientId",
        }).sort({
            _id: 'desc'
        });
        is_layout = true;
        res.render("../view/profileNotification/index", {
            is_layout: is_layout,
            agentData:agentData,
            clientData:clientData
        });
    } else {
        res.redirect("/");
    }
};

exports.updateProfileInfo = async(req, res) => {

    if (req.session.email) {

        const iUpdateAgentId = req.body.iUpdateAgentId;
        const vAction = req.body.vAction;

        if (vAction == 'approved') {

            var objForUpdate = {};
            const agentData = await UpdateAgentModel.findOne({
                _id: iUpdateAgentId
            });

            if (agentData.vFirstName != '') objForUpdate.vFirstName = agentData.vFirstName;
            if (agentData.vLastName != '') objForUpdate.vLastName = agentData.vLastName;
            if (agentData.vCity != '') objForUpdate.vCity = agentData.vCity;
            if (agentData.vCountry != '') objForUpdate.vCountry = agentData.vCountry;
            if (agentData.vZipCode != '') objForUpdate.vZipCode = agentData.vZipCode;
            if (agentData.vPhone != '') objForUpdate.vPhone = agentData.vPhone;
            if (agentData.vAddress != '') objForUpdate.vAddress = agentData.vAddress;
            if (agentData.iAgentType != '') objForUpdate.iAgentType = agentData.iAgentType;
            if (agentData.eStatus != '') objForUpdate.eStatus = agentData.eStatus;

            objForUpdate = {
                "$set": objForUpdate
            };

            try {

                //send notification
                const CRITERIA = {
                    vNotification: `Admin approve your profile request`,
                    iUserId: agentData.iAgentId,
                    vLink: `/profile/edit/${agentData.iAgentId}`,
                    vTimezone: req.session.vTimezone,
                }
                GeneralLibrary.sendNotification(CRITERIA);

                AgentModel.findOneAndUpdate({
                    _id: agentData.iAgentId
                }, objForUpdate).exec(function(error, result) {
                    if (!error) {
                        UpdateAgentModel.deleteOne({
                            _id: agentData._id
                        }).exec();
                        res.redirect("/profile-notification");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }

        }
        if (vAction == 'reject') {
            const agentData = await UpdateAgentModel.findOne({
                _id: iUpdateAgentId
            });

            //send notification
            const CRITERIA = {
                vNotification: `Admin reject your profile request`,
                iUserId: agentData.iAgentId,
                vLink: `/profile/edit/${agentData.iAgentId}`,
                vTimezone: req.session.vTimezone,
            }
            GeneralLibrary.sendNotification(CRITERIA);

            UpdateAgentModel.deleteOne({
                _id: iUpdateAgentId
            }).exec(function(error, result) {
                if (error) {
                    console.log(error)
                } else {
                    res.redirect("/profile-notification")
                }
            });
        }

    }
}

exports.updateClientProfileInfo = async(req, res) => {

    if (req.session.email) {

        const iUpdateClientId = req.body.iUpdateClientId;
        const vAction = req.body.vAction;

        if (vAction == 'approved') {

            var objForUpdate = {};
            const clientData = await UpdateClientModel.findOne({
                _id: iUpdateClientId
            });

            if (clientData.vFirstName != '') objForUpdate.vFirstName = clientData.vFirstName;
            if (clientData.vLastName != '') objForUpdate.vLastName = clientData.vLastName;
            if (clientData.vCity != '') objForUpdate.vCity = clientData.vCity;
            if (clientData.vCountry != '') objForUpdate.vCountry = clientData.vCountry;
            if (clientData.vZipCode != '') objForUpdate.vZipCode = clientData.vZipCode;
            if (clientData.vPhone != '') objForUpdate.vPhone = clientData.vPhone;
            if (clientData.vAddress != '') objForUpdate.vAddress = clientData.vAddress;

            objForUpdate = {
                "$set": objForUpdate
            };

            try {

                //send notification
                const CRITERIA = {
                    vNotification: `Admin approve your profile request`,
                    iUserId: clientData.iClientId,
                    vLink: `/profile/edit/${clientData.iClientId}`,
                    vTimezone: req.session.vTimezone,
                }
                GeneralLibrary.sendNotification(CRITERIA);

                ClientModel.findOneAndUpdate({
                    _id: clientData.iClientId
                }, objForUpdate).exec(function(error, result) {
                    if (!error) {
                        UpdateClientModel.deleteOne({
                            _id: clientData._id
                        }).exec();
                        res.redirect("/profile-notification");

                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }

        }
        if (vAction == 'reject') {
            const clientData = await UpdateClientModel.findOne({
                _id: iUpdateClientId
            });

            //send notification
            const CRITERIA = {
                vNotification: `Admin reject your profile request`,
                iUserId: clientData.iClientId,
                vLink: `/profile/edit/${clientData.iClientId}`,
                vTimezone: req.session.vTimezone,
            }
            GeneralLibrary.sendNotification(CRITERIA);

            UpdateClientModel.deleteOne({
                _id: iUpdateClientId
            }).exec(function(error, result) {
                if (error) {
                    console.log(error)
                } else {
                    res.redirect("/profile-notification")
                }
            });
        }

    }
}