const async = require("hbs/lib/async");
const AdminTypeModel = require("../model/AdminTypeModel");
const Paginator = require("../library/PaginatorLibrary");
const CompanyDetailModel = require("../model/CompanyDetailModel");
const AgentModel = require("../model/AgentModel");
const ProjectCommissionModel = require("../model/ProjectCommissionModel");
const puppeteer = require('puppeteer');
const moment = require('moment');
const fs = require('fs');

exports.index = async (req, res) => {

    if (req.session.email) {

        var commissionData = await ProjectCommissionModel.find()
        .populate("projectId", "vProject iProjectUniqueId").exec();
       
        //weekname
        var currentWeek = moment().format('W');
        const weekWiseCommission = [];
        for (let i = 1; i <= currentWeek; i++) {
            weekWiseCommission.push(commissionData.filter(result => { return parseInt(moment(new Date(result.dtAddedDate)).format('W')) === parseInt(i) }))
        }
        is_layout = true;
        res.render("../view/project_commission/index", {
            is_layout: is_layout,
            weekWiseCommission: weekWiseCommission.reverse()
        });
    } else {
        res.redirect("/");
    }

};

exports.ajax_listing = async (req, res) => {

    var iPayrollId = req.body.iPayrollId;
    var vAction = req.body.vAction;
    var vPage = req.body.vPage;

    var agentData = await AgentModel.findOne({
        _id: req.session.userid
    });

    if (vAction === "search") {

        var SQL = {};
        // if (vTitle.length > 0) {
        //     var vTitleSearch = new RegExp(vTitle, "i");
        //     SQL.vTitle = vTitleSearch;
        // }

        // SQL.iAgentId = agentData._id
        try {
            // Pagination
            var companyData = await CompanyDetailModel.findOne().exec();
            var dataCount = await ProjectCommissionModel.count(SQL).exec();
            let vPage = 1;
            let vItemPerPage = companyData.vItemPerPage;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;
            // End

            
            var commissionData = await ProjectCommissionModel.find(SQL)
            .populate("projectId", "vProject iProjectUniqueId")
            .populate("agentId", "iUserID vFirstName vLastName")
            .skip(start).limit(limit).exec();
           
            //weekname
            var currentWeek = moment().format('W');
            const weekWiseCommission = [];
            for (let i = 1; i <= currentWeek; i++) {
                weekWiseCommission.push(commissionData.filter(result => { return parseInt(moment(new Date(result.dtAddedDate)).format('W')) === parseInt(i) }))
            }

            res.render("../view/project_commission/ajax_listing", {
                layout: false,
                // commissionData: commissionData,
                weekWiseCommission: weekWiseCommission.reverse(),
                paginator: paginator,
                datalimit: limit,
                dataCount: dataCount
            });
        } catch (error) {
            res.render("../view/project_commission/ajax_listing", {
                layout: false,
                weekWiseCommission: error
            });
        }
    }
}
