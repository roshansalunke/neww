const async = require("hbs/lib/async");
const ProjectModel = require("../model/ProjectModel");
const ProjectTypeModel = require("../model/ProjectTypeModel");
const ProjectAttachmentModel = require("../model/ProjectAttachmentModel");
const Paginator = require("../library/PaginatorLibrary");
const ClientModel = require("../model/ClientModel");
const AddendumModel = require("../model/AddendumModel");
const GeneralLibrary = require("../library/GeneralLibrary");
const ProjectContractModel = require("../model/ProjectContractModel");
const puppeteer = require('puppeteer');
const DeveloperModel = require('../model/DeveloperModel');
const DeveloperTypeModel = require('../model/DeveloperTypeModel');
const ProjectTeamModel = require("../model/ProjectTeamModel");
const ProjectPaymentModel = require("../model/ProjectPaymentModel");
const CompanyDetailModel = require("../model/CompanyDetailModel");
const ContractModel = require("../model/ContractModel");
const SubProjectModel = require("../model/SubProjectModel");
const ProjectCommissionModel = require('../model/ProjectCommissionModel');
const ip = require('ip');
const fs = require("fs-extra");
const path = require("path");
const moment = require('moment');
const {
    handlebars
} = require("hbs");

exports.index = async (req, res) => {
    if (req.session.email) {

        var projectData = await ProjectModel.find().populate({
            path: 'iProjectTypeId',
            select: 'vTitle'
        }).populate('projectContract', 'iProjectId')
            .populate('ClientUserId', 'vFirstName vLastName iUserID')
            .populate("projectTeam", 'iProjectId')
            .populate({
                path: 'projectManager',
                populate: {
                    path: 'devData',
                    select: 'iUserID vFirstName vLastName'
                }
            }).sort({_id:"desc"});

        is_layout = true;
        res.render("../view/project/index", {
            is_layout: is_layout,
            projectData:projectData
        });
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {
        try {
            ProjectModel.findOneAndUpdate({
                _id: req.body.iProjectId
            }, {
                "$set": {
                    "eStatus": req.body.eStatus
                }
            }).exec(function (error, result) {
                if (!error) {
                    res.redirect("/project");
                } else {
                    console.log(error);
                }
            });
        } catch (error) {
            console.log(error);
        }
    }

};

exports.view = async (req, res) => {

    if (req.session.email) {
        try {
            var iProjectId = req.params.iProjectId;

            var projectData = await ProjectModel.findOne({
                _id: iProjectId
            }).populate({
                path: 'iProjectTypeId',
                select: 'vTitle'
            }).populate('ClientUserId', 'vFirstName vLastName iUserID');

            var projectPaymentData = await ProjectPaymentModel.find({
                iProjectId: iProjectId
            });

            var projectAttachmentData = await ProjectAttachmentModel.find({
                iProjectId: iProjectId
            });
            is_layout = true;

            res.render("../view/project/view", {
                is_layout: is_layout,
                projectData: projectData,
                projectPaymentData: projectPaymentData,
                projectAttachmentData: projectAttachmentData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.amount = async (req, res) => {
    if (req.session.email) {
        try {
            var iProjectId = req.params.iProjectId;

            var projectData = await ProjectModel.findOne({
                _id: iProjectId
            });
            var clientData = await ClientModel.find();
            var projectTypeData = await ProjectTypeModel.find();
            var addendumData = await AddendumModel.findOne({
                iProjectTypeId: projectData.iProjectTypeId
            });

            // var price = projectData.iTechHour * addendumData.iDevelopment;
            // var iMinimumPrice = price / addendumData.iBillableHours;

            is_layout = true;
            res.render("../view/project/amount", {
                is_layout: is_layout,
                clientData: clientData,
                projectData: projectData,
                projectTypeData: projectTypeData,
                addendumData: addendumData
                // iMinimumPrice: iMinimumPrice.toFixed(2)
            });

        } catch (error) {
            console.log(error);
            res.redirect("/project");
        }
    } else {
        res.redirect("/");
    }
};

exports.amount_action = async (req, res) => {
    if (req.session.email) {
        if (req.body.iProjectId) {
            try {
                var dtProjectStartDate = moment(req.body.dtProjectStartDate, "MM/DD/YYYY").add(5, 'days').format("MM/DD/YYYY")
                var ProjectTypeData = await ProjectTypeModel.findOne({
                    _id: req.body.iProjectTypeId
                });
                var UPDATE_QUERY = {
                    "$set": {
                        "iClientUserId": req.body.iClientUserId,
                        "iProjectTypeId": req.body.iProjectTypeId,
                        "eIsRecurring": ProjectTypeData.eRecurrying,
                        "iTechPrice": req.body.iTechPrice,
                        "iMinimumPrice": parseFloat(req.body.iMinimumPrice).toFixed(2),
                        "dtProjectStartOriginDate": dtProjectStartDate,
                        "dtProjectStartDate": req.body.dtProjectStartDate,
                        "dtDeadlineDate": req.body.dtDeadlineDate,
                        "iTechHour": req.body.iTechHour,
                    }
                }
                ProjectModel.findOneAndUpdate({
                    _id: req.body.iProjectId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/project");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        }
    }
};

const compile = async function (filename, datajson) {
    var filePath = path.join(process.cwd(), '../assets/assets/project_contract/', filename);
    const html = await fs.readFile(filePath, 'utf8');
    return handlebars.compile(html)(datajson)
};

exports.download_contract = async (req, res) => {

    var iProjectId = req.params.iProjectId;
    var IPAddress = ip.address();
    var milis = new Date().getTime();

    var projectData = await ProjectModel.findOne({
        _id: iProjectId
    })
        .populate({
            path: 'iProjectTypeId',
            select: 'vTitle'
        }).lean();

    var projectContractData = await ProjectContractModel.findOne({
        iProjectId: projectData._id
    }).lean();

    //project contract from admin side
    var contractDetail = await ContractModel.findOne({
        iProjectTypeId: projectData.iProjectTypeId._id,
        ePaymentPhases: projectData.ePaymentPhases
    }).lean();

    //send data to handlebar file
    var contractData = {
        ...projectContractData,
        contractDetail,
        IPAddress: IPAddress
    };

    var vTitle = projectData.iProjectTypeId.vTitle;
    var ePaymentPhases = projectData.ePaymentPhases;
    var filename = vTitle.replace(/ /g, '_') + '_' + ePaymentPhases + '.handlebars';
    const htmlContent = await compile(filename, contractData);

    var data = htmlContent.replace("{{iContractProjectId}}", projectContractData.iContractProjectId)
        .replace("{{vFirstClientInitials}}", projectContractData.vFirstClientInitials)
        .replace("{{dtFirstClientInitialsDate}}", projectContractData.dtFirstClientInitialsDate)
        .replace("{{iSalesPrice}}", projectContractData.iSalesPrice)
        .replace("{{iFirstPhaseAmt}}", projectContractData.iFirstPhaseAmt)
        .replace("{{iSecondPhaseAmt}}", projectContractData.iSecondPhaseAmt)
        .replace("{{iSecondPhaseDate}}", projectContractData.iSecondPhaseDate)
        .replace("{{iThirdPhaseAmt}}", projectContractData.iThirdPhaseAmt)
        .replace("{{iThirdPhaseDate}}", projectContractData.iThirdPhaseDate)
        .replace("{{iFourPhaseAmt}}", projectContractData.iFourPhaseAmt)
        .replace("{{iFourPhaseDate}}", projectContractData.iFourPhaseDate)
        .replace("{{vSecondClientInitials}}", projectContractData.vSecondClientInitials)
        .replace("{{dtSecondClientInitialsDate}}", projectContractData.dtSecondClientInitialsDate)
        .replace("{{vContractClientEmail}}", projectContractData.vContractClientEmail)
        .replace("{{ePartnership}}", projectContractData.ePartnership)
        .replace("{{vThirdClientInitials}}", projectContractData.vThirdClientInitials)
        .replace("{{dtThirdClientInitialsDate}}", projectContractData.dtThirdClientInitialsDate)
        .replace("{{vSeoService}}", projectContractData.vSeoService)
        .replace(/{{iAnnualAmt}}/g, projectContractData.iAnnualAmt);

    const browser = await puppeteer.launch({
        headless: true
    });

    const page = await browser.newPage();
    await page.setContent(data)

    // create a pdf buffer
    const pdfBuffer = await page.pdf({
        format: 'A4',
        displayHeaderFooter: false,
        margin: {
            top: "50px",
            bottom: "50px",
            left: "20px",
            right: "20px"
        },
    })

    res.setHeader('Content-Type', 'application/pdf');
    res.setHeader('Content-Disposition', 'attachment; filename=' + vTitle.replace(/ /g, '_') + '_' + ePaymentPhases + '_' + milis + '.pdf');
    res.setHeader('Content-Length', pdfBuffer.length);
    return res.end(pdfBuffer);
}

exports.project_team = async (req, res) => {
    if (req.session.email) {
        var iProjectId = req.params.iProjectId;
        const projectTeam = await ProjectTeamModel.findOne({
            iProjectId: iProjectId
        }).populate([{
            path: 'iProjectId',
            select: 'vProject'
        }, {
            path: 'DeveloperId iProjectManagerId',
            select: 'vFirstName vLastName vEmail vPhone iDeveloperTypeId',
            populate: 'iDeveloperTypeId'

        }]);
        is_layout = true;
        res.render("../view/project/project_team", {
            is_layout: is_layout,
            projectTeam: projectTeam,
        });

    } else {
        res.redirect("/");
    }
}

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iProjectId = req.body.iProjectId;

        try {

            if (iProjectId) {
                //single record deleted
                if (vAction === "delete") {
                    await ProjectModel.deleteOne({
                        _id: iProjectId
                    }).then(function(result){

                        var projectCollections = [ProjectContractModel, SubProjectModel, ProjectAttachmentModel, ProjectPaymentModel, ProjectCommissionModel, ProjectTeamModel];
                        projectCollections.forEach(async element => {
                            await element.deleteMany({
                                iProjectId: {
                                    $in: iProjectId
                                }
                            })
                        });
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let projectID = iProjectId.split(',');
                    await ProjectModel.deleteMany({
                        _id: {
                            $in: projectID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}