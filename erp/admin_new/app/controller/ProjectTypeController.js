const async = require("hbs/lib/async");
const ProjectTypeModel = require("../model/ProjectTypeModel");

exports.index = async (req, res) => {

    if (req.session.email) {
        var ProjectTypeData = await ProjectTypeModel.find()

        is_layout = true;
        res.setHeader('Cache-Control', 'max-age=3600');
        res.render("../view/project_type/index", {
            is_layout: is_layout,
            ProjectTypeData: ProjectTypeData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/project_type/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iProjectTypeId) {
            try {
                ProjectTypeModel.findOneAndUpdate({
                    _id: req.body.iProjectTypeId
                }, {
                    "$set": {
                        "vTitle": req.body.vTitle,
                        "eRecurrying": req.body.eRecurrying
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/project-type");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const ProjectType = new ProjectTypeModel({
                vTitle: req.body.vTitle,
                eRecurrying: req.body.eRecurrying
            });
            try {
                ProjectType.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/project-type");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iProjectTypeId = req.params.iProjectTypeId;

    if (req.session.email) {
        try {
            var ProjectTypeData = await ProjectTypeModel.findOne({
                _id: iProjectTypeId
            });
            is_layout = true;
            res.render("../view/project_type/add", {
                is_layout: is_layout,
                ProjectTypeData: ProjectTypeData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iProjectTypeId = req.body.iProjectTypeId;
        try {
            if (iProjectTypeId) {

                //single record deleted
                if (vAction === "delete") {
                    await ProjectTypeModel.deleteOne({
                        _id: iProjectTypeId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let projectTypeID = iProjectTypeId.split(',');
                    await ProjectTypeModel.deleteMany({
                        _id: {
                            $in: projectTypeID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}