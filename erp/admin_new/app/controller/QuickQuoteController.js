const QuickQuoteModel = require("../model/QuickQuoteModel");
const Paginator = require("../library/PaginatorLibrary");
const CompanyDetailModel = require("../model/CompanyDetailModel");

exports.index = async (req, res) => {
    if (req.session.email) {

        var quickQuoteData = await QuickQuoteModel.find();
        is_layout = true;
        res.render("../view/quick_quote/index", {
            is_layout: is_layout,
            quickQuoteData:quickQuoteData
        });
    } else {
        res.redirect("/");
    }
};

exports.add = async (req, res) => {
    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/quick_quote/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {
    if (req.session.email) {
        if (req.body.iQuickQuoteId) {
            try {
                QuickQuoteModel.findOneAndUpdate({
                    _id: req.body.iQuickQuoteId
                }, {
                    "$set": {
                        "vPages": req.body.vPages,
                        "vBetterPrice": req.body.vBetterPrice,
                        "vBestPrice": req.body.vBestPrice,
                        "iOrderId": req.body.iOrderId,
                        "eStatus": req.body.eStatus
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/quick-quote");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const QuickQuoteObj = new QuickQuoteModel({
                vPages: req.body.vPages,
                vBetterPrice: req.body.vBetterPrice,
                vBestPrice: req.body.vBestPrice,
                iOrderId: req.body.iOrderId,
                eStatus: req.body.eStatus
            });
            try {
                QuickQuoteObj.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/quick-quote");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async (req, res) => {
    var iQuickQuoteId = req.params.iQuickQuoteId;
    if (req.session.email) {
        try {
            var quickQuoteData = await QuickQuoteModel.findOne({
                _id: iQuickQuoteId
            });
            is_layout = true;
            res.render("../view/quick_quote/add", {
                is_layout: is_layout,
                quickQuoteData: quickQuoteData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iQuickQuoteId = req.body.iQuickQuoteId;

        try {
            if (iQuickQuoteId) {
                //single record deleted
                if (vAction === "delete") {
                    await QuickQuoteModel.deleteOne({
                        _id: iQuickQuoteId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let quoteID = iQuickQuoteId.split(',');
                    await QuickQuoteModel.deleteMany({
                        _id: {
                            $in: quoteID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}