const async = require("hbs/lib/async");
const RoleModel = require("../model/RoleModel");
const AdminTypeModel = require("../model/AdminTypeModel");
const Paginator = require("../library/PaginatorLibrary");
const DeveloperTypeModel = require("../model/DeveloperTypeModel");

exports.index = async(req, res) => {

    if (req.session.email) {

        var roleData = await RoleModel.find().populate({
            path: 'vTitle',
            select: 'vTitle'
        });
        is_layout = true;
        res.render("../view/role/index", {
            is_layout: is_layout,
            roleData:roleData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async(req, res) => {

    if (req.session.email) {
        try {
            // var adminTypeData = await AdminTypeModel.find().exec();
            var developerTypeData = await DeveloperTypeModel.find().sort('iPosition');

            is_layout = true;
            res.render("../view/role/add", {
                is_layout: is_layout,
                developerTypeData: developerTypeData
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async(req, res) => {

    if (req.session.email) {

        if (req.body.iRoleId) {
            try {
                RoleModel.findOneAndUpdate({ _id: req.body.iRoleId }, {
                    "$set": {
                        "vTitle": req.body.vTitle,
                        "eStatus": req.body.eStatus
                    }
                }).exec(function(error, result) {
                    if (!error) {
                        res.redirect("/role");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const Role = new RoleModel({
                vTitle: req.body.vTitle,
                eStatus: req.body.eStatus
            });
            try {
                Role.save(function(error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/role");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async(req, res) => {
    if (req.session.email) {
        try {
            var iRoleId = req.params.iRoleId;
            var adminTypeData = await AdminTypeModel.find().exec();
            var developerTypeData = await DeveloperTypeModel.find();

            var roleData = await RoleModel.findOne({ _id: iRoleId });
            is_layout = true;
            res.render("../view/role/add", {
                is_layout: is_layout,
                roleData: roleData,
                // adminTypeData: adminTypeData
                developerTypeData: developerTypeData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.delete = async (req, res) => {

    if (req.session.email) {
        const vAction = req.body.vAction;
        const iRoleId = req.body.iRoleId;
        try {
            if (iRoleId) {
                //single record deleted
                if (vAction === "delete") {
                    await RoleModel.deleteOne({
                        _id: iRoleId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let roleID = iRoleId.split(',');
                    await RoleModel.deleteMany({
                        _id: {
                            $in: roleID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}