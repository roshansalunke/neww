const async = require("hbs/lib/async");
const RoleModuleMasterModel = require("../model/RoleModuleMasterModel");
const RoleModulePermissionModel = require("../model/RoleModulePermissionModel");
const Paginator = require("../library/PaginatorLibrary");

exports.index = async (req, res) => {

    if (req.session.email) {

        var roleModuleMasterData = await RoleModuleMasterModel.find();
        is_layout = true;
        res.render("../view/role_module_master/index", {
            is_layout: is_layout,
            roleModuleMasterData:roleModuleMasterData
        });
    } else {
        res.redirect("/");
    }

};

exports.ajax_listing = async (req, res) => {
    var iRoleModuleMasterId = req.body.iRoleModuleMasterId;
    var vTitle = req.body.vTitle;
    var eStatus = req.body.eStatus;
    var vAction = req.body.vAction;
    var vPage = req.body.vPage;

    if (vAction === "delete" && iRoleModuleMasterId != null) {
        await RoleModuleMasterModel.deleteOne({ _id: iRoleModuleMasterId });
        await RoleModulePermissionModel.deleteMany({ iRoleModuleMasterId: { $in: iRoleModuleMasterId } });
    }

    if (vAction === "multiple_delete" && iRoleModuleMasterId != null) {
        let roleID = iRoleModuleMasterId.split(',');

        await RoleModuleMasterModel.deleteMany({ _id: { $in: roleID } });
        await RoleModulePermissionModel.deleteMany({ iRoleModuleMasterId: { $in: iRoleModuleMasterId } });

    }

    if (vAction === "search") {
        var SQL = {};
        if (vTitle.length > 0) {
            var vTitleSearch = new RegExp(vTitle, "i");
            SQL.vTitle = vTitleSearch;
        }
        if (eStatus.length > 0) {
            var eStatusSearch = eStatus;
            SQL.eStatus = eStatusSearch;
        }

        try {
            // Pagination
            var dataCount = await RoleModuleMasterModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = 10;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;
            // End

            var roleModuleMasterData = await RoleModuleMasterModel.find(SQL).skip(start).limit(limit);

            res.render("../view/role_module_master/ajax_listing", {
                layout: false,
                roleModuleMasterData: roleModuleMasterData,
                paginator: paginator,
            });
        } catch (error) {
            res.render("../view/role_module_master/ajax_listing", {
                layout: false,
                roleModuleMasterData: error
            });
        }
    } else {
        try {
            // Pagination
            var dataCount = await RoleModuleMasterModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = 10;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;
            // End

            var roleModuleMasterData = await RoleModuleMasterModel.find(SQL).skip(start).limit(limit);

            res.render("../view/role_module_master/ajax_listing", {
                layout: false,
                roleModuleMasterData: roleModuleMasterData,
                paginator: paginator,
            });
        } catch (error) {
            res.render("../view/role_module_master/ajax_listing", {
                layout: false,
                roleModuleMasterData: error
            });
        }
    }
}

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/role_module_master/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iRoleModuleMasterId) {
            try {
                RoleModuleMasterModel.findOneAndUpdate({ _id: req.body.iRoleModuleMasterId }, {
                    "$set": {
                        "vTitle": req.body.vTitle,
                        "vModuleName": req.body.vModuleName,
                        "eStatus": req.body.eStatus
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/role-module-master");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const RoleModuleMaster = new RoleModuleMasterModel({
                vTitle: req.body.vTitle,
                vModuleName: req.body.vModuleName,
                eStatus: req.body.eStatus
            });
            try {
                RoleModuleMaster.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/role-module-master");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iRoleModuleMasterId = req.params.iRoleModuleMasterId;

    if (req.session.email) {
        try {
            var roleModuleMasterData = await RoleModuleMasterModel.findOne({ _id: iRoleModuleMasterId });
            is_layout = true;
            res.render("../view/role_module_master/add", {
                is_layout: is_layout,
                roleModuleMasterData: roleModuleMasterData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {

    if (req.session.email) {
        const vAction = req.body.vAction;
        const iRoleModuleMasterId = req.body.iRoleModuleMasterId;
        try {
            if (iRoleModuleMasterId) {
                //single record deleted
                if (vAction === "delete") {
                    await RoleModuleMasterModel.deleteOne({
                        _id: iRoleModuleMasterId
                    }).then(async (result) => {
                        await RoleModulePermissionModel.deleteMany({
                            iRoleModuleMasterId: { $in: iRoleModuleMasterId }
                        });
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let roleID = iRoleModuleMasterId.split(',');
                    await RoleModuleMasterModel.deleteMany({
                        _id: {
                            $in: roleID
                        }
                    }).then(async (result) => {
                        await RoleModulePermissionModel.deleteMany({
                            iRoleModuleMasterId: { $in: iRoleModuleMasterId }
                        });
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}