const async = require("hbs/lib/async");
const RoleModel = require("../model/RoleModel");
const Paginator = require("../library/PaginatorLibrary");
const RoleModuleMasterModel = require('../model/RoleModuleMasterModel');
const RoleModulePermissionModel = require('../model/RoleModulePermissionModel');

exports.index = async(req, res) => {

    if (req.session.email) {
        var roleData = await RoleModel.find().populate({
            path: 'vTitle',
            select: 'vTitle'
        });
        is_layout = true;
        res.render("../view/role_module_permission/index", {
            is_layout: is_layout,
            roleData: roleData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async(req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/role_module_permission/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.edit = async(req, res) => {

    if (req.session.email) {
        try {
            var iRoleId = req.params.iRoleId;
            var roleData = await RoleModel.find().populate({
                path: 'vTitle',
                select: 'vTitle'
            });
            var roleModuleMasterData = await RoleModuleMasterModel.find().sort('_id').exec();
            var roleModulePermissionData = await RoleModulePermissionModel.find({ iRoleId: iRoleId }).sort('_id').exec();
            is_layout = true;

            res.render("../view/role_module_permission/add", {
                is_layout: is_layout,
                roleData: roleData,
                roleModuleMasterData: roleModuleMasterData,
                roleModulePermissionData: roleModulePermissionData,
                iRoleId: iRoleId
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.add_action = async(req, res) => {

    if (req.session.email) {

        try {
            var roleModuleMasterData = await RoleModuleMasterModel.find().exec();
            var arrlen = roleModuleMasterData.length;

            if (req.body.iRoleId) {
                await RoleModulePermissionModel.deleteMany({ iRoleId: { $in: req.body.iRoleId } }).exec();

                for (let index = 0; index < arrlen; index++) {

                    const element = roleModuleMasterData[index];
                    const value = req.body[element._id];
                    var eCreate = 'No';
                    var eRead = 'No';
                    var eUpdate = 'No';
                    var eDelete = 'No';

                    if (value) {
                        if (value.includes('eCreate')) {
                            eCreate = 'Yes'
                        }
                        if (value.includes('eRead')) {
                            eRead = 'Yes'
                        }
                        if (value.includes('eUpdate')) {
                            eUpdate = 'Yes'
                        }
                        if (value.includes('eDelete')) {
                            eDelete = 'Yes'
                        }
                    }

                    const RolePermission = new RoleModulePermissionModel({
                        iRoleId: req.body.iRoleId,
                        iRoleModuleMasterId: element._id,
                        eCreate: eCreate,
                        eRead: eRead,
                        eUpdate: eUpdate,
                        eDelete: eDelete,
                    });

                    RolePermission.save();
                }

                setTimeout(function() {
                    res.redirect('/role-module-permission/edit/' + req.body.iRoleId);
                }, 300)
            }

        } catch (error) {
            console.log(error);
        }

    } else {
        res.redirect("/");
    }
};