const async = require("hbs/lib/async");
const SecurityQuestionModel = require("../model/SecurityQuestionModel");

exports.index = async (req, res) => {

    if (req.session.email) {

        var securityQuestionData = await SecurityQuestionModel.find()
        is_layout = true;
        res.render("../view/security_question/index", {
            is_layout: is_layout,
            securityQuestionData: securityQuestionData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/security_question/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iSecurityQuestionId) {
            try {
                SecurityQuestionModel.findOneAndUpdate({
                    _id: req.body.iSecurityQuestionId
                }, {
                    "$set": {
                        "vQuestion": req.body.vQuestion,
                        "eStatus": req.body.eStatus
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/security");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const SecurityQuestion = new SecurityQuestionModel({
                vQuestion: req.body.vQuestion,
                eStatus: req.body.eStatus
            });
            try {
                SecurityQuestion.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/security");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iSecurityQuestionId = req.params.iSecurityQuestionId;

    if (req.session.email) {
        try {
            var securityQuestionData = await SecurityQuestionModel.findOne({
                _id: iSecurityQuestionId
            });
            is_layout = true;
            res.render("../view/security_question/add", {
                is_layout: is_layout,
                securityQuestionData: securityQuestionData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iSecurityQuestionId = req.body.iSecurityQuestionId;

        try {
            if (iSecurityQuestionId) {
                //single record deleted
                if (vAction === "delete") {
                    await SecurityQuestionModel.deleteOne({
                        _id: iSecurityQuestionId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let securityID = iSecurityQuestionId.split(',');
                    await SecurityQuestionModel.deleteMany({
                        _id: {
                            $in: securityID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}