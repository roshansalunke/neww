const async = require("hbs/lib/async");
const SocialSettingModel = require("../model/SocialSettingModel");

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/social_setting/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iSocialSettingId) {
            try {
                let datetime = new Date();

                SocialSettingModel.findOneAndUpdate({ _id: req.body.iSocialSettingId }, {
                    "$set": {
                        "vFacebook": req.body.vFacebook,
                        "vInstagram": req.body.vInstagram,
                        "vTwitter": req.body.vTwitter,
                        "vLinkedin": req.body.vLinkedin,
                        "dtAddedDate": datetime
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("back");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const Social = new SocialSettingModel({
                vFacebook: req.body.vFacebook,
                vInstagram: req.body.vInstagram,
                vTwitter: req.body.vTwitter,
                vLinkedin: req.body.vLinkedin
            });
            try {
                Social.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("back");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iSocialSettingId = req.params.iSocialSettingId;

    if (req.session.email) {
        try {
            var socialData = await SocialSettingModel.findOne({ _id: iSocialSettingId });
            is_layout = true;
            res.render("../view/social_setting/add", {
                is_layout: is_layout,
                socialData: socialData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};