const async = require("hbs/lib/async");
const SystemEmailModel = require("../model/SystemEmailModel");
const Paginator = require("../library/PaginatorLibrary");

exports.index = async (req, res) => {

    if (req.session.email) {

        var systemEmailData = await SystemEmailModel.find();
        is_layout = true;
        res.render("../view/systemEmail/index", {
            is_layout: is_layout,
            systemEmailData:systemEmailData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/systemEmail/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iSystemEmailId) {
            try {
                SystemEmailModel.findOneAndUpdate({ _id: req.body.iSystemEmailId }, {
                    "$set": {
                        "vEmailCode": req.body.vEmailCode,
                        "vEmailTitle": req.body.vEmailTitle,
                        "vFromName": req.body.vFromName,
                        "vFromEmail": req.body.vFromEmail,
                        "vCcEmail": req.body.vCcEmail,
                        "vBccEmail": req.body.vBccEmail,
                        "vEmailSubject": req.body.vEmailSubject,
                        "tEmailMessage": req.body.tEmailMessage,
                        "tSmsMessage": req.body.tSmsMessage,
                        "tInternalMessage": req.body.tInternalMessage,
                        "eStatus": req.body.eStatus,
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/system-email");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const SystemEmail = new SystemEmailModel({
                vEmailCode: req.body.vEmailCode,
                vEmailTitle: req.body.vEmailTitle,
                vFromName: req.body.vFromName,
                vFromEmail: req.body.vFromEmail,
                vCcEmail: req.body.vCcEmail,
                vBccEmail: req.body.vBccEmail,
                vEmailSubject: req.body.vEmailSubject,
                tEmailMessage: req.body.tEmailMessage,
                tSmsMessage: req.body.tSmsMessage,
                tInternalMessage: req.body.tInternalMessage,
                eStatus: req.body.eStatus,
            });
            try {
                SystemEmail.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/system-email");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iSystemEmailId = req.params.iSystemEmailId;

    if (req.session.email) {
        try {
            var systemEmailData = await SystemEmailModel.findOne({ _id: iSystemEmailId });
            is_layout = true;
            res.render("../view/systemEmail/add", {
                is_layout: is_layout,
                systemEmailData: systemEmailData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iSystemEmailId = req.body.iSystemEmailId;

        try {
            if (iSystemEmailId) {
                //single record deleted
                if (vAction === "delete") {
                    await SystemEmailModel.deleteOne({
                        _id: iSystemEmailId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let systemEmailID = iSystemEmailId.split(',');
                    await SystemEmailModel.deleteMany({
                        _id: {
                            $in: systemEmailID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}