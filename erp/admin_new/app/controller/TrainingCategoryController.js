const async = require("hbs/lib/async");
const TrainingCategoryModel = require("../model/TrainingCategoryModel");

exports.index = async (req, res) => {

    if (req.session.email) {

        var trainingCategoryData = await TrainingCategoryModel.find();
        is_layout = true;
        res.render("../view/training_category/index", {
            is_layout: is_layout,
            trainingCategoryData:trainingCategoryData
        });
    } else {
        res.redirect("/");
    }

};

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/training_category/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iTrainingCategoryId) {
            try {
                TrainingCategoryModel.findOneAndUpdate({
                    _id: req.body.iTrainingCategoryId
                }, {
                    "$set": {
                        "vCategory": req.body.vCategory,
                        "eStatus": req.body.eStatus
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/training-category");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const trainingCategory = new TrainingCategoryModel({
                vCategory: req.body.vCategory,
                eStatus: req.body.eStatus
            });
            try {
                trainingCategory.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/training-category");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iTrainingCategoryId = req.params.iTrainingCategoryId;

    if (req.session.email) {
        try {
            var trainingCategoryData = await TrainingCategoryModel.findOne({
                _id: iTrainingCategoryId
            });
            is_layout = true;
            res.render("../view/training_category/add", {
                is_layout: is_layout,
                trainingCategoryData: trainingCategoryData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.delete = async (req, res) => {
    if (req.session.email) {
        const vAction = req.body.vAction;
        const iTrainingCategoryId = req.body.iTrainingCategoryId;

        try {
            if (iTrainingCategoryId) {
                //single record deleted
                if (vAction === "delete") {
                    await TrainingCategoryModel.deleteOne({
                        _id: iTrainingCategoryId
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }

                //miltiple records deleted
                if (vAction === "multiple_delete") {
                    let tcID = iTrainingCategoryId.split(',');
                    await TrainingCategoryModel.deleteMany({
                        _id: {
                            $in: tcID
                        }
                    }).then(function (result) {
                        req.flash("success", "Successfully deleted");
                        res.send({ status: 200 })
                    });
                }
            }
        } catch (error) {
            req.flash('error', "someting want wrong.")
            res.send({ status: 500, message: error.message })
        }
    } else {
        res.redirect("/");
    }
}