const UserModel = require("../model/UserModel");
const EmailLibrary = require('../library/EmailLibrary');
const SystemEmailModel = require("../model/SystemEmailModel");
const AdminTypeModel = require("../model/AdminTypeModel");
const DepartmentModel = require("../model/DepartmentModel");
const DepartmentRoleModel = require("../model/DepartmentRoleModel");
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const GeneralLibrary = require("../library/GeneralLibrary");
const SecurityQuestionModel = require("../model/SecurityQuestionModel");
const SecurityAnswerModel = require("../model/SecurityAnswerModel");
const ClientResetPwModel = require("../model/ClientResetPwModel");
const async = require("hbs/lib/async");
const AgentTypeModel = require("../model/AgentTypeModel");
const AgentModel = require("../model/AgentModel");

exports.index = async (req, res) => {

      if (req.session.email) {
        try {
            const departmentData = await DepartmentModel.find({vTitle:{$ne : 'CEO & Administration'}});
            var agentTypeData = await AgentTypeModel.find();
            is_layout = true;
            res.render("../view/user/add", {
                is_layout: is_layout,
                userData: null,
                departmentData:departmentData,
                agentTypeData: agentTypeData,
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.getRole = async (req, res) => {
    if (req.session.email) {
        try {
            var iDepartmentId = req.body.iDepartmentId;
            const DepartmentData = await DepartmentRoleModel.find({iDepartmentId:iDepartmentId});
            res.send(DepartmentData)
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.getAllAgent = async (req, res) => {
    if (req.session.email) {
        try {
            var agentData = await AgentModel.find();
            res.send(agentData)
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {
    if (req.session.email) {
        let image_name = null;

        if (req.body.iUserId) {
            try {
                if (req.files != null) {
                    // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'profile/');
                    // image_name = awsURL;

                    /**** Local Files Upload ****/
                    let CRITERIA = {
                        files: req.files.vImage,
                        path: 'profile'
                    };
                    image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                    /**** Local END ****/
                }

                if (req.files != null) {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vImage": image_name,
                            "vEmail": req.body.vEmail,
                            "vDepartment": req.body.vDepartment,
                            "vRole":req.body.vRole,
                            "vCity": req.body.vCity,
                            "vCountry": req.body.vCountry,
                            "vZipCode": req.body.vZipCode,
                            "vState": req.body.vState,
                            "vPhone": req.body.vPhone,
                            "vAddress": req.body.vAddress,
                            // "eStatus": req.body.eStatus
                        }
                    }
                } else {
                    var UPDATE_QUERY = {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vEmail": req.body.vEmail,
                            "vDepartment": req.body.vDepartment,
                            "vRole":req.body.vRole,
                            "vCity": req.body.vCity,
                            "vCountry": req.body.vCountry,
                            "vZipCode": req.body.vZipCode,
                            "vState": req.body.vState,
                            "vPhone": req.body.vPhone,
                            "vAddress": req.body.vAddress,
                            // "eStatus": req.body.eStatus
                        }
                    }
                }

                UserModel.findOneAndUpdate({
                    _id: req.body.iUserId
                }, UPDATE_QUERY).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/user");
                    } else {
                        console.log(error);
                    }
                });

            } catch (error) {
                console.log(error);
            }
        } else {
            if (req.files != null) {
                // var awsURL = await GeneralLibrary.awsFileUpload(req.files.vImage, 'profile/');
                // image_name = awsURL;

                /**** Local Files Upload ****/
                let CRITERIA = {
                    files: req.files.vImage,
                    path: 'profile'
                };
                image_name = GeneralLibrary.localFilesUpload(CRITERIA);
                /**** Local END ****/
            }

            const addedDate = new Date().toLocaleDateString();
            var randomNumber = Math.floor(100000 + Math.random() * 900000);
            var verifyCode = Math.floor(1000000 + Math.random() * 9000000);
            var verifyToken = crypto.randomBytes(20).toString("hex");

            const vPassword = crypto.randomBytes(10).toString("hex");

            const User = new UserModel({
                vFirstName: req.body.vFirstName,
                vLastName: req.body.vLastName,
                vEmail: req.body.vEmail,
                vCity: req.body.vCity,
                vCountry: req.body.vCountry,
                vZipCode: req.body.vZipCode,
                vState: req.body.vState,
                vPassword: vPassword,
                vImage: image_name,
                vDepartment:req.body.vDepartment,
                vRole: req.body.vRole,
                vPhone: req.body.vPhone,
                vAddress: req.body.vAddress,
                eStatus: 'Active',
                dtAddedDate: addedDate,
                iUserID: randomNumber,
                vVerifyCode: verifyCode,
                vVerifyToken: verifyToken
            });

            let email_template = await SystemEmailModel.find({
                'vEmailCode': 'USER_REGISTER'
            });

            try {
                User.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        const vVerifyURL = process.env.APP_URL + "verify/" + result.vVerifyToken;

                        let criteria = {
                            user_name: result.vFirstName + ' ' + result.vLastName,
                            email_to: result.vEmail,
                            password: vPassword,
                            email_subject: "ERP",
                            email_template: email_template,
                            verifycode: result.vVerifyCode,
                            verifyurl: vVerifyURL
                        }
                        EmailLibrary.send_email(criteria);
                        res.redirect("/user");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async (req, res) => {
    var iUserId = req.params.iUserId;
    if (req.session.email) {
        try {
            var userData = await UserModel.findOne({
                _id: iUserId
            });
            const adminTypeData = await AdminTypeModel.find();
            is_layout = true;
            res.render("../view/user/add", {
                is_layout: is_layout,
                userData: userData,
                adminTypeData: adminTypeData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};


exports.verifytoken = async (req, res) => {

    var iVerifyToken = req.params.iVerifyToken;

    try {
        var adminData = await UserModel.findOne({
            vVerifyToken: iVerifyToken
        })

        if (adminData != null) {
            is_layout = false;
            res.render("../view/user/verifytoken", {
                is_layout: is_layout,
                iVerifyToken: iVerifyToken
            });
        }
    } catch (error) {
        console.log(error);
    }
};

exports.verify_action = async (req, res) => {
    const vCode = req.body.vCode;
    const vToken = req.body.token;
    try {
        const adminData = await UserModel.findOne({
            vVerifyCode: vCode
        });

        if (adminData == null) {
            req.flash('error', 'Verify code invalid');
            res.redirect('/admin-verify/' + vToken);
        } else {
            if (adminData.eVerify != 'Yes') {

                UserModel.findOneAndUpdate({
                    vVerifyCode: vCode
                }, {
                    "$set": {
                        "eVerify": "Yes",
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        req.flash('success', 'Verify successfully');
                        res.redirect('/admin-security-question/' + vToken);
                    }
                });
            } else {
                req.flash('error', 'Already verify this code.');
                res.redirect('/admin-verify/' + vToken);
            }
        }
    } catch (error) {
        console.log(error);
    }
};

exports.security_question = async (req, res) => {
    try {
        var iVerifyToken = req.params.iVerifyToken;

        let questionData = await SecurityQuestionModel.find().limit(6);

        is_layout = false;
        res.render("../view/user/security_question", {
            is_layout: is_layout,
            questionData: questionData,
            iVerifyToken: iVerifyToken
        });
    } catch (error) {
        console.log(error);
    }
};

exports.security_action = async (req, res) => {
    const vToken = req.body.token;

    try {
        const adminData = await UserModel.findOne({
            vVerifyToken: vToken
        });

        if (adminData == null) {
            req.flash('error', 'Admin not found.');
            res.redirect('/admin-security-question/' + vToken);
        } else {
            if (adminData.eVerify == 'Yes') {
                var QA = req.body.vAnswer;
                var userID = adminData._id.toString();
                var allData = [];

                QA.forEach(element => {
                    allData.push({
                        vAnswer: Object.values(element).toString(),
                        iQuestionId: Object.keys(element).toString(),
                        iUserId: userID,
                        eType: 'admin'
                    });
                });

                try {
                    SecurityAnswerModel.insertMany(allData, function (error, result) {
                        if (error) {
                            console.log(error);
                        } else {
                            res.redirect('/admin-reset/' + vToken);
                        }
                    })
                } catch (error) {
                    console.log(error);
                }
            } else {
                req.flash('error', 'Check your email and verify code.');
                res.redirect('/admin-verify/' + vToken);
            }
        }
    } catch (error) {
        console.log(error);
    }
};

exports.reset = async (req, res) => {
    var iVerifyToken = req.params.iVerifyToken;
    try {
        is_layout = false;
        res.render("../view/user/reset_password", {
            is_layout: is_layout,
            iVerifyToken: iVerifyToken
        });
    } catch (error) {
        console.log(error);
    }
};

exports.reset_action = async (req, res) => {
    const vToken = req.body.token;

    try {
        const userData = await UserModel.findOne({
            vVerifyToken: vToken
        });

        if (userData == null) {
            req.flash('error', 'Admin not found');
            res.redirect('/reset/' + vToken);
        } else {
            if (userData.eVerify == 'Yes') {
                const hash = await bcrypt.hash(req.body.vPassword, 10);
                UserModel.findOneAndUpdate({
                    vVerifyToken: vToken
                }, {
                    "$set": {
                        "vPassword": hash,
                    }
                }).exec(function (error, result) {
                    if (!error) {
                        ClientResetPwModel.deleteOne({
                            iUserId: result._id
                        }).exec();
                        req.flash('success', 'New password set successfully.');
                        res.redirect('/login');
                    }
                });
            } else {
                req.flash('error', 'Check your email and verify code.');
                res.redirect('/admin-reset/' + vToken);
            }
        }
    } catch (error) {
        console.log(error);
    }
};

exports.check_unique_email = async (req, res) => {

    var vEmail = req.body.vEmail;

    try {
        var adminData = await UserModel.findOne({
            vEmail: vEmail
        })

        if (adminData != null) {
            res.send("1")
        } else {
            res.send("0")
        }
    } catch (error) {
        console.log(error);
    }
};