const fb_Contact_Us_Today_Model = require("../../model/formBuilder/fb_Contact_Us_Today_Model");
const Paginator = require("../../library/PaginatorLibrary");
const CompanyDetailModel = require("../../model/CompanyDetailModel");
const FormBuilderModel = require("../../model/FormBuilderModel");

exports.index = async (req, res) => {
	if (req.session.email) {
		var SQL = {};
		if(req.session.usertype !== "Admin"){
			SQL.iUserId = req.session.userid;
		}
		var Contact_Us_TodayData = await fb_Contact_Us_Today_Model.find(SQL);
		var data = await FormBuilderModel.findOne({ vSlug: res.locals.route }).exec();
		res.render("../view/formBuilder/index", {
			is_layout: true,
			ajaxData: Contact_Us_TodayData,
			fields: data.vFormBuilder,
			userType:req.session.usertype,
		});
	} else {
		res.redirect("/");
	}
};

exports.add = async (req, res) => {
	if (req.session.email) {
		try {
			var data = await FormBuilderModel.findOne({ vSlug: res.locals.route }).exec();
			is_layout = true;
			res.render("../view/formBuilder/add", {
				is_layout: is_layout,
				fields: data.vFormBuilder,
				datas: data
			});
		} catch (error) {
			console.log(error);
		}
	} else {
		res.redirect("/");
	}
};

exports.add_action = async (req, res) => {
	if (req.session.email) {
		if (req.body.iDataId) {
			try {
				fb_Contact_Us_Today_Model.findOneAndUpdate({_id: req.body.iDataId}, {
					"$set": {
						"iUserId": req.session.userid,
						"eFillUpStatus": "Yes",
						"vFirstName": req.body.vFirstName,
						"vLastName": req.body.vLastName,
						"vEmail": req.body.vEmail,
						"vPhone": req.body.vPhone,
						"vAddress": req.body.vAddress,
						"vCity": req.body.vCity,
						"vState": req.body.vState,
						"vZipCode": req.body.vZipCode,
						"vDomain": req.body.vDomain,
						"vHosting": req.body.vHosting,
						"tDescription": req.body.tDescription,
					}
				}).exec(function (error, result) {
					if (!error) {
						res.redirect("/" + res.locals.route);
					} else {
						console.log(error);
					}
				});
			} catch (error) {
				console.log(error);
			}
		} else {
			const Contact_Us_TodayJson = new fb_Contact_Us_Today_Model({
			iUserId: req.session.userid,
			eFillUpStatus: "Yes",
			vFirstName: req.body.vFirstName,
			vLastName: req.body.vLastName,
			vEmail: req.body.vEmail,
			vPhone: req.body.vPhone,
			vAddress: req.body.vAddress,
			vCity: req.body.vCity,
			vState: req.body.vState,
			vZipCode: req.body.vZipCode,
			vDomain: req.body.vDomain,
			vHosting: req.body.vHosting,
			tDescription: req.body.tDescription,
			});
			try {
				Contact_Us_TodayJson.save(function (error, result) {
					if (error) {
						console.log(error);
					} else {
						res.redirect("/" + res.locals.route);
					}
				})
			} catch (error) {
				console.log(error);
			}
		}
	}
};

exports.edit = async (req, res) => {
	var iDataId = req.params.iDataId;
	if (req.session.email) {
		try {
			var Contact_Us_TodayData = await fb_Contact_Us_Today_Model.findOne({
				_id: iDataId
			});
			var data = await FormBuilderModel.findOne({ vSlug: res.locals.route }).exec();
			is_layout = true;
			res.render("../view/formBuilder/add", {
				is_layout: is_layout,
				fbData: Contact_Us_TodayData,
				fields: data.vFormBuilder,
				datas: data
			});
		} catch (error) {
			console.log(error);
		}
	} else {
		res.redirect("/");
	}
};

exports.delete = async (req, res) => {
	if (req.session.email) {
	var vAction = req.body.vAction;
	var iDataId = req.body.iDataId;
		try {
			if (iDataId) {
				//single record deleted
				if (vAction === "delete") {
					await fb_Contact_Us_Today_Model.deleteOne({
						_id: iDataId
					}).then(function (result) {
						req.flash("success", "Successfully deleted");
						res.send({ status: 200 })
					});
				}
				//miltiple records deleted
				if (vAction === "multiple_delete") {
					let fbDataId = iDataId.split(",");
						await fb_Contact_Us_Today_Model.deleteMany({
							_id: {
								$in: fbDataId
							}
						}).then(function (result) {
						req.flash("success", "Successfully deleted");
						res.send({ status: 200 })
					});
				}
			}
		} catch (error) {
			req.flash("error", "someting want wrong.")
			res.send({ status: 500, message: error.message })
		}
	} else {
		res.redirect("/");
	}
}
