const {
    rejects
} = require("assert");
const {
    Handlebars
} = require("express-handlebars");
const req = require("express/lib/request");
const async = require("hbs/lib/async");
const {
    resolve
} = require("path");
const CompanyDetailModel = require("../model/CompanyDetailModel");
const RoleModuleMasterModel = require("../model/RoleModuleMasterModel");
const RoleModulePermissionModel = require("../model/RoleModulePermissionModel");
var handlebars = require("handlebars");
var MomentHandler = require("handlebars.moment");
const moment = require('moment');

module.exports = {
    if_equal: function (a, b, options) {
        if (a == b) {
            return options.fn(this);
        }
        return options.inverse(this);
    },
    InArray: function (ele, list, options) {

        if(typeof(list) === 'string'){
            var list1 = list.trim().split(',');
            if (list1.includes(ele)) {
                return options.fn(this)
            }
            return options.inverse(this);
        }else{
            if (list.includes(ele)) {
                return options.fn(this)
            }
            return options.inverse(this);
        }
    },

    in_Array: function (ele, list, options) {

        if (list.indexOf(ele.toString()) > -1) {
            return options.fn(this)
        }
        return options.inverse(this);
    },
    isSelected: function (value, key) {
        return value == key ? "selected" : "";
    },
    contains: (Needle, Haystack, options) => {
        Needle = Haystack;
        Haystack = Haystack;
        return (Haystack.indexOf(Needle) > -1) ? options.fn(this) : options.inverse(this);
    },
    if_condition: function (value1, value2, options) {
        if (value1 === value2) {
            return options.fn(this);
        }
        return options.inverse(this);
    },
    if_count: function (value, option) {
        if (value) {
            if (value.length > 0) {
                return option.fn(this);
            }
            return option.inverse(this);
        } else {
            return option.inverse(this);
        }
    },
    substr: function (str, length) {
        var maxLength = length;
        if (str.length > maxLength) {
            return str.substr(0, maxLength) + '...';
        } else {
            return str;
        }
    },
    if_permission: async function (usertype, path, options) {
        // var roleModulePermissionData = await RoleModulePermissionModel.find({ iRoleId: usertype });
        // for (let index = 0; index < roleModulePermissionData.length; index++) {
        //     const iModuleMasterId = roleModulePermissionData[index].iRoleModuleMasterId;
        //     var roleModuleMasterData = await RoleModuleMasterModel.findOne({ _id: iModuleMasterId });
        //     roleModulePermissionData[index].iRoleModuleMasterId = roleModuleMasterData.vModuleName;
        // }

        // var data = roleModulePermissionData.find(i => i.iRoleModuleMasterId === path);

        // // return data.eRead;
        // if (data.eRead == 'Yes') {
        //     return options.inverse(this);
        // }
        // return options.fn(this);

        // RoleModulePermissionModel.find({ iRoleId: usertype }).populate({
        //     path: 'iRoleModuleMasterId',
        //     select: 'vModuleName'
        // }).then((permissionData) => {
        //     var data = permissionData.find(i => i.iRoleModuleMasterId.vModuleName == path);
        //     var permission = data.eRead;
        //     return permission;
        // });
    },
    user_name: function (val1, val2) {
        var v1 = val1.charAt(0);
        var v2 = val2.charAt(0);

        return v1.toUpperCase() + v2.toUpperCase();
    },
    checkInArray: function (value, arrayData,options) {
        var result = arrayData.includes(value);
        if (result == true) {
            return options.inverse(this);
        }
        return options.fn(this);
    },
    dateFormate: function (date, format) {
        var newDate = new Date(date);
        var fullDate = moment(newDate).format(format);
        return fullDate;
    },
    check_ternary: function (val1) {
        if (val1 == null) {
            return 'Pending';
        } else {
            return val1;
        }
    },
    if_pagination: function (val1, val2, options) {
        if (val1 < val2) {
            return options.fn(this);
        }
        return options.inverse(this);
    },
    arrLength: function (arr) {
        var arrlen = arr.length;
        return arrlen;
    },
    fromNowTime: function (value) {
        var data = moment(value).fromNow();
        return data;
    },
    if_greterThen: function (a, b, options) {
        if (parseInt(a) > parseInt(b)) {
            return options.fn(this);
        }
        return options.inverse(this);
    },
    getWeek:function(date){
        if(date){
            var week = moment(new Date(date)).format('W');
            return 'WK'+week
        }
    },
    totalAmt:function(data){

        var amt = 0;
        const totalAmount = data.map((result) => {
            amt += parseFloat(result.iAmount)
        });

        return amt.toFixed(2);
    },
    sub:function(val,index){
        return val - index
    }
};

MomentHandler.registerHelpers(handlebars);