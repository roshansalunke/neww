var nodemailer = require('nodemailer');
var fs = require("fs");
const EmailCredentialModel = require("../model/EmailCredentialModel");
const {
    resolve
} = require('path');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

module.exports = {
    send_email: function (criteria) {
        EmailCredentialModel.find({
                where: {}
            })
            .then(data => {
                if (data[0].vUsername != "") {
                    fs.readFile('./app/view/template/email.html', {
                        encoding: 'utf-8'
                    }, function (err, html) {
                        if (err) {
                            console.log({
                                "status": "error",
                                "code": "404",
                                "message": err,
                                "data": null
                            });
                        } else {
                            var email_message = criteria.email_template[0].tEmailMessage
                                .replace("#USERNAME#", criteria.user_name)
                                .replace("#EMAIL#", "<b>" + criteria.email_to + "</b>")
                                .replace("#PASSWORD#", "<b>" + criteria.password + "</b>")
                                .replace("#VERIFYCODE#", "<b>" + criteria.verifycode + "</b>")
                                .replace("#VERIFYLINK#", criteria.verifyurl)
                                .replace("#VERIFYHREFLINK#", criteria.verifyurl);

                            var message = html.replace("#CONTENT#", email_message);

                            const msg = {
                                to: criteria.email_to,
                                from: 'administration@hombolt.net',
                                subject: criteria.email_subject,
                                html: message
                            };
                            sgMail.send(msg).then(() => {
                                console.log('Email sent');
                            }).catch((error) => {
                                console.error(error);
                            });

                            // var transporter = nodemailer.createTransport({
                            //     host: data[0].vHost,
                            //     port: data[0].vPort,
                            //     secure: false,
                            //     auth: {
                            //         user: data[0].vUsername,
                            //         pass: data[0].vPassword
                            //     }
                            // });
                            // var mailOptions = {
                            //     from: data[0].vFromAddress,
                            //     to: criteria.email_to,
                            //     subject: criteria.email_subject,
                            //     html: message
                            // };
                            // transporter.sendMail(mailOptions, function (error, info) {
                            //     if (error) {
                            //         var response = {
                            //             "status": "error",
                            //             "code": "404",
                            //             "message": error,
                            //             "data": null
                            //         };
                            //     } else {
                            //         var response = {
                            //             "status": "success",
                            //             "code": "200",
                            //             "message": "Email Sent successfully"
                            //         };
                            //     }

                            //     console.log("mail", response);
                            //     return response;
                            // });
                        }
                    });
                } else {
                    var response = {
                        "status": "error",
                        "code": "404",
                        "message": "Email Credentials Not Found",
                        "data": null
                    };
                    console.log(response);
                    return response;
                }
            })
            .catch(err => {
                var response = {
                    "status": "error",
                    "code": "404",
                    "message": err,
                    "data": null
                };
                console.log(response);
                return response;
            });
    }
}