const async = require("hbs/lib/async");
const fs = require('fs');
const AWS = require('aws-sdk');
const CompanyDetailModel = require("../model/CompanyDetailModel");
const RoleModulePermissionModel = require("../model/RoleModulePermissionModel");
const RoleModuleMasterModel = require("../model/RoleModuleMasterModel");
const NotificationModel = require("../model/NotificationModel");
const {
    ElastiCache
} = require("aws-sdk");
const moment = require('moment');

module.exports = {

    company_information: async function (req, res, next) {
        var SQL = {};
        var companyData = await CompanyDetailModel.find().lean().exec();
        return companyData;
    },
    awsFileUpload: async function (fileName, path) {

        const s3 = new AWS.S3({
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
            endpoint: process.env.AWS_URL,
            s3ForcePathStyle: true,
        });

        const fileContent = fileName.data;

        const filePath = 'erp/' + path + fileName.name;

        const params = {
            Bucket: process.env.AWS_BUCKET,
            Key: filePath,
            Body: fileContent,
            Bucket: process.env.AWS_BUCKET,
            CreateBucketConfiguration: {
                LocationConstraint: process.env.AWS_DEFAULT_REGION
            },
            ACL: 'public-read',
            ContentType: fileName.mimetype
        };

        const awsURL = await s3.upload(params).promise();

        return awsURL.Location;
    },

    awsFileRead: async function (fileName, path) {

        const s3 = new AWS.S3({
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
            endpoint: process.env.AWS_URL,
            s3ForcePathStyle: true,
        });

        const filePath = 'erp/' + path + fileName;

        const params = {
            Bucket: process.env.AWS_BUCKET,
            Key: filePath,
        };

        var data = await s3.getObject(params).promise();
        var html = Buffer.from(data.Body).toString('utf8');

        return html;
    },
    permission: async function (CRITERIA) {
        var roleModulePermissionData = await RoleModulePermissionModel.find({
            iRoleId: CRITERIA.userType
        });
        for (let index = 0; index < roleModulePermissionData.length; index++) {
            const iModuleMasterId = roleModulePermissionData[index].iRoleModuleMasterId;
            var roleModuleMasterData = await RoleModuleMasterModel.findOne({
                _id: iModuleMasterId
            });
            roleModulePermissionData[index].iRoleModuleMasterId = roleModuleMasterData.vModuleName;
        }

        var data = roleModulePermissionData.find(i => i.iRoleModuleMasterId === CRITERIA.URL[1]);

        return data;
    },

    sendNotification: async function (CRITERIA) {
        var dtAddedDate = moment().format('YYYY-MM-DD HH:mm:ss')
        const notificationData = new NotificationModel({
            vNotification: CRITERIA.vNotification,
            iUserId: CRITERIA.iUserId,
            vLink: CRITERIA.vLink,
            eUserSend: 'admin',
            dtAddedDate: dtAddedDate,
            vTimezone: CRITERIA.vTimezone
        });
        notificationData.save();
    },
    localFilesUpload: function (CRITERIA) {
        /** File extention */
        if (CRITERIA.files.mimetype == "image/png") {
            var ext = ".png";
        }
        if (CRITERIA.files.mimetype == "image/jpg") {
            var ext = ".jpg";
        }
        if (CRITERIA.files.mimetype == "image/jpeg") {
            var ext = ".jpeg";
        }
        if (CRITERIA.files.mimetype == "application/pdf") {
            var ext = ".pdf";
        }
        if (CRITERIA.files.mimetype == "image/webp") {
            var ext = ".webp";
        }
        /** End ext */

        /** File name create */
        image_name = Date.now() + ext;
        /** End name */

        /** Make dir  */
        var dir = "../assets/assets/" + CRITERIA.path;
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
        /** End dir */

        /** Move file */
        const vImage = CRITERIA.files;
        vImage.mv("../assets/assets/" + CRITERIA.path + '/' + image_name);
        /** End move */

        let image_url;
        if (process.env.APP_ENV == 'development') {
            image_url = process.env.PUBLIC_URL + CRITERIA.path + '/' + image_name;
        } else {
            image_url = process.env.PUBLIC_URL + 'assets/' + CRITERIA.path + '/' + image_name;
        }

        return image_url;
    }
}