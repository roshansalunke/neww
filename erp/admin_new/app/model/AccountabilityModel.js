const mongoose = require("mongoose");

const AccountabilitySchema = mongoose.Schema({
    iAgentId:{
        type: String,
    },
    vWeek: {
        type: String,
    },
    vContact: {
        type: String,
        // required: true,
    },
    vInterview: {
        type: String,
        // required: true,
    },
    vClient: {
        type: String,
        // required: true,
    },
    vSales: {
        type: String,
        // required: true,
    },
    dtWeekStartDate:{
        type: String,
    },
    dtWeekEndDate:{
        type: String,
    },
    eIsCorn:{
        type: String,
        enum: ['Yes', 'No'],
        default: 'No',
    },
    dtAddedDate:{
        type: String,
    },
    dtUpdatedDate:{
        type: String,
    }
});

AccountabilitySchema.virtual("agentId", {
    ref: 'agent',
    localField: 'iAgentId',
    foreignField: '_id',
    justOne: "true"
})
AccountabilitySchema.set('toObject', { virtuals: true });
AccountabilitySchema.set('toJSON', { virtuals: true });


module.exports = mongoose.model("accountability", AccountabilitySchema);