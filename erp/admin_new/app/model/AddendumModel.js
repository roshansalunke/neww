const mongoose = require("mongoose");

const AddendumSchema = mongoose.Schema({
    iProjectTypeId: {
        type: String,
        required: true,
        ref: 'project_type'
    },
    eType:{
        type: String,
        enum: ['Percentage', 'Doller'],
        default: 'Percentage',
    },
    vName: {
        type: String,
        required: true,
    },
    iPresident: {
        type: String,
        required: true,
    },
    iPresidentMargin: {
        type: String,
        required: true,
    },
    iVicePresident: {
        type: String,
        required: true,
    },
    iVicePresidentMargin: {
        type: String,
        required: true,
    },
    iSeniorManagement: {
        type: String,
        required: true,
    },
    iSeniorManagementMargin: {
        type: String,
        required: true,
    },
    iSalesDirector: {
        type: String,
        required: true,
    },
    iSalesDirectorMargin: {
        type: String,
        required: true,
    },
    iSalesRepresentative: {
        type: String,
        required: true,
    },
    iDevelopment: {
        type: String,
        required: true,
    },
    iBillableHours: {
        type: String,
        // required: true,
    },
    iMinPrice: {
        type: String,
        // required: true,
    },
    eIsRecurring: {
        type: String,
        enum: ['Yes', 'No'],
        // required: true,
    },
    vQuote:{
        type: String,
    },
    iQuoteId:{
        type: String,
    },
    eQuoteLevel:{
        type: String,
        enum: ['Batter', 'Best'],
        default: 'Batter',
    },
});

module.exports = mongoose.model("addendum", AddendumSchema);