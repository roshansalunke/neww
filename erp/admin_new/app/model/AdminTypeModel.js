const mongoose = require("mongoose");

const AdminTypeSchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    }
});

module.exports = mongoose.model("admin_type", AdminTypeSchema);