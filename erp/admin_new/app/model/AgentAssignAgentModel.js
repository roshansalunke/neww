const mongoose = require("mongoose");

const AgentAssignAgentSchema = mongoose.Schema({
    iAgentId: {
        type: String,
        required: true,
        ref: "agent"
    },
    iSubAgentId: {
        type: String,
        required: true,
        ref: "agent"
    },
});

AgentAssignAgentSchema.virtual("agentData", {
    ref: 'agent',
    localField: 'iSubAgentId',
    foreignField: '_id',
})

AgentAssignAgentSchema.set('toObject', { virtuals: true });
AgentAssignAgentSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model("agent_assign_agent", AgentAssignAgentSchema);