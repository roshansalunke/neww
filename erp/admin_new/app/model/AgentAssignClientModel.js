const mongoose = require("mongoose");

const AgentAssignClientSchema = mongoose.Schema({
    iAgentId: {
        type: String,
        required: true,
    },
    iClientId: {
        type: String,
        required: true,
        ref: 'client'
    },
    dtAddedDate: {
        type: String,
    },
});

module.exports = mongoose.model("agent_assign_client", AgentAssignClientSchema);