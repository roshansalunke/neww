const mongoose = require("mongoose");

const OnboardingTypeSchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    }
});

module.exports = mongoose.model("onboarding_type", OnboardingTypeSchema);