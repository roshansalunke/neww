const mongoose = require("mongoose");

const AgentSchema = mongoose.Schema({
    iUserID: {
        type: String,
    },
    vFirstName: {
        type: String,
        required: true,
    },
    vLastName: {
        type: String,
        required: true,
    },
    vEmail: {
        type: String,
        required: true,
    },
    vPassword: {
        type: String,
        required: true,
    },
    vCity: {
        type: String,
        required: true,
    },
    vCountry: {
        type: String,
        required: true,
    },
    vZipCode: {
        type: String,
        required: true,
    },
    vReferrer: {
        type: String,
        ref: "agent",
        default: null
    },
    iAgentType: {
        type: String,
        required: true,
        ref: "agent_type"
    },
    vPhone: {
        type: String,
        required: true,
    },
    vAddress: {
        type: String,
        required: true,
    },
    eUserType: {
        type: String,
        enum: ['Agent'],
        default: 'Agent'
    },
    dtAddedDate: {
        type: String,
    },
    eStatus: {
        type: String,
        enum: ['Active','Terminated','Production','Quit','Inactive'],
        default: 'Active',
        required: true,
    },
    eContractStatus: {
        type: String,
        enum: ['Pending', 'Active', 'Suspended', 'Terminated'],
        default: 'Pending',
    },
    eClearmarket: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No',
    },
    vOnboardingContract: {
        type: Object,
    },
    vOnboardingApplication: {
        type: Object,
    },
    vTaxPayerContract: {
        type: Object
    },
    vAuthorizationContract: {
        type: Object
    },
    vDrivingLicenseContract: {
        type: Object
    },
    // vAdminAddContract: {
    //     type: String,
    //     enum: ['Yes', 'No'],
    //     default: 'No'
    // },
    vImage: {
        type: String
    },
    eVerify: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No'
    },
    vVerifyCode: {
        type: String,
        //required: true
    },
    vVerifyToken: {
        type: String,
        //required: true
    },
    vResetPasswordBy: {
        type: String,
    },
    iPasswordAttempt: {
        type: String,
    },
});

AgentSchema.virtual("SubAgent", {
    ref: 'agent_assign_agent',
    localField: '_id',
    foreignField: 'iAgentId',
})

AgentSchema.virtual("subagentCount", {
    ref: 'agent_assign_agent',
    localField: '_id',
    foreignField: 'iAgentId',
    count: true
});

AgentSchema.set('toObject', {
    virtuals: true
});
AgentSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model("agent", AgentSchema);