const mongoose = require("mongoose");

const AgentTrainingSchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
    },
    iTrainingCategoryId: {
        type: String,
        required: true,
        ref: "training_category"
    },
    vUrl: {
        type: String,
        required: true,
    },
    tDescription: {
        type: String,
        required: true,
    },
    vFile: {
        type: String,
        // required: true,
    },
    iAgentType: {
        type: String,
        required: true,
        ref: "agent_type"
    }
});

module.exports = mongoose.model("agent_training", AgentTrainingSchema);