const mongoose = require("mongoose");

const AgentTypeSchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
    },
    iPosition: {
        type: String,
        required: true,
    },
    vContractFile: {
        type: String,
        //required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    }
});

module.exports = mongoose.model("agent_type", AgentTypeSchema);