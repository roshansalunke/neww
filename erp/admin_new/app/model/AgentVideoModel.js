const mongoose = require("mongoose");

const AgentVideoSchema = mongoose.Schema({
    vTitle: {
        type: String,
        default: null
    },
    eCategory: {
        type: String,
        default: null
    },
    vLink: {
        type: String,
        default: null
    },
    tDescription: {
        type: String,
        default: null
    },
    vImage: {
        type: String,
        default: null
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
    }
});

module.exports = mongoose.model("agent_video", AgentVideoSchema);