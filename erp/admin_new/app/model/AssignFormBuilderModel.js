const mongoose = require("mongoose");

const AssignFormBuilderSchema = mongoose.Schema({
    iFormBuilderId: {
        type: String,
    },
    iDepartmentRoleId: {
        type: String,
    },
    iUserId: {
        type: String
    },
    dtAddedDate: {
        type: String
    }
});

module.exports = mongoose.model("assign_page_builder", AssignFormBuilderSchema);