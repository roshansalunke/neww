const mongoose = require("mongoose");

const BannerSchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
    },
    vImage: {
        type: String,
        required: true,
    },
    vPanel: {
        type: String,
        enum: ['Client', 'Agent', 'Admin', 'Developer']
    },
    vPanelScreen: {
        type: String,
        enum: ['Login', 'Register']
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    }
});

module.exports = mongoose.model("banner", BannerSchema);