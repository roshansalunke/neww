const mongoose = require("mongoose");

const BlogCategorySchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    },
    dtAddedDate: {
        type: String,
    }
});

module.exports = mongoose.model("blog_category", BlogCategorySchema);