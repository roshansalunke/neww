const mongoose = require("mongoose");

const BlogSchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
    },
    tDescription: {
        type: String,
        required: true,
    },
    iBlogCategoryId: {
        type: String,
        required: true,
        ref: "blog_category"
    },
    vLink: {
        type: String,
    },
    vImage: {
        type: String,
    },
    eType: {
        type: String,
        enum: ['All', 'Client', 'Agent', 'Developer'],
        default: 'All',
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    },
    dtAddedDate: {
        type: String,
    }
});

module.exports = mongoose.model("blog", BlogSchema);