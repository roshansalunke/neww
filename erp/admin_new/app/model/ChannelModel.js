const mongoose = require("mongoose");

const ChannelSchema = mongoose.Schema({
    vTitle: {
        type: String,
    },
    vUserEmail: {
        type: String,
    },
    vUserData:{
        iUserId: String,
        vUserProfile: String,
        vUserName: String,
        vUserToken: String
    },
    vPageData: {
        vPageId: String,
        vPageName: String,
        vPageToken: String,
        vPageProfile: String
    },
    dtAddedDate: {
        type: String,
    }
});

module.exports = mongoose.model("channel", ChannelSchema);