const mongoose = require("mongoose");

const ChatSchema = mongoose.Schema({
    iSenderId: {
        type: String,
        required: true,
    },
    iReceiverId: {
        type: String,
        required: true,
    },
    vMessage: {
        type: String,
    },
    dtAddedDate: {
        type: String
    },
    dtUpdatedDate: {
        type: String
    },
});

module.exports = mongoose.model("chat", ChatSchema);