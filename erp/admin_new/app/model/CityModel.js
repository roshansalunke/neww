const mongoose = require("mongoose");

const CitySchema = mongoose.Schema({
    id: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    state_id: {
        type: String,
        required: true,
        ref: "state"
    },
});

module.exports = mongoose.model("city", CitySchema);