const mongoose = require("mongoose");

const ClientSchema = mongoose.Schema({
    iUserID: {
        type: String,
    },
    vFirstName: {
        type: String,
        required: true,
    },
    vLastName: {
        type: String,
        required: true,
    },
    vEmail: {
        type: String,
        required: true,
    },
    vPassword: {
        type: String,
        required: true,
    },
    vReferrer: {
        type: String,
        ref: 'agent',
        default: null
    },
    vCity: {
        type: String,
        required: true,
    },
    vCountry: {
        type: String,
        required: true,
    },
    vZipCode: {
        type: String,
        required: true,
    },

    vPhone: {
        type: String,
        required: true,
    },
    vAddress: {
        type: String,
        required: true,
    },
    vVerifyCode: {
        type: String,
        required: true
    },
    vVerifyToken: {
        type: String,
        required: true
    },
    eUserType: {
        type: String,
        enum: ['Client'],
        default: 'Client'
    },
    eVerify: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No'
    },
    dtAddedDate: {
        type: String,
    },
    vImage: {
        type: String,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    },
    vResetPasswordBy: {
        type: String,
    }
});
// ClientSchema.virtual("Referrer", {
//     ref: 'agent',
//     localField: 'vReferrer',
//     foreignField: '_id',
//     justOne: true
// })

// ClientSchema.set('toObject', { virtuals: true });
// ClientSchema.set('toJSON', { virtuals: true });
module.exports = mongoose.model("client", ClientSchema);