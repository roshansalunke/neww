const mongoose = require("mongoose");

const ClientResetPwSchema = mongoose.Schema({
    iClientId: {
        type: String,
        ref: "client"
    },
    iUserId: {
        type: String,
        ref: "user"
    },
    iAgentId: {
        type: String,
        ref: "agent"
    },
    iDeveloperId: {
        type: String,
        ref: "developer"
    },
    vFile: {
        type: String,
    },
    dtAddedDate: {
        type: String,
    },
});

module.exports = mongoose.model("client_reset_password", ClientResetPwSchema);