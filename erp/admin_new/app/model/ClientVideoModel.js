const mongoose = require("mongoose");

const ClientVideoSchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
    },
    vLink: {
        type: String,
        required: true,
    },
    tDescription: {
        type: String,
        required: true,
    },
    vImage: {
        type: String,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    }
});

module.exports = mongoose.model("client_video", ClientVideoSchema);