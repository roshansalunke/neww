const mongoose = require("mongoose");

const ContractHistorySchema = mongoose.Schema({
    iAgentId: {
        type: String,
    },
    vOnboardingContract: {
        type: Object,
    },
    vOnboardingApplication: {
        type: Object,
    },
    vTaxPayerContract: {
        type: Object
    },
    vAuthorizationContract: {
        type: Object
    },
    vDrivingLicenseContract: {
        type: Object
    }
});

module.exports = mongoose.model("contract_history", ContractHistorySchema);