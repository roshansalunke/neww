const mongoose = require('mongoose');

const ContractSchema = mongoose.Schema({
    iProjectTypeId: {
        type: String,
        // required: true,
    },
    iAgentContractTypeId: {
        type: String,
        // required: true,
    },
    vDetail: {
        type: String,
        required: true,
    },
    vPdfDetail: {
        type: String,
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        required: true,
    },
    ePaymentPhases: {
        type: String,
        enum: ['ThreePhase', 'FourPhase', 'FullPayment', 'Annual', 'SemiAnnual', 'Quarterly', 'Monthly'],
    }

})

module.exports = mongoose.model('contract_content', ContractSchema);