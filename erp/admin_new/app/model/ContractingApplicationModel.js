const mongoose = require("mongoose");

const ContractingApplicationSchema = mongoose.Schema({
    iAgentId: {
        type: String,
        required: true,
    },
    vName: {
        type: String,
        required: true,
    },
    vAddress: {
        type: String,
        required: true,
    },
    vBirthDate: {
        type: String,
        required: true,
    },
    vSecurityNumber: {
        type: String,
        required: true,
    },
    eLicenseRevoked: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No'
    },
    dtLicenseRevokedDate: {
        type: String,
        required: true,
    },
    vLicenseRevokedDes: {
        type: String,
        // required: true,
    },
    eConvicted: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No'
    },
    dtConvictedDate: {
        type: String,
    },
    vConvictedDes: {
        type: String,
    },
    eFelony: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No'
    },
    dtFelonyDate: {
        type: String,
    },
    vFelonyDes: {
        type: String,
    },
    eTheft: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No'
    },
    dtTheftDate: {
        type: String,
    },
    vTheftDes: {
        type: String,
    },
    vAuthorizedPersonFirstName: {
        type: String,
    },
    vAuthorizedPersonLastName: {
        type: String,
    },
    vAuthorizedPersonMiddleName: {
        type: String,
    },
    vAuthorizedPersonOtherName: {
        type: String,
    },
    vYearsUsed: {
        type: String,
    },
    eWorkOfCorporation: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No'
    },
    vEmployeeSignature: {
        type: String,
    },
    dtApplicationDate: {
        type: String,
    },
    dtAddedDate: {
        type: String,
    }
});

module.exports = mongoose.model("onboarding_application", ContractingApplicationSchema);