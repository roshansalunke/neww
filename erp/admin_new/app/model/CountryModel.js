const mongoose = require("mongoose");

const CountrySchema = mongoose.Schema({
    id: {
        type: String,
        required: true,
    },
    sortname: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model("country", CountrySchema);