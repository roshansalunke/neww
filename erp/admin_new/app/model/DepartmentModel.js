const mongoose = require("mongoose");

const DepartmentSchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
    },
    iPosition: {
        type: Number,
    },
    vDivId:{
        type: String,
    },
    vDataTarget:{
        type: String,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    },
});

//project Manager
DepartmentSchema.virtual("roleData", {
    ref: 'department_role',
    localField: '_id',
    foreignField: 'iDepartmentId',
});

DepartmentSchema.set('toObject', {
    virtuals: true
});
DepartmentSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model("department", DepartmentSchema);