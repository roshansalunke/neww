const mongoose = require("mongoose");

const DepartmentRoleSchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
    },
    iDepartmentId: {
        type: String,
        required: true,
        ref:'department'
    },
    iPosition: {
        type: Number,
    },
    isRole:{
        type: String,
        enum: ['Yes', 'No'],
        default: 'Yes',
        required: true,
    },
    vUrl: {
        type: String,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    }
});

module.exports = mongoose.model("department_role", DepartmentRoleSchema);