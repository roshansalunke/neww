const mongoose = require("mongoose");

const DeveloperSchema = mongoose.Schema({
    iUserID: {
        type: String,
    },
    iDeveloperTypeId: {
        type: String,
        ref: "developer_type",
        default: null
    },
    vFirstName: {
        type: String,
        required: true,
    },
    vLastName: {
        type: String,
        required: true,
    },
    vEmail: {
        type: String,
        required: true,
    },
    vPassword: {
        type: String,
        // required: true,
    },
    vCity: {
        type: String,
        required: true,
    },
    vCountry: {
        type: String,
        required: true,
    },
    vZipCode: {
        type: String,
        required: true,
    },
    vPhone: {
        type: String,
        required: true,
    },
    vAddress: {
        type: String,
        required: true,
    },
    dtAddedDate: {
        type: String,
    },
    vImage: {
        type: String,
    },
    vVerifyCode: {
        type: String,
        required: true
    },
    vVerifyToken: {
        type: String,
        required: true
    },
    eVerify: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No'
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    },
    iPasswordAttempt: {
        type: String,
    },
    vResetPasswordBy: {
        type: String,
    },
    vTimezone: {
        type: String,
        default: 'Asia/Kolkata'
    }
});

module.exports = mongoose.model("developer", DeveloperSchema);