const mongoose = require("mongoose");

const DeveloperProjectSchema = mongoose.Schema({
    iProjectId: {
        type: String,
        ref: "project",
        default: null
    },
    iDeveloperId: {
        type: String,
        required: true,
    },
    dtAddedDate: {
        type: String,
    }
});

DeveloperProjectSchema.virtual("devData", {
    ref: 'developer',
    localField: 'iDeveloperId',
    foreignField: '_id',
    justOne: "true"
})
DeveloperProjectSchema.set('toObject', { virtuals: true });
DeveloperProjectSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model("developer_project", DeveloperProjectSchema);