const mongoose = require("mongoose");

const DeveloperTypeSchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
    },
    iPosition: {
        type: String,
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    }
});

module.exports = mongoose.model("developer_type", DeveloperTypeSchema);