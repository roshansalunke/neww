const mongoose = require("mongoose");

const EmailCredentialSchema = mongoose.Schema({
    vMailer: {
        type: String,
        required: true,
    },
    vHost: {
        type: String,
        required: true,
    },
    vPort: {
        type: String,
        required: true,
    },
    vUsername: {
        type: String,
        required: true,
    },
    vPassword: {
        type: String,
        required: true,
    },
    vEncryption: {
        type: String,
        required: true,
    },
    vFromAddress: {
        type: String,
        required: true,
    },
    vFromName: {
        type: String,
        required: true,
    }
});

module.exports = mongoose.model("email_credential", EmailCredentialSchema);