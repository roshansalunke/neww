const mongoose = require("mongoose");

const FormBuilderSchema = mongoose.Schema({
    vPage: {
        type: String,
    },
    vFormBuilder: {
        type: Array,
    },
    vSlug: {
        type: String
    },
    vLayout: {
        type: String
    },
    vFormUnique: {
        type: String
    }
});

module.exports = mongoose.model("page_builder", FormBuilderSchema);