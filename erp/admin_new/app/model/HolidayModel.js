const mongoose = require("mongoose");

const HolidaySchema = mongoose.Schema({
    dtHolidayDate: {
        type: String,
        required: true,
    },
    vTitle: {
        type: String,
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    }
});

module.exports = mongoose.model("holiday", HolidaySchema);