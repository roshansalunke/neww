const mongoose = require("mongoose");

const IFFNewBusiness = mongoose.Schema({
    vCategory:{
        type: String,
        enum: ['new_business', 'sales_presentations','handouts'],
    },
    vService: {
        type: String,
    },
    vPrice: {
        type: String,
    },
    vStructureDoc: {
        type: String,
        default: null
    },
    vAddedDoc: {
        type: String,
        default: null
    },
    vVideoLink: {
        type: String
    },
    vAddedDate: {
        type: String
    },
    vUpdatedDate: {
        type: String
    }
});

module.exports = mongoose.model("in_field_form", IFFNewBusiness);