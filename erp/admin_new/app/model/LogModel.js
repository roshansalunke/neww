const mongoose = require("mongoose");

const LogSchema = mongoose.Schema({
    vDescription: {
        type: String,
        default: null
    },
    eUserType: {
        type: String,
        enum: ['Admin', 'Agent', 'Client', 'Developer'],
        default: null
    },
    vUserName: {
        type: String,
        default: null
    },
    vIpAddress: {
        type: String,
        default: null
    },
    dtAddedDateTime: {
        type: String,
        default: null
    },
});

module.exports = mongoose.model("activity_log", LogSchema);