const mongoose = require("mongoose");

const NotificationSchema = mongoose.Schema({
    vNotification: {
        type: String,
        required: true,
    },
    iUserId: {
        type: String,
    },
    eUserSend: {
        type: String,
        enum: ['admin', 'agent', 'client', 'developer'],
        default: 'No',
    },
    eUserSeen: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No',
    },
    eAdminSeen: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No',
    },
    vLink: {
        type: String
    },
    dtAddedDate: {
        type: String,
    },
    vTimezone: {
        type: String,
    }
});

module.exports = mongoose.model("notification", NotificationSchema);