const mongoose = require("mongoose");

const PayrollPaymentSchema = mongoose.Schema({
    iProjectId: {
        type: String,
        // required: true,
        ref: "project",
        default: null
    },
    iAmount: {
        type: String,
    },
    ePaymentPhases: {
        type: String,
        enum: ['ThreePhase', 'FourPhase', 'FullPayment', 'Annual', 'SemiAnnual', 'Quarterly', 'Monthly'],
    },
    ePaymentStep: {
        type: String,
    },
    ePaymentStatus: {
        type: String,
        default: null,
    },
   /* payment_type: {
        type: String,
        default: null
    },
    paypal_payid: {
        type: String,
        default: null
    },
    payment_intents: {
        type: String,
        default: null
    },
    receipt_url: {
        type: String,
        default: null
    },
    charge_id: {
        type: String,
        default: null
    },
    checkout_session_id: {
        type: String,
        default: null
    },
    subscription_id: {
        type: String,
        default: null
    },
    vTransactionId: {
        type: String,
        default: null
    }, */
    dtPaymentDate: {
        type: String,
        default: null
    },
});

module.exports = mongoose.model("payroll_payment", PayrollPaymentSchema);