const mongoose = require("mongoose");

const ProjectAttachmentSchema = mongoose.Schema({
    iProjectId: {
        type: String,
        required: true,
        ref: 'client'
    },
    vAttachment: {
        type: String,
    }
});

module.exports = mongoose.model("project_attachment", ProjectAttachmentSchema);