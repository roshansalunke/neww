const mongoose = require("mongoose");

const ProjectCommissionSchema = mongoose.Schema({
    iProjectId: {
        type: String,
        ref: "project",
        default: null
    },
    iProjectPaymentId: {
        type: String,
        ref: "project_payment",
        default: null
    },
    iAgentId:{
        type: String,
    },
    iAmount: {
        type: String,
    },
    dtAddedDate: {
        type: String,
        default: null
    },
});

//project
ProjectCommissionSchema.virtual("projectId", {
    ref: 'project',
    localField: 'iProjectId',
    foreignField: '_id',
    justOne: "true"
})

//Agent
ProjectCommissionSchema.virtual("agentId", {
    ref: 'agent',
    localField: 'iAgentId',
    foreignField: '_id',
    justOne: "true"
})

ProjectCommissionSchema.set('toObject', { virtuals: true });
ProjectCommissionSchema.set('toJSON', { virtuals: true });


module.exports = mongoose.model("project_commission", ProjectCommissionSchema);