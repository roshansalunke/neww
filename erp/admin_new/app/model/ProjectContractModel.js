const mongoose = require("mongoose");

const ProjectContractSchema = mongoose.Schema({
    iProjectId: {
        type: String,
        required: true,
    },
    iAgentId: {
        type: String,
        required: true,
    },
    iClientUserId: {
        type: String
    },
    ePaymentPhases: {
        type: String,
    },
    vClientName: {
        type: String,
        // required: true,
    },
    vClientEmail: {
        type: String,
        // required: true,
    },
    vBusinessName: {
        type: String,
        // required: true,
    },
    vContractAgentId: {
        type: String,
        required: true,
    },
    vContractAgentName: {
        type: String,
        required: true,
    },
    iContractProjectId: {
        type: String,
        // required: true,
    },
    vFirstClientInitials: {
        type: String,
        // required: true,
    },
    dtFirstClientInitialsDate: {
        type: String,
        // required: true,
    },
    iSalesPrice: {
        type: String,
        required: true,
    },
    iFirstPhaseAmt: {
        type: String,
        // required: true,
    },
    iFirstPhaseDate: {
        type: String,
        // required: true,
    },
    iSecondPhaseAmt: {
        type: String,
        // required: true,
    },
    iSecondPhaseDate: {
        type: String,
        // required: true,
    },
    iThirdPhaseAmt: {
        type: String,
        // required: true,
    },
    iThirdPhaseDate: {
        type: String,
        // required: true,
    },
    iFourPhaseAmt: {
        type: String,
        // required: true,
    },
    iFourPhaseDate: {
        type: String,
        // required: true,
    },
    vSecondClientInitials: {
        type: String,
        // required: true,
    },
    dtSecondClientInitialsDate: {
        type: String,
        // required: true,
    },
    vContractClientEmail: {
        type: String,
        // required: true,
    },
    ePartnership: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'Yes',
    },
    vThirdClientInitials: {
        type: String,
        // required: true,
    },
    dtThirdClientInitialsDate: {
        type: String,
        // required: true,
    },
    vContractClientName: {
        type: String,
        // required: true,
    },
    vContractClientSignature: {
        type: String,
        // required: true,
    },
    dtContractClientDate: {
        type: String,
    },
    vContractClientAddress: {
        type: String,
        // required: true,
    },
    vSalesRepName: {
        type: String,
        required: true,
    },
    vSalesRepSignature: {
        type: String,
        // required: true,
    },
    dtSalesRepDate: {
        type: String,
        required: true,
    },
    eSignature: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'Yes',
    },
    eAgentContractStatus: {
        type: String,
        enum: ['done', 'pending'],
        default: 'pending',
    },
    eClientContractStatus: {
        type: String,
        enum: ['done', 'pending'],
        default: 'pending',
    },
    vSeoService: {
        type: String,
    },
    iAnnualAmt: {
        type: String,
    },
});

module.exports = mongoose.model("project_contract", ProjectContractSchema);