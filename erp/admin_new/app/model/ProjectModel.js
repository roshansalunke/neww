const mongoose = require("mongoose");

const ProjectSchema = mongoose.Schema({
    iAgentId: {
        type: String,
    },
    iClientUserId: {
        type: String,
        required: true,
    },
    iProjectUniqueId: {
        type: String,
    },
    vProject: {
        type: String,
        required: true,
    },
    vDescription: {
        type: String,
    },
    iProjectTypeId: {
        type: String,
        required: true,
        ref: 'project_type'
    },
    eStatus: {
        type: String,
        enum: ['Pending', 'Approve', 'Development','Completed', 'Archived'],
        default: 'Pending',
    },
    iTechPrice: {
        type: String,
    },
    iMinimumPrice: {
        type: String,
    },
    iSalesPrice: {
        type: String,
    },
    dtAddedDate: {
        type: String,
    },
    dtProjectStartOriginDate: {
        type: String,
    },
    dtProjectStartDate: {
        type: String,
    },
    dtDeadlineDate: {
        type: String,
    },
    iTechHour: {
        type: String,
    },
    eIsRecurring: {
        type: String,
        enum: ['Yes', 'No'],
        required: true,
    },
    ePaymentPhases: {
        type: String,
        enum: ['ThreePhase', 'FourPhase', 'FullPayment', 'Annual', 'SemiAnnual', 'Quarterly', 'Monthly'],
    },
    eAddOn: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No',
    },
    vQuote: {
        type: String
    },
    vQuotePage:{
        type: String
    },
    vQuoteLevel:{
        type: String
    },
    iPayrollAmt:{
        type: String
    }
});

ProjectSchema.virtual("ClientUserId", {
    ref: 'client',
    localField: 'iClientUserId',
    foreignField: 'iUserID',
    justOne: true,
})

ProjectSchema.virtual("AgentId", {
    ref: 'agent',
    localField: 'iAgentId',
    foreignField: '_id',
    justOne: true,
})

ProjectSchema.virtual("projectContract", {
    ref: 'project_contract',
    localField: '_id',
    foreignField: 'iProjectId',
    match: { 'eAgentContractStatus': 'done', "eClientContractStatus": "done" },
    justOne: true,
});

ProjectSchema.virtual("projectTeam", {
    ref: 'project_team',
    localField: '_id',
    foreignField: 'iProjectId',
    justOne: true,
})

//pm
ProjectSchema.virtual("projectManager", {
    ref: 'developer_project',
    localField: '_id',
    foreignField: 'iProjectId',
    justOne: "true"
})

ProjectSchema.virtual("addendum", {
    ref: 'addendum',
    localField: 'iProjectTypeId',
    foreignField: 'iProjectTypeId',
    justOne: "true"
});

ProjectSchema.set('toObject', { virtuals: true });
ProjectSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model("project", ProjectSchema);