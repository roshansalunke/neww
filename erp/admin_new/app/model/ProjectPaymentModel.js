const mongoose = require("mongoose");

const ProjectPaymentSchema = mongoose.Schema({
    iAgentId: {
        type: String,
        ref: "agent",
        default: null
    },
    iClientUserId: {
        type: String,
        // required: true,
    },
    iProjectId: {
        type: String,
        // required: true,
        ref: "project",
        default: null
    },
    iSalesPrice: {
        type: String,
    },
    iDiscountPrice: {
        type: String,
    },
    ePaymentPhases: {
        type: String,
        enum: ['ThreePhase', 'FourPhase', 'FullPayment', 'Annual', 'SemiAnnual', 'Quarterly', 'Monthly'],
    },
    ePaymentStep: {
        type: String,
    },
    ePaymentStatus: {
        type: String,
        default: null,
    },
    payment_type: {
        type: String,
        default: null
    },
    paypal_payid: {
        type: String,
        default: null
    },
    payment_intents: {
        type: String,
        default: null
    },
    receipt_url: {
        type: String,
        default: null
    },
    charge_id: {
        type: String,
        default: null
    },
    checkout_session_id: {
        type: String,
        default: null
    },
    subscription_id: {
        type: String,
        default: null
    },
    dtPaymentDate: {
        type: String,
        default: null
    },
    vTransactionId: {
        type: String,
        default: null
    },
});

ProjectPaymentSchema.virtual("vClientName", {
    ref: 'client',
    localField: 'iClientUserId',
    foreignField: 'iUserID',
    justOne: "true"
});

ProjectPaymentSchema.set('toObject', {
    virtuals: true
});

ProjectPaymentSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model("project_payment", ProjectPaymentSchema);