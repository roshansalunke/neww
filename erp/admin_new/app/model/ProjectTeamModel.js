const mongoose = require("mongoose");

const ProjectTeamSchema = mongoose.Schema({
    iProjectId: {
        type: String,
        ref: 'project',
        default: null
    },
    iProjectManagerId: {
        type: String,
        required: true,
        ref: 'developer',
    },
    // iTeamLeaderId: {
    //     type: Array,
    //     required: true,
    //     ref: 'developer'
    // },
    iDeveloperId: {
        type: Array,
        default: null
    },
    // iDesignerId: {
    //     type: Array,
    //     ref: 'developer',
    //     default: null
    // },
    // iQAId: {
    //     type: Array,
    //     required: true,
    //     ref: 'developer'
    // },
    dtAddedDate: {
        type: String,
        required: true,
    }

});

//DeveloperId
ProjectTeamSchema.virtual("DeveloperId", {
    ref: 'developer',
    localField: 'iDeveloperId',
    foreignField: '_id',
})

ProjectTeamSchema.set('toObject', { virtuals: true });
ProjectTeamSchema.set('toJSON', { virtuals: true });
module.exports = mongoose.model("project_team", ProjectTeamSchema);