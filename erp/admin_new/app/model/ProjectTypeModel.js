const mongoose = require("mongoose");

const ProjectTypeSchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
    },
    eRecurrying: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'Yes',
        required: true,
    }
});

module.exports = mongoose.model("project_type", ProjectTypeSchema);