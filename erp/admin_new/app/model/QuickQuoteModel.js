const mongoose = require("mongoose");

const QuickQuoteSchema = mongoose.Schema({
    vPages: {
        type: String,
        default: null
    },
    vBetterPrice: {
        type: String,
        default: null
    },
    vBestPrice: {
        type: String,
        default: null
    },
    iOrderId: {
        type: Number,
        default: null
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
    }
});

module.exports = mongoose.model("quick_quote", QuickQuoteSchema);