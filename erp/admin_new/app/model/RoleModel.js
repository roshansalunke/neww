const mongoose = require("mongoose");

const RoleSchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
        // ref: 'admin_type'
        ref: "developer_type"
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    }
});

module.exports = mongoose.model("role", RoleSchema);