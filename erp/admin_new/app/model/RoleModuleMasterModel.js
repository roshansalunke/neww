const mongoose = require("mongoose");

const RoleModuleMasterSchema = mongoose.Schema({
    vTitle: {
        type: String,
        required: true,
    },
    vModuleName: {
        type: String,
        require: true
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    }
});

module.exports = mongoose.model("role_module_master", RoleModuleMasterSchema);