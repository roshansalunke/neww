const mongoose = require("mongoose");

const RoleModulePermissionSchema = mongoose.Schema({
    iRoleId: {
        type: String,
    },
    iRoleModuleMasterId: {
        type: String,
        ref: 'role_module_master'
    },
    eCreate: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No'
    },
    eRead: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No'
    },
    eUpdate: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No'
    },
    eDelete: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No'
    }
});

module.exports = mongoose.model("role_module_permission", RoleModulePermissionSchema);