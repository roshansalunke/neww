const mongoose = require("mongoose");

const SecurityAnswerSchema = mongoose.Schema({
    iQuestionId: {
        type: String,
        required: true,
    },
    vAnswer: {
        type: String,
    },
    iUserId: {
        type: String,
    },
    eType: {
        type: String,
        enum: ['admin', 'client', 'agent', 'developer'],
    }
});

module.exports = mongoose.model("security_answer", SecurityAnswerSchema);