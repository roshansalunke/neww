const mongoose = require("mongoose");

const SecurityQuestionSchema = mongoose.Schema({
    vQuestion: {
        type: String,
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    }
});

module.exports = mongoose.model("security_question", SecurityQuestionSchema);