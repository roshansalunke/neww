const mongoose = require("mongoose");

const SendImportantVideoSchema = mongoose.Schema({
    iImportantVideoId: {
        type: String,
        required: true,
    },
    iUserid: {
        type: String,
        required: true,
    },
    eWatch: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No',
        required: true,
    },
});

module.exports = mongoose.model("send_important_video", SendImportantVideoSchema);