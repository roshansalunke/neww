const { Timestamp } = require("mongodb");
const mongoose = require("mongoose");

const SocialSettingSchema = mongoose.Schema({
    vFacebook: {
        type: String,
        required: true,
    },
    vInstagram: {
        type: String,
        required: true,
    },
    vTwitter: {
        type: String,
        required: true,
    },
    vLinkedin: {
        type: String,
        required: true,
    },
    dtAddedDate: {
        type: Date,
    }
});

module.exports = mongoose.model("social_setting", SocialSettingSchema);