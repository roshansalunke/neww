const mongoose = require("mongoose");

const StateSchema = mongoose.Schema({
    id: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    country_id: {
        type: String,
        required: true,
        ref: "country"
    },
});

module.exports = mongoose.model("state", StateSchema);