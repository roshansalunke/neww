const mongoose = require("mongoose");

const SubProjectSchema = mongoose.Schema({
    iProjectId: {
        type: String,
        required: true,
        ref: "project"
    },
    iSubProjectId: {
        type: String,
        required: true,
        ref: "project"
    },
});

module.exports = mongoose.model("sub_project", SubProjectSchema);