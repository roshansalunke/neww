const mongoose = require("mongoose");

const TrainingCategorySchema = mongoose.Schema({
    vCategory: {
        type: String,
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    }
});

module.exports = mongoose.model("training_category", TrainingCategorySchema);