const mongoose = require("mongoose");

const UpdateAgentSchema = mongoose.Schema({
    iAgentId: {
        type: String,
        ref: 'agent'
    },
    vFirstName: {
        type: String,
        // required: true,
    },
    vLastName: {
        type: String,
        // required: true,
    },
    vEmail: {
        type: String,
        // required: true,
    },
    vPassword: {
        // type: String,
    },
    vCity: {
        type: String,
        // required: true,
    },
    vCountry: {
        type: String,
    },
    vZipCode: {
        type: String,
    },
    // vReferrer: {
    //     type: String,
    //     ref: "agent"
    // },
    iAgentType: {
        type: String,
        // required: true,
        ref: "agent_type",
        default: null
    },
    eStatus: {
        type: String,
        enum: ['Active','Terminated','Production','Quit','Inactive'],
        default: 'Active',
    },
    vPhone: {
        type: String,
    },
    vAddress: {
        type: String,
    },
});

module.exports = mongoose.model("update_agent", UpdateAgentSchema);