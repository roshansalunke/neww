const mongoose = require("mongoose");

const UpdateClientSchema = mongoose.Schema({
    iClientId: {
        type: String,
        ref: "client",
        default: null
    },
    vFirstName: {
        type: String,
        // required: true,
    },
    vLastName: {
        type: String,
        // required: true,
    },
    vEmail: {
        type: String,
        // required: true,
    },
    vCity: {
        type: String,
        // required: true,
    },
    vCountry: {
        type: String,
        // required: true,
    },
    vZipCode: {
        type: String,
        // required: true,
    },
    vPhone: {
        type: String,
        // required: true,
    },
    vAddress: {
        type: String,
        // required: true,
    },
    eUserType: {
        type: String,
        enum: ['Client'],
        default: 'Client'
    },
    dtAddedDate: {
        type: String,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        // required: true,
    },
});

module.exports = mongoose.model("update_client", UpdateClientSchema);