const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
    iUserID: {
        type: String,
    },
    vFirstName: {
        type: String,
        required: true,
    },
    vLastName: {
        type: String,
        required: true,
    },
    vImage: {
        type: String,
    },
    vEmail: {
        type: String,
        required: true,
    },
    vPassword: {
        type: String,
        required: true,
    },
    eType:{
        type: String, // only for super admin
    },
    vDepartment:{
        type: String,
        required: true,
    },
    vRole: {
        type: String,
        // required: true,
    },
    vCity: {
        type: String,
        required: true,
    },
    vCountry: {
        type: String,
        required: true,
    },
    vZipCode: {
        type: String,
        required: true,
    },
    vState: {
        type: String,
        required: true,
    },
    vPhone: {
        type: String,
        required: true,
    },
    vAddress: {
        type: String,
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        // required: true,
    },
    eVerify: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No'
    },
    vVerifyCode: {
        type: String,
        required: true
    },
    vVerifyToken: {
        type: String,
        required: true
    },
    dtAddedDate: {
        type: String,
    },
    iPasswordAttempt: {
        type: String,
    },
    vResetPasswordBy: {
        type: String,
    },
    vTimezone: {
        type: String,
    }
});

//pm
UserSchema.virtual("rolePosition", {
    ref: 'department_role',
    localField: 'vRole',
    foreignField: '_id',
    justOne: "true"
})
UserSchema.set('toObject', { virtuals: true });
UserSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model("user", UserSchema);