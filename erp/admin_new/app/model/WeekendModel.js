const mongoose = require("mongoose");

const WeekendSchema = mongoose.Schema({
    vWeekend: {
        type: String,
        required: true,
    },
    // eStatus: {
    //     type: String,
    //     enum: ['Active', 'Inactive'],
    //     default: 'Active',
    //     required: true,
    // }
});

module.exports = mongoose.model("Weekend", WeekendSchema);