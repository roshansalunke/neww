const mongoose = require("mongoose");

const Contact_Us_Today_Schema = mongoose.Schema({
	vFirstName: {
		type: String,
	},
	vLastName: {
		type: String,
	},
	vEmail: {
		type: String,
	},
	vPhone: {
		type: String,
	},
	vAddress: {
		type: String,
	},
	vCity: {
		type: String,
	},
	vState: {
		type: String,
	},
	vZipCode: {
		type: String,
	},
	vDomain: {
		type: String,
	},
	vHosting: {
		type: String,
	},
	tDescription: {
		type: String,
	},
	iUserId: {
		type: String,
	},
	eFillUpStatus: {
		type: String,
		enum: ["Yes","No"],
		default: "No",
	},
});

module.exports = mongoose.model("fb_Contact_Us_Today", Contact_Us_Today_Schema);