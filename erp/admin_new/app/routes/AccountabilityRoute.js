const express = require("express");
const router = express.Router();
const AccountabilityController = require("../controller/AccountabilityController");

router.get("/", AccountabilityController.index);

module.exports = router;