const express = require("express");
const router = express.Router();
const AddendumController = require("../controller/AddendumController");

router.get("/", AddendumController.index);
router.get("/add", AddendumController.add);
router.post("/add_action", AddendumController.add_action);
router.get("/edit/:iAddendumId", AddendumController.edit);
router.post("/delete", AddendumController.delete);

module.exports = router;