const express = require("express");
const router = express.Router();
const AdminController = require("../controller/AdminController");

router.get("/", AdminController.index);
router.get("/add", AdminController.add);
router.post("/add_action", AdminController.add_action);
router.get("/edit/:iAdminId", AdminController.edit);
router.post('/check-uniq-email', AdminController.check_uniq_email);
router.post("/delete", AdminController.delete);

module.exports = router;