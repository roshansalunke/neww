const express = require("express");
const router = express.Router();
const AdminTypeController = require("../controller/AdminTypeController");

router.get("/", AdminTypeController.index);
router.get("/add", AdminTypeController.add);
router.post("/add_action", AdminTypeController.add_action);
router.get("/edit/:iAdminTypeId", AdminTypeController.edit);
router.post("/delete", AdminTypeController.delete);

module.exports = router;