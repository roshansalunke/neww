const express = require("express");
const router = express.Router();
const AgentHierarchyController = require("../controller/AgentHierarchyController");

router.get("/", AgentHierarchyController.index);
router.post("/ajax_listing", AgentHierarchyController.ajax_listing);
router.post("/subagent", AgentHierarchyController.subagent);
router.get("/agentOverview/:iAgentId", AgentHierarchyController.agentOverview);


module.exports = router;