const express = require("express");
const router = express.Router();
const AgentController = require("../controller/AgentController");

router.get("/", AgentController.index);
router.get("/add", AgentController.add);
router.post("/add_action", AgentController.add_action);
router.get("/edit/:iAgentId", AgentController.edit);
router.post("/referrerAgent", AgentController.referrerAgent);
router.post("/add_referrerAgent", AgentController.add_referrerAgent);
router.post("/reset_action", AgentController.reset_action);
router.post("/delete", AgentController.delete);

module.exports = router;