const express = require("express");
const router = express.Router();
const AgentTrainingController = require("../controller/AgentTrainingController");

router.get("/", AgentTrainingController.index);
router.get("/add", AgentTrainingController.add);
router.post("/add_action", AgentTrainingController.add_action);
router.get("/edit/:iAgentTrainingId", AgentTrainingController.edit);
router.post("/delete", AgentTrainingController.delete);

module.exports = router;