const express = require("express");
const router = express.Router();
const AgentTypeController = require("../controller/AgentTypeController");

router.get("/", AgentTypeController.index);
router.get("/add", AgentTypeController.add);
router.post("/add_action", AgentTypeController.add_action);
router.get("/edit/:iAgentTypeId", AgentTypeController.edit);
router.post("/delete", AgentTypeController.delete);

module.exports = router;