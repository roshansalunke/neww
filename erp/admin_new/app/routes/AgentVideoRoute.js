const express = require("express");
const router = express.Router();
const AgentVideoController = require("../controller/AgentVideoController");

router.get("/", AgentVideoController.index);
router.get("/add", AgentVideoController.add);
router.post("/add_action", AgentVideoController.add_action);
router.get("/edit/:iAgentVideoId", AgentVideoController.edit);
router.post("/delete", AgentVideoController.delete);

module.exports = router;