const express = require("express");
const router = express.Router();
const BannerController = require("../controller/BannerController");

router.get("/", BannerController.index);
router.get("/add", BannerController.add);
router.post("/add_action", BannerController.add_action);
router.get("/edit/:iBannerId", BannerController.edit);
router.post("/delete", BannerController.delete);

module.exports = router;