const express = require("express");
const router = express.Router();
const BlogCategoryController = require("../controller/BlogCategoryController");

router.get("/", BlogCategoryController.index);
router.get("/add", BlogCategoryController.add);
router.post("/add_action", BlogCategoryController.add_action);
router.get("/edit/:iBlogCategoryId", BlogCategoryController.edit);
router.post("/delete", BlogCategoryController.delete);

module.exports = router;