const express = require("express");
const router = express.Router();
const BlogController = require("../controller/BlogController");

router.get("/", BlogController.index);
router.get("/add", BlogController.add);
router.post("/add_action", BlogController.add_action);
router.get("/edit/:iBlogId", BlogController.edit);
router.post("/delete", BlogController.delete);

module.exports = router;