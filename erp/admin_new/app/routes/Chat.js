const express = require("express");
const router = express.Router();
const ChatController = require("../controller/ChatController");

router.get("/", ChatController.index);

module.exports = router;