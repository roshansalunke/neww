const express = require("express");
const router = express.Router();
const ClientResetPwController = require("../controller/ClientResetPwController");

router.get("/", ClientResetPwController.index);
// router.post("/ajax_client", ClientResetPwController.ajax_client);
// router.post("/ajax_admin", ClientResetPwController.ajax_admin);
// router.post("/ajax_agent", ClientResetPwController.ajax_agent);
// router.post("/ajax_dev", ClientResetPwController.ajax_dev);
router.get("/view/:iClientResetId", ClientResetPwController.view);
router.post("/forgetPassword_action", ClientResetPwController.forgetPassword_action);

module.exports = router;