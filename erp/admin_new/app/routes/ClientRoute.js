const express = require("express");
const router = express.Router();
const ClientController = require("../controller/ClientController");

router.get("/", ClientController.index);
router.get("/add", ClientController.add);
router.post("/add_action", ClientController.add_action);
router.get("/edit/:iClientId", ClientController.edit);
router.get("/view/:iClientId", ClientController.view);
router.post("/delete", ClientController.delete);

module.exports = router;