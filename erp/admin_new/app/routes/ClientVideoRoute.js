const express = require("express");
const router = express.Router();
const ClientVideoController = require("../controller/ClientVideoController");

router.get("/", ClientVideoController.index);
router.get("/add", ClientVideoController.add);
router.post("/add_action", ClientVideoController.add_action);
router.get("/edit/:iClientVideoId", ClientVideoController.edit);
router.post("/delete", ClientVideoController.delete);

module.exports = router;