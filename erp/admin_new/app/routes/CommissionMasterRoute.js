const express = require("express");
const router = express.Router();
const CommissionMasterController = require("../controller/CommissionMasterController");

router.get("/", CommissionMasterController.index);
router.get("/add", CommissionMasterController.add);
router.get("/add/:iProjectId", CommissionMasterController.get_project);
router.post("/calCommission",CommissionMasterController.calCommission);
router.post("/payroll_action",CommissionMasterController.payroll_action);

module.exports = router;