const express = require("express");
const router = express.Router();
const CompanyDetailController = require("../controller/CompanyDetailController");

router.get("/add", CompanyDetailController.add);
router.post("/add_action", CompanyDetailController.add_action);
router.get("/edit/:iCompanyDetailId", CompanyDetailController.edit);

module.exports = router;