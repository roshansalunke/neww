const express = require("express");
const router = express.Router();
const ContractController = require("../controller/ContractController");

router.get("/", ContractController.index);
router.get("/add", ContractController.add);
router.post("/add_action", ContractController.add_action);
router.get("/edit/:iContractId", ContractController.edit);
router.post("/delete", ContractController.delete);

module.exports = router;