const express = require("express");
const router = express.Router();
const DashboardController = require("../controller/DashboardController");

router.get("/", DashboardController.index);
router.post("/delete",DashboardController.delete)

module.exports = router;
