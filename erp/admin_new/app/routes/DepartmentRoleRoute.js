const express = require("express");
const router = express.Router();
const DepartmentRoleController = require("../controller/DepartmentRoleController");

router.get("/", DepartmentRoleController.index);
router.get("/add", DepartmentRoleController.add);
router.post("/add_action", DepartmentRoleController.add_action);
router.get("/edit/:iDepartmentRoleId", DepartmentRoleController.edit);
router.post("/delete", DepartmentRoleController.delete);

module.exports = router;