const express = require("express");
const router = express.Router();
const DepartmentController = require("../controller/DepartmentController");

router.get("/", DepartmentController.index);
router.get("/add", DepartmentController.add);
router.post("/add_action", DepartmentController.add_action);
router.get("/edit/:iDepartmentId", DepartmentController.edit);
router.post("/delete", DepartmentController.delete);

module.exports = router;