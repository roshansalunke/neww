const express = require("express");
const router = express.Router();
const DepartmentUserController = require("../controller/DepartmentUserController");

router.get("/:iRoleId", DepartmentUserController.index);
router.get("/add/:iRoleId", DepartmentUserController.add);
router.post("/add_action", DepartmentUserController.add_action);
router.get("/edit/:iUserId", DepartmentUserController.edit);
router.post("/delete", DepartmentUserController.delete);

module.exports = router;