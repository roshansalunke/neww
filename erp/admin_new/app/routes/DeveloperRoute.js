const express = require("express");
const router = express.Router();
const DeveloperController = require("../controller/DeveloperController");

router.get("/", DeveloperController.index);
router.get("/add", DeveloperController.add);
router.post("/add_action", DeveloperController.add_action);
router.get("/edit/:iDeveloperId", DeveloperController.edit);
router.get("/view/:iDeveloperId", DeveloperController.view);
router.post("/delete", DeveloperController.delete);

module.exports = router;