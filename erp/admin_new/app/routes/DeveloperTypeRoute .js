const express = require("express");
const router = express.Router();
const DeveloperTypeController = require("../controller/DeveloperTypeController");

router.get("/", DeveloperTypeController.index);
router.get("/add", DeveloperTypeController.add);
router.post("/add_action", DeveloperTypeController.add_action);
router.get("/edit/:iDeveloperTypeId", DeveloperTypeController.edit);
router.post("/delete", DeveloperTypeController.delete);

module.exports = router;