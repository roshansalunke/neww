const express = require("express");
const router = express.Router();
const EmailSettingController = require("../controller/EmailSettingController");

router.get("/add", EmailSettingController.add);
router.post("/add_action", EmailSettingController.add_action);
router.get("/edit/:iEmailSettingId", EmailSettingController.edit);
router.post("/test_email_action", EmailSettingController.test_email_action);

module.exports = router;