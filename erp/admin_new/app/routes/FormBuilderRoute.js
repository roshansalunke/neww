const express = require("express");
const router = express.Router();
const FormBuilderController = require("../controller/FormBuilderController");

router.get("/", FormBuilderController.index);
router.post("/add_action", FormBuilderController.add_action);
router.get("/add", FormBuilderController.add);
router.get("/edit/:iFormBuilderId", FormBuilderController.edit);
router.post("/update_action", FormBuilderController.update_action);
router.get("/view/:iFormBuilderId", FormBuilderController.view);
router.post("/ajax__", FormBuilderController.ajax__);
router.get("/download/:iFormBuilderId",FormBuilderController.downloadTemplate);
router.get("/assign-form/:iFormBuilderId",FormBuilderController.assign_form);
router.post("/ajax_role",FormBuilderController.ajax_role);
router.post("/assign_action",FormBuilderController.assign_action);
router.post("/delete",FormBuilderController.delete);

module.exports = router;