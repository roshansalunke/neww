const express = require("express");
const router = express.Router();
const FormController = require("../controller/FormController");

router.get("/", FormController.index);
router.post("/ajax_listing", FormController.ajax_listing);

module.exports = router;