const express = require("express");
const router = express.Router();
const HolidayController = require("../controller/HolidayController");

router.get("/", HolidayController.index);
router.get("/add", HolidayController.add);
router.post("/add_action", HolidayController.add_action);
router.get("/edit/:iHolidayId", HolidayController.edit);
router.post("/weekend_action", HolidayController.weekend_action);
router.post("/delete", HolidayController.delete);

module.exports = router;