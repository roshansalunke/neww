const express = require("express");
const router = express.Router();
const InFieldsFormController = require("../controller/InFieldsFormController");

router.get("/", InFieldsFormController.index);
router.get("/new-business", InFieldsFormController.newBusiness);
router.post("/business_action",InFieldsFormController.business_action);
router.post("/ajax_business",InFieldsFormController.ajax_business);
router.get("/new-business-edit/:iFormId",InFieldsFormController.business_edit);

router.post("/ajax_sales",InFieldsFormController.ajax_sales);
router.get("/sales-presentations", InFieldsFormController.salesPresentations);
router.post("/sales_action",InFieldsFormController.sales_action);
router.get("/sales-presentations-edit/:iFormId",InFieldsFormController.sales_edit);

router.get("/handouts", InFieldsFormController.handouts);
router.post("/handouts_action",InFieldsFormController.handouts_action);
router.post("/ajax_handouts",InFieldsFormController.ajax_handouts);
router.get("/handouts-edit/:iFormId",InFieldsFormController.handouts_edit);

module.exports = router;