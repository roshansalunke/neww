const express = require("express");
const router = express.Router();
const InvoiceController = require("../controller/InvoiceController");

router.get("/", InvoiceController.index);
router.get("/receipt-download/:iReceiptId", InvoiceController.receipt_download);
router.get("/invoice-download/:iInvoiceId", InvoiceController.invoice_download);
// router.get("/edit/:iAdminTypeId", InvoiceController.edit);
// router.post("/invoice_ajax_listing", InvoiceController.invoice_ajax_listing);

module.exports = router;