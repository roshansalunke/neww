const express = require("express");
const router = express.Router();
const LeadCenterController = require("../controller/LeadCenterController");

router.get("/", LeadCenterController.index);
router.get("/add", LeadCenterController.add);
router.post("/add_action", LeadCenterController.add_action);
router.get("/edit/:iLeadCenterId", LeadCenterController.edit);
router.post("/delete", LeadCenterController.delete);

module.exports = router;