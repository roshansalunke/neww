const express = require("express");
const router = express.Router();
const LogController = require("../controller/LogController");

router.get("/", LogController.index);
router.post("/clear_logs", LogController.clear_logs);

module.exports = router;