const express = require("express");
const router = express.Router();
const LoginController = require("../controller/LoginController");
const UserController = require("../controller/UserController");

router.get("/", LoginController.index);
router.post("/login_action", LoginController.login_action);
router.get("/logout", LoginController.logout);
router.get("/forget-password", LoginController.forget_password);

router.get("/verify/:iVerifyToken", LoginController.verifytoken);
router.post("/verify/verify_action", LoginController.verify_action);
router.get("/security-question/:iVerifyToken", LoginController.security_question);
router.post("/security-question/security_action", LoginController.security_action);
router.get("/reset/:iVerifyToken", LoginController.reset);
router.post("/reset/reset_action", LoginController.reset_action);

//reset password
router.get("/email-verification", LoginController.email_verification);
router.post("/verifyEmail_action", LoginController.verifyEmail_action)
router.get("/admin-security-question", LoginController.admin_security_question);
router.post("/admin_securityQuestion_action", LoginController.admin_securityQuestion_action);
router.get("/identity-verification", LoginController.identity_verification)
router.post("/identity_varification_action", LoginController.identity_verification_action);
router.get("/thank-you", LoginController.thank_you);

module.exports = router;