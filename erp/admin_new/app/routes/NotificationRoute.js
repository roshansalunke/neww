const express = require("express");
const router = express.Router();
const NotificationController = require("../controller/NotificationController");

router.get("/", NotificationController.view);
router.post("/changeSeenStatus", NotificationController.changeSeenStatus);
router.post("/read_notification", NotificationController.read_notification);

module.exports = router;