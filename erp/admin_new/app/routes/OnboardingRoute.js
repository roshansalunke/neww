const express = require("express");
const router = express.Router();
const OnboardingController = require("../controller/OnboardingController");

router.get("/", OnboardingController.index);
// router.get("/add", OnboardingController.add);
// router.post("/add_action", OnboardingController.add_action);
// router.get("/edit/:iOfferId", OnboardingController.edit);
router.post("/contract", OnboardingController.contract);
router.post("/applicationContract", OnboardingController.applicationContract);
router.post("/depositContract", OnboardingController.depositContract);
router.post("/edit_contract_status", OnboardingController.edit_contract_status);
router.post("/reset_contract", OnboardingController.reset_contract);
router.post("/restore_contract", OnboardingController.restore_contract);
router.get("/download_contract/:iAgentId", OnboardingController.download_contract);
router.get("/download_application/:iAgentId", OnboardingController.download_application);
router.get("/download_authorizationfrm/:iAgentId", OnboardingController.download_authorizationfrm);
router.post("/upload_contract", OnboardingController.upload_contract);

module.exports = router;