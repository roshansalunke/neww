const express = require("express");
const router = express.Router();
const ProfileNotificationController = require("../controller/ProfileNotificationController");

router.get("/", ProfileNotificationController.index);
router.post("/updateProfileInfo", ProfileNotificationController.updateProfileInfo);
router.post("/updateClientProfileInfo", ProfileNotificationController.updateClientProfileInfo);
// router.get("/edit/:iUserId", ProfileNotificationController.edit);
// router.get("/change-password/:iUserId", ProfileNotificationController.changePassword);
// router.post("/change-password-action", ProfileNotificationController.changePasswordAction);

module.exports = router;