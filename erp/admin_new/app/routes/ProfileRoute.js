const express = require("express");
const router = express.Router();
const ProfileController = require("../controller/ProfileController");

router.post("/add_action", ProfileController.add_action);
router.get("/edit/:iUserId", ProfileController.edit);
router.get("/change-password/:iUserId", ProfileController.changePassword);
router.post("/change-password-action", ProfileController.changePasswordAction);

module.exports = router;