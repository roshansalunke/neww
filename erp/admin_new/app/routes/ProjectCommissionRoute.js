const express = require("express");
const router = express.Router();
const ProjectCommissionController = require("../controller/ProjectCommissionController");

router.get("/", ProjectCommissionController.index);
// router.post("/ajax_listing", ProjectCommissionController.ajax_listing);

module.exports = router;