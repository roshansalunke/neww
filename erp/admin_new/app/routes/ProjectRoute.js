const express = require("express");
const router = express.Router();
const ProjectController = require("../controller/ProjectController");

router.get("/", ProjectController.index);
router.post("/add_action", ProjectController.add_action);
router.get("/view/:iProjectId", ProjectController.view);
router.get("/amount/:iProjectId", ProjectController.amount);
router.post("/amount_action", ProjectController.amount_action);
router.get("/download-contract/:iProjectId", ProjectController.download_contract);
router.get("/project-team/:iProjectId", ProjectController.project_team);
router.post("/delete", ProjectController.delete);

module.exports = router;