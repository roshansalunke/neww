const express = require("express");
const router = express.Router();
const ProjectTypeController = require("../controller/ProjectTypeController");

router.get("/", ProjectTypeController.index);
router.get("/add", ProjectTypeController.add);
router.post("/add_action", ProjectTypeController.add_action);
router.get("/edit/:iProjectTypeId", ProjectTypeController.edit);
router.post("/delete", ProjectTypeController.delete);

module.exports = router;