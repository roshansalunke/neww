const express = require("express");
const router = express.Router();
const QuickQuoteController = require("../controller/QuickQuoteController");

router.get("/", QuickQuoteController.index);
router.get("/add", QuickQuoteController.add);
router.post("/add_action", QuickQuoteController.add_action);
router.get("/edit/:iQuickQuoteId", QuickQuoteController.edit);
router.post("/delete", QuickQuoteController.delete);

module.exports = router;