const express = require("express");
const router = express.Router();
const RoleModuleMasterController = require("../controller/RoleModuleMasterController");

router.get("/", RoleModuleMasterController.index);
router.get("/add", RoleModuleMasterController.add);
router.post("/add_action", RoleModuleMasterController.add_action);
router.get("/edit/:iRoleModuleMasterId", RoleModuleMasterController.edit);
router.post("/delete", RoleModuleMasterController.delete);

module.exports = router;