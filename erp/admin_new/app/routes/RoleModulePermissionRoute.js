const express = require("express");
const router = express.Router();
const RoleModulePermissionController = require("../controller/RoleModulePermissionController");

router.get("/", RoleModulePermissionController.index);
router.get("/add", RoleModulePermissionController.add);
router.post('/add_action', RoleModulePermissionController.add_action)
router.get("/edit/:iRoleId", RoleModulePermissionController.edit);

module.exports = router;