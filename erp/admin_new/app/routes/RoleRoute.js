const express = require("express");
const router = express.Router();
const RoleController = require("../controller/RoleController");

router.get("/", RoleController.index);
router.get("/add", RoleController.add);
router.post("/add_action", RoleController.add_action);
router.get("/edit/:iRoleId", RoleController.edit);
router.post("/delete", RoleController.delete);

module.exports = router;