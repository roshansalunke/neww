const express = require("express");
const router = express.Router();
const SecurityQuestionController = require("../controller/SecurityQuestionController");

router.get("/", SecurityQuestionController.index);
router.get("/add", SecurityQuestionController.add);
router.post("/add_action", SecurityQuestionController.add_action);
router.get("/edit/:iSecurityQuestionId", SecurityQuestionController.edit);
router.post("/delete", SecurityQuestionController.delete);

module.exports = router;