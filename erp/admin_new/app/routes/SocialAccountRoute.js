const express = require("express");
const router = express.Router();
const SocialAccountController = require("../controller/SocialAccountController");
const FacebookController = require("../controller/FacebookController");
const InstagramController = require("../controller/InstagramController");

router.get("/", SocialAccountController.index);

//login flow api
router.get("/fb-login", FacebookController.fbLogin);
router.get("/callback", FacebookController.callback);
router.post("/fb-page",FacebookController.pageDetails);

//get all post api 
router.get("/add", SocialAccountController.add);

//cretae post api
router.post('/create-post', SocialAccountController.createPost);
// router.post("/create-schedule-post", SocialAccountController.createSchedulePost);

//calendar api
// router.get("/calendar",FacebookController.fbCalendarPostData);
router.get("/calendar", SocialAccountController.socialCalendar);

//engagement api
// router.post("/engagement",SocialAccountController.socialEngagement);
router.get("/engagement",SocialAccountController.socialEngagement);

router.post("/engagement-channel",SocialAccountController.socialEngagementChannel);
// router.get("/engagement",FacebookController.fbPostEngamentData);
router.post("/fb-engagement-post",FacebookController.fbEngagementPost);
router.post("/fb-comment-reply",FacebookController.fbCommentReply);

//channels api
router.get("/channels",SocialAccountController.channelsManage);
router.post('/remove-channel',SocialAccountController.removeChannel);
router.post("/refresh-channel",SocialAccountController.refreshChannel);
router.get("/connect-channel",SocialAccountController.connectChannel);
router.get('/channel-list',SocialAccountController.channelList);

//analytics api
router.get("/analytics",SocialAccountController.analyticsIndex);

//instaApi
router.post("/insta-login",InstagramController.instaLogin);
router.post("/insta-page",InstagramController.instaPageDetails);


module.exports = router;