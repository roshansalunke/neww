const express = require("express");
const router = express.Router();
const SocialSettingController = require("../controller/SocialSettingController");

router.get("/add", SocialSettingController.add);
router.post("/add_action", SocialSettingController.add_action);
router.get("/edit/:iSocialSettingId", SocialSettingController.edit);

module.exports = router;