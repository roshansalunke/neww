const express = require("express");
const router = express.Router();
const TrainingCategoryController = require("../controller/TrainingCategoryController");

router.get("/", TrainingCategoryController.index);
router.get("/add", TrainingCategoryController.add);
router.post("/add_action", TrainingCategoryController.add_action);
router.get("/edit/:iTrainingCategoryId", TrainingCategoryController.edit);
router.post("/delete", TrainingCategoryController.delete);

module.exports = router;
