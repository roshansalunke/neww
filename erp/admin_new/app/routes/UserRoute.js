const express = require("express");
const router = express.Router();
const UserController = require("../controller/UserController");

router.get("/", UserController.index);
router.post('/getRole',UserController.getRole);
router.post("/getAllAgent",UserController.getAllAgent);
router.post("/add_action", UserController.add_action);
router.get("/edit/:iUserId", UserController.edit);
router.post("/check_email", UserController.check_unique_email);

module.exports = router;