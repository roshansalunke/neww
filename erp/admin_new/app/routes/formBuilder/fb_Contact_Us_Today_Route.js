module.exports = app => {

	const express = require("express");
	const router = express.Router();
	const fb_Contact_Us_Today_Controller = require("../../controller/formBuilder/fb_Contact_Us_Today_Controller");

	router.get("/", fb_Contact_Us_Today_Controller.index);
	router.get("/add", fb_Contact_Us_Today_Controller.add);
	router.post("/add_action", fb_Contact_Us_Today_Controller.add_action);
	router.get("/edit/:iDataId", fb_Contact_Us_Today_Controller.edit);
	router.post("/delete", fb_Contact_Us_Today_Controller.delete);

	app.use("/contact-us-today", router);
}