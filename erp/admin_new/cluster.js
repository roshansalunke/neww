const os = require('os');
const cluster = require('cluster');


if (cluster.isMaster) { 
    for (let i = 0; i < os.cpus().length; i++) {
        cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);
        cluster.fork();
    }); 
}else{
    require('./index.js');
}


