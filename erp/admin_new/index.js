const express = require("express");
const path = require("path");
const cors = require("cors");
const bodyParser = require("body-parser");
const mongodb = require("mongodb");
const mongoose = require("mongoose");
const {
    engine
} = require("express-handlebars");
const session = require("express-session");
const fileupload = require("express-fileupload");
const app = express();
const port = process.env.PORT || 6001;
const socket = require("socket.io", {
    forceNew: true
});
const CompanyDetailModel = require("./app/model/CompanyDetailModel");
const UserModel = require("./app/model/UserModel");
const NotificationModel = require("./app/model/NotificationModel");

require("dotenv/config");

const LoginRoute = require("./app/routes/LoginRoute");
const DashboardRoute = require("./app/routes/DashboardRoute");
const UserRoute = require("./app/routes/UserRoute");
const ProfileRoute = require("./app/routes/ProfileRoute");
const CompanyDetailRoute = require("./app/routes/CompanyDetailRoute");
const EmailSettingRoute = require("./app/routes/EmailSettingRoute");
const SocialSettingRoute = require("./app/routes/SocialSettingRoute");
const SystemEmailRoute = require("./app/routes/SystemEmailRoute");
const AgentRoute = require("./app/routes/AgentRoute");
const ClientRoute = require("./app/routes/ClientRoute");
const AdminTypeRoute = require("./app/routes/AdminTypeRoute");
const AgentTypeRoute = require("./app/routes/AgentTypeRoute");
const BlogCategoryRoute = require("./app/routes/BlogCategoryRoute");
const BlogRoute = require("./app/routes/BlogRoute");
const ProjectTypeRoute = require("./app/routes/ProjectTypeRoute");
const SecurityQuestionRoute = require("./app/routes/SecurityQuestionRoute");
const ProjectRoute = require("./app/routes/ProjectRoute");
const TrainingCategoryRoute = require("./app/routes/TrainingCategoryRoute");
const AgentTrainingRoute = require("./app/routes/AgentTrainingRoute");
const ChatRoute = require("./app/routes/Chat");
const AddendumRoute = require("./app/routes/AddendumRoute");
const RoleRoute = require("./app/routes/RoleRoute");
const RoleModuleMasterRoute = require("./app/routes/RoleModuleMasterRoute");
const RoleModulePermissionRoute = require("./app/routes/RoleModulePermissionRoute");
const AgentHierarchyRoute = require("./app/routes/AgentHierarchyRoute");
const OnboardingRoute = require("./app/routes/OnboardingRoute");
const ProfileNotificationRoute = require("./app/routes/ProfileNotificationRoute");
const ContractRoute = require("./app/routes/ContractRoute");
const ClientResetPwRoute = require("./app/routes/ClientResetPwRoute");
const DeveloperTypeRoute = require("./app/routes/DeveloperTypeRoute ");
const DeveloperRoute = require("./app/routes/DeveloperRoute");
const HolidayRoute = require("./app/routes/HolidayRoute");
const InvoiceRoute = require("./app/routes/InvoiceRoute");
const BannerRoute = require("./app/routes/BannerRoute");
const NotificationRoute = require("./app/routes/NotificationRoute");
const ClientVideoRoute = require("./app/routes/ClientVideoRoute");
const AgentVideoRoute = require("./app/routes/AgentVideoRoute");
const LogRoute = require("./app/routes/LogRoute");
const ProjectCommissionRoute = require("./app/routes/ProjectCommissionRoute");
const QuickQuoteRoute = require("./app/routes/QuickQuoteRoute");
const LeadCenterRoute = require("./app/routes/LeadCenterRoute");
const AccountabilityRoute = require("./app/routes/AccountabilityRoute");
const DepartmentRoute = require("./app/routes/DepartmentRoute");
const DepartmentRoleRoute = require("./app/routes/DepartmentRoleRoute");
const InFieldsFormRoute = require("./app/routes/InFieldsFormRoute");
const AdminRoute = require("./app/routes/AdminRoute");
const DepartmentUserRoute = require('./app/routes/DepartmentUserRoute');
const FormBuilderRoute = require('./app/routes/FormBuilderRoute');
const CommissionMasterRoute = require('./app/routes/CommissionMasterRoute');
const FormRoute = require('./app/routes/FormRoute')

const async = require("hbs/lib/async");
const flash = require("express-flash");
const DepartmentModel = require("./app/model/DepartmentModel");

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(fileupload());
app.use(cors());
app.use(flash());
app.use(
    session({
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: false,
    })
);
app.use(async function (req, res, next) {
    res.locals.session = req.session;
    res.locals.sessionFlash = req.flash();
    next();
});

mongoose.set('strictQuery', true)

mongoose.connect(
    process.env.DB_CONNECTION, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    },
    () => console.log("Connected")
);

app.set("port", process.env.PORT || 6001);
app.set("views", path.join(__dirname, "app/view"));
app.engine(
    "handlebars",
    engine({
        defaultLayout: "index",
        runtimeOptions: {
            allowProtoPropertiesByDefault: true,
            allowProtoMethodsByDefault: true
        },
        partialsDir: __dirname + "/app/view/layouts",
        helpers: require("./app/helper/LogicHelper"),
        library: require('./app/library/GeneralLibrary'),
    })
);
app.set("view engine", "handlebars");
app.use(express.static(__dirname + "/public/assets"));

app.use(async function (req, res, next) {
    if (req.session.email) {
        try {
            await CompanyDetailModel.findOne({}).lean().exec().then((companydata) => {
                res.locals.companyData = companydata;
            });
            await UserModel.findOne({
                _id: req.session.userid
            }).populate('rolePosition').lean().exec().then((loginUserdata) => {
                res.locals.loginUserData = loginUserdata;
            });

            const notiCount = await NotificationModel.find({
                eUserSend: {
                    $ne: 'admin'
                },
                eAdminSeen: 'No'
            }).count();

            res.locals.notificationCount = notiCount;
            var current_route = req.originalUrl.replace(/\?.*$/, '').split("/");
            res.locals.route = current_route[1];
            res.locals.route2 = current_route[2] ? current_route[1] + '/' + current_route[2] : current_route[1];
            res.locals.route3 = current_route[3];

            //left menu
            const departments = await DepartmentModel.find({
                'vTitle': {
                    $ne: 'CEO & Administration'
                }
            }).populate({
                path: 'roleData',
                options: {
                    sort: {
                        "iPosition": 'asc'
                    }
                }
            }).sort({
                'iPosition': 'asc'
            });

            var DepartmentArray = departments.map(result2 => {
                result2.roleArray = result2.roleData.map(review => review.vUrl)
                return result2;
            });

            res.locals.departments = DepartmentArray;

            next();
        } catch (error) {
            console.log(error);
        }
    } else {
        await CompanyDetailModel.findOne({}).lean().exec().then((companydata) => {
            res.locals.companyData = companydata;
        });
        res.locals.loginUserData = null;
        next();
    }
});

app.use("/", LoginRoute);
app.use("/login", LoginRoute);
require("./app/config/FormBuilderConfig")(app);
app.use("/dashboard", DashboardRoute);
app.use("/user", UserRoute);
app.use("/profile", ProfileRoute);
app.use("/company-detail", CompanyDetailRoute);
app.use("/email-setting", EmailSettingRoute);
app.use("/social-setting", SocialSettingRoute);
app.use("/system-email", SystemEmailRoute);
app.use("/agent", AgentRoute);
app.use("/client", ClientRoute);
app.use("/admin-type", AdminTypeRoute);
app.use("/agent-type", AgentTypeRoute);
app.use("/blog-category", BlogCategoryRoute);
app.use("/blog", BlogRoute);
app.use("/project-type", ProjectTypeRoute);
app.use("/security", SecurityQuestionRoute);
app.use("/project", ProjectRoute);
app.use("/training-category", TrainingCategoryRoute);
app.use("/agent-training", AgentTrainingRoute);
app.use("/chat", ChatRoute);
app.use("/addendum", AddendumRoute);
app.use("/role", RoleRoute);
app.use("/role-module-master", RoleModuleMasterRoute);
app.use("/role-module-permission", RoleModulePermissionRoute);
app.use("/agent-hierarchy", AgentHierarchyRoute);
app.use("/onboarding", OnboardingRoute);
app.use("/profile-notification", ProfileNotificationRoute);
app.use("/contract", ContractRoute);
app.use("/client-reset-request", ClientResetPwRoute);
app.use("/developer-type", DeveloperTypeRoute);
app.use("/developer", DeveloperRoute);
app.use("/holiday", HolidayRoute);
app.use("/invoice", InvoiceRoute);
app.use("/banner", BannerRoute);
app.use("/notification", NotificationRoute);
app.use("/client-video", ClientVideoRoute);
app.use("/agent-video", AgentVideoRoute);
app.use("/activity-log", LogRoute);
app.use("/project-commission", ProjectCommissionRoute);
app.use("/quick-quote", QuickQuoteRoute);
app.use("/lead-center", LeadCenterRoute);
app.use("/accountability", AccountabilityRoute);
app.use('/department', DepartmentRoute);
app.use('/department-role', DepartmentRoleRoute);
app.use('/in-field-form', InFieldsFormRoute);
app.use('/admin', AdminRoute);
app.use('/department-user', DepartmentUserRoute);
app.use('/form-builder', FormBuilderRoute);
app.use('/commission-master', CommissionMasterRoute);
app.use('/form', FormRoute);


app.get("/", function (req, res) {
    if (req.session.email) {
        res.redirect("/dashboard");
    } else {
        res.redirect("/");
    }
});

app.use((req, res, next)=> {
    if (req.session.email) {
        res.redirect("/dashboard");
    } else {
        res.redirect("/");
    }
    // res.status(404).send('Page not found');
  });
  

var server = app.listen(port, "0.0.0.0", () => {
    console.log("Server is running on port: " + port);
});

// app.use(express.static("public"));
const io = socket(server, {
    cors: {
        origin: "*",
    },
});

const SERVER_CLIENTS = {};

//  user connection
io.on("connection", (socket) => {
    SERVER_CLIENTS[socket.id] = socket;
    //Message event
    socket.on("message", (msg) => {
        console.log("msg", msg);
        for (let i in SERVER_CLIENTS)
            SERVER_CLIENTS[i].emit("message-broadcast", msg);
    });

    //  user disconnect
    socket.on("disconnect", () => {
        console.log("disconnect");
        delete SERVER_CLIENTS[socket.id];
        io.emit("message", "A user has left the chat");
    });
});